import os

years = [
    2011,
    2012
    ]
bins = [
    (   0, 800),
    ( 800,1100),
    (1100,1400),
    (1400,1700),
    (1700,2000),
    (2000,4500),
    ]

for year in years:
    for bmin, bmax in bins:
        print '*********', year, bmin, bmax
        nfin = './logs/%d.%d.%d'%(year,bmin,bmax)
        fin = open(nfin, 'r').read()
        lines = fin.split('\n')[:-1]
        effCC=0
        eeffCC=0
        effTP=0
        eeffTP=0
        for line in lines:
            what = line.replace(' ', '').split(':')
            if 'effC&C' in what[0]:
                print '     eff C&C : %.2f '%(float(what[1])*100)
                effCC=float(what[1])
            if 'efferrC&C' in what[0]:
                print ' err eff C&C : %.2f '%(float(what[1])*100)
                eeffCC=float(what[1])
            if 'effT&P' in what[0]:
                print '     eff T&P : %.2f '%(float(what[1])*100)
                effTP=float(what[1])
            if 'efferrT&P' in what[0]:
                print ' err eff T&P : %.2f '%(float(what[1])*100)
                eeffTP=float(what[1])
        sigma = float((effCC-effTP))/(eeffCC**2+eeffTP**2)**0.5
        print '       sigma : %.1f'%(sigma)
