"""
Run this script with:
lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python testCode.py
"""

from ROOT import gROOT
import pandas
import root_numpy
import sys
import array as arr
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190312/srcEff')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190312/effTRIGGmu/getEff')
from reweighter2D import myReweighter
pathTuples = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/'
from hep_ml import reweight
import numpy
import matplotlib
matplotlib.use('Agg')  # Force matplotlib to not use any Xwindows backend.
from matplotlib import pyplot as plt

year= str(sys.argv[1])
D_PtransMIN = str(sys.argv[2])
D_PtransMAX = str(sys.argv[3])

nfinCS = pathTuples+'B2JpsiK_mumu/selected/Data-'+year+'-JPsiMuMu.root.bak'
nfinMC = pathTuples+'MC/noTrigg/BdKpipi/MC-'+year+'-BdKpipi-noTrigg_Unfiltered.root'
selectionCS = 'muplus_PT>1000 && muplus_PT<14000 && muplus_eta>2 && muplus_eta<4.7'
#selectionCS+=' && muminus_L0MuonDecision_TOS && (muminus_Hlt1TrackMuonDecision_TOS || muminus_Hlt1TrackAllL0Decision_TOS || muminus_Hlt1SingleMuonHighPTDecision_TOS)'
#selectionCS+= '&& (muplus_Hlt2Topo2BodyBBDTDecision_TIS || muplus_Hlt2Topo3BodyBBDTDecision_TIS)'
selectionMC ='(signalCategory == 1) && (D_peak) && Mu_IPChi2>25  && D_Ptrans>%s && D_Ptrans<%s'%(D_PtransMIN,D_PtransMAX)
#selectionMC+='&& L0Global==1 && Hlt1Global==1 && Hlt2Global==1'
cutCUTandCOUNT = 'passTRIGGmu = Mu_L0MuonDecision_TOS & (Mu_Hlt1TrackMuonDecision_TOS | Mu_Hlt1TrackAllL0Decision_TOS | Mu_Hlt1SingleMuonHighPTDecision_TOS)'
#cutCUTandCOUNT = 'passTRIGGmu = Mu_Hlt1TrackMuonDecision_TOS'
cutTAGandPROBE = 'passTRIGGmu = muplus_L0MuonDecision_TOS & (muplus_Hlt1TrackMuonDecision_TOS | muplus_Hlt1TrackAllL0Decision_TOS | muplus_Hlt1SingleMuonHighPTDecision_TOS)'
#cutTAGandPROBE = 'passTRIGGmu = muplus_Hlt1TrackMuonDecision_TOS'



def cutANDcount(nfin):
    print '*********** : cutANDcount'
    columns=['Mu_L0MuonDecision_TOS', 'Mu_Hlt1TrackMuonDecision_TOS', 'Mu_Hlt1TrackAllL0Decision_TOS', 'Mu_Hlt1SingleMuonHighPTDecision_TOS', 'nSPDHits']
    tin = root_numpy.root2array(nfin, treename='DMutuple', branches=columns,selection=selectionMC)
    tin = pandas.DataFrame(tin)
#    cut = 'passTRIGGmu = Mu_L0MuonDecision_TOS & Mu_Hlt1TrackAllL0Decision_TOS'
#    cut = 'passTRIGGmu = Mu_L0MuonDecision_TOS'
    tin.eval(cutCUTandCOUNT)
    p = 0
    f = 0
    for i in range(0, len(tin)):
        if tin['passTRIGGmu'][i] == True: p+=1
        else: f+=1

    den = len(tin)
    eff = float(p)/float(p+f)
    eff_err = (eff*(1-eff)/float(den))**(0.5)
    print 'Denominator : ', den
    print '  Numerator : ', p
    print '        p+f : ', p+f
    print '     effC&C : ', eff
    print '  efferrC&C : ', eff_err


def rew(tinCS, tinMC, binning=[12,6], nw='w_test'):
    print '*********** : rew'
    print '    len(CS) : ', len(tinCS)
    print '    len(MC) : ', len(tinMC)
    #reweighter = reweight.BinsReweighter([30], n_neighs=2)
    reweighter = reweight.GBReweighter()
    reweighter.fit(tinCS, tinMC)
    weights = reweighter.predict_weights(tinCS)
    weights = numpy.array(weights, dtype=[(nw, 'f8')])
    sumWeights=0
    for sw in weights[nw]:
            sumWeights +=sw
    normFactor=sumWeights/len(tinCS);
    weights[nw]=weights[nw]/normFactor

    
    bins = numpy.linspace(500, 14500, 100)
    plt.hist(tinMC['Mu_PT'], bins=bins, histtype='step', label='MC', normed=True)
    plt.hist(tinCS['muplus_PT'], bins=bins, histtype='step', label='CS original', normed=True)
    plt.hist(tinCS['muplus_PT'], bins=bins, histtype='step', label='CS rew', weights=weights[nw], normed=True)
    plt.xlabel('mu_PT [MeV]')
    plt.legend(loc='best')
    plt.savefig('testPT_%s_%s_%s.pdf'%(year,D_PtransMIN,D_PtransMAX))
    plt.close()
    bins = numpy.linspace(1.7, 5., 100)
    plt.hist(tinMC['Mu_ETA'], bins=bins, histtype='step', label='MC', normed=True)
    plt.hist(tinCS['muplus_eta'], bins=bins, histtype='step', label='CS original', normed=True)
    plt.hist(tinCS['muplus_eta'], bins=bins, histtype='step', label='CS rew', weights=weights[nw], normed=True)
    plt.xlabel('mu_ETA')
    plt.legend(loc='best')
    plt.savefig('testETA_%s_%s_%s.pdf'%(year,D_PtransMIN,D_PtransMAX))
    plt.close()
    
    bins = numpy.linspace(0, 10, 100)
    plt.hist(tinMC['Mu_IP'], bins=bins, histtype='step', label='MC', normed=True)
    plt.hist(tinCS['muplus_IP_OWNPV'], bins=bins, histtype='step', label='CS original', normed=True)
    plt.hist(tinCS['muplus_IP_OWNPV'], bins=bins, histtype='step', label='CS rew', weights=weights[nw], normed=True)
    plt.xlabel('mu_IP')
    plt.legend(loc='best')
    plt.savefig('testIP_%s_%s_%s.pdf'%(year,D_PtransMIN,D_PtransMAX))
    plt.close()

    '''
    bins = numpy.linspace(0, 800, 100)
    plt.hist(tinMC['nSPDHits'], bins=bins, histtype='step', label='MC', normed=True)
    plt.hist(tinCS['nSPDHits'], bins=bins, histtype='step', label='CS original', normed=True)
    plt.hist(tinCS['nSPDHits'], bins=bins, histtype='step', label='CS rew', weights=weights[nw], normed=True)
    plt.xlabel('SPDHtis')
    plt.legend(loc='best')
    plt.savefig('testnSPDHits.pdf')
    plt.close()
    '''
    return weights

def calcEff(tin, weights):
    print '*********** : calEff'
    p=0
    ep=0
    f=0
    ef=0
#    cut = 'passTRIGGmu = muplus_L0MuonDecision_TOS & muplus_Hlt1TrackAllL0Decision_TOS'
#    cut = 'passTRIGGmu = muplus_L0MuonDecision_TOS'
    tin.eval(cutTAGandPROBE)
    for i in range(0,len(tin)):
        if tin['passTRIGGmu'][i]: 
            p+=weights[i]
            ep+=weights[i]**2
        else: 
            f+=weights[i]
            ef+=weights[i]**2
    
    ep = ep**(0.5)
    ef = ef**(0.5)
    den = p+f
    eff = float(p)/float(p+f)
    eff_err = (eff*(1-eff)/float(den))**(0.5)
    eff_err2 = (((ep*f)**2+(ef*p)**2)/(p+f)**4)**(0.5)
    print 'Denominator : ', den
    print '  Numerator : ', p
    print '        p+f : ', p+f
    print '     effT&P : ', eff
    print '    err eff : ', eff_err
    print '  efferrT&P : ', eff_err2

        


def tagANDprobe():
    print '*********** : TagANDProbe'
    columnsCS=['muplus_PT', 'muplus_eta', 'muplus_IP_OWNPV']
#    columnsCS=['muplus_PT', 'muplus_eta']
    #columnsCS=['log(muplus_IPCHI2_OWNPV)']
    tinCS = root_numpy.root2array(nfinCS, treename='DecayTree', branches=columnsCS, selection=selectionCS)
    tinCS = pandas.DataFrame(tinCS)
    
    columnsMC=['Mu_PT', 'Mu_ETA', 'Mu_IP']
 #   columnsMC=['Mu_PT', 'Mu_ETA']
    #columnsMC=['log(Mu_IPChi2)']
    tinMC = root_numpy.root2array(nfinMC, treename='DMutuple', branches=columnsMC,selection=selectionMC)
    tinMC = pandas.DataFrame(tinMC)
    w_CS = rew(tinCS, tinMC, nw='w_test')
    
    bins = numpy.linspace(0, 5, 100)
    plt.hist(w_CS['w_test'], bins=bins, histtype='step', label='weights')
    plt.xlabel('w')
    plt.legend(loc='best')
    plt.savefig('testW_%s_%s_%s.pdf'%(year,D_PtransMIN, D_PtransMAX))
    plt.close()

    #columnsCS=['muplus_L0MuonDecision_TOS', 'muplus_Hlt1TrackMuonDecision_TOS', 'muplus_Hlt1TrackAllL0Decision_TOS', 'muplus_Hlt1SingleMuonHighPTDecision_TOS', 'muplus_Hlt2Topo2BodyBBDTDecision_TIS','muplus_Hlt2Topo3BodyBBDTDecision_TIS']
    columnsCS=['muplus_L0MuonDecision_TOS', 'muplus_Hlt1TrackMuonDecision_TOS', 'muplus_Hlt1TrackAllL0Decision_TOS', 'muplus_Hlt1SingleMuonHighPTDecision_TOS']
    tinCS = root_numpy.root2array(nfinCS, treename='DecayTree', branches=columnsCS, selection=selectionCS)
    tinCS = pandas.DataFrame(tinCS)
    calcEff(tinCS, w_CS['w_test'])

cutANDcount(nfinMC)
tagANDprobe()
