#include <iostream>
#include "TString.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TH1D.h"
//#include "/home/LHCB/fferrari/exclusive-SL-Bs-decays/tools/Tools.h"
#include "TMath.h"
using namespace std;


void getHist(TChain *tin, int Nbins, const float* bins, int Ncats, const int* cats, TString nfout, TString nhout, TString nw);
void doEffHist(TString nfin1, TString nhin1,TString nfin2, TString nhin2,TString nfout, TString nhout, double scaleFactor=-1.);
void getEfficiencies(TString nfin, TString nhin, double *effs, double *effs_err);
void createFout(TString nfout, TString mode="RECREATE");

int getEff_D_Ptrans(TString mode = "BdKpipi"){
  /*
  TString nfout = "./test.root";
  createFout(nfout);
  TString pathIN = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC/";
  TChain tin_num("DMutuple");
  tin_num.Add(pathIN+"FinalSelection/"+mode+"/MC-"+mode+"-AfterSelection_Unfiltered.root");
  TChain tin_den("DMutuple");
  tin_den.Add(pathIN+"noTrigg_noPID_noOffL/"+mode+"/MagDw/MC-2011-"+mode+"Dw-noTrigg_noPID_noOffL_Pythia6Unfiltered_0.root");
  tin_den.Add(pathIN+"noTrigg_noPID_noOffL/"+mode+"/MagDw/MC-2011-"+mode+"Dw-noTrigg_noPID_noOffL_Pythia8Unfiltered_0.root");
  tin_den.Add(pathIN+"noTrigg_noPID_noOffL/"+mode+"/MagDw/MC-2012-"+mode+"Dw-noTrigg_noPID_noOffL_Pythia6Unfiltered_0.root");
  tin_den.Add(pathIN+"noTrigg_noPID_noOffL/"+mode+"/MagDw/MC-2012-"+mode+"Dw-noTrigg_noPID_noOffL_Pythia8Unfiltered_0.root");
  tin_den.Add(pathIN+"noTrigg_noPID_noOffL/"+mode+"/MagUp/MC-2011-"+mode+"Up-noTrigg_noPID_noOffL_Pythia6Unfiltered_0.root");
  tin_den.Add(pathIN+"noTrigg_noPID_noOffL/"+mode+"/MagUp/MC-2011-"+mode+"Up-noTrigg_noPID_noOffL_Pythia8Unfiltered_0.root");
  tin_den.Add(pathIN+"noTrigg_noPID_noOffL/"+mode+"/MagUp/MC-2012-"+mode+"Up-noTrigg_noPID_noOffL_Pythia6Unfiltered_0.root");
  tin_den.Add(pathIN+"noTrigg_noPID_noOffL/"+mode+"/MagUp/MC-2012-"+mode+"Up-noTrigg_noPID_noOffL_Pythia8Unfiltered_0.root");
  int cats[2] = {1,2};
  getHist(&tin_num, 2, cats,  nfout,  "num_BdKpipi",  "w_FF_corr");
  getHist(&tin_den, 2, cats,  nfout,  "den_BdKpipi",  "w_FF_corr");
  doEffHist( nfout,  "den_BdKpipi_1", nfout, "num_BdKpipi_1", nfout, "eff_BdKpipi_1");
  doEffHist( nfout,  "den_BdKpipi_2", nfout, "num_BdKpipi_2", nfout, "eff_BdKpipi_2");
  double effs[6]; 
  double effs_err[6];
  getEfficiencies(nfout, "eff_BdKpipi_1", effs, effs_err);
  getEfficiencies(nfout, "eff_BdKpipi_2", effs, effs_err);
  */
  return 1;
}

void getEfficiencies(TString nfin, TString nhin, double *effs, double *effs_err){
  TFile fin(nfin, "READ");
  TH1D *hin = (TH1D*)fin.Get(nhin);
  const unsigned int Nbins = hin->GetNbinsX();
  for (unsigned int i=0; i<Nbins; ++i){
    effs[i] = hin->GetBinContent(i+1);
    effs_err[i] = hin->GetBinError(i+1);
    double binMin = hin->GetXaxis()->GetBinLowEdge(i+1);
    double binMax = hin->GetXaxis()->GetBinLowEdge(i+1)+hin->GetXaxis()->GetBinWidth(i+1);
    cout << TString::Format(" $ %.1f < D_Ptrans < %.1f $      eff   =    $ %.8f    \\pm     %.8f $\n", binMin, binMax, effs[i], effs_err[i]);
  } 
 fin.Close();
}

void doEffHist(TString nfin1, TString nhin1,TString nfin2, TString nhin2,TString nfout, TString nhout, double scaleFactor){
  TFile fin1(nfin1, "READ");
  TH1D *hin1 = (TH1D*)fin1.Get(nhin1);
  TFile fin2(nfin2, "READ");
  TH1D *hin2 = (TH1D*)fin2.Get(nhin2);
  TFile fout(nfout, "UPDATE");
  const int Nbins = hin1->GetNbinsX();
  double bins[Nbins+1];
  hin1->GetXaxis()->GetLowEdge(bins);
  bins[Nbins] = hin1->GetXaxis()->GetBinUpEdge(Nbins);
    
  TH1D hout(nhout,nhout, Nbins, bins);

  if (scaleFactor <= 0.){
    for (int i=1; i<=Nbins; ++i){
      double eff = (double)hin2->GetBinContent(i)/(double)hin1->GetBinContent(i);
      double eff_err  = TMath::Sqrt(eff*(1.-eff)/(double)hin1->GetBinContent(i));
      hout.SetBinContent(i, eff);
      hout.SetBinError(i,eff_err);
    }
  } else {
    TH1D *hin1_tmp = (TH1D*)hin1->Clone();
    TH1D *hin2_tmp = (TH1D*)hin2->Clone();
    hin1_tmp->Scale(1./hin1->Integral());
    hin2_tmp->Scale(1./hin2->Integral()*scaleFactor);
    for (int i=1; i<=Nbins; ++i){
      double eff = (double)hin2_tmp->GetBinContent(i)/(double)hin1_tmp->GetBinContent(i);
      double eff_err  = eff*TMath::Sqrt(TMath::Power((double)hin1_tmp->GetBinError(i)/(double)hin1_tmp->GetBinError(i),2)+TMath::Power((double)hin2_tmp->GetBinError(i)/(double)hin2_tmp->GetBinError(i),2));
      hout.SetBinContent(i, eff);
      hout.SetBinError(i,eff_err);    
    }
    delete hin1_tmp;
    delete hin2_tmp;
  }

  fout.WriteTObject(&hout, hout.GetName(), "overwrite");
  fout.Close();
  fin1.Close();
  fin2.Close();
}

void getHist(TChain *tin, int Nbins, const float* bins, int Ncats, const int* cats, TString nfout, TString nhout, TString nw="none"){
  TH1::SetDefaultSumw2(true);
  int SigCat(0);
  double D_Ptrans(0.0), w(1.0);
  bool D_peak = false;
  tin->SetBranchStatus("*",0);
  tin->SetBranchStatus("signalCategory",1);
  tin->SetBranchStatus("D_Ptrans",1);
  tin->SetBranchAddress("signalCategory", &SigCat);
  tin->SetBranchAddress("D_Ptrans", &D_Ptrans);
  TString D="D";
  if (nhout.Contains("BsSignal"))
    D="Ds";
  tin->SetBranchStatus (D+"_peak",1);
  tin->SetBranchAddress(D+"_peak", &D_peak);
  
  if (nw!="none"){
    tin->SetBranchStatus(nw,1);
    tin->SetBranchAddress(nw, &w);
  }
  TFile fout(nfout, "UPDATE");
  TH1D *hout[Ncats];
  for (int i = 0; i<Ncats; ++i){
    TString ncat = TString::Format("_%d", cats[i]);
    hout[i] = new TH1D(nhout+ncat, nhout+ncat, Nbins, bins);
  }
  for (int j = 0; j < tin->GetEntries(); ++j){
    tin->GetEntry(j);
    for (int i = 0; i<Ncats; ++i){
      if (cats[i]){
	if ((SigCat == cats[i]) && D_peak)
	  hout[i]->Fill(D_Ptrans, w);
	else continue;
      } else 
	if (D_peak)
	  hout[i]->Fill(D_Ptrans, w);
    }
  }
  
  for (int i = 0; i<Ncats; ++i){
    fout.WriteTObject(hout[i], hout[i]->GetName(), "overwrite");
    //    cout << "Written :" << hout[i]->GetName() << endl;
  }
  tin->SetBranchStatus("*",1);
  for (int i = 0; i<Ncats; ++i)
    delete hout[i];
  fout.Close();
}

void createFout(TString nfout, TString mode){
  TFile *fout = new TFile(nfout, mode);
  fout->Close();
}
