"""
Run this script with: 
lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python reweight2D.py
"""

import os, sys
import root_numpy
import pandas
from hep_ml import reweight
import numpy as np
from rootpy.io import root_open
from ROOT import *
from shutil import copyfile

def myReweighter(nfinCSs, input_CSs, nfinMCs, nw='weight',
                 ntCSin='DecayTree', input_CSws=None, 
                 ntMCin='DMutuple',  mySel='(1)', nwMC='w_FF_corr',
                 columns_MC = ['Mu_PT','Mu_ETA'], binning=[12,6]
                 ):
    
    NCSs = len(input_CSs)
    print 'NCSs : ',NCSs
    input_CS = input_CSs[0]
    input_CSw = None
    if input_CSws is not None:
        input_CSw= input_CSws[0]
    print len(input_CS)
    for i in range(1,NCSs):
        input_CS = input_CS.append( input_CSs[i], ignore_index=True)
        print len(input_CS)
        if input_CSws is not None:
            input_CSw= np.append(input_CSw, input_CSws[i])
    
    #READ THE DATASETS (N.B. DATASETS SHOULD HAVE THE SAME NUMBER OF ENTRIES AND THE SAME VARIABLE NAMES)
    print 'Reading MC dataset...'
    input_MC_tmp = root_numpy.root2array(nfinMCs[0], treename=ntMCin, branches=columns_MC,selection=mySel)
    input_MC = pandas.DataFrame(input_MC_tmp)
    input_MCw = None
    if nwMC != 'none':
        input_MCw_tmp= root_numpy.root2array(nfinMCs[0], treename=ntMCin, branches=[nwMC],selection=mySel)
        input_MCw= input_MCw_tmp[nwMC]
    NMCs = len(nfinMCs)
    print 'NMCs : ', NMCs
    for i in range(1,NMCs):
        input_MC_tmp = root_numpy.root2array(nfinMCs[i], treename=ntMCin, branches=columns_MC,selection=mySel)
        input_MC = input_MC.append(pandas.DataFrame(input_MC_tmp), ignore_index=True)
        print len(input_MC)
        if nwMC != 'none':
            input_MCw_tmp= root_numpy.root2array(nfinMCs[i], treename=ntMCin, branches=[nwMC],selection=mySel)
            input_MCw= np.append(input_MCw, input_MCw_tmp[nwMC])
        
    print 'Number of entries -->  CS:', len(input_CS),' MC: ',len(input_MC), (len(input_MCw) if (input_MCw is not None) else 'no MC weight')
    #print input_CS
    #print input_CSw

    ##### REWEIGHT FUNCTION. FOR TESTING PURPOSES, 
    ##### CHOOSE 'n_estimators' VERY LOW (10 IS OK). 
    ##### n_estimators IS THE NUMBER OF TREES THAT 
    ##### WILL BE TRAINED TO PERFORM THE REWEIGHTING
    print 'Reweighting... ', mySel, '-->', nw
#    reweighter = reweight.BinsReweighter(binning, n_neighs=2)
    reweighter = reweight.GBReweighter(n_estimators=25)
    reweighter.fit(input_CS, input_MC, original_weight=input_CSw, target_weight=input_MCw)
    ## Calculate weights for each CS
    for inputCS_tmp,nfinCS_tmp in zip(input_CSs,nfinCSs):
        gb_weights_up = reweighter.predict_weights(inputCS_tmp, original_weight=input_CSw)
        new_column = np.array(gb_weights_up , dtype=[(nw, 'f8')])
        sumWeights=0
        for sw in new_column[nw]:
            sumWeights +=sw 
        normFactor=sumWeights/len(inputCS_tmp);
        new_column[nw]=new_column[nw]/normFactor
        root_numpy.array2root(new_column, nfinCS_tmp, ntCSin)

    return 1


