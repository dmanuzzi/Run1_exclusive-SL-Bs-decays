"""
Run this script with: 
lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python reweight2D_AlreadyMerged.py
"""

import os, sys
import root_numpy
import pandas
from hep_ml import reweight
import numpy as np
from rootpy.io import root_open
from ROOT import *
from shutil import copyfile


def myReweighter(nfCSin, input_CS, nfMCin, nw='weight',
                 ntCSin='DecayTree', input_CSw=None, 
                 ntMCin='DMutuple',  mySel='(1)', nwMC='w_FF_corr',
                 columns_MC = ['Mu_PT','Mu_ETA'], binning=[12,6]
                 ):
    #READ THE DATASETS (N.B. DATASETS SHOULD HAVE THE SAME NUMBER OF ENTRIES AND THE SAME VARIABLE NAMES)
    print 'Reading MC dataset...'
    input_MC = root_numpy.root2array(nfMCin, treename=ntMCin, branches=columns_MC,selection=mySel)
    input_MC = pandas.DataFrame(input_MC)
    input_MCw= root_numpy.root2array(nfMCin, treename=ntMCin, branches=[nwMC],selection=mySel)
    input_MCw= input_MCw[nwMC]
        
    print "Number of entries",str(len(input_CS))+" "+str(len(input_MC))
    #print input_CS
    #print input_CSw

    ##### REWEIGHT FUNCTION. FOR TESTING PURPOSES, 
    ##### CHOOSE 'n_estimators' VERY LOW (10 IS OK). 
    ##### n_estimators IS THE NUMBER OF TREES THAT 
    ##### WILL BE TRAINED TO PERFORM THE REWEIGHTING
    print 'Reweighting... ', mySel, '-->', nw

    reweighter = reweight.BinsReweighter(binning, n_neighs=2)
    reweighter.fit(input_CS, input_MC, original_weight=input_CSw, target_weight=input_MCw)
    gb_weights_up = reweighter.predict_weights(input_CS)

    new_column = np.array(gb_weights_up , dtype=[(nw, 'f8')])
    sumWeights=0
    for sw in new_column[nw]:
        sumWeights +=sw 
        
    normFactor=sumWeights/len(input_CS);
    new_column[nw]=new_column[nw]/normFactor
    root_numpy.array2root(new_column, nfCSin, ntCSin)

    return 1


