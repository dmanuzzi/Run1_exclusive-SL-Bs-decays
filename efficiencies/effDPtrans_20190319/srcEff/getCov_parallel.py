from ROOT import TChain
import array as arr
import pickle
import time
pathGETEFF = ''
def f_getCov_parallel(func_getCov, tag1, years, pols, cats, bins, tag2, CSs, nfout):
    fout = open(nfout, 'w')
    fout.close()
    calcCov = True
    if (('BdKpipi11' in tag1) or ('BdKpipi11' in tag2)):
        if not (('BdKpipi11' in tag1) and ('BdKpipi11' in tag2)): 
            calcCov = False

    nstep = ''
    if 'PIDmu' in nfout: nstep = 'PIDmu'
    elif 'TRIGGmu' in nfout: nstep = 'TRIGGmu'
    ntins = []
    if calcCov:
        for year in years:
            for pol in pols:
                ntins.append(CSs[year][pol]['nfin'])
    resultsCcov = {}
    resultsDcov = {}
    nbins = ['bin%d'%(i) for i  in range(1, len(bins))]
    nbins += ['tot']
    for h in range(0,len(cats)):
        for k  in range(h,len(cats)):
            for i in range(0,len(nbins)):
                for j in range(i,len(nbins)):
                    if calcCov:
                        nw_1 = 'w_%s_sigCat%d_%s_%s'%(nstep,cats[h],tag1,nbins[i])
                        nw_2 = 'w_%s_sigCat%d_%s_%s'%(nstep,cats[k],tag2,nbins[j])
                        ntins_w1 = []
                        ntins_w2 = []
                        for year in years:
                            for pol in pols:
                                ntins_w1.append(CSs[year][pol]['nfin'].replace('.root','_'+nw_1+'.root'))
                                ntins_w2.append(CSs[year][pol]['nfin'].replace('.root','_'+nw_2+'.root'))
                        setup = (func_getCov, ntins, ntins_w1, ntins_w2, nw_1, nw_2, nfout)                        
                        nfsetup =  pathGETEFF+'_'+nw_1+'__'+nw_2+'.pickle'
                        fsetup = open(nfsetup, 'wb')
                        pickle.dump(setup, fsetup)
                        fsetup.close()
                        ## submit to the queue
                        ftmp = open('./tmp.sh', 'w')
                        ftmp.write('#!/bin/bash \n')
                        ftmp.write('. $VO_LHCB_SW_DIR/lib/LbLogin.sh \n')
                        ftmp.write('cd %s \n'%(pathGETEFF))
                        ftmp.write('lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python %s/sbatch_getCov_parallel.py %s \n'%(pathSRCEFF, nfout))
                        ftmp.close()
                        os.chmod('./tmp.sh',0755)
                        command = 'qsub -N {nw} -e {path}/err/{nw}.txt -o {path}/out/{nw}.txt ./tmp.sh'.format(nw=nw_1+'__'+nw_2, path=pathGETEFF+'/logs')
                        print 'qsub '+nw_1+'__'+nw_2
                        os.system(command)
           
    #waitORgo
    while(waitORgo()):
        time.sleep(5)

    for h in range(0,len(cats)):
        resultsCcov[cats[h]] = {}
        resultsDcov[cats[h]] = {}
        for k  in range(h,len(cats)):
            resultsCcov[cats[h]][cats[k]] = {}
            resultsDcov[cats[h]][cats[k]] = {}
            for i in range(0,len(nbins)):
                resultsCcov[cats[h]][cats[k]][nbins[i]] = {}
                resultsDcov[cats[h]][cats[k]][nbins[i]] = {}
                for j in range(i,len(nbins)):
                    if calcCov:
                        nw_1 = 'w_%s_sigCat%d_%s_%s'%(nstep,cats[h],tag1,nbins[i])
                        nw_2 = 'w_%s_sigCat%d_%s_%s'%(nstep,cats[k],tag2,nbins[j])
                        lines_fout = open(nfout,'r').read().split('\n')[:-1]
                        for line in lines_fout:
                            if (nw_1 in line) and (nw_2 in line):
                                l = line.split(' ')
                                resultsCcov[cats[h]][cats[k]][nbins[i]][nbins[j]] = { 'cov' : l[]}
                                resultsDcov[cats[h]][cats[k]][nbins[i]][nbins[j]] = { 'cov' : l[]}
                    else:
                        resultsCcov[cats[h]][cats[k]][nbins[i]][nbins[j]] = { 'cov' : 0.}
                        resultsDcov[cats[h]][cats[k]][nbins[i]][nbins[j]] = { 'cov' : 0.}

    return resultsCcov, resultsDcov
