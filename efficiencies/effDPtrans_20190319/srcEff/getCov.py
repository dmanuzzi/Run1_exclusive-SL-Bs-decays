from ROOT import TChain
import array as arr
def f_getCov(func_getCov, tag1, years, pols, cats, bins, tag2, CSs, nfout):
    fout = open(nfout, 'w')
    fout.close()
   
    calcCov = True
    if (('BdKpipi11' in tag1) or ('BdKpipi11' in tag2)):
        if not (('BdKpipi11' in tag1) and ('BdKpipi11' in tag2)): 
            calcCov = False

    nstep = ''
    if 'PIDmu' in nfout: nstep = 'PIDmu'
    elif 'TRIGGmu' in nfout: nstep = 'TRIGGmu'
    
    tin =  TChain('DecayTree')
    if calcCov:
        for year in years:
            for pol in pols:
                tin.Add(CSs[year][pol]['nfin'])
    resultsCcov = {}
    resultsDcov = {}
    nbins = ['bin%d'%(i) for i  in range(1, len(bins))]
    nbins += ['tot']
    for h in range(0,len(cats)):
        resultsCcov[cats[h]] = {}
        resultsDcov[cats[h]] = {}
        for k  in range(h,len(cats)):
            resultsCcov[cats[h]][cats[k]] = {}
            resultsDcov[cats[h]][cats[k]] = {}
            for i in range(0,len(nbins)):
                resultsCcov[cats[h]][cats[k]][nbins[i]] = {}
                resultsDcov[cats[h]][cats[k]][nbins[i]] = {}
                for j in range(i,len(nbins)):
                    if calcCov:
                        nw_1 = 'w_%s_sigCat%d_%s_%s'%(nstep,cats[h],tag1,nbins[i])
                        nw_2 = 'w_%s_sigCat%d_%s_%s'%(nstep,cats[k],tag2,nbins[j])
                        tin_w1 = TChain("DecayTree")
                        tin_w2 = TChain("DecayTree")
                        for year in years:
                            for pol in pols:
                                tin_w1.Add(CSs[year][pol]['nfin'].replace('.root','_'+nw_1+'.root'))
                                tin_w2.Add(CSs[year][pol]['nfin'].replace('.root','_'+nw_2+'.root'))
                        covs = arr.array('f', [0.0,0.0,0.0,0.0])
                        func_getCov(tin, tin_w1, tin_w2,  nw_1, nw_2, covs, nfout)
                        resultsCcov[cats[h]][cats[k]][nbins[i]][nbins[j]] = { 'cov' : covs[2]}
                        resultsDcov[cats[h]][cats[k]][nbins[i]][nbins[j]] = { 'cov' : covs[3]}
                    else:
                        resultsCcov[cats[h]][cats[k]][nbins[i]][nbins[j]] = { 'cov' : 0.}
                        resultsDcov[cats[h]][cats[k]][nbins[i]][nbins[j]] = { 'cov' : 0.}
    return resultsCcov, resultsDcov
