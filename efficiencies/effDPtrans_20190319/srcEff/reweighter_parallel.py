"""
Run this script with: 
lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python reweighter_parallel.py <ninputs>
"""

import os, sys
import root_numpy
import pandas
from hep_ml import reweight
import numpy as np
from rootpy.io import root_open
from ROOT import *
from shutil import copyfile
import pickle


print 'here reweighter_parallel.py'
ninputs = str(sys.argv[1])
print 'input file : ', ninputs
finputs = open(ninputs, 'rb')
nfinCSs, nfinMCs, nw, ntCSin, ntMCin, sel_CS, sel_MC, nwMC, columns_CS, columns_MC = pickle.load(finputs)
finputs.close()

print '   nfinCSs :', nfinCSs
print '   nfinMCs :', nfinMCs
print '        nw :', nw
print 'columns_CS : ', columns_CS
print 'columns_MC : ', columns_MC
print '    sel_CS : ', sel_CS
print '    sel_MC : ', sel_MC
print '     nt_CS : ', ntCSin
print '     nt_MC : ', ntMCin

NCSs = len(nfinCSs)
print '      NCSs : ',NCSs
input_CSs = []

input_CS_tmp = root_numpy.root2array(nfinCSs[0], treename=ntCSin, branches=columns_CS, selection=sel_CS)
input_CS = pandas.DataFrame(input_CS_tmp)
input_CSs.append(input_CS)
input_CSw = None
print len(input_CS)
for i in range(1,NCSs):
    input_CS_tmp = root_numpy.root2array(nfinCSs[i], treename=ntCSin, branches=columns_CS, selection=sel_CS)
    input_CS_tmp = pandas.DataFrame(input_CS_tmp)
    input_CSs.append(input_CS_tmp)
    input_CS = input_CS.append( input_CS_tmp, ignore_index=True)
    print len(input_CS)

#READ THE DATASETS (N.B. DATASETS SHOULD HAVE THE SAME NUMBER OF ENTRIES AND THE SAME VARIABLE NAMES)
print 'Reading MC dataset...'
input_MC_tmp = root_numpy.root2array(nfinMCs[0], treename=ntMCin, branches=columns_MC,selection=sel_MC)
input_MC = pandas.DataFrame(input_MC_tmp)
input_MCw = None
if nwMC != 'none':
    input_MCw_tmp= root_numpy.root2array(nfinMCs[0], treename=ntMCin, branches=[nwMC],selection=sel_MC)
    input_MCw= input_MCw_tmp[nwMC]
NMCs = len(nfinMCs)
print 'NMCs : ', NMCs
for i in range(1,NMCs):
    input_MC_tmp = root_numpy.root2array(nfinMCs[i], treename=ntMCin, branches=columns_MC,selection=sel_MC)
    input_MC = input_MC.append(pandas.DataFrame(input_MC_tmp), ignore_index=True)
    print len(input_MC)
    if nwMC != 'none':
        input_MCw_tmp= root_numpy.root2array(nfinMCs[i], treename=ntMCin, branches=[nwMC],selection=sel_MC)
        input_MCw= np.append(input_MCw, input_MCw_tmp[nwMC])

print 'Number of entries -->  CS:', len(input_CS),' MC: ',len(input_MC), (len(input_MCw) if (input_MCw is not None) else 'no MC weight')
print 'Reweighting... ', sel_MC, '-->', nw
#    reweighter = reweight.BinsReweighter(binning, n_neighs=2)
reweighter = reweight.GBReweighter(n_estimators=25)
reweighter.fit(input_CS, input_MC, original_weight=input_CSw, target_weight=input_MCw)
    ## Calculate weights for each CS
for inputCS_tmp,nfinCS_tmp in zip(input_CSs,nfinCSs):
    gb_weights_up = reweighter.predict_weights(inputCS_tmp, original_weight=input_CSw)
    new_column = np.array(gb_weights_up , dtype=[(nw, 'f8')])
    sumWeights=0
    for sw in new_column[nw]:
        sumWeights +=sw 
    normFactor=sumWeights/len(inputCS_tmp);
    new_column[nw]=new_column[nw]/normFactor
    #root_numpy.array2root(new_column, nfinCS_tmp, ntCSin)
    nfoutCS_tmp = nfinCS_tmp.replace('.root', '_'+nw+'.root')
    root_numpy.array2root(new_column, nfoutCS_tmp, ntCSin)



