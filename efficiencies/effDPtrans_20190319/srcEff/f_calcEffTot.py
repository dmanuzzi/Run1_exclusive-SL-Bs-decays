def f_calcEff11and12(din):
    tags = din.keys()
    dtags = {}
    dout = {}
    for tag in tags:
        tag_tmp = tag.split('_')
        dtags[tag_tmp[0]] = {}
    for tag in tags:
        tag_tmp = tag.split('_')
        dtags[tag_tmp[0]][tag_tmp[1]] = din[tag]
    '''    
    print '*******************************************'
    for mode in dtags.keys():
        for year in dtags[mode].keys():
            for step in dtags[mode][year].keys():
                for cat in dtags[mode][year][step].keys():
                    for val in dtags[mode][year][step][cat].keys():
                        print mode,year,step,cat,val, dtags[mode][year][step][cat][val]
                        dim_box = dim
                        dim = len(dtags[mode][year][step][cat][val])
                        if (dim_box != 0) and (dim!=dim_box):
                            print 'hai sbagliato il numero dei bins'
                            return None
    print '*******************************************'
    '''
    for mode in dtags.keys():
        if len(dtags[mode].keys()) != 2: continue
        dout[mode] = {}
        cats = dtags[mode]['2012']['EffTot'].keys()
        for cat in cats:
            dout[mode][cat] = {}
            effTot = []
            effTot_err = []
            for i in range(0, len(dtags[mode]['2012']['EffTot'][cat]['eff'])):
                eff2011  = dtags[mode]['2011']['EffTot'][cat]['eff'][i]
                eff2012  = dtags[mode]['2012']['EffTot'][cat]['eff'][i]
                Ngen2011 = dtags[mode]['2011']['Ngen'][cat]['Ngen'][i]
                Ngen2012 = dtags[mode]['2012']['Ngen'][cat]['Ngen'][i]
                effTot_tmp = (float(Ngen2011)*eff2011+float(Ngen2012)*eff2012)/(float(Ngen2011+Ngen2012))
                effTot.append(effTot_tmp)
                
                Ngen2011_err = dtags[mode]['2011']['Ngen'][cat]['Ngen_err'][i]
                Ngen2012_err = dtags[mode]['2012']['Ngen'][cat]['Ngen_err'][i]
                eff2011_err  = dtags[mode]['2011']['EffTot'][cat]['eff_err'][i]
                eff2012_err  = dtags[mode]['2012']['EffTot'][cat]['eff_err'][i]
                NgenTot = float(Ngen2011+Ngen2012)
                q1 = eff2011_err*Ngen2011/NgenTot
                q2 = eff2012_err*Ngen2012/NgenTot
                q3 = Ngen2011_err*Ngen2012*(eff2011-eff2012)/(NgenTot**2)
                q4 = Ngen2012_err*Ngen2011*(eff2012-eff2011)/(NgenTot**2)
                effTot_err_tmp = (q1**2+q2**2+q3**2+q4**2)**(0.5)
                effTot_err.append(effTot_err_tmp)
                dout[mode][cat]['eff'] = effTot
                dout[mode][cat]['eff_err'] = effTot_err
    return dout


def f_calcEff2011(din2012, dinFull2011, dinFull2012):
    dout = {}
    cats = din2012['EffTot'].keys()
    if cats != dinFull2011['EffTot'].keys(): print 'WARNING: mismatch in cats between din2012 and dinFull2011'
    if cats != dinFull2012['EffTot'].keys(): print 'WARNING: mismatch in cats between din2012 and dinFull2012'
    for cat in cats:
        dout[cat] = {'eff': [], 'eff_err' : []}
        effs2012 = din2012['EffTot'][cat]['eff']
        effsFull2011 = dinFull2011['EffTot'][cat]['eff']
        effsFull2012 = dinFull2012['EffTot'][cat]['eff']
        effs2012_err = din2012['EffTot'][cat]['eff_err']
        effsFull2011_err = dinFull2011['EffTot'][cat]['eff_err']
        effsFull2012_err = dinFull2012['EffTot'][cat]['eff_err']
        for i in range(0, len(effs2012)):
            eff2011_i = effs2012[i] + effsFull2011[i] - effsFull2012[i]
            dout[cat]['eff'].append(eff2011_i)
            eff2011_err_i = (effs2012_err[i]**2+effsFull2011_err[i]**2+effsFull2012[i]**2)**(0.5)
            dout[cat]['eff_err'].append(eff2011_err_i)
    return dout
def f_calcNgen2011(din2012, dinFull2011, dinFull2012):
    dout = {}
    cats = din2012['Ngen'].keys()
    if cats != dinFull2011['Ngen'].keys(): print 'WARNING: mismatch in cats between din2012 and dinFull2011'
    if cats != dinFull2012['Ngen'].keys(): print 'WARNING: mismatch in cats between din2012 and dinFull2012'
    for cat in cats:
        dout[cat] = {'Ngen': [], 'Ngen_err' : []}
        for i in range(0, len(din2012['Ngen'][cat]['Ngen'])):
            Ngen2012 = din2012['Ngen'][cat]['Ngen'][i]
            NgenFull2011 = dinFull2011['Ngen'][cat]['Ngen'][i]
            NgenFull2012 = dinFull2012['Ngen'][cat]['Ngen'][i]
            Ngen2012_err = din2012['Ngen'][cat]['Ngen_err'][i]
            NgenFull2011_err = dinFull2011['Ngen'][cat]['Ngen_err'][i]
            NgenFull2012_err = dinFull2012['Ngen'][cat]['Ngen_err'][i]
            Ngen2011 = float(Ngen2012) * float(NgenFull2011) / float(NgenFull2012)
            dout[cat]['Ngen'].append(Ngen2011)
            Ngen2011_err = Ngen2011*((Ngen2012_err/float(Ngen2012))**2+\
                                     (NgenFull2011_err/float(NgenFull2011))**2+\
                                     (NgenFull2012_err/float(NgenFull2012))**2)**(0.5)
            dout[cat]['Ngen_err'].append(Ngen2011_err)
    return dout

def f_calcEffTot(din, cats):
    din['EffTot'] = {}
    for cat in cats: 
        effKin    = din['noTrigg_noPID'][cat]['eff']
        effPIDhad  = din['noTrigg_noPIDmu'][cat]['eff']
        effPIDmu   = din['noTrigg'][cat]['eff']
        effTRIGGmu = din['noHLT2'][cat]['eff']
        effHLT2    = din['FinalSelection'][cat]['eff']
        effKin_err     = din['noTrigg_noPID'][cat]['eff_err']
        effPIDhad_err  = din['noTrigg_noPIDmu'][cat]['eff_err']
        effPIDmu_err   = din['noTrigg'][cat]['eff_err']
        effTRIGGmu_err = din['noHLT2'][cat]['eff_err']
        effHLT2_err    = din['FinalSelection'][cat]['eff_err']
        din['EffTot'][cat] = {'eff': [], 'eff_err' : []}
        effTot = []
        effTot_err = []
        for i in range(0,len(effKin)):
            effTot_tmp = effKin[i]*effPIDhad[i]*effPIDmu[i]*effTRIGGmu[i]*effHLT2[i]
            effTot_err_tmp = effTot_tmp*((effKin_err[i]/effKin[i])**2+\
                                             (effPIDhad_err[i]/effPIDhad[i])**2+\
                                             (effPIDmu_err[i]/effPIDmu[i])**2+\
                                             (effTRIGGmu_err[i]/effTRIGGmu[i])**2+\
                                             (effHLT2_err[i]/effHLT2[i])**2)**(0.5)
            effTot.append(effTot_tmp)
            effTot_err.append(effTot_err_tmp)
        din['EffTot'][cat]['eff'] = effTot
        din['EffTot'][cat]['eff_err'] = effTot_err
    return din
