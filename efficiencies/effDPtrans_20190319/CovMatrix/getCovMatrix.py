def f_getRatios(tag_num, tag_den, cat_num, cat_den, Bins, AllVars):
    ratios = []
    C = AllVars[tag_den]['C'][cat_den]['tot']['p']
    D = AllVars[tag_den]['D'][cat_den]['tot']['f']
    K = AllVars[tag_den]['K'][cat_den]['tot']['K']

    Nbins=len(Bins)-1
    for g in range(1,Nbins+1):
        c = AllVars[tag_num]['C'][cat_num]['bin%d'%(g)]['p']
        d = AllVars[tag_num]['D'][cat_num]['bin%d'%(g)]['f']
        k = AllVars[tag_num]['K'][cat_num]['bin%d'%(g)]['K']
        rg = (k*c*(C+D)) / (K*C*(c+d))
        ratios.append(rg)

    c = AllVars[tag_num]['C'][cat_num]['tot']['p']
    d = AllVars[tag_num]['D'][cat_num]['tot']['f']
    k = AllVars[tag_num]['K'][cat_num]['tot']['K']
    rg = (k*c*(C+D)) / (K*C*(c+d))
    ratios.append(rg)

    return ratios

def f_getCovMatrix(tag_num, tag_den, cat_num, cat_den, Bins, AllVars, AllCovs):
    Nbins=len(Bins)-1
    res = [[0.0 for g in range(0,Nbins)] for l in range(0,Nbins)]
        
    for g in range(1,Nbins+1):
        for l in range(1,Nbins+1):
            V = f_getV(g, l, tag_num, tag_den, cat_num, cat_den, AllVars, AllCovs)
            Dr_g, Dr_l = f_getDr(g,l, tag_num, tag_den, cat_num, cat_den, AllVars)
            res_tmp = 0
            for i in range(0,9):
                for j in range(0,9):
                    res_tmp += Dr_g[i]*Dr_l[j]*V[i][j]
            res[g-1][l-1] = res_tmp
    return res

def f_getDr(g,l, tag_num, tag_den, cat_num, cat_den, AllVars):
    
    C = AllVars[tag_den]['C'][cat_den]['tot']['p']
    D = AllVars[tag_den]['D'][cat_den]['tot']['f']
    K = AllVars[tag_den]['K'][cat_den]['tot']['K']

    #____________________________________________________________
    c = AllVars[tag_num]['C'][cat_num]['bin%d'%(g)]['p']
    d = AllVars[tag_num]['D'][cat_num]['bin%d'%(g)]['f']
    k = AllVars[tag_num]['K'][cat_num]['bin%d'%(g)]['K']

    dg = [0.0 for i in range(0,15)]    
    dg[0] =   ( k*d*(C+D) ) / ( C*K*(c+d)**2 )    #dr_g/dc_g
    dg[1] = - ( k*c*(C+D) ) / ( C*K*(c+d)**2 )    #dr_g/dd_g
    dg[2] =   (   c*(C+D) ) / ( C*K*(c+d)    )    #dr_g/dk_g
    dg[6] = - ( c*k*D     ) / ( (C**2)*K*(c+d) )  #dr_g/dC
    dg[7] =   ( c*k       ) / ( C*K*(c+d)      )  #dr_g/dD
    dg[8] = - ( c*k*(C+D) ) / ( C*(K**2)*(c+d) )  #dr_g/DK

    #__________________________________________________________
    c = AllVars[tag_num]['C'][cat_num]['bin%d'%(l)]['p']
    d = AllVars[tag_num]['D'][cat_num]['bin%d'%(l)]['f']
    k = AllVars[tag_num]['K'][cat_num]['bin%d'%(l)]['K']

    dl = [0.0 for i in range(0,15)]    
    dl[3] =   ( k*d*(C+D) ) / ( C*K*(c+d)**2 )    #dr_l/dc_l
    dl[4] = - ( k*c*(C+D) ) / ( C*K*(c+d)**2 )    #dr_l/dd_l
    dl[5] =   (   c*(C+D) ) / ( C*K*(c+d)    )    #dr_l/dk_l
    dl[6] = - ( c*k*D     ) / ( (C**2)*K*(c+d) )  #dr_l/dC
    dl[7] =   ( c*k       ) / ( C*K*(c+d)      )  #dr_l/dD
    dl[8] = - ( c*k*(C+D) ) / ( C*(K**2)*(c+d) )  #dr_l/dK

    return dg, dl


    

def f_getV(g, l, tag_num, tag_den, cat_num, cat_den, AllVars, AllCovs):
    #print 'g = ', g, 'l= ',l
    V = [[0.0 for i in range(0,9)] for j in range(0,9)]
    tag1 = tag_num
    tag2 = tag_den
    cat1 = cat_num
    cat2 = cat_den
                         
    if tag2 not in AllCovs[tag1].keys(): 
        tag2 = tag_num
        tag1 = tag_den

    V[0][0] = AllCovs[tag_num][tag_num]['Ccov'][cat_num][cat_num]['bin%d'%(g)]['bin%d'%(g)]['cov']
    V[1][1] = AllCovs[tag_num][tag_num]['Dcov'][cat_num][cat_num]['bin%d'%(g)]['bin%d'%(g)]['cov']
    V[2][2] = AllVars[tag_num]['K'][cat_num]['bin%d'%(g)]['K_err']**2
    V[3][3] = AllCovs[tag_num][tag_num]['Ccov'][cat_num][cat_num]['bin%d'%(l)]['bin%d'%(l)]['cov']
    V[4][4] = AllCovs[tag_num][tag_num]['Dcov'][cat_num][cat_num]['bin%d'%(l)]['bin%d'%(l)]['cov']
    V[5][5] = AllVars[tag_num]['K'][cat_num]['bin%d'%(l)]['K_err']**2
    V[6][6] = AllCovs[tag_den][tag_den]['Ccov'][cat_den][cat_den]['tot']['tot']['cov']
    V[7][7] = AllCovs[tag_den][tag_den]['Dcov'][cat_den][cat_den]['tot']['tot']['cov']
    V[8][8] = AllVars[tag_num]['K'][cat_den]['tot']['K_err']**2

    G=g
    L=l
    if g>l:
       G=l
       L=g
    V[0][3]   = V[3][0]  = AllCovs[tag_num][tag_num]['Ccov'][cat_num][cat_num]['bin%d'%(G)]['bin%d'%(L)]['cov']
    V[1][4]   = V[4][1]  = AllCovs[tag_num][tag_num]['Dcov'][cat_num][cat_num]['bin%d'%(G)]['bin%d'%(L)]['cov']

    V[0][6]  = V[6][0] = AllCovs[tag1][tag2]['Ccov'][cat1][cat2]['bin%d'%(g)]['tot']['cov']
    V[1][7]  = V[7][1] = AllCovs[tag1][tag2]['Dcov'][cat1][cat2]['bin%d'%(g)]['tot']['cov']

    V[3][6]  = V[6][3] = AllCovs[tag1][tag2]['Ccov'][cat1][cat2]['bin%d'%(l)]['tot']['cov']
    V[4][7]  = V[7][4] = AllCovs[tag1][tag2]['Dcov'][cat1][cat2]['bin%d'%(l)]['tot']['cov']

#    for i in range(0, len(V)):
#        print V[i]

    return V
