
def f_printAllVars(AllVars, nfout):
    fout = open(nfout, 'w')
    for tag in AllVars.keys():
        for obj in AllVars[tag].keys():
            for cat in AllVars[tag][obj].keys():
                for nbin in AllVars[tag][obj][cat].keys():
                    for key,val in AllVars[tag][obj][cat][nbin].iteritems():
                        line = '%s %s %d %s %s %f \n'%(tag,obj,cat,nbin,key,val)
                        fout.write(line)

    fout.close()
    return 0

def f_printAllCovs(AllCovs, nofut):
    fout = open(nfout, 'w')
    for tag1 in AllCovs.keys():
        for tag2 in AllCovs[tag1].keys():
            for obj in AllCovs[tag1][tag2].keys():
                for cat1 in AllCovs[tag1][tag2][obj].keys():
                    for cat2 in AllCovs[tag1][tag2][obj][cat1].keys():
                        for nbin1 in AllCovs[tag1][tag2][obj][cat1][cat2].keys():
                            for nbin2 in AllCovs[tag1][tag2][obj][cat1][cat2][nbin1].keys():
                                for key,val in AllCovs[tag1][tag2][obj][cat1][cat2][nbin1][nbin2].iteritems():
                                    line = '%s %s %s %d %d %s %s %s %f \n'%(tag1,tag2,obj,cat1,cat2,nbin1,nbin2, key,val)
                                    fout.write(line)

    fout.close()
    return 0

