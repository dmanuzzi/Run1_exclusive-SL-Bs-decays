def f_calcAllEffRatios(din):
    dout = {}
    dout['BsSignal_i/BdKKpi_tot']   = f_calcEffRatios(din['Binned']['BsSignal'], din['Integrated']['BsSignal'], din['Integrated']['BdKKpi'])
    dout['BsSignal_i/BdKpipi_tot']  = f_calcEffRatios(din['Binned']['BsSignal'], din['Integrated']['BsSignal'], din['Integrated']['BdKpipi'])
    dout['BsSignal_i/BsSignal_tot'] = f_calcEffRatios(din['Binned']['BsSignal'], din['Integrated']['BsSignal'], din['Integrated']['BsSignal'])
    dout['BdKpipi_i/BdKKpi_tot']    = f_calcEffRatios(din['Binned']['BdKpipi'],  din['Integrated']['BdKpipi'],  din['Integrated']['BdKKpi'])
    dout['BdKpipi_i/BdKpipi_tot']   = f_calcEffRatios(din['Binned']['BdKpipi'],  din['Integrated']['BdKpipi'],  din['Integrated']['BdKpipi'])
    dout['BdKpipi_i/BsSignal_tot']  = f_calcEffRatios(din['Binned']['BdKpipi'],  din['Integrated']['BdKpipi'],  din['Integrated']['BsSignal'])
    dout['BdKKpi_i/BdKKpi_tot']     = f_calcEffRatios(din['Binned']['BdKKpi'],   din['Integrated']['BdKKpi'],   din['Integrated']['BdKKpi'])
    dout['BdKKpi_i/BdKpipi_tot']    = f_calcEffRatios(din['Binned']['BdKKpi'],   din['Integrated']['BdKKpi'],   din['Integrated']['BdKpipi'])
    dout['BdKKpi_i/BsSignal_tot']   = f_calcEffRatios(din['Binned']['BdKKpi'],   din['Integrated']['BdKKpi'],   din['Integrated']['BsSignal'])
    return dout

def f_calcEffRatios(dinBinned, dinIntegrated_num, dinIntegrated_den):
    dout = {}
    cats = dinBinned.keys()
    if cats != dinIntegrated_num.keys(): print 'WARNING: mismatch in cats between dinBinned and dinIntegrated_num'
    if cats != dinIntegrated_den.keys(): print 'WARNING: mismatch in cats between dinBinned and dinIntegrated_den'

    for cat in cats:
        dout[cat] = {}
        effBinned     = dinBinned[cat]['eff']
        effBinned_err = dinBinned[cat]['eff_err']
        effIntegrated_num     = dinIntegrated_num[cat]['eff'][0]
        effIntegrated_num_err = dinIntegrated_num[cat]['eff_err'][0]
        effIntegrated_den     = dinIntegrated_den[cat]['eff'][0]
        effIntegrated_den_err = dinIntegrated_den[cat]['eff_err'][0]
        dout[cat] = {}
        for i in range(0, len(effBinned)):
            ratio_tmp = effBinned[i]/effIntegrated_den
            ratio_err_tmp = ratio_tmp*((effBinned_err[i]/effBinned[i])**2+(effIntegrated_den_err/effIntegrated_den)**2)**(0.5)
            dout[cat][str(i+1)] = {'eff' : ratio_tmp, 'eff_err' : ratio_err_tmp}
        ratio_tmp = effIntegrated_num/effIntegrated_den
        ratio_err_tmp = ratio_tmp*((effIntegrated_num_err/effIntegrated_num)**2+(effIntegrated_den_err/effIntegrated_den)**2)**(0.5)
        dout[cat]['tot'] = {'eff' : ratio_tmp, 'eff_err' : ratio_err_tmp}
    return dout

