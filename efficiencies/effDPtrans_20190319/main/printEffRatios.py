def f_printEffRatios(inputs):
    print '\\begin{table}'
    print '\\centring'
    print '\\begin{tabular}{cc|ccccccc}'
    print '\\hline'
    for ratios, covMatrix, tag in inputs:
        line = tag
        for i in range(0,len(ratios)-1):
            line += ' & $%.3f \\pm %.3f$'%(ratios[i], covMatrix[i][i]**(0.5))
        line += ' & $%.3f \\pm  $'%(ratios[-1:][0])
        line += ' \\\\'
        print line
    print '\\hline'
    print '\\end{tabular}' 
    print '\\caption{Eff. Ratios}'
    print '\\end{table}'

