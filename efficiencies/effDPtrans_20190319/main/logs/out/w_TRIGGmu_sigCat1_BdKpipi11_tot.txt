********************************************************************************
*                         ---- LHCb Login v9r2p6 ----                          *
*      Building with gcc49 on slc6 x86_64 system (x86_64-slc6-gcc49-opt)       *
********************************************************************************
 --- User_release_area is set to /home/LHCB/dmanuzzi/cmtuser
 --- LHCBPROJECTPATH is set to:
    /cvmfs/lhcb.cern.ch/lib/lhcb
    /cvmfs/lhcb.cern.ch/lib/lcg/releases
    /cvmfs/lhcb.cern.ch/lib/lcg/app/releases
    /cvmfs/lhcb.cern.ch/lib/lcg/external
--------------------------------------------------------------------------------
here reweighter_parallel.py
input file :  /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/rew/srcRew/w_TRIGGmu_sigCat1_BdKpipi11_tot.pickle
   nfinCSs : ['/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/selected/Data-2011-JPsiMuMuUp.root', '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/selected/Data-2011-JPsiMuMuDw.root']
   nfinMCs : ['/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC//noTrigg/BdKpipi/MagUp/MC-2011-BdKpipiUp-noTrigg_Pythia6Unfiltered_0.root', '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC//noTrigg/BdKpipi/MagUp/MC-2011-BdKpipiUp-noTrigg_Pythia8Unfiltered_0.root', '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC//noTrigg/BdKpipi/MagDw/MC-2011-BdKpipiDw-noTrigg_Pythia6Unfiltered_0.root', '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC//noTrigg/BdKpipi/MagDw/MC-2011-BdKpipiDw-noTrigg_Pythia8Unfiltered_0.root']
        nw : w_TRIGGmu_sigCat1_BdKpipi11_tot
columns_CS :  ['muplus_PT', 'muplus_eta', 'muplus_IP_OWNPV']
columns_MC :  ['Mu_PT', 'Mu_ETA', 'Mu_IP']
    sel_CS :  1
    sel_MC :  (D_peak) && (signalCategory==1)
     nt_CS :  DecayTree
     nt_MC :  DMutuple
      NCSs :  2
129714
310971
Reading MC dataset...
NMCs :  4
22725
33574
45280
Number of entries -->  CS: 310971  MC:  45280 45280
Reweighting...  (D_peak) && (signalCategory==1) --> w_TRIGGmu_sigCat1_BdKpipi11_tot
