import sys
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/srcEff')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effMConly')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/rew')
from getK      import f_getK
from getCD import f_getCD
from f_reweigh2D_TRIGGmu import f_prepareCSsBu2JPsiK


def f_getAllVarsInRatios(configs):
    CSsBu2JPsiK = f_prepareCSsBu2JPsiK('/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/selected/', True, True)
    results = {}
    for years, modes, pols, pythias, cats, bins, inpathMC, outpathMC, tag in configs:
        results[tag] = {}
        results[tag]['K'] = f_getK(tag, modes, years,cats=cats, bins=bins,
                                   pythias=pythias, pols=pols,
                                   inpath=inpathMC, outpath=outpathMC)
        results[tag]['C'], results[tag]['D'] = f_getCD(years, modes, pols, pythias, cats, bins, inpathMC, outpathMC, tag, CSsBu2JPsiK)
    print results
    return results



        



