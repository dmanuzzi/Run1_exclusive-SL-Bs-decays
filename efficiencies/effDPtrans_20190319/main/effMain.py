'''
Run this with:
lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python effMain.py
'''
date = '20190319'
# path of saved MC tuples for each step of the selection
InputPathMC = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC/'
#path where to save .txt output files 
OutputPath  = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_'+date+'/results/' 
Polarities = ['Up', 'Dw']
Pythias    = ['Pythia6', 'Pythia8']
Bins =  [0.0,800.0,1100.0,1400.0,1700.0,2000.0,4500.0]
Bins =  [0.0,1700.0,4500.0]
Configs = [
    (['2011'],  ['BdKpipi'], Polarities, Pythias, [1], Bins, InputPathMC, OutputPath, 'BdKpipi11' ),
#    (['2012'],  ['BdKpipi'], Polarities, Pythias, [1,2], Bins, InputPathMC, OutputPath, 'BdKpipi12' ),
#    (['2012'],  ['BdKKpi'],  Polarities, Pythias, [1,2], Bins, InputPathMC, OutputPath, 'BdKKpi'  ),
#    (['2012'],  ['BsSignal'],Polarities, Pythias, [1,2], Bins, InputPathMC, OutputPath, 'BsSignal')
   ]
################################################################
################################################################
################################################################
import json
import sys
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_'+date+'/CovMatrix')
from f_getAllVarsInRatios import f_getAllVarsInRatios
from printEffRatios import f_printEffRatios
from getCovMatrix import f_getCovMatrix, f_getRatios
from f_getAllCov import f_getAllCov
################################################################
################################################################
#AllVars = f_getAllVarsInRatios(Configs)
#foutAllVars = open('../results/AllVars_'+date+'.txt', 'w')
#foutAllVars.write(json.dumps(AllVars))
#foutAllVars.close()
finAllVars = open('../results/AllVars_'+date+'.txt', 'r')
AllVars2 = json.loads(finAllVars.read())
finAllVars.close()
################################################################

AllCovs = f_getAllCov(Configs)
foutAllCovs = open('../results/AllCovs_'+date+'.txt', 'w')
foutAllCovs.write(json.dumps(AllCovs))
foutAllCovs.close()
finAllCovs = open('../results/AllCovs_'+date+'.txt', 'r')
AllCovs2 = json.loads(finAllCovs.read())
finAllCovs.close()
################################################################
################################################################


tags = ['BdKpipi11', 'BdKpipi12','BdKKpi', 'BsSignal']
tags = ['BdKpipi11']
inputs = []
for cat in [1]:
	for tag2 in tags:
		for tag1 in tags:
			print tag1+' OVER '+tag2+'  cat '+str(cat)
			ratios = f_getRatios(tag1, tag2,str(cat),str(cat), Bins, AllVars2)
			cov_matrix = f_getCovMatrix(tag1, tag2,str(cat),str(cat), Bins, AllVars2, AllCovs2)
			for i in range(0, len(cov_matrix)):
				print cov_matrix[i]
			print ratios
			inputs.append((ratios, cov_matrix, '%s\\_i/%s\\_tot & cat=%d'%(tag1,tag2,cat)))

f_printEffRatios(inputs)

