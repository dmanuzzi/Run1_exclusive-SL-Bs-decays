from ROOT import gROOT
import sys
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/srcEff/')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/rew')
from getCov import f_getCov
from f_reweigh2D_TRIGGmu import f_prepareCSsBu2JPsiK

#gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/getEff/getEffTRIGGmuBinned.cpp++')
getCov_TRIGGmu    = __import__('ROOT.getCov_TRIGGmu'   ,globals(),locals(),['getCov_TRIGGmu'])


def f_getAllCov(configs):
    CSsBu2JPsiK = f_prepareCSsBu2JPsiK('/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/selected/', False, False)
#    CSsBu2JPsiK = None

    results = {}
    for i in range(0, len(configs)):
        years1, modes1, pols1, pythias1, cats1, bins1, inpathMC1, outpathMC1, tag1 = configs[i]
        results[tag1] = {}
        for j in range(i, len(configs)):
            years2, modes2, pols2, pythias2, cats2, bins2, inpathMC2, outpathMC2, tag2 = configs[j]
            results[tag1][tag2] = {}
            nfoutCD = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/covTRIGGmu_%s_%s.txt'%(tag1,tag2)
            results[tag1][tag2]['Ccov'], results[tag1][tag2]['Dcov'] = f_getCov(getCov_TRIGGmu, tag1, years2, pols2, cats2, bins2, tag2, CSsBu2JPsiK, nfoutCD)

    print results
    return results



        



