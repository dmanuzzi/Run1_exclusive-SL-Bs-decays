#!/bin/bash
## before using this, do:
# lhcb-proxy-init
# run with:
# /home/LHCB/dmanuzzi/LHCb_package/UraniaDev_v7r0/run ./run_PIDCalib.sh 

here=$(pwd)
mcPATH="/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC/"
#MC_type[0]='BsSignal'
#MC_type[1]='BdKKpi'
MC_type[0]='BdKpipi'

polarity[0]='MagUp'
polarity[1]='MagDown'
pol[0]='Up'
pol[1]='Dw'
year[0]='21r1'
year[1]='21'
YEAR[0]='2011'
YEAR[1]='2012'


############## splitting signalCategories #####################################
#cd $mcPATH
#srcSelectionPATH="/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/srcSelection/"
#for pol in 'Up' 'Dw'; do #pol loop
#    for pythia in 'Pythia6' 'Pythia8'; do #pythia loop
#       for cat in 1 2; do
#	   pathOUT='/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effPIDhad/PIDCalib/tuples/'
#	   #BsSignal
#	   lb-run ROOT/6.08.06 root -l -b -q $srcSelectionPATH/SelSigCategory.C+'('$cat', "./noTrigg_noPID/BsSignal/Mag'$pol'/MC-2012-BsSignal'$pol'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0.root", "DMutuple", "'$pathOUT'MC-2012-BsSignal'$pol'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0_SigCat'$cat'.root")'
#	   #BdKKpi
#	   lb-run ROOT/6.08.06 root -l -b -q $srcSelectionPATH/SelSigCategory.C+'('$cat', "./noTrigg_noPID/BdKKpi/Mag'$pol'/MC-2012-BdKKpi'$pol'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0.root", "DMutuple", "'$pathOUT'MC-2012-BdKKpi'$pol'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0_SigCat'$cat'.root")'
#	   #BdKpipi
#	   lb-run ROOT/6.08.06 root -l -b -q $srcSelectionPATH/SelSigCategory.C+'('$cat', "./noTrigg_noPID/BdKpipi/Mag'$pol'/MC-2012-BdKpipi'$pol'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0.root", "DMutuple", "'$pathOUT'MC-2012-BdKpipi'$pol'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0_SigCat'$cat'.root")'
#	   lb-run ROOT/6.08.06 root -l -b -q $srcSelectionPATH/SelSigCategory.C+'('$cat', "./noTrigg_noPID/BdKpipi/Mag'$pol'/MC-2011-BdKpipi'$pol'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0.root", "DMutuple", "'$pathOUT'MC-2011-BdKpipi'$pol'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0_SigCat'$cat'.root")'
#       done
#    done
#done
#######################################################################

cd $here

K_CUT="DLLK>4 && MC12TuneV2_ProbNNK>0.2"
Pi_CUT="DLLK<10 && MC12TuneV2_ProbNNpi>0.5"
K_Cut="[$K_CUT]"
Pi_Cut="[$Pi_CUT]"
KCut="[K,K,$K_CUT]" 
Pi1Cut="[Pi1,Pi,$Pi_CUT]"
Pi2Cut="[Pi2,Pi,$Pi_CUT]" 

scriptsPATH=$PIDPERFSCRIPTSROOT/scripts/python/MultiTrack
BinScheme="./my_customBinning.py"
nBinScheme="my_Scheme1"
Kin_Cut_K=" P>3000 && PT>500 "
Kin_Cut_Pi=" P>5000 && PT>500 "


for polarity in 'MagUp' 'MagDown'; do
    for year in '21' '21r1'; do
	python $scriptsPATH/MakePerfHistsRunRange.py  $year $polarity "K"  "$K_Cut"  "P" "ETA" ""  
	#-c "$Kin_Cut_K"  
	#--minRun=103000 --maxRun=115000
	#--binSchemeFile="$BinScheme" --schemeName="$nBinScheme"
	python $scriptsPATH/MakePerfHistsRunRange.py  $year $polarity "Pi" "$Pi_Cut" "P" "ETA" ""  
	#-c "$Kin_Cut_Pi" 
	#--minRun=103000 --maxRun=115000
	#--binSchemeFile="$BinScheme" --schemeName="$nBinScheme"
    done
done



for ((i=0; i<2; ++i)); do #polarity loop
    for ((h=0; h<2; ++h)); do #year loop
	for ((k=0; k<1; ++k)); do #MC type loop
	    for pythia in 'Pythia6' 'Pythia8'; do
		echo $i $h $k $pythia
		MC="./tuples/MC-"${YEAR[$h]}"-"${MC_type[$k]}${pol[$i]}'-noTrigg_noPID_'$pythia'UnfilteredPIDCorr_0'
		for j in 1 2; do #sigCat loop
		    echo 'stocazzo'
#		    python $scriptsPATH/PerformMultiTrackCalib.py ${year[$h]} ${polarity[$i]} $MC'_SigCat'$j'.root' "DMutuple" "./"${MC_type[$k]}$"/MyPIDResults_"${YEAR[$h]}"_"${polarity[$i]}$pythia"_AllCuts.root" "$KCut" "$Pi1Cut" "$Pi2Cut"  -X "P" -Y "ETA" -Z "" -x "P" -y "ETA" | tee "./"${MC_type[$k]}$"/MyPIDResults"${YEAR[$h]}"_"${polarity[$i]}$pythia'_AllCuts_SigCat'$j'.txt'  
		done
	    done
	done
    done
done


for ((k=0; k<1; ++k)); do #MC type loop
    for ((h=0; h<2; ++h)); do #year loop
	for ((i=0; i<2; ++i)); do #polarity loop		
	    for ((j=1; j<=2; ++j)); do #sigCat loop
		for pythia in 'Pythia6' 'Pythia8'; do
		   # fileIN="./"${MC_type[$k]}$"/MyPIDResults"${YEAR[$h]}"_"${polarity[$i]}$pythia'_AllCuts_SigCat'$j'.txt'
		   # eff=$(cat $fileIN | grep 'Naive event' | awk '{ sum = $6 } END { print sum }')
		   # err=$(cat $fileIN | grep 'Naive event' | awk '{ sum = $8 } END { print sum }')
		   # echo ${MC_type[$k]} ${YEAR[$h]} ${pol[$i]} 'SigCat'$j $pythia "......  " $eff "+/-" $err
		    echo 'stocazzo'
		done
	    done
	    echo "----------------------------------------------------------------------"
	done
   done
    echo "----------------------------------------------------------------------"
done