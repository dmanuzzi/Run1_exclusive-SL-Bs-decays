#include "my_PIDCalib_2D_BdKpipi.cpp"
#include "/home/LHCB/fferrari/exclusive-SL-Bs-decays/Run1/tools/Tools.h"
#include <fstream>
#include "TFile.h"
#include "TString.h"
int getEffPIDCalibBinned(){
  TString nfout = "./PIDCalibBinnedResults.txt";
  TFile fout(nfout, "RECREATE");
  fout.Close();

  ofstream pout(nfout, std::ios_base::app);
  for (unsigned int i=0; i< Constants::NDptBins; ++i){
    TString DPtransCut = TString::Format("(%f < D_Ptrans && D_Ptrans < %f)", Constants::DptBins[i],Constants::DptBins[i+1]);
    TString cut = "w_FF_corr*D_peak*"+DPtransCut;
    cout << "\n --------" << cut << "--------\n";
    pout << "\n --------" << cut << "--------\n";
    my_PIDCalib_2D_BdKpipi(cut);
  }
  return 1;
}
