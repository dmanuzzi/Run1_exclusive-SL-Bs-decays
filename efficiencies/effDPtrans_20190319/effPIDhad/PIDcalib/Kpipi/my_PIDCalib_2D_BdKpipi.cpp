#include "iostream"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCut.h"
#include <fstream>
using namespace std;
double effKpipi(TTree *tin_MC, TString part, TString year, TString pol, TString nw, TString nfout, TString tag, TString pathHERE = "./");
//double effKKpiTOT_1(TString nfin_MC, TString nfin_MC_f, TString MC_type, TString pol, TString nw, TString nfout);
double effKpipiTOT_2(TString nfin_MC, TString nfin_MC_f, TString MC_type, TString pol, TString nw, TString nfout);
int my_PIDCalib_2D_BdKpipi(TString nw = "w_FF*D_peak"){
  TString pathHERE = "./";
  TString pathMC="/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC_new/noTrigg_noPID/";

  TString nfout = "./my_PIDCalib_output_2D_BdKpipi.root";
  TFile *fout = new TFile(nfout, "RECREATE");
  fout->Close();



  TString nfin_MC("none"), nfin_MC_f("none");
  double (*effTOT)(TString nfin_MC, TString nfin_MC_f, TString MC_type, TString pol, TString nw, TString nfout);
  effTOT = effKpipiTOT_2;


  
  nfin_MC = pathMC+"BdKpipi/MagUp/MC-2011-BdKpipiUp-noTrigg_noPID_Pythia6Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BdKpipi", "Up", nw, nfout);
  nfin_MC = pathMC+"BdKpipi/MagUp/MC-2011-BdKpipiUp-noTrigg_noPID_Pythia8Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BdKpipi", "Up", nw, nfout);
  nfin_MC = pathMC+"BdKpipi/MagUp/MC-2012-BdKpipiUp-noTrigg_noPID_Pythia6Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BdKpipi", "Up", nw, nfout);
  nfin_MC = pathMC+"BdKpipi/MagUp/MC-2012-BdKpipiUp-noTrigg_noPID_Pythia8Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BdKpipi", "Up", nw, nfout);
  
  nfin_MC = pathMC+"BdKpipi/MagDw/MC-2011-BdKpipiDw-noTrigg_noPID_Pythia6Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BdKpipi", "Dw", nw, nfout);
  nfin_MC = pathMC+"BdKpipi/MagDw/MC-2011-BdKpipiDw-noTrigg_noPID_Pythia8Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BdKpipi", "Dw", nw, nfout);
  nfin_MC = pathMC+"BdKpipi/MagDw/MC-2012-BdKpipiDw-noTrigg_noPID_Pythia6Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BdKpipi", "Dw", nw, nfout);
  nfin_MC = pathMC+"BdKpipi/MagDw/MC-2012-BdKpipiDw-noTrigg_noPID_Pythia8Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BdKpipi", "Dw", nw, nfout);
  

  nfin_MC = pathMC+"BuKpipi/MagUp/MC-2011-BuKpipiUp-noTrigg_noPID_Pythia6Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BuKpipi", "Up", nw, nfout);
  nfin_MC = pathMC+"BuKpipi/MagUp/MC-2011-BuKpipiUp-noTrigg_noPID_Pythia8Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BuKpipi", "Up", nw, nfout);
  nfin_MC = pathMC+"BuKpipi/MagUp/MC-2012-BuKpipiUp-noTrigg_noPID_Pythia6Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BuKpipi", "Up", nw, nfout);
  nfin_MC = pathMC+"BuKpipi/MagUp/MC-2012-BuKpipiUp-noTrigg_noPID_Pythia8Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BuKpipi", "Up", nw, nfout);
  
  nfin_MC = pathMC+"BuKpipi/MagDw/MC-2011-BuKpipiDw-noTrigg_noPID_Pythia6Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BuKpipi", "Dw", nw, nfout);
  nfin_MC = pathMC+"BuKpipi/MagDw/MC-2011-BuKpipiDw-noTrigg_noPID_Pythia8Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BuKpipi", "Dw", nw, nfout);
  nfin_MC = pathMC+"BuKpipi/MagDw/MC-2012-BuKpipiDw-noTrigg_noPID_Pythia6Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BuKpipi", "Dw", nw, nfout);
  nfin_MC = pathMC+"BuKpipi/MagDw/MC-2012-BuKpipiDw-noTrigg_noPID_Pythia8Unfiltered_0.root";
  effTOT(nfin_MC, nfin_MC_f, "BuKpipi", "Dw", nw, nfout);
  

  return 1; 
}
/*
double effKKpiTOT_1(TString nfin_MC, TString nfin_MC_f, TString MC_type, TString pol, TString nw, TString nfout){
  TCut cut_sigCat = "signalCategory==0";
  TCut cut_K1     = "K1_PIDK>-2 && K1_ProbNNk>0.175";
  TCut cut_K2     = "K2_PIDK>-2 && K2_ProbNNk>0.625";
  //  TCut cut_Pi     = "Pi_PIDK<10 && Pi_ProbNNpi>0.25";  
  double ris_K1=0., ris_K2=0., ris_Pi=0.;

  TChain *tin_MC  = new TChain("DMutuple");
  tin_MC->Add(nfin_MC);
  tin_MC->AddFriend("DMutuple", nfin_MC_f);
  cut_sigCat = "signalCategory==1";
  ris_K1 = effKKpi(tin_MC, "K1", pol, TString(nw.Data()*(cut_sigCat)), nfout, MC_type+"_K1_sigCat1");
  ris_K2 = effKKpi(tin_MC, "K2", pol, TString(nw.Data()*(cut_sigCat && cut_K1)), nfout, MC_type+"_K2_sigCat1");
  ris_Pi = effKKpi(tin_MC, "Pi", pol, TString(nw.Data()*(cut_sigCat && cut_K1 && cut_K2)), nfout, MC_type+"_Pi_sigCat1");
  cout << " "+MC_type+pol+" result (sigCat1) : " << ris_K1*ris_K2*ris_Pi << endl;
  cut_sigCat = "signalCategory==2";
  ris_K1 = effKKpi(tin_MC, "K1", pol, TString(nw.Data()*(cut_sigCat)), nfout, MC_type+"_K1_sigCat2");
  ris_K2 = effKKpi(tin_MC, "K2", pol, TString(nw.Data()*(cut_sigCat && cut_K1)), nfout, MC_type+"_K2_sigCat2");
  ris_Pi = effKKpi(tin_MC, "Pi", pol, TString(nw.Data()*(cut_sigCat && cut_K1 && cut_K2)), nfout, MC_type+"_Pi_sigCat2");
  cout << " "+MC_type+pol+" result (sigCat2) : " << ris_K1*ris_K2*ris_Pi << endl;
  delete tin_MC;
  return 1;
}
*/
double effKpipiTOT_2(TString nfin_MC, TString nfin_MC_f, TString MC_type, TString pol, TString nw, TString nfout){

  TChain *tin_MC  = new TChain("DMutuple");
  tin_MC->Add(nfin_MC);
  if (nfin_MC_f != "none")
    tin_MC->AddFriend("DMutuple", nfin_MC_f);
  TString year("none");
  if (nfin_MC.Contains("2011"))
    year="2011";
  else if (nfin_MC.Contains("2012"))
    year="2012";
  else
    year="allYears";
  TString pythia("none");
  if (nfin_MC.Contains("Pythia6"))
    pythia="Pythia6";
  else if (nfin_MC.Contains("Pythia8"))
    pythia="Pythia8";
  else
    pythia="allPythias";
  
  ofstream pout("PIDCalibBinnedResults.txt", std::ios_base::app);
  if (nfin_MC.Contains("BdKpipi")){
    for (int i=1; i<=4; ++i){
      double ris_Pi1=0., ris_Pi2=0., ris_K=0.;
      TString ni = TString::Format("%d", i);
      TString cut_sigCat = "signalCategory=="+ni;
      ris_Pi1 = effKpipi(tin_MC, "Pi1", year, pol, TString(nw.Data()*(cut_sigCat)), nfout, MC_type+"_"+year+"_"+pythia+"_Pi1_sigCat"+ni);
      ris_Pi2 = effKpipi(tin_MC, "Pi2", year, pol, TString(nw.Data()*(cut_sigCat)), nfout, MC_type+"_"+year+"_"+pythia+"_Pi2_sigCat"+ni);
      ris_K   = effKpipi(tin_MC, "K"  , year, pol, TString(nw.Data()*(cut_sigCat)), nfout, MC_type+"_"+year+"_"+pythia+"_K_sigCat"+ni  );
      cout << " "+MC_type+pol+" "+year+" "+pythia+" result (sigCat"+ni+") : " << ris_Pi1*ris_Pi2*ris_K << endl;
      pout << " "+MC_type+pol+" "+year+" "+pythia+" result (sigCat"+ni+") : " << ris_Pi1*ris_Pi2*ris_K << endl;
    }
  } else {
    double ris_Pi1=0., ris_Pi2=0., ris_K=0.;
    ris_Pi1 = effKpipi(tin_MC, "Pi1", year, pol, nw.Data(), nfout, MC_type+"_"+year+"_"+pythia+"_Pi1");
    ris_Pi2 = effKpipi(tin_MC, "Pi2", year, pol, nw.Data(), nfout, MC_type+"_"+year+"_"+pythia+"_Pi2");
    ris_K   = effKpipi(tin_MC, "K"  , year, pol, nw.Data(), nfout, MC_type+"_"+year+"_"+pythia+"_K"  );
    cout << " "+MC_type+pol+" "+year+" "+pythia+" result : " << ris_Pi1*ris_Pi2*ris_K << endl;
    pout << " "+MC_type+pol+" "+year+" "+pythia+" result : " << ris_Pi1*ris_Pi2*ris_K << endl;
  }
  delete tin_MC;
  return 1;
}

double effKpipi(TTree *tin_MC, TString part, TString year, TString pol,  TString nw, TString nfout, TString tag, TString pathHERE){
  TH1::SetDefaultSumw2();
  int POL = -1;
  if (pol == "Up") POL = 1;
  if (pol == "Dw") POL = 0;

  TString stripV("none");
  if (year=="2011")
    stripV="Strip21r1";
  else if (year=="2012")
    stripV="Strip21";
  else
    return 0.;
  TString binScheme("");

  TString nfin_PIDCalib_K_Dw = pathHERE+"PerfHists_K_"+stripV+"_MagDown"+binScheme+"_P_ETA.root";
  TString nfin_PIDCalib_K_Up = pathHERE+"PerfHists_K_"+stripV+"_MagUp"+binScheme+"_P_ETA.root";
  TString nfin_PIDCalib_Pi_Dw= pathHERE+"PerfHists_Pi_"+stripV+"_MagDown"+binScheme+"_P_ETA.root";
  TString nfin_PIDCalib_Pi_Up= pathHERE+"PerfHists_Pi_"+stripV+"_MagUp"+binScheme+"_P_ETA.root";
  TString nh_PIDCalib_K_passed  = "PassedHist_K_DLLK>4 && MC12TuneV2_ProbNNK>0.2_All__K_P_K_Eta";
  TString nh_PIDCalib_K_total   = "TotalHist_K_DLLK>4 && MC12TuneV2_ProbNNK>0.2_All__K_P_K_Eta";
  TString nh_PIDCalib_Pi_passed = "PassedHist_Pi_DLLK<10 && MC12TuneV2_ProbNNpi>0.5_All__Pi_P_Pi_Eta";
  TString nh_PIDCalib_Pi_total  = "TotalHist_Pi_DLLK<10 && MC12TuneV2_ProbNNpi>0.5_All__Pi_P_Pi_Eta"; 
  TString nfin_PIDCalib, nh_PIDCalib_passed, nh_PIDCalib_total;
  if (part == "K"){
    nfin_PIDCalib = (POL)? nfin_PIDCalib_K_Up : nfin_PIDCalib_K_Dw;
    nh_PIDCalib_passed = nh_PIDCalib_K_passed;
    nh_PIDCalib_total  = nh_PIDCalib_K_total;
  }
  if (part=="Pi1" || part=="Pi2"){
    nfin_PIDCalib = (POL)? nfin_PIDCalib_Pi_Up : nfin_PIDCalib_Pi_Dw;
    nh_PIDCalib_passed = nh_PIDCalib_Pi_passed;
    nh_PIDCalib_total  = nh_PIDCalib_Pi_total;
  }
    
  TFile *fin_PIDCalib = TFile::Open(nfin_PIDCalib, "READ");

  TH2F *h_PIDCalib_passed = (TH2F*)fin_PIDCalib->Get(nh_PIDCalib_passed);
  TH2F *h_PIDCalib_total  = (TH2F*)fin_PIDCalib->Get(nh_PIDCalib_total);

  const int NbinsX=h_PIDCalib_passed->GetNbinsX();
  const int NbinsY=h_PIDCalib_passed->GetNbinsY();

  Double_t binsX[NbinsX+1],binsY[NbinsY+1];
  h_PIDCalib_total->GetXaxis()->GetLowEdge(binsX);
  h_PIDCalib_total->GetYaxis()->GetLowEdge(binsY);
  binsX[NbinsX]=h_PIDCalib_total->GetXaxis()->GetXmax();
  binsY[NbinsY]=h_PIDCalib_total->GetYaxis()->GetXmax();
  //  tin_MC->Print("*_corr");
  TH2F *h_MC = new TH2F("h_MC_"+tag+pol, "h_MC_"+tag+pol, NbinsX, binsX, NbinsY, binsY);
  tin_MC->Draw(part+"_ETA:"+part+"_P>>h_MC_"+tag+pol, nw, "goff");
  //double N_MC = tin_MC->GetEntries(nw); ////// mmmmmmmmmmmmm controllare
  
  TH2F *h = new TH2F("h_"+tag+pol ,"h_"+tag+pol, NbinsX, binsX, NbinsY, binsY);
  h->Divide(h_PIDCalib_passed, h_PIDCalib_total);
  h->Multiply(h_MC);
    
  double integral=0., N_MC=0.;
  integral = h->Integral();
  N_MC = h_MC->Integral();
  /*
  for (int x=1; x <= NbinsX; ++x){
    for (int y=1; y <= NbinsY; ++y){
      int i = h->GetBin(x,y);
      integral = integral+h->GetBinContent(i);
      i = h_MC->GetBin(x,y);
      N_MC = N_MC+h_MC->GetBinContent(i);
    }
  }
  */
  double result = integral/N_MC;
  
  //cout << part << endl;
  //cout << "    entries = " << N_MC << endl;
  //cout << "    result  = " << result << endl; 

  TFile *fout = TFile::Open(nfout, "UPDATE");
  fout->WriteTObject(h, h->GetName(), "Overwrite");
  fout->WriteTObject(h_MC, h_MC->GetName(), "Overwrite");
  fout->WriteTObject(h_PIDCalib_passed, h_PIDCalib_passed->GetName()+TString("___")+pol, "Overwrite");
  fout->WriteTObject(h_PIDCalib_total,  h_PIDCalib_total->GetName()+TString("___")+pol, "Overwrite");  
  fout->Close();
  return result;
}
