
dEff_web = { 'BdKpipi11' : { 'Pythia6' : {'Up' : { 'eff' : 0.15885, 'eff_err' : 0.00058}, 
                                          'Dw' : { 'eff' : 0.15852, 'eff_err' : 0.00097}},
                             'Pythia8' : {'Up' : { 'eff' : 0.1673, 'eff_err' : 0.0011}, 
                                          'Dw' : { 'eff' : 0.1671, 'eff_err' : 0.0012}}
                             },
             'BdKpipi12' : { 'Pythia6' : {'Up' : { 'eff' : 0.16099, 'eff_err' : 0.00059}, 
                                          'Dw' : { 'eff' : 0.16014, 'eff_err' : 0.00084}},
                             'Pythia8' : {'Up' : { 'eff' : 0.16972, 'eff_err' : 0.00087}, 
                                          'Dw' : { 'eff' : 0.17066, 'eff_err' : 0.00088}}
                             },
             'BdKKpi'    : { 'Pythia6' : {'Up' : { 'eff' : 0.17102, 'eff_err' : 0.00042}, 
                                          'Dw' : { 'eff' : 0.17181, 'eff_err' : 0.00042}},
                             'Pythia8' : {'Up' : { 'eff' : 0.18081, 'eff_err' : 0.00045}, 
                                          'Dw' : { 'eff' : 0.17971, 'eff_err' : 0.00045}}
                             },
             'BsSignal'  : { 'Pythia6' : {'Up' : { 'eff' : 0.17242, 'eff_err' : 0.00063}, 
                                          'Dw' : { 'eff' : 0.17126, 'eff_err' : 0.00063}},
                             'Pythia8' : {'Up' : { 'eff' : 0.18033, 'eff_err' : 0.00066}, 
                                          'Dw' : { 'eff' : 0.18129, 'eff_err' : 0.00093}}
                             },
             }

from getEffGenLevCut import f_readEffGenLevCut
from getNbk import f_readNbk
tags = [('BdKpipi11', 'BdKpipi', '2011'), 
        ('BdKpipi12', 'BdKpipi', '2012'), 
        ('BdKKpi',    'BdKKpi',  '2012'),
        ('BsSignal',  'BsSignal','2012')]
pythias = ['Pythia6', 'Pythia8']
pols = ['Up','Dw']
dEff_my = {}
for tag, mode, year in tags:
    fstate = 'KKpi'
    if 'Kpipi' in mode: fstate = 'Kpipi'
    pathGen  = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/GenLevMC/MC_%s'%(fstate)
    dEff_my[tag] = {} 
    for pythia in pythias:
        dEff_my[tag][pythia] = {}
        for pol in pols:
            POL = pol
            if pol == 'Dw': POL = 'Down'
            eff = f_readEffGenLevCut(mode, year, pythia, POL, pathGen)
            Nbk = f_readNbk(mode, year, pythia, pol)
            dEff_my[tag][pythia][pol] = {'eff' : eff['eff'], 'eff_err' : eff['eff_err'], 'num' : eff['num'], 'den' : eff['den'],  'Nbk' : Nbk}

for tag, mode, year in tags:
    num_web = 0
    den_web = 0
    num_my = 0
    den_my = 0
    for pythia in pythias:
        for pol in pols:
            eff_web = dEff_web[tag][pythia][pol]['eff']
            eff_web_err = dEff_web[tag][pythia][pol]['eff_err']
            eff_my = dEff_my[tag][pythia][pol]['eff']
            eff_my_err = dEff_my[tag][pythia][pol]['eff_err']
            Nbk = dEff_my[tag][pythia][pol]['Nbk']
            line = '%s %s %s    %.4f \\pm %.4f  |  %.4f \\pm %.4f   |  %d'%(tag, pythia, pol, eff_web, eff_web_err, eff_my, eff_my_err, Nbk)
            num_web+=Nbk
            den_web+=Nbk/eff_web
            num_my+=dEff_my[tag][pythia][pol]['num']
            den_my+=dEff_my[tag][pythia][pol]['den']
            print line
    eff_web = num_web/den_web
    eff_web_err = (eff_web*(1-eff_web)/den_web)**(0.5)
    eff_my = float(num_my)/float(den_my)
    eff_my_err = (eff_my*(1-eff_my)/den_my)**(0.5)
    line = '=== %s  %.5f \\pm %.5f  |  %.5f \\pm %.5f'%(tag, eff_web, eff_web_err, eff_my, eff_my_err)
    print line

