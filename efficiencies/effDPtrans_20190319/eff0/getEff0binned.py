from ROOT import TChain, gROOT
import array as arr
from getEff0integrated import f_getEff0integrated

gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/srcEff/getEff_D_Ptrans.cpp++')
getHist    = __import__('ROOT.getHist'   ,globals(),locals(),['getHist'])
createFout = __import__('ROOT.createFout',globals(),locals(),['createFout'])
doEffHist  = __import__('ROOT.doEffHist' ,globals(),locals(),['doEffHist'])
getEfficiencies =  __import__('ROOT.getEfficiencies', globals(), locals(),['getEfficiencies'])
normal_tools = (getHist,createFout,doEffHist,getEfficiencies)

def f_getEff0binned(mode, year, pythia, pol, pathGen, pathTuples, ntin, nw, nfout, tools=normal_tools):
    getHist,createFout,doEffHist,getEfficiencies = tools
    results = {}
    eff0integrated = f_getEff0integrated(mode,year, pythia, pol, pathGen)
    createFout(nfout, "UPDATE")
    nfin = pathTuples+'/{year}_{mode}0_{pythia}Unfiltered_Mag{pol}_{GenLevCut}_TM.root'
    nfin_num = nfin.format(year=year, mode=mode, pythia=pythia, pol=pol, GenLevCut='ListOfDaughtersInLHCb')
    nfin_den = nfin.format(year=year, mode=mode, pythia=pythia,pol=pol, GenLevCut='none')
    tin_num = TChain(ntin)
    tin_den = TChain(ntin)
    tin_num.Add(nfin_num)
    tin_den.Add(nfin_den)
    cats = [0]
    tmp_cats = arr.array('i', cats)
    TAG = '%s_%s_Unfiltered_Mag%s'%(year,mode,pol)
    results[TAG] = {}
    getHist(tin_num, len(cats), tmp_cats,  nfout, 'h_num_'+TAG,  nw)
    getHist(tin_den, len(cats), tmp_cats,  nfout, 'h_den_'+TAG,  nw)
    for cat in cats:
        scaleFactor = eff0integrated['eff']
        print scaleFactor
        doEffHist( nfout,  'h_den_%s_%d'%(TAG,cat), nfout, 'h_num_%s_%d'%(TAG,cat), nfout, 'eff_%s_%d'%(TAG,cat), scaleFactor);
        leffs = leffs_err = [1, 2, 3, 4, 5, 6]
        effs     = arr.array('d', leffs)
        effs_err = arr.array('d', leffs_err)
        getEfficiencies(nfout, 'eff_%s_%d'%(TAG,cat), effs,  effs_err)
        effs = effs.tolist()
        effs_err = effs_err.tolist()
        results[TAG][cat] = {'eff' : effs, 'eff_err' : effs_err}
                    
    return results

'''
pathGen = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/GenLevMC/output/MC_Kpipi'
modes = ['BdKpipi', 'BuKpipi']
years = ['2011', '2012']
pythias = ['Pythia6', 'Pythia8']
pols  = ['Up','Down']

#modes = ['BdKpipi']
#years = ['2011']
#pythias = ['Pythia6']
#pols  = ['Up']

pathTuples = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/eff0/eff_D_ptrans/selection/TruthMatched/'
ntin = 'MCDecayTree'
nw = 'none'
outPath = './'
nfout = outPath+'/fout_eff0byMYGEN.root'
########################################################################################
########################################################################################

results = {}
for mode in modes:
    for year in years:
        for pythia in pythias:
            for pol in pols:
                results['%s %s %s %s'%(mode,year,pythia,pol)] = f_getEff0binned(mode, year, pythia, pol, pathGen, pathTuples, ntin, nw, nfout)

print results
'''
