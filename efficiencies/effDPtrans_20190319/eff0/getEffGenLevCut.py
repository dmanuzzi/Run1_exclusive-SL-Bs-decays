import os


def f_readEffGenLevCut_Tabulated(mode, year, pythia, pol):
    dict_effGenLev = {
        'BdKpipi' : { '2011' : {'Pythia6' : {'Up': { 'eff' : 0.1588 , 'eff_err' : 0.0006},
                                             'Dw': { 'eff' : 0.1585 , 'eff_err' : 0.0010}
                                             },
                                'Pythia8' : {'Up': { 'eff' : 0.1673 , 'eff_err' : 0.0011},
                                             'Dw': { 'eff' : 0.1671 , 'eff_err' : 0.0012}
                                             }
                                },
                      '2012' : {'Pythia6' : {'Up': { 'eff' : 0.1610 , 'eff_err' : 0.0006},
                                             'Dw': { 'eff' : 0.1601 , 'eff_err' : 0.0008}
                                             },
                                'Pythia8' : {'Up': { 'eff' : 0.1697 , 'eff_err' : 0.0009},
                                             'Dw': { 'eff' : 0.1701 , 'eff_err' : 0.0009}
                                             }
                                }
                      },
        'BdKKpi' : { '2012' : {'Pythia6' : {'Up': { 'eff' : 0.1710 , 'eff_err' : 0.0004},
                                            'Dw': { 'eff' : 0.1718 , 'eff_err' : 0.0004}
                                            },
                               'Pythia8' : {'Up': { 'eff' : 0.1808 , 'eff_err' : 0.0004},
                                            'Dw': { 'eff' : 0.1797 , 'eff_err' : 0.0004}
                                            }
                               }
                     },
        'BsSignal' : { '2012' : {'Pythia6' : {'Up': { 'eff' : 0.1724 , 'eff_err' : 0.0006},
                                              'Dw': { 'eff' : 0.1713 , 'eff_err' : 0.0006}
                                              },
                                 'Pythia8' : {'Up': { 'eff' : 0.1803 , 'eff_err' : 0.0007},
                                              'Dw': { 'eff' : 0.1813 , 'eff_err' : 0.0009}
                                              }
                                 }
                       }

        }
    return dict_effGenLev[mode][year][pythia][pol]['eff'], dict_effGenLev[mode][year][pythia][pol]['eff_err']

def f_getEffGenLevCut(tag, modes, years, pythias, pols, pathGen):
    print '========================================================='
    num = 0
    den = 0
    for mode in modes:
        for year in years:
            for pythia in  pythias:
                for pol in pols:
                    dict_tmp = f_readEffGenLevCut(mode, year, pythia, pol, pathGen)
                    num += dict_tmp['num']
                    den += dict_tmp['den']                    
    effGenLevCut = float(num)/float(den)
    effGenLevCut_err = (effGenLevCut*(1.0-effGenLevCut)/den)**(0.5)
    return effGenLevCut, effGenLevCut_err

def f_readEffGenLevCut(mode,year, pythia, POL, pathGen):
    pol = POL
    if POL == 'Dw': pol = 'Down'
    dirName = '%s_%s0_%sUnfiltered_Mag%s_ListOfDaughtersInLHCb'%(year,mode,pythia,pol)
    dirs = os.popen('ls %s/%s'%(pathGen,dirName)).read()
    Nsjobs = len(dirs.split('\n')[:-1])
    if Nsjobs==0: return 0
#                print ' N sub-jobs        : ',Nsjobs
    num = 0
    den = 0
    for sjob in range(0,Nsjobs):
        log  = open('%s/%s/%d/GeneratorLog.xml'%(pathGen,dirName,sjob)).read()
        lines_log = log.split('\n')[:-1]
        index = -1
        for line_log in lines_log:
            if '<efficiency name = "generator level cut">' in line_log:
                index = lines_log.index(line_log)
        if index == -1:
            print ' ERROR             :  Generetor Level Cut information not found in %s/%s_%s%d_%s%s/Mag%s/%d/GeneratorLog.xml'%(pathGen,year,cat,mode,pythia,filt,pol,sjob)
            continue
        tmp_num = int(lines_log[index+1].split('>')[1].split('<')[0])
        tmp_den = int(lines_log[index+2].split('>')[1].split('<')[0])
        num += tmp_num
        den += tmp_den
        #                   print ' N before          : ',den
        #                   print ' N after           : ',num
        tmp_eff = float(tmp_num)/float(tmp_den)
        tmp_eff_err = (tmp_eff*(1-tmp_eff)/tmp_den)**0.5
#        print '%d eff.              : %.4f \pm %.4f'%(sjob,tmp_eff,tmp_eff_err)
    eff = float(num)/float(den)
    eff_err = (eff*(1-eff)/den)**0.5
    print mode, year, pythia, POL, 'num =',num,' den=',den, 'effGenLevCut = %.4f \pm %.4f'%(eff,eff_err) 
    results = { 'num' : num, 'den':den, 'eff': eff, 'eff_err':eff_err}
    return results
    
