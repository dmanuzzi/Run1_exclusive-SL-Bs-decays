import sys
sys.path.insert(0,'/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/GenLevMC/decfiles/getDecFiles/')
from  dictBKpath import *

def f_getNbk(tag, modes, years, pythias, pols):
    Nbk = 0
    for mode in modes:
        for year in years:
            for pythia in  pythias:
                for pol in pols:
                    Nbk+= f_readNbk(mode,year, pythia,pol)
    return Nbk


def f_readNbk(mode, year, pythia, pol):
    POL = pol 
    if pol == 'Dw' : POL = 'Down'
    fin = open('/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/eff0/path_NBK.txt','r').read()
    lines = fin.split('\n')[:-1]
    lines_path_Nbk = []
    for line in lines:
        lines_path_Nbk.append( line.split(' ') )
    for line_path_Nbk in lines_path_Nbk:
        path = line_path_Nbk[0]
        Nbk  = line_path_Nbk[1]
        if path == paths[year][POL][mode][pythia]['Unfiltered']:
            print mode, year, pythia, pol, 'Nbk = ', Nbk
            return int(Nbk)
