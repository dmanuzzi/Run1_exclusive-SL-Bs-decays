import ROOT
import os

ROOT.gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/eff0/selection/doTM.cpp+')

doTM = __import__('ROOT.doTM', globals(),locals(),['doTM'])

pathIN = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/GenLevMC/output/'
pols = ['Up', 'Down']
#pols = ['Up']
pythias = ['Pythia6Unfiltered', 'Pythia8Unfiltered']
#pythias = ['Pythia8Unfiltered']
modes_years = [
    ('BdKpipi', '2011', [1,2]),
    ('BdKpipi', '2012', [1,2]),
#    ('BuKpipi', '2011'),
#    ('BuKpipi', '2012'),
    ('BdKKpi', '2012', [1,2]),
    ('BsSignal', '2012', [1,2])
              ]
#GenLevCuts = ['none', 'ListOfDaughtersInLHCb']
GenLevCuts  =['none']
nfin = pathIN+'/MC_GenLev_20190306/{year}_{mode}{cat}_{pythia}_Mag{pol}_{GenLevCut}/%d/myMC-{mode}{pol}.root'
nfout= './TruthMatched_20190306/{year}_{mode}{cat}_{pythia}_Mag{pol}_{GenLevCut}_TM.root'
for mode,year,cats in modes_years:
    for GenLevCut in GenLevCuts:
        for pythia in pythias:
            for pol in pols:
                for cat in cats:
                    tmp_nfin = nfin.format(year=year,mode=mode,cat=cat,pythia=pythia,pol=pol,GenLevCut=GenLevCut)
                    tmp_nfout=nfout.format(year=year,mode=mode,cat=cat,pythia=pythia,pol=pol,GenLevCut=GenLevCut)
                    nsubjobs=250
                    doTM(tmp_nfin, 'DMutuple/MCDecayTree', tmp_nfout, 'MCDecayTree',nsubjobs, cat)



'''
for cat in cats:
    os.system('hadd ./TruthMatched_20190221/MC-BdKpipi%d-onlyTM.root %s %s %s %s %s %s %s %s'%(cat,
            nfout.format(year='2011', mode='BdKpipi',pythia='Pythia6Unfiltered',pol='Up'  ,GenLevCut='none',cat=cat),
            nfout.format(year='2011', mode='BdKpipi',pythia='Pythia6Unfiltered',pol='Down',GenLevCut='none',cat=cat),
            nfout.format(year='2011', mode='BdKpipi',pythia='Pythia8Unfiltered',pol='Up'  ,GenLevCut='none',cat=cat),
            nfout.format(year='2011', mode='BdKpipi',pythia='Pythia8Unfiltered',pol='Down',GenLevCut='none',cat=cat),
            nfout.format(year='2012', mode='BdKpipi',pythia='Pythia6Unfiltered',pol='Up'  ,GenLevCut='none',cat=cat),
            nfout.format(year='2012', mode='BdKpipi',pythia='Pythia6Unfiltered',pol='Down',GenLevCut='none',cat=cat),
            nfout.format(year='2012', mode='BdKpipi',pythia='Pythia8Unfiltered',pol='Up'  ,GenLevCut='none',cat=cat),
            nfout.format(year='2012', mode='BdKpipi',pythia='Pythia8Unfiltered',pol='Down',GenLevCut='none',cat=cat)
        ))
'''
'''
os.system('hadd ./TruthMatched_20190221/MC-BdKKpi-onlyTM.root %s %s %s %s'%(
        nfout.format(year='2012', mode='BdKKpi',pythia='Pythia6Unfiltered',pol='Up'  ,GenLevCut='none'),
        nfout.format(year='2012', mode='BdKKpi',pythia='Pythia6Unfiltered',pol='Down',GenLevCut='none'),
        nfout.format(year='2012', mode='BdKKpi',pythia='Pythia8Unfiltered',pol='Up'  ,GenLevCut='none'),
        nfout.format(year='2012', mode='BdKKpi',pythia='Pythia8Unfiltered',pol='Down',GenLevCut='none')
        ))
os.system('hadd ./TruthMatched_20190221/MC-BsSignal-onlyTM.root %s %s %s %s'%(
        nfout.format(year='2012', mode='BsSignal',pythia='Pythia6Unfiltered',pol='Up'  ,GenLevCut='none'),
        nfout.format(year='2012', mode='BsSignal',pythia='Pythia6Unfiltered',pol='Down',GenLevCut='none'),
        nfout.format(year='2012', mode='BsSignal',pythia='Pythia8Unfiltered',pol='Up'  ,GenLevCut='none'),
        nfout.format(year='2012', mode='BsSignal',pythia='Pythia8Unfiltered',pol='Down',GenLevCut='none')
        ))
'''
