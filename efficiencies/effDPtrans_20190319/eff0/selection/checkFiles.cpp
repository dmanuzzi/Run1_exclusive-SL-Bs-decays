#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include <fstream>
#include "TString.h"
int checkFiles(){
  TString year = "2011";
  TString mode = "BdKpipi";
  TString pythias[2] = {"Pythia6Ufiletered","Pythia8Ufiltered"};
  TString pols[2] = {"Up", "Down"};
  
  TString pathin = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/GenLevMC/output/MC_GenLev_20190204/";
  TString nfin_tmp= pathin+year+"_"+mode+"0_%s_Mag%s_none/%d/myMC-"+mode+"%s.root";
  TString nfout = "./missingMC.txt";
  ofstream pout(nfout, std::ios_base::app);
  pout << endl;
  for (int i_pythia = 0; i_pythia<2;++i_pythia){
    for (int i_pol = 0; i_pol<2; ++i_pol){
      for (int i_job=0; i_job<80; ++i_job){
	TString nfin = TString::Format(nfin_tmp.Data(), pythias[i_pythia].Data(), pols[i_pol].Data(), i_job, pols[i_pol].Data() );
	ifstream s_fin(nfin);
	if (!s_fin.good()){
	    pout << TString::Format("case A) %s %s %s %s %d", year.Data(), mode.Data(), pythias[i_pythia].Data(), pols[i_pol].Data(), i_job) << endl;
	} else {
	  TFile fin(nfin, "READ");
	  TTree *tin = (TTree*)fin.Get("DMutuple/MCDecayTree");
	  int N = tin->GetEntries();
	  if (N<100) 
	    pout << TString::Format("case B) %s %s %s %s %d", year.Data(), mode.Data(), pythias[i_pythia].Data(), pols[i_pol].Data(), i_job) << endl;
	  else
	    continue;
	} 
      }
    }
  }


  return 1;
}

