#include <iostream>
#include "TString.h"
#include "TChain.h"
#include "TFile.h"
#include "/home/LHCB/dmanuzzi/Bs2DsBrRatio/exclusive-SL-Bs-decays/Run1/rewFF/FormFactorsRew.C"
#include "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tools/Tools.h"
#include "TVector3.h"
#include <fstream>
#include <vector>
#include "TLorentzVector.h"
using namespace std;


int doTM(TString nfin="", TString ntin="DMutuple/MCDecayTree", TString nfout="test.root", TString ntout="MCDecayTree", int Nsubjobs=1, int cat = 0, bool Verbose = true, bool doRew=false){
  int PdgCodeD(0), PdgCodeB(0), PdgCodeDS(0), PdgCodeh1(0),PdgCodeh2(0),PdgCodeh3(0);
  TString nh1(""), nh2(""), nh3("");
  if (nfin.Contains("Kpipi")){
    nh1 = "Pi1", nh2 = "Pi2", nh3 = "K";
    PdgCodeD  = PdgCode::D;
    PdgCodeB  = PdgCode::B0;
    PdgCodeDS = PdgCode::DS;
    PdgCodeh1 = PdgCode::Pi;
    PdgCodeh2 = PdgCode::Pi;
    PdgCodeh3 = PdgCode::K;
  }
  if (nfin.Contains("BdKKpi")){
    nh1 = "K1", nh2 = "K2", nh3 = "Pi";
    PdgCodeD  = PdgCode::D;
    PdgCodeB  = PdgCode::B0;
    PdgCodeDS = PdgCode::DS;
    PdgCodeh1 = PdgCode::K;
    PdgCodeh2 = PdgCode::K;
    PdgCodeh3 = PdgCode::Pi;
  }
  if (nfin.Contains("BsSignal")){
    nh1 = "K1", nh2 = "K2", nh3 = "Pi";
    PdgCodeD  = PdgCode::Ds;
    PdgCodeB  = PdgCode::Bs;
    PdgCodeDS = PdgCode::DsS;
    PdgCodeh1 = PdgCode::K;
    PdgCodeh2 = PdgCode::K;
    PdgCodeh3 = PdgCode::Pi;  
  }

  cout << "//////////////////////////////////////////////////////////" << endl;
  cout << nfin << endl;
  if (Verbose)
    cout << "setup: " << nh1 << "   " << nh2 << "   " << nh3 << "   " << PdgCodeD << "   " << PdgCodeB << "   " << PdgCodeDS << endl; 
  TChain *tin = new TChain(ntin);
  for (int j=0; j<Nsubjobs; ++j){
    TString nfin_tmp = TString::Format(nfin, j); 
    ifstream fin_tmp(nfin_tmp);
    if (fin_tmp.good())
      tin->Add(nfin_tmp);
    else {
      cout << nfin_tmp+" can not be added to the TChain: let's skip it" << endl;
      continue;
    }
  }
  //  tin->Print();
  /*
  FormFactorsRew *rew = new FormFactorsRew();
  if (cat==1){
    vector<double> parCurrent;
    parCurrent.push_back(1.128);
    //    parCurrent.push_back(1.074);
    rew->SetCurrentModel("CLN", parCurrent, false, false);
    vector<double> parNew;
    parNew.push_back(1.18);
    //    parNew.push_back(1.074);
    rew->SetNewModel("CLN", parNew);
  }
  */
  int signalCategory(0);
  int h1_ID(0), h2_ID(0), h3_ID(0), D_ID(0), Mu_ID(0), Nu_mu_ID(0), B_ID(0); 
  int D_MC_MOTHER_KEY(0), Mu_MC_MOTHER_KEY(0), Nu_mu_MC_MOTHER_KEY(0), D_MC_GD_MOTHER_KEY(0);
  int h1_MC_MOTHER_ID(0), h2_MC_MOTHER_ID(0), h3_MC_MOTHER_ID(0), D_MC_MOTHER_ID(0), Mu_MC_MOTHER_ID(0), Nu_mu_MC_MOTHER_ID(0);
  int D_MC_GD_MOTHER_ID(0), Mu_MC_GD_MOTHER_ID(0), Nu_mu_MC_GD_MOTHER_ID(0);;
  Double_t B_TRUEORIGINVERTEX_X(0.), B_TRUEORIGINVERTEX_Y(0.), B_TRUEORIGINVERTEX_Z(0.),B_TRUEENDVERTEX_X(0.),B_TRUEENDVERTEX_Y(0.),B_TRUEENDVERTEX_Z(0.);
  Double_t D_TRUEP_X(0.),D_TRUEP_Y(0.),D_TRUEP_Z(0.),D_TRUEP_E(0.);
  Double_t B_TRUEP_X(0.),B_TRUEP_Y(0.),B_TRUEP_Z(0.),B_TRUEP_E(0.);
  Double_t w_FF(1.);

  tin->SetBranchStatus("*", 1);
  tin->SetBranchAddress(              "B_ID",               &B_ID);
  tin->SetBranchAddress(           nh1+"_ID",              &h1_ID);
  tin->SetBranchAddress(           nh2+"_ID",              &h2_ID);
  tin->SetBranchAddress(           nh3+"_ID",              &h3_ID);
  tin->SetBranchAddress(              "D_ID",               &D_ID);
  tin->SetBranchAddress(             "Mu_ID",              &Mu_ID);
  tin->SetBranchAddress(          "Nu_mu_ID",           &Nu_mu_ID);
  tin->SetBranchAddress( nh1+"_MC_MOTHER_ID",    &h1_MC_MOTHER_ID);
  tin->SetBranchAddress( nh2+"_MC_MOTHER_ID",    &h2_MC_MOTHER_ID);
  tin->SetBranchAddress( nh3+"_MC_MOTHER_ID",    &h3_MC_MOTHER_ID);
  tin->SetBranchAddress(    "D_MC_MOTHER_ID",     &D_MC_MOTHER_ID);
  tin->SetBranchAddress(   "D_MC_MOTHER_KEY",    &D_MC_MOTHER_KEY);
  tin->SetBranchAddress(   "Mu_MC_MOTHER_ID",    &Mu_MC_MOTHER_ID);
  tin->SetBranchAddress(  "Mu_MC_MOTHER_KEY",   &Mu_MC_MOTHER_KEY);
  tin->SetBranchAddress("Nu_mu_MC_MOTHER_ID", &Nu_mu_MC_MOTHER_ID);
  tin->SetBranchAddress("Nu_mu_MC_MOTHER_KEY",&Nu_mu_MC_MOTHER_KEY);
  tin->SetBranchAddress( "D_MC_GD_MOTHER_ID",  &D_MC_GD_MOTHER_ID);
  tin->SetBranchAddress("D_MC_GD_MOTHER_KEY", &D_MC_GD_MOTHER_KEY);
  tin->SetBranchAddress("Mu_MC_GD_MOTHER_ID", &Mu_MC_GD_MOTHER_ID);
  tin->SetBranchAddress("Nu_mu_MC_GD_MOTHER_ID", &Nu_mu_MC_GD_MOTHER_ID);
  tin->SetBranchAddress("B_TRUEORIGINVERTEX_X", &B_TRUEORIGINVERTEX_X);
  tin->SetBranchAddress("B_TRUEORIGINVERTEX_Y", &B_TRUEORIGINVERTEX_Y);
  tin->SetBranchAddress("B_TRUEORIGINVERTEX_Z", &B_TRUEORIGINVERTEX_Z);
  tin->SetBranchAddress("B_TRUEENDVERTEX_X", &B_TRUEENDVERTEX_X);
  tin->SetBranchAddress("B_TRUEENDVERTEX_Y", &B_TRUEENDVERTEX_Y);
  tin->SetBranchAddress("B_TRUEENDVERTEX_Z", &B_TRUEENDVERTEX_Z);
  tin->SetBranchAddress("D_TRUEP_X", &D_TRUEP_X);
  tin->SetBranchAddress("D_TRUEP_Y", &D_TRUEP_Y);
  tin->SetBranchAddress("D_TRUEP_Z", &D_TRUEP_Z);
  tin->SetBranchAddress("D_TRUEP_E", &D_TRUEP_E);
  tin->SetBranchAddress("B_TRUEP_X", &B_TRUEP_X);
  tin->SetBranchAddress("B_TRUEP_Y", &B_TRUEP_Y);
  tin->SetBranchAddress("B_TRUEP_Z", &B_TRUEP_Z);
  tin->SetBranchAddress("B_TRUEP_E", &B_TRUEP_E);


  TFile *fout = new TFile(nfout, "RECREATE");
  TTree *tout = tin->CloneTree(0);
  tout->SetName(ntout);
  Double_t D_Ptrans(0.);
  tout->Branch("D_Ptrans", &D_Ptrans, "D_Ptrans/D");
  tout->Branch("signalCategory", &signalCategory, "signalCategory/I");
  //tout->Branch("w_FF", &w_FF, "w_FF/D");
  const int nentries = tin->GetEntries();
  int i = 0;
  for (i=0; i<nentries; ++i){
    //if (i>10) break;
    tin->GetEntry(i);
    if (i%1000==0){
      cout << "\r at entry " << i << " of " << nentries;
      cout.flush();
    }
    // check the muon
    if (abs(Mu_ID)   != PdgCode::Mu)   { if (Verbose) cout << " Wrong Mu_ID = " << Mu_ID << endl;       continue;}
    if (abs(Nu_mu_ID)!= PdgCode::NuMu) { if (Verbose) cout << " Wrong Nu_Mu_ID = " << Nu_mu_ID << endl; continue;}

    // check that the D is correctly reconstructed
    if (abs(D_ID)   != PdgCodeD)  { if (Verbose) cout << " Wrong D_ID = " << D_ID << "\n";        continue;}
    if (abs(h1_ID)  != PdgCodeh1) { if (Verbose) cout << " Wrong "+nh1+"_ID = " << h1_ID << "\n"; continue;}
    if (abs(h2_ID)  != PdgCodeh2) { if (Verbose) cout << " Wrong "+nh2+"_ID = " << h2_ID << "\n"; continue;}
    if (abs(h3_ID)  != PdgCodeh3) { if (Verbose) cout << " Wrong "+nh3+"_ID = " << h3_ID << "\n"; continue;}
    if (h1_MC_MOTHER_ID != D_ID)  { if (Verbose) cout << " Wrong "+nh1+"_MC_MOTHER_ID = " << h1_MC_MOTHER_ID << "\n"; continue;}
    if (h2_MC_MOTHER_ID != D_ID)  { if (Verbose) cout << " Wrong "+nh2+"_MC_MOTHER_ID = " << h2_MC_MOTHER_ID << "\n"; continue;}
    if (h3_MC_MOTHER_ID != D_ID)  { if (Verbose) cout << " Wrong "+nh3+"_MC_MOTHER_ID = " << h3_MC_MOTHER_ID << "\n"; continue;}

    // matching for bkg is completed
    if (nfin.Contains("BuKpipi")) {
      if (abs(B_ID) != PdgCode::B) {cout << "Wrong B_ID== " << B_ID << " instead of "<< PdgCode::B << endl; continue;}
      signalCategory = -1;      
    } 
    // matching for signal
    else if (nfin.Contains("BdKpipi") || nfin.Contains("BdKKpi") || nfin.Contains("BsSignal")) {
      if      (abs(B_ID) != PdgCodeB) {if (Verbose) cout << "Wrong B_ID = " << B_ID << "\n"; continue;}
      if      ((D_MC_MOTHER_ID    == B_ID) && (Mu_MC_MOTHER_ID == B_ID) && D_MC_MOTHER_KEY==Mu_MC_MOTHER_KEY) { signalCategory = 1; }
      else if ((D_MC_GD_MOTHER_ID == B_ID) && (Mu_MC_MOTHER_ID == B_ID) && (abs(D_MC_MOTHER_ID) == PdgCodeDS) && D_MC_GD_MOTHER_KEY==Mu_MC_MOTHER_KEY){ signalCategory = 2; }
      else if (Mu_MC_MOTHER_ID    == B_ID)                                                                    { signalCategory = 3; }
      else if ((abs(Mu_MC_MOTHER_ID) == PdgCode::Tau) && (Mu_MC_GD_MOTHER_ID == B_ID))                        { signalCategory = 4; }
      else {
	signalCategory = -1;
	}

      if (signalCategory!=cat && cat != 0){
	if (Verbose){
	  cout << "signalCategory == " << signalCategory <<" for entry " << i << endl;
	  /*
	  cout <<  "B_ID "<< B_ID << endl;
	  cout <<  "D_ID "<< D_ID << endl;
	  cout <<  nh1+"_MC_MOTHER_ID "<<    h1_MC_MOTHER_ID << endl;
	  cout <<  nh2+"_MC_MOTHER_ID "<<    h2_MC_MOTHER_ID << endl;
	  cout <<  nh3+"_MC_MOTHER_ID "<<    h3_MC_MOTHER_ID << endl;
	  cout <<     "D_MC_MOTHER_ID "<<     D_MC_MOTHER_ID << endl;
	  cout <<    "Mu_MC_MOTHER_ID "<<    Mu_MC_MOTHER_ID << endl;
	  cout <<  "D_MC_GD_MOTHER_ID "<<  D_MC_GD_MOTHER_ID << endl;
	  cout << "Mu_MC_GD_MOTHER_ID "<< Mu_MC_GD_MOTHER_ID << endl;
	  */
	 }
	continue;
      }
    }
    
    TVector3 B_v(B_TRUEENDVERTEX_X,B_TRUEENDVERTEX_Y,B_TRUEENDVERTEX_Z);
    TVector3 pv(B_TRUEORIGINVERTEX_X,B_TRUEORIGINVERTEX_Y,B_TRUEORIGINVERTEX_Z);
    TVector3 D_p(D_TRUEP_X,D_TRUEP_Y,D_TRUEP_Z);
    TVector3 bf = B_v-pv;
    D_Ptrans = D_p.Perp(bf);
    /*
    if (signalCategory==1 && cat==1 && doRew) {
      TLorentzVector BLab(B_TRUEP_X,B_TRUEP_Y,B_TRUEP_Z,B_TRUEP_E);
      TLorentzVector DLab(D_TRUEP_X,D_TRUEP_Y,D_TRUEP_Z,D_TRUEP_E);
      w_FF = rew->GetWeight(BLab, DLab);
      if (w_FF!=w_FF) {
	std::cout << "w is NaN, this should never happen!" << std::endl;
	continue;
      }
      if (w_FF>2.) continue;//to remove spikes in the reweighted distribution
    } 
    else {
	w_FF = 1.;
    }
    */
    tout->Fill();
  }
  cout << "\r at entry " << i << " of " << nentries << endl;
  cout << "Entries selected: " << tout->GetEntries() << endl;
  cout << endl;
  fout->Write();
  cout << "Output file: " << fout->GetName() << endl;
  fout->Close();

  return 1;
}
