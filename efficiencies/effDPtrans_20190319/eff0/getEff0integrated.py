import os
from math import sqrt

def f_getEff0integrated(mode, year, pythia, POL, pathGen):    
    pol = POL
    if POL == 'Dw': pol = 'Down'
    print '========================================================='
    dirName = '%s_%s0_%sUnfiltered_Mag%s_ListOfDaughtersInLHCb'%(year,mode,pythia,pol)
    dirs = os.popen('ls %s/%s'%(pathGen,dirName)).read()
    Nsjobs = len(dirs.split('\n')[:-1])
    if Nsjobs==0: return 0
#                print ' N sub-jobs        : ',Nsjobs
    num = 0
    den = 0
    for sjob in range(0,Nsjobs):
        log  = open('%s/%s/%d/GeneratorLog.xml'%(pathGen,dirName,sjob)).read()
        lines_log = log.split('\n')[:-1]
        index = -1
        for line_log in lines_log:
            if '<efficiency name = "generator level cut">' in line_log:
                index = lines_log.index(line_log)
        if index == -1:
            print ' ERROR             :  Generetor Level Cut information not found in %s/%s_%s%d_%s%s/Mag%s/%d/GeneratorLog.xml'%(pathGen,year,cat,mode,pythia,filt,pol,sjob)
            continue
        tmp_num = int(lines_log[index+1].split('>')[1].split('<')[0])
        tmp_den = int(lines_log[index+2].split('>')[1].split('<')[0])
        num += tmp_num
        den += tmp_den
        #                   print ' N before          : ',den
        #                   print ' N after           : ',num
        tmp_eff = float(tmp_num)/float(tmp_den)
        tmp_eff_err = sqrt(tmp_eff*(1-tmp_eff)/tmp_den)
        #print '%d eff.              : %.4f \pm %.4f'%(sjob,tmp_eff,tmp_eff_err)
    eff = float(num)/float(den)
    eff_err = sqrt(eff*(1-eff)/den)
    results = { 'num' : num, 'den':den, 'eff': eff, 'eff_err':eff_err}
#    print dirName, results
    return results

'''
pathGen = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/GenLevMC/output/MC_Kpipi'
modes = ['BdKpipi', 'BuKpipi']
years = ['2011', '2012']
pols  = ['Up','Down']
pythias = ['Pythia6', 'Pythia8']
results = {}
for mode in modes:
    results[mode] = {}
    for year in years:
        results[mode][year] = {}
        for pythia in pythias:
            results[mode][year][pythia] = {}
            for pol in pols:
                results[mode][year][pythia][pol] = f_getEff0integrated(mode, year, pythia, pol, pathGen)

            
print '\n\n\n\n\n'
print '================== EFF 0 TABLES ========================='
for mode in results.keys():
    for year in results[mode].keys():
        for pythia in results[mode][year].keys():
            for pol in results[mode][year][pythia].keys():
                Num = results[mode][year][pythia][pol]['num']
                Den = results[mode][year][pythia][pol]['den'] 
                Eff = results[mode][year][pythia][pol]['eff'] 
                Eff_err = results[mode][year][pythia][pol]['eff_err']
                line = '{mode} {year} {pythia} {pol}   & {num} & {den} & ${EFF}$ \\\\'
                print line.format(mode=mode, year=year, pythia=pythia, pol=pol, num=Num, den=Den,EFF='%.4f \pm %.4f'%(Eff, Eff_err))
print '\n\n\n\n\n'
'''
