from getEff0integrated import f_getEff0integrated
from ROOT import TChain, TH1D
import math
import array as arr

def f_getNgenIntegrated(mode, year, pythia, pol, pathGen):
    dict_eff0tot = f_getEff0integrated(mode, year, pythia, pol, pathGen)
    eff0 = dict_eff0tot['eff']
    err_eff0 = dict_eff0tot['eff_err']
    Nbk = f_getNbk(mode, year, pythia, pol)
    print 'eff0', eff0
    print 'err_eff0', err_eff0
    print 'Nbk ', Nbk
    fout = open('/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/results/N_bk.txt', 'w+')
    fout.write('%s %s %s %s %d %f %f \n'%(mode, year, pythia, pol, Nbk, eff0, err_eff0))
    fout.close()

    return Nbk, eff0, err_eff0


def f_getNgenBinned(mode, year, pythia, POL, cats, bins, pathGen, pathTuples):
    pol = POL
    if POL == 'Dw': pol = 'Down'
    Nbk, eff0, err_eff0 = f_getNgenIntegrated(mode, year, pythia, pol, pathGen)
    
    nfin = pathTuples+'/{year}_{mode}0_{pythia}Unfiltered_Mag{pol}_none_TM.root'
    nfin = nfin.format(year=year, mode=mode, pythia=pythia,pol=pol)
    tin  = TChain("MCDecayTree")
    tin.Add(nfin)
    Ntot = tin.GetEntries()
    #print mode, year, pythia, POL,'--->  Ntot =', Ntot
    BINS = arr.array('f', bins)
    Nbins = len(bins)-1
    results = {}
    for cat in cats:
        N_i = []
        nhout = 'h_Ngen_%s_%s_%s_%s_%d'%(mode, year, pythia, pol,cat)
        hout  =TH1D(nhout,nhout, Nbins, BINS)
        N_toPrint =[]
        for i in range(0,Nbins):
            Ntmp = 0
            if cat<=0:
                N = tin.GetEntries('%s < D_Ptrans && D_Ptrans < %s'%(bins[i],bins[i+1]))
            else:
                N = tin.GetEntries('(%s < D_Ptrans && D_Ptrans < %s) && (signalCategory == %d)'%(bins[i],bins[i+1], cat))
                         
            if N==0: N=0.0000000001
            N_toPrint.append(float(N))
            f = float(N)/float(Ntot)
            f_err = math.sqrt(f*(1-f)/float(Ntot))
            Ni = NgenTot*f
            err_Ni = NgenTot*f*math.sqrt(f_err/f*f_err/f+err_NgenTot/NgenTot*err_NgenTot/NgenTot)
            N_i.append( (Ni, err_Ni) )
            hout.SetBinContent(i+1,Ni)
            hout.SetBinError(i+1,err_Ni)
        results[cat] = (N_i, hout, (NgenTot, err_NgenTot))
        print 'cat =',cat, 'Evt Gen per bin = ',N_toPrint
    return results

