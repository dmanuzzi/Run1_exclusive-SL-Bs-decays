'''
This macro calculate eff. as a function of D_Ptrans
from MC selected tuples and save output histograms in a root file
The binning definition is here: 
'''
from ROOT import TChain, gROOT, TH1D, TFile
import os
import math
import array as arr
import sys
sys.path.insert(0,'/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/eff0')
from getNgenBinned import f_getNgenBinned_2

def f_getDenNoCuts_2(mode, year, pythia, pol, cats):
    bins = arr.array('f', [0,800,1100,1400,1700,2000,4500])
    h_den = {}
    for cat in cats:
        h_den[cat] = TH1D('h_den_%s_%d'%(tag,cat),'h_den_%s_%d'%(tag,cat), 6, bins)
    for year in years:
        for mode in modes:
            for pol in pols:
                for pythia in pythias:
                    pathTuples = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/eff0/eff_D_ptrans/selection/TruthMatched'
                    pathGen    = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/GenLevMC/output/MC_Kpipi'
                    tmp_res = f_getNgenBinned_2(mode, year, pythia, pol,  cats, pathGen, pathTuples)
                    for cat in cats:
                        #tmp_res[cat][1].Print()
                        h_den[cat].Add(tmp_res[cat][1])
    tmp_fout = TFile(nfout, 'UPDATE')
    for cat in cats:
        h_den[cat].Write()
    tmp_fout.Close()


ntin  = "DMutuple"
InputPath = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC/'
OutputPath= "./results/"
Tags = ['BdKpipiTest', 'BuKpipiTest']
Polarities = ['Up', 'Dw'] 
#Pythias = ['Pythia6', 'Pythia8', 'Pythia8-ReDecay'] 
Pythias = ['Pythia6', 'Pythia8'] 
#Den_Num = ('noTrigg_noPID_noOffL', 'FinalSelection')
Den_Num = ('noCuts', 'FinalSelection')
Configs = [
    (['2011', '2012'], ['BdKpipi'], Polarities , Pythias, ['Unfiltered'], [0,1,2,3,4], Den_Num, 'DMutuple', InputPath, OutputPath, Tags[0]),
    (['2011', '2012'], ['BuKpipi'], Polarities , Pythias, ['Unfiltered'], [0]        , Den_Num, 'DMutuple', InputPath, OutputPath, Tags[1])
#    (['2011'], ['BdKpipi'], ['Up'] , ['Pythia6'], ['Unfiltered'], [0,1,2,3,4], Den_Num, 'DMutuple', InputPath, OutputPath, Tags[0]),
   ]
#######################################################################################
#######################################################################################
gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/srcEff/getEff_D_Ptrans.cpp++')
getHist    = __import__('ROOT.getHist'   ,globals(),locals(),['getHist'])
createFout = __import__('ROOT.createFout',globals(),locals(),['createFout'])
doEffHist  = __import__('ROOT.doEffHist' ,globals(),locals(),['doEffHist'])
getEfficiencies =  __import__('ROOT.getEfficiencies', globals(), locals(),['getEfficiencies'])
results = {}
for config in Configs:
    print config
    years, modes, pols, pythias, filts, cats, den_num, ntin, inpath, outpath, tag = config
    cutstep_den, cutstep_num = den_num
    arr_cats = arr.array('i', cats)
        
    results[tag] = {}
    tin_num = TChain(ntin)
    tin_den = TChain(ntin)
    ### FILL TChains for eff numerator and denominator
    for year in years:
        for mode in modes:
            for pol in pols:
                for pythia in pythias:
                    for filt in filts:
                        nfin_tmp = '{path}/{cutstep}/{mode}/Mag{pol}/MC-{year}-{mode}{pol}-{cutstep}_{pythia}{filt}_*.root'
                        nfin_num = nfin_tmp.format(path=inpath, cutstep=cutstep_num, mode=mode, pol=pol, year=year, pythia=pythia, filt=filt)
                        tin_num.Add(nfin_num);
                        if cutstep_den != 'noCuts':
                            nfin_den = nfin_tmp.format(path=inpath, cutstep=cutstep_den, mode=mode, pol=pol, year=year, pythia=pythia, filt=filt)
                            tin_den.Add(nfin_den);                        
    nfout = outpath+'/fout_'+tag+'.root'
    createFout(nfout)
    nw = 'none'
    if mode == 'BdKpipi': nw = 'w_FF:corr'
    getHist(tin_num, len(cats), arr_cats,  nfout, 'h_num_'+tag, 'none')
    if cutstep_den != 'noCuts':
        getHist(tin_den, len(cats), arr_cats,  nfout, 'h_den_'+tag,  'none')
    else:
        f_getDenNoCuts_2(mode, year, pythia, pol, cats)

    for cat in cats:
        doEffHist( nfout,  'h_den_%s_%d'%(tag,cat), nfout, 'h_num_%s_%d'%(tag,cat), nfout, 'eff_%s_%d'%(tag,cat));
        leffs = leffs_err = [1, 2, 3, 4, 5, 6]
        effs     = arr.array('d', leffs)
        effs_err = arr.array('d', leffs_err)
        getEfficiencies(nfout, 'eff_%s_%d'%(tag,cat), effs,  effs_err)
        effs = effs.tolist()
        effs_err = effs_err.tolist()
        results[tag][cat] = {'eff' : effs, 'eff_err' : effs_err}

print '\n\n\n\n\n\n'
print results
