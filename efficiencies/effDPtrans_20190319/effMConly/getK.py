'''
This macro calculate eff. as a function of D_Ptrans
from MC selected tuples and save output histograms in a root file
The binning definition is here: 
'''

from ROOT import gROOT, TFile, TChain, TH1D
import os
import array as arr
import sys
sys.path.insert(0,'/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/eff0')
from getNbk           import f_getNbk, f_readNbk
from getEffGenLevCut  import f_getEffGenLevCut, f_readEffGenLevCut_Tabulated
from getEffDPtransCut import f_getEffDPtransCut
import json
gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/srcEff/getEff_D_Ptrans.cpp++')
getHist    = __import__('ROOT.getHist'   ,globals(),locals(),['getHist'])
createFout = __import__('ROOT.createFout',globals(),locals(),['createFout'])

#######################################################################################
#######################################################################################

def f_getK(tag, modes, years, cats = [1,2], bins = [0,800,1100,1400,1700,2000,4500],
           pythias = ['Pythia6', 'Pythia8'], pols = ['Up','Dw'], ntin = 'DMutuple',
           inpath = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC/',
           outpath = './results'):

    print '      tag : ', tag
    print '    modes : ', modes
    print '    years : ', years
    print '     cats : ', cats
    print '  binning : ', bins
    print '  pythias : ', pythias
    print '     pols : ', pols
    print '   inpath : ', inpath

    results = {}
    dictNgen = f_getNgen(tag, modes, years, pythias, pols, cats, bins)
    dictNcut = f_getAllNcut(dictNgen, tag, modes, years, pythias, pols, cats, bins, inpath, outpath)
    nbins = ['bin%d'%(i) for i in range(1, len(bins))]
    nbins += ['tot']

    for cat in cats:
        results[cat] = {}
        for nbin in nbins:
            x = dictNcut[cat][nbin]['N012']
            y = dictNcut[cat][nbin]['N01!2']
            z = dictNcut[cat][nbin]['N0!1']
            u = dictNcut[cat][nbin]['N!0']
            x_err = dictNcut[cat][nbin]['N012_err']
            y_err = dictNcut[cat][nbin]['N01!2_err']
            z_err = dictNcut[cat][nbin]['N0!1_err']
            u_err = dictNcut[cat][nbin]['N!0_err']
            
            K0 = float(x+y+z)/float(x+y+z+u)
            K2 = float(x)/float(x+y)
            K=K0*K2

            dKdx =  float( u*(x**2+2*x*y+y*(y+z))+y*(x+y+z)**2   ) / float( ((x+y)**2)*(u+x+y+z)**2 )
            dKdy = -float( x*(z*(u+z)+x**2+2*x*(y+z)+y**2+2*y*z) ) / float( ((x+y)**2)*(u+x+y+z)**2 )
            dKdz =  float( u*x                                   ) / float( (x+y)*(u+x+y+z)**2 )
            dKdu = -float( x*(x+y+z)                             ) / float( (x+y)*(u+x+y+z)**2 )                          
            K_err = ( (dKdx*x_err)**2+(dKdy*y_err)**2+(dKdz*z_err)**2+(dKdu*u_err)**2 )**(0.5)
            results[cat][nbin] = {'K' : K, 'K_err': K_err}
    return results


#######################################################################################

def f_getNgen(tag, modes, years, pythias, pols, cats, Bins):
    print '=========================================='
    pathTuples = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190318/eff0/selection/TruthMatched_20190306'  
    fstate = 'KKpi'
    if 'Kpipi' in tag: fstate = 'Kpipi'
    pathGen    = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/GenLevMC/output/MC_%s'%(fstate)
    NgenTot = 0
    NgenTot_err = 0
    NbkTot = 0
    effGenLevCut = 0
    err_effGenLevCu = 0
    effGenLevCut_sumW = 0
    for mode in modes:
        for year in years:
            for pythia in pythias:
                for pol in pols:
                    Nbk = f_readNbk(mode, year, pythia, pol)
                    effGenLev, effGenLev_err = f_readEffGenLevCut_Tabulated(mode, year, pythia, pol)
                    NgenTot += float(Nbk)/effGenLev
                    NgenTot_err += ( effGenLev_err*float(Nbk)/(effGenLev**2) )**2
                    NbkTot += Nbk
                    effGenLevCut += effGenLev/(effGenLev_err**2)
                    effGenLevCut_sumW += 1./(effGenLev_err**2)
    NgenTot_err = NgenTot_err**(0.5)
    effGenLevCut = effGenLevCut/effGenLevCut_sumW
    err_effGenLevCut = ( 1./effGenLevCut_sumW )**(0.5)
    dict_effDPtransCut = f_getEffDPtransCut(tag, modes, years, pythias, pols, cats, Bins, pathTuples)

    results = {}
    nbins = ['bin%d'%(i) for i in range(1, len(Bins))]
    nbins += ['tot']
    results  = {}
    for cat in cats:
        results[cat] = {} 
        for nbin in nbins:
            effDPtransCut = dict_effDPtransCut[cat][nbin]['eff']
            err_effDPtransCut = dict_effDPtransCut[cat][nbin]['eff_err']
            Ngen = NgenTot*effDPtransCut
            Ngen_err = Ngen*( (float(NgenTot_err)/float(NgenTot))**2 + (err_effDPtransCut/effDPtransCut)**2 )**(0.5)
            results[cat][nbin] = {'Ngen' : Ngen, 'Ngen_err' : Ngen_err, 
                                  'effGenLevCut' : effGenLevCut, 'effGenLevCut_err': err_effGenLevCut,
                                  'effDPtransCut' : effDPtransCut, 'effDPtransCut_err' : err_effDPtransCut}
            print tag, cat, nbin, 'Nbk = %d'%Nbk, '     effGenLevCut = %.4f \pm %.4f'%(effGenLevCut,err_effGenLevCut), '    effPDtransCut %.4f \pm %.4f'%(dict_effDPtransCut[cat][nbin]['eff'],dict_effDPtransCut[cat][nbin]['eff_err'])
    return results

#######################################################################################

def f_getAllNcut(dictNgen, tag, modes, years, pythias, pols, cats, bins, inpath, outpath):
#    cutsteps = ['noTrigg_noPID_noOffL', 'noTrigg_noPID', 'noTrigg_noPIDmu', 'noTrigg', 'noHLT', 'noHLT2', 'FinalSelection']
    cutsteps = ['noTrigg', 'noHLT2', 'FinalSelection']
    nfout = outpath+'/effMConly-%s.root'%(tag) 
    createFout(nfout)

    yields = {}
    for cutstep in cutsteps:
        yields[cutstep] = f_getNcut(cutstep, tag, modes, years, pythias, pols, cats, bins, inpath, outpath)
    results = {}
    nbins = ['bin%d'%i for i in range(1,len(bins))]
    nbins += ['tot']
    yields['Ngen'] = dictNgen
    for cat in cats:
        results[cat] = {}
        for nbin in nbins:
            N012 = yields['FinalSelection'][cat][nbin]
            N01  = yields['noHLT2'][cat][nbin]
            N0   = yields['noTrigg'][cat][nbin]
            Ngen = dictNgen[cat][nbin]['Ngen']
            Ngen_err = dictNgen[cat][nbin]['Ngen_err']
            N01not2 = N01-N012
            N0not1  = N0-N01
            Nnot0   = Ngen-N0
            Nnot0_err = (N0+Ngen_err**2)**(0.5)
            results[cat][nbin] = { 'N012'  : N012,    'N012_err'  : N012**(0.5),
                                   'N01!2' : N01not2, 'N01!2_err' : N01not2**(0.5),
                                   'N0!1'  : N0not1,  'N0!1_err'  : N0not1**(0.5),
                                   'N!0'   : Nnot0,   'N!0_err'   : Nnot0_err
                                   }

            print tag, cat, nbin, 'N012 %d   N01 %d   N0 %d   Ngen %d \pm %.0f'%(N012, N01, N0, Ngen, Ngen_err)
            foutMConlyYields = open('./yieldsMConly_%s.txt'%(tag), 'w')
            foutMConlyYields.write(json.dumps(yields))
            foutMConlyYields.close()

    return results

#######################################################################################

def f_getNcut(cutstep, tag, modes, years, pythias, pols, cats, bins, inpath, outpath):
    tin = TChain("DMutuple") 
    for year in years:
        for mode in modes:
            for pol in pols:
                for pythia in pythias:
                        nfin_tmp = '{path}/{cutstep}/{mode}/Mag{pol}/MC-{year}-{mode}{pol}-{cutstep}_{pythia}Unfiltered_*.root'
                        nfin = nfin_tmp.format(path=inpath, cutstep=cutstep, mode=mode, pol=pol, year=year, pythia=pythia)
                        tin.Add(nfin);

    arr_cats = arr.array('i', cats)        
    arr_bins = arr.array('f', bins) 
    nfout = outpath+'/effMConly-%s.root'%(tag) 
    getHist(tin, len(bins)-1, arr_bins, len(cats), arr_cats, nfout, 'h_%s_%s'%(tag,cutstep), 'w_FF_corr')
    results = {}
    
    for cat in cats:
        results[cat] = {}
        Ytot = 0
        fin = TFile.Open(nfout, 'READ')
        h = TH1D()            
        fin.GetObject('h_%s_%s_%d'%(tag,cutstep,cat), h)
        for i in range(1,len(bins)):
            nbin = 'bin%d'%(i)
            results[cat][nbin] = h.GetBinContent(i)
            Ytot += h.GetBinContent(i)
        fin.Close()
        results[cat]['tot'] = Ytot
    return results

