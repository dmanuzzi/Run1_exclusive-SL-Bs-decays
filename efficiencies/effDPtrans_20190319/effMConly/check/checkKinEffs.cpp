#include <iostream>
#include "TFile.h"
#include "TString.h"
#include "TChain.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TPad.h"
#include "TF1.h"

using namespace std;


int DOcheckKinEffs(TString mode,   TString cat, double *eff, double *err, TString year="2012"){
  TH1::SetDefaultSumw2();
  TString pathRec = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC/noTrigg_noPID_noOffL/"+mode+"/";
  //    TString pathGen = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/eff0/selection/TruthMatched/";
  //TString pathGen = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/GenLevMC/TruthMatched_20190204/";
  TString pathGen = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/GenLevMC/TruthMatched_20190221/";

  TChain tRec("DMutuple");
  tRec.Add(pathRec+"MagUp/MC-"+year+"-"+mode+"Up-noTrigg_noPID_noOffL_Pythia8Unfiltered_0.root");
  tRec.Add(pathRec+"MagDw/MC-"+year+"-"+mode+"Dw-noTrigg_noPID_noOffL_Pythia8Unfiltered_0.root");
  tRec.Add(pathRec+"MagUp/MC-"+year+"-"+mode+"Up-noTrigg_noPID_noOffL_Pythia6Unfiltered_0.root");
  tRec.Add(pathRec+"MagDw/MC-"+year+"-"+mode+"Dw-noTrigg_noPID_noOffL_Pythia6Unfiltered_0.root");
  TChain tGen("MCDecayTree");
   // tGen.Add(pathGen+year+"_"+mode+"0_Pythia8Unfiltered_MagUp_none_TM.root");
   //  tGen.Add(pathGen+year+"_"+mode+"0_Pythia8Unfiltered_MagDown_none_TM.root");
   //  tGen.Add(pathGen+year+"_"+mode+"0_Pythia6Unfiltered_MagUp_none_TM.root");
   //  tGen.Add(pathGen+year+"_"+mode+"0_Pythia6Unfiltered_MagDown_none_TM.root");
  tGen.Add(pathGen+year+"_"+mode+cat+"_Pythia8Unfiltered_MagUp_none_TM.root");
  tGen.Add(pathGen+year+"_"+mode+cat+"_Pythia8Unfiltered_MagDown_none_TM.root");
  tGen.Add(pathGen+year+"_"+mode+cat+"_Pythia6Unfiltered_MagUp_none_TM.root");
  tGen.Add(pathGen+year+"_"+mode+cat+"_Pythia6Unfiltered_MagDown_none_TM.root");
  cout << "tRec  " << tRec.GetEntries() << endl;
  cout << "tGen  " << tGen.GetEntries() << endl;
    
  double bins[7] = {0.0,800.0,1100.0,1400.0,1700.0,2000.0,4500.0};
  TH1D *hRec = new TH1D("hRec_"+mode+year+"_cat"+cat, "hRec_"+mode+year+"_cat"+cat, 6, bins);
  hRec->GetXaxis()->SetTitle("p_{#perp}(D)  [MeV/c]");
  hRec->SetLineColor(4);
  tRec.Draw("D_Ptrans>>hRec_"+mode+year+"_cat"+cat,"(signalCategory=="+cat+" && D_peak)*w_FF_corr", "goff");
  hRec->Scale(1./hRec->Integral());
  hRec->SetMinimum(0.);
  hRec->SetMaximum(0.26);

  TH1D *hGen = new TH1D("hGen_"+mode+year+"_cat"+cat, "hGen_"+mode+year+"_cat"+cat, 6, bins);
  tGen.Draw("D_Ptrans>>hGen_"+mode+year+"_cat"+cat,"(signalCategory=="+cat+")", "goff");
  //tGen.Draw("D_Ptrans>>hGen_"+mode+year+"_cat"+cat,"(signalCategory=="+cat+")*w_FF", "goff");

  hRec->Print();
  hGen->Print();
  TH1D *hGenEff = new TH1D("hGenEff_"+mode+year+"_cat"+cat, "hGenEff_"+mode+year+"_cat"+cat, 6, bins);
  hGenEff->SetLineColor(8);
  for (int i=1; i<=6; ++i){
    double vbin = hGen->GetBinContent(i);
    double ebin = hGen->GetBinError(i);
    cout << "vbin = " << vbin << "      eff = " << eff[i-1] <<  endl;
    double vbinEff = vbin*eff[i-1];
    hGenEff->SetBinContent(i,vbinEff);
    double ebinEff = vbinEff*TMath::Sqrt(ebin*ebin/(vbin*vbin)+err[i-1]*err[i-1]/(eff[i-1]*eff[i-1]));
    //double ebinEff = TMath::Sqrt(eff[i-1]*(1.-eff[i-1])/vbin);
    hGenEff->SetBinError(i, ebinEff);
  }
  hGenEff->Scale(1./hGenEff->Integral());

  /*  double integral = hGenEff->Integral();    
  for (int i=1; i<=6; ++i){
    double vbinEff = hGenEff->GetBinContent(i);
    double ebinEff = hGenEff->GetBinError(i);
    hGenEff->SetBinContent(i, vbinEff/integral);
    hGenEff->SetBinError(i,ebinEff/integral);
    }*/
  hGenEff->SetMinimum(0.);
  hGenEff->SetMaximum(0.26);
  for (int i=1; i<=6;++i)
    cout << "hGen" << hGenEff->GetBinContent(i) << " || hRec" << hRec->GetBinContent(i) << endl;
  hGenEff->Print();
  TH1D *ratio = new TH1D("hRatio_"+mode+year+"_cat"+cat,"hRatio_"+mode+year+"_cat"+cat,6,bins);
  ratio->Divide(hGenEff,hRec);
  ratio->SetMinimum(0.8);
  ratio->SetMaximum(1.2);
  ratio->SetFillColor(kRed);
  ratio->SetLineColor(kRed);
  
  ratio->SetTitle(" ");
  ratio->SetMarkerColor(kRed);
  ratio->SetMarkerStyle(20);
  ratio->SetMarkerSize(0.5);
  ratio->GetXaxis()->SetTitle("p_{#perp}(D) [MeV/c]");
  ratio->GetYaxis()->SetTitle("Gen*eff/reco");
  ratio->GetYaxis()->SetLabelSize(0.08);
  ratio->GetYaxis()->SetTitleOffset(0.4);
  ratio->GetYaxis()->SetTitleSize(0.1);
  
  
  TF1 *funit = new TF1("funit", "1", 0, 4500);
  double Chi2 = 0;
  for (int i=1; i<=6;++i){
    if (ratio->GetBinError(i)<1.e-12) continue;
    Chi2 = Chi2 + (ratio->GetBinContent(i)-1.)*(ratio->GetBinContent(i)-1.)/(ratio->GetBinError(i)*ratio->GetBinError(i));
  }
  TString nChi2_1;
  nChi2_1.Form("#chi^{2} = %.1f", Chi2);
  cout << "Chi2 = " << Chi2 << endl;

  TString nndof_1;
  nndof_1.Form(",  ndof = %d", 6);
  nChi2_1=nChi2_1+nndof_1;
   
  TCanvas* c1 = new TCanvas("canv_"+mode+year+"_cat"+cat,"canv_"+mode+year+"_cat"+cat, 900,700);
  c1->SetFillColor(kWhite);

  TPad* upperPad = new TPad("upperPad", "upperPad_"+mode+year+"_cat"+cat, .005, .2425, .995, .995);
  TPad* lowerPad = new TPad("lowerPad", "lowerPad_"+mode+year+"_cat"+cat, .005, .005, .995, .2425);
  lowerPad->SetGrid();
  lowerPad->Draw();
  upperPad->Draw();
  
  upperPad->cd();
  hRec->Draw();
  hGenEff->Draw("same");
  
  TLegend *leg = new TLegend(0.68,0.73,0.9,0.9);
  leg->SetFillStyle(0);
  leg->AddEntry(hRec, "MC reco", "lep");
  leg->AddEntry(hGenEff,   "(MC gen)*eff", "lep");
  leg->Draw();
  
  
  TPaveText * tagTEX = new TPaveText(0.68,0.6925,0.9,0.6175,"NDCBR");
  tagTEX->SetFillStyle(0);
  tagTEX->SetTextSize(0.05);
  tagTEX->AddText(mode+year+"_cat"+cat);
  tagTEX->Draw("same");
  

  lowerPad->cd();
  funit->SetLineColor(1);
  ratio->Draw("");
  funit->Draw("same");
  ratio->Draw("same");
  
  TPaveText * title1 = new TPaveText(0.18,0.65,0.65,0.9,"NDCBR");
  title1->SetFillStyle(0);
  title1->SetTextSize(0.2);
  title1->AddText(nChi2_1.Data());
  title1->Draw("same");
  
  c1->SaveAs("./"+TString(c1->GetName())+".pdf");

  /*
  delete hGenEff;
  delete hGen;
  delete hRec;
  delete ratio;
  delete c1;
  */
  return 1;
}


int checkKinEffs(){
   // first MC GenLEv
  double bins0[7] = {0.00758,0.00870,0.01007,0.01122,0.01260,0.01791}; 
  double bins1[7] = {0.00852,0.01018,0.01157,0.01286,0.01447,0.04658}; 
  double bins2[7] = {0.00814,0.00947,0.01081,0.01220,0.01320,0.01885}; 
  double bins3[7] = {0.00936,0.01046,0.01236,0.01413,0.01560,0.04810}; 
  double bins4[7] = {0.00783,0.00940,0.01051,0.01234,0.01359,0.01892}; 
  double bins5[7] = {0.00887,0.01048,0.01222,0.01413,0.01594,0.05361}; 
  double bins6[7] = {0.00783,0.00863,0.00988,0.01131,0.01209,0.01666};
  double bins7[7] = {0.00840,0.00965,0.01111,0.01224,0.01382,0.03093};
 
  double errs0[7] = {0.00009,0.00011,0.00012,0.00012,0.00014,0.00025}; 
  double errs1[7] = {0.00009,0.00013,0.00015,0.00018,0.00027,0.00243}; 
  double errs2[7] = {0.00011,0.00013,0.00013,0.00014,0.00015,0.00027}; 
  double errs3[7] = {0.00010,0.00013,0.00016,0.00021,0.00030,0.00257}; 
  double errs4[7] = {0.00003,0.00004,0.00003,0.00003,0.00004,0.00005}; 
  double errs5[7] = {0.00003,0.00004,0.00004,0.00005,0.00007,0.00037}; 
  double errs6[7] = {0.00005,0.00005,0.00005,0.00005,0.00005,0.00007}; 
  double errs7[7] = {0.00002,0.00003,0.00003,0.00003,0.00005,0.00016}; 
  
  /*
  double bins0[7] = {0.00768,0.00885,0.00997,0.01133,0.01241,0.01765}; 
  double bins1[7] = {0.00858,0.01003,0.01146,0.01306,0.01457,0.04639}; 
  double bins2[7] = {0.00813,0.00943,0.01072,0.01206,0.01352,0.01882}; 
  double bins3[7] = {0.00922,0.01078,0.01234,0.01396,0.01572,0.04774}; 
 
  double errs0[7] = {0.00005,0.00006,0.00006,0.00007,0.0007,0.0012}; 
  double errs1[7] = {0.00005,0.00006,0.00007,0.00009,0.0012,0.0088}; 
  double errs2[7] = {0.00006,0.00007,0.00007,0.00008,0.0009,0.0013}; 
  double errs3[7] = {0.00006,0.00008,0.00009,0.00011,0.0014,0.0093}; 
  */

  DOcheckKinEffs("BdKpipi","1", bins0,errs0);
  DOcheckKinEffs("BdKpipi","2", bins1,errs1);
  DOcheckKinEffs("BdKpipi","1", bins2,errs2, "2011");
  DOcheckKinEffs("BdKpipi","2", bins3,errs3, "2011");
  //  DOcheckKinEffs("BdKKpi", "1", bins4,errs4);
  //  DOcheckKinEffs("BdKKpi", "2", bins5,errs5);
  //  DOcheckKinEffs("BsSignal","1", bins6,errs6);
  //  DOcheckKinEffs("BsSignal","2", bins7,errs7);
  
  return 1;
}
