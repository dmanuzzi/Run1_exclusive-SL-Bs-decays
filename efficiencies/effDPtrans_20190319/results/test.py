import json
import sys
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/CovMatrix')
from getCovMatrix import f_getCovMatrix, f_getRatios
from ROOT import TH1D, TCanvas
import array as arr

finAllVars = open('./AllVars.txt', 'r')
AllVars2 = json.loads(finAllVars.read())
finAllVars.close()
Bins =  [0.0,800.0,1100.0,1400.0,1700.0,2000.0,4500.0]

print 'BdKpipi OVER BsSignal'
ratios_BdKpipi_OVER_BsSignal     = f_getRatios('BdKpipi', 'BsSignal',str(1),str(1), Bins, AllVars2)
print ratios_BdKpipi_OVER_BsSignal

print 'BsSignal OVER BsSignal'
ratios_BsSignal_OVER_BsSignal     = f_getRatios('BsSignal', 'BsSignal',str(1),str(1), Bins, AllVars2)
print ratios_BsSignal_OVER_BsSignal

h = TH1D('KKpi_i/KKpi_i', 'KKpi_i/KKpi_i', 6, 0, 6)
for i in range(0,len(ratios_BdKpipi_OVER_BsSignal)-1):
    print ratios_BdKpipi_OVER_BsSignal[i]/ratios_BsSignal_OVER_BsSignal[i]
    h.SetBinContent(i+1, ratios_BdKpipi_OVER_BsSignal[i]/ratios_BsSignal_OVER_BsSignal[i])
c = TCanvas('c1')
h.Draw()
#cov_matrix_BdKpipi_OVER_BdKKpi = f_getCovMatrix('BdKpipi', 'BdKKpi',str(1),str(1), Bins, AllVars2, AllCovs2)
#for i in range(0, len(cov_matrix_BdKpipi_OVER_BdKKpi)):
#        print cov_matrix_BdKpipi_OVER_BdKKpi[i]
