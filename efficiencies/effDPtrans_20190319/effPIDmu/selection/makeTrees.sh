#!/bin/bash

###############################################
#
# Script for creating TTree from all the
# RooDataSets which contains PIDCalib samples
#
###############################################
 
here=$(pwd)

cd /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/tagANDprobe/DataSets
for fin in *.root; do
    echo $fin
    /home/LHCB/dmanuzzi/LHCb_package/UraniaDev_v7r0/run python /home/LHCB-T3/dmanuzzi/LHCb_package/UraniaDev_v7r0/PIDCalib/PIDPerfScripts/scripts/python/Plots/CreateTTreeFromDataset.py $fin ../trees/$fin
done
cd $here