//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue May 15 20:25:42 2018 by ROOT version 5.34/36
// from TTree MyTuple/MyTuple
// found on file: input_tuples/B2JPsiK_NegMuonTag.root
//////////////////////////////////////////////////////////

#ifndef B2JPsiK_selection_h
#define B2JPsiK_selection_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.
   const Int_t kMaxB_ENDVERTEX_COV = 1;
   const Int_t kMaxB_OWNPV_COV = 1;
   const Int_t kMaxJPsi_ENDVERTEX_COV = 1;
   const Int_t kMaxJPsi_OWNPV_COV = 1;
   const Int_t kMaxJPsi_ORIVX_COV = 1;
   const Int_t kMaxmu_tag_OWNPV_COV = 1;
   const Int_t kMaxmu_tag_ORIVX_COV = 1;
   const Int_t kMaxmu_probe_OWNPV_COV = 1;
   const Int_t kMaxmu_probe_ORIVX_COV = 1;
   const Int_t kMaxK_OWNPV_COV = 1;
   const Int_t kMaxK_ORIVX_COV = 1;

class B2JPsiK_selection {
public :
  //---------------------------------------------------------- 
  TString nfout;  
  TString ntag;
  TString nprobe;

  bool passOffLine();
  bool passOnLine();
  bool passTestCuts();
  bool SetChain(TTree *tree, bool plus_is_tag);
  //----------------------------------------------------------
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain
   
  // Declaration of leaf types
   Double_t        B_ENDVERTEX_X;
   Double_t        B_ENDVERTEX_Y;
   Double_t        B_ENDVERTEX_Z;
   Double_t        B_ENDVERTEX_XERR;
   Double_t        B_ENDVERTEX_YERR;
   Double_t        B_ENDVERTEX_ZERR;
   Double_t        B_ENDVERTEX_CHI2;
   Int_t           B_ENDVERTEX_NDOF;
   Float_t         B_ENDVERTEX_COV_[3][3];
   Double_t        B_OWNPV_X;
   Double_t        B_OWNPV_Y;
   Double_t        B_OWNPV_Z;
   Double_t        B_OWNPV_XERR;
   Double_t        B_OWNPV_YERR;
   Double_t        B_OWNPV_ZERR;
   Double_t        B_OWNPV_CHI2;
   Int_t           B_OWNPV_NDOF;
   Float_t         B_OWNPV_COV_[3][3];
   Double_t        B_IP_OWNPV;
   Double_t        B_IPCHI2_OWNPV;
   Double_t        B_FD_OWNPV;
   Double_t        B_FDCHI2_OWNPV;
   Double_t        B_DIRA_OWNPV;
   Double_t        B_P;
   Double_t        B_PT;
   Double_t        B_PE;
   Double_t        B_PX;
   Double_t        B_PY;
   Double_t        B_PZ;
   Double_t        B_MM;
   Double_t        B_MMERR;
   Double_t        B_M;
   Int_t           B_ID;
   Double_t        B_TAU;
   Double_t        B_TAUERR;
   Double_t        B_TAUCHI2;
   Bool_t          B_L0Global_Dec;
   Bool_t          B_L0Global_TIS;
   Bool_t          B_L0Global_TOS;
   Bool_t          B_Hlt1Global_Dec;
   Bool_t          B_Hlt1Global_TIS;
   Bool_t          B_Hlt1Global_TOS;
   Bool_t          B_Hlt1Phys_Dec;
   Bool_t          B_Hlt1Phys_TIS;
   Bool_t          B_Hlt1Phys_TOS;
   Bool_t          B_Hlt2Global_Dec;
   Bool_t          B_Hlt2Global_TIS;
   Bool_t          B_Hlt2Global_TOS;
   Bool_t          B_Hlt2Phys_Dec;
   Bool_t          B_Hlt2Phys_TIS;
   Bool_t          B_Hlt2Phys_TOS;
   Bool_t          B_L0AnyDecision_Dec;
   Bool_t          B_L0AnyDecision_TIS;
   Bool_t          B_L0AnyDecision_TOS;
   Bool_t          B_L0MuonDecision_Dec;
   Bool_t          B_L0MuonDecision_TIS;
   Bool_t          B_L0MuonDecision_TOS;
   Bool_t          B_L0GlobalDecision_Dec;
   Bool_t          B_L0GlobalDecision_TIS;
   Bool_t          B_L0GlobalDecision_TOS;
   Bool_t          B_Hlt1TrackMVADecision_Dec;
   Bool_t          B_Hlt1TrackMVADecision_TIS;
   Bool_t          B_Hlt1TrackMVADecision_TOS;
   Bool_t          B_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          B_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          B_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          B_Hlt1SingleMuonHighPTDecision_Dec;
   Bool_t          B_Hlt1SingleMuonHighPTDecision_TIS;
   Bool_t          B_Hlt1SingleMuonHighPTDecision_TOS;
   Bool_t          B_Hlt1TrackAllL0Decision_Dec;
   Bool_t          B_Hlt1TrackAllL0Decision_TIS;
   Bool_t          B_Hlt1TrackAllL0Decision_TOS;
   Bool_t          B_Hlt1TrackMuonDecision_Dec;
   Bool_t          B_Hlt1TrackMuonDecision_TIS;
   Bool_t          B_Hlt1TrackMuonDecision_TOS;
   Bool_t          B_Hlt1TrackMuonMVADecision_Dec;
   Bool_t          B_Hlt1TrackMuonMVADecision_TIS;
   Bool_t          B_Hlt1TrackMuonMVADecision_TOS;
   Bool_t          B_Hlt1GlobalDecision_Dec;
   Bool_t          B_Hlt1GlobalDecision_TIS;
   Bool_t          B_Hlt1GlobalDecision_TOS;
   Bool_t          B_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          B_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          B_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          B_Hlt1DiMuonLowMassDecision_Dec;
   Bool_t          B_Hlt1DiMuonLowMassDecision_TIS;
   Bool_t          B_Hlt1DiMuonLowMassDecision_TOS;
   Bool_t          B_Hlt1SingleMuonNoIPDecision_Dec;
   Bool_t          B_Hlt1SingleMuonNoIPDecision_TIS;
   Bool_t          B_Hlt1SingleMuonNoIPDecision_TOS;
   Bool_t          B_Hlt2Topo2BodyDecision_Dec;
   Bool_t          B_Hlt2Topo2BodyDecision_TIS;
   Bool_t          B_Hlt2Topo2BodyDecision_TOS;
   Bool_t          B_Hlt2Topo3BodyDecision_Dec;
   Bool_t          B_Hlt2Topo3BodyDecision_TIS;
   Bool_t          B_Hlt2Topo3BodyDecision_TOS;
   Bool_t          B_Hlt2Topo4BodyDecision_Dec;
   Bool_t          B_Hlt2Topo4BodyDecision_TIS;
   Bool_t          B_Hlt2Topo4BodyDecision_TOS;
   Bool_t          B_Hlt2TopoMu2BodyDecision_Dec;
   Bool_t          B_Hlt2TopoMu2BodyDecision_TIS;
   Bool_t          B_Hlt2TopoMu2BodyDecision_TOS;
   Bool_t          B_Hlt2TopoMu3BodyDecision_Dec;
   Bool_t          B_Hlt2TopoMu3BodyDecision_TIS;
   Bool_t          B_Hlt2TopoMu3BodyDecision_TOS;
   Bool_t          B_Hlt2TopoMu4BodyDecision_Dec;
   Bool_t          B_Hlt2TopoMu4BodyDecision_TIS;
   Bool_t          B_Hlt2TopoMu4BodyDecision_TOS;
   Bool_t          B_Hlt2GlobalDecision_Dec;
   Bool_t          B_Hlt2GlobalDecision_TIS;
   Bool_t          B_Hlt2GlobalDecision_TOS;
   Bool_t          B_Hlt2SingleMuonDecision_Dec;
   Bool_t          B_Hlt2SingleMuonDecision_TIS;
   Bool_t          B_Hlt2SingleMuonDecision_TOS;
   Double_t        JPsi_CosTheta;
   Double_t        JPsi_ENDVERTEX_X;
   Double_t        JPsi_ENDVERTEX_Y;
   Double_t        JPsi_ENDVERTEX_Z;
   Double_t        JPsi_ENDVERTEX_XERR;
   Double_t        JPsi_ENDVERTEX_YERR;
   Double_t        JPsi_ENDVERTEX_ZERR;
   Double_t        JPsi_ENDVERTEX_CHI2;
   Int_t           JPsi_ENDVERTEX_NDOF;
   Float_t         JPsi_ENDVERTEX_COV_[3][3];
   Double_t        JPsi_OWNPV_X;
   Double_t        JPsi_OWNPV_Y;
   Double_t        JPsi_OWNPV_Z;
   Double_t        JPsi_OWNPV_XERR;
   Double_t        JPsi_OWNPV_YERR;
   Double_t        JPsi_OWNPV_ZERR;
   Double_t        JPsi_OWNPV_CHI2;
   Int_t           JPsi_OWNPV_NDOF;
   Float_t         JPsi_OWNPV_COV_[3][3];
   Double_t        JPsi_IP_OWNPV;
   Double_t        JPsi_IPCHI2_OWNPV;
   Double_t        JPsi_FD_OWNPV;
   Double_t        JPsi_FDCHI2_OWNPV;
   Double_t        JPsi_DIRA_OWNPV;
   Double_t        JPsi_ORIVX_X;
   Double_t        JPsi_ORIVX_Y;
   Double_t        JPsi_ORIVX_Z;
   Double_t        JPsi_ORIVX_XERR;
   Double_t        JPsi_ORIVX_YERR;
   Double_t        JPsi_ORIVX_ZERR;
   Double_t        JPsi_ORIVX_CHI2;
   Int_t           JPsi_ORIVX_NDOF;
   Float_t         JPsi_ORIVX_COV_[3][3];
   Double_t        JPsi_FD_ORIVX;
   Double_t        JPsi_FDCHI2_ORIVX;
   Double_t        JPsi_DIRA_ORIVX;
   Double_t        JPsi_P;
   Double_t        JPsi_PT;
   Double_t        JPsi_PE;
   Double_t        JPsi_PX;
   Double_t        JPsi_PY;
   Double_t        JPsi_PZ;
   Double_t        JPsi_MM;
   Double_t        JPsi_MMERR;
   Double_t        JPsi_M;
   Int_t           JPsi_ID;
   Double_t        JPsi_TAU;
   Double_t        JPsi_TAUERR;
   Double_t        JPsi_TAUCHI2;
   Bool_t          JPsi_L0Global_Dec;
   Bool_t          JPsi_L0Global_TIS;
   Bool_t          JPsi_L0Global_TOS;
   Bool_t          JPsi_Hlt1Global_Dec;
   Bool_t          JPsi_Hlt1Global_TIS;
   Bool_t          JPsi_Hlt1Global_TOS;
   Bool_t          JPsi_Hlt1Phys_Dec;
   Bool_t          JPsi_Hlt1Phys_TIS;
   Bool_t          JPsi_Hlt1Phys_TOS;
   Bool_t          JPsi_Hlt2Global_Dec;
   Bool_t          JPsi_Hlt2Global_TIS;
   Bool_t          JPsi_Hlt2Global_TOS;
   Bool_t          JPsi_Hlt2Phys_Dec;
   Bool_t          JPsi_Hlt2Phys_TIS;
   Bool_t          JPsi_Hlt2Phys_TOS;
   Bool_t          JPsi_L0AnyDecision_Dec;
   Bool_t          JPsi_L0AnyDecision_TIS;
   Bool_t          JPsi_L0AnyDecision_TOS;
   Bool_t          JPsi_L0MuonDecision_Dec;
   Bool_t          JPsi_L0MuonDecision_TIS;
   Bool_t          JPsi_L0MuonDecision_TOS;
   Bool_t          JPsi_L0GlobalDecision_Dec;
   Bool_t          JPsi_L0GlobalDecision_TIS;
   Bool_t          JPsi_L0GlobalDecision_TOS;
   Bool_t          JPsi_Hlt1TrackMVADecision_Dec;
   Bool_t          JPsi_Hlt1TrackMVADecision_TIS;
   Bool_t          JPsi_Hlt1TrackMVADecision_TOS;
   Bool_t          JPsi_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          JPsi_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          JPsi_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          JPsi_Hlt1SingleMuonHighPTDecision_Dec;
   Bool_t          JPsi_Hlt1SingleMuonHighPTDecision_TIS;
   Bool_t          JPsi_Hlt1SingleMuonHighPTDecision_TOS;
   Bool_t          JPsi_Hlt1TrackAllL0Decision_Dec;
   Bool_t          JPsi_Hlt1TrackAllL0Decision_TIS;
   Bool_t          JPsi_Hlt1TrackAllL0Decision_TOS;
   Bool_t          JPsi_Hlt1TrackMuonDecision_Dec;
   Bool_t          JPsi_Hlt1TrackMuonDecision_TIS;
   Bool_t          JPsi_Hlt1TrackMuonDecision_TOS;
   Bool_t          JPsi_Hlt1TrackMuonMVADecision_Dec;
   Bool_t          JPsi_Hlt1TrackMuonMVADecision_TIS;
   Bool_t          JPsi_Hlt1TrackMuonMVADecision_TOS;
   Bool_t          JPsi_Hlt1GlobalDecision_Dec;
   Bool_t          JPsi_Hlt1GlobalDecision_TIS;
   Bool_t          JPsi_Hlt1GlobalDecision_TOS;
   Bool_t          JPsi_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          JPsi_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          JPsi_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          JPsi_Hlt1DiMuonLowMassDecision_Dec;
   Bool_t          JPsi_Hlt1DiMuonLowMassDecision_TIS;
   Bool_t          JPsi_Hlt1DiMuonLowMassDecision_TOS;
   Bool_t          JPsi_Hlt1SingleMuonNoIPDecision_Dec;
   Bool_t          JPsi_Hlt1SingleMuonNoIPDecision_TIS;
   Bool_t          JPsi_Hlt1SingleMuonNoIPDecision_TOS;
   Bool_t          JPsi_Hlt2Topo2BodyDecision_Dec;
   Bool_t          JPsi_Hlt2Topo2BodyDecision_TIS;
   Bool_t          JPsi_Hlt2Topo2BodyDecision_TOS;
   Bool_t          JPsi_Hlt2Topo3BodyDecision_Dec;
   Bool_t          JPsi_Hlt2Topo3BodyDecision_TIS;
   Bool_t          JPsi_Hlt2Topo3BodyDecision_TOS;
   Bool_t          JPsi_Hlt2Topo4BodyDecision_Dec;
   Bool_t          JPsi_Hlt2Topo4BodyDecision_TIS;
   Bool_t          JPsi_Hlt2Topo4BodyDecision_TOS;
   Bool_t          JPsi_Hlt2TopoMu2BodyDecision_Dec;
   Bool_t          JPsi_Hlt2TopoMu2BodyDecision_TIS;
   Bool_t          JPsi_Hlt2TopoMu2BodyDecision_TOS;
   Bool_t          JPsi_Hlt2TopoMu3BodyDecision_Dec;
   Bool_t          JPsi_Hlt2TopoMu3BodyDecision_TIS;
   Bool_t          JPsi_Hlt2TopoMu3BodyDecision_TOS;
   Bool_t          JPsi_Hlt2TopoMu4BodyDecision_Dec;
   Bool_t          JPsi_Hlt2TopoMu4BodyDecision_TIS;
   Bool_t          JPsi_Hlt2TopoMu4BodyDecision_TOS;
   Bool_t          JPsi_Hlt2GlobalDecision_Dec;
   Bool_t          JPsi_Hlt2GlobalDecision_TIS;
   Bool_t          JPsi_Hlt2GlobalDecision_TOS;
   Bool_t          JPsi_Hlt2SingleMuonDecision_Dec;
   Bool_t          JPsi_Hlt2SingleMuonDecision_TIS;
   Bool_t          JPsi_Hlt2SingleMuonDecision_TOS;
   Double_t        mu_tag_CosTheta;
   Double_t        mu_tag_OWNPV_X;
   Double_t        mu_tag_OWNPV_Y;
   Double_t        mu_tag_OWNPV_Z;
   Double_t        mu_tag_OWNPV_XERR;
   Double_t        mu_tag_OWNPV_YERR;
   Double_t        mu_tag_OWNPV_ZERR;
   Double_t        mu_tag_OWNPV_CHI2;
   Int_t           mu_tag_OWNPV_NDOF;
   Float_t         mu_tag_OWNPV_COV_[3][3];
   Double_t        mu_tag_IP_OWNPV;
   Double_t        mu_tag_IPCHI2_OWNPV;
   Double_t        mu_tag_ORIVX_X;
   Double_t        mu_tag_ORIVX_Y;
   Double_t        mu_tag_ORIVX_Z;
   Double_t        mu_tag_ORIVX_XERR;
   Double_t        mu_tag_ORIVX_YERR;
   Double_t        mu_tag_ORIVX_ZERR;
   Double_t        mu_tag_ORIVX_CHI2;
   Int_t           mu_tag_ORIVX_NDOF;
   Float_t         mu_tag_ORIVX_COV_[3][3];
   Double_t        mu_tag_P;
   Double_t        mu_tag_PT;
   Double_t        mu_tag_PE;
   Double_t        mu_tag_PX;
   Double_t        mu_tag_PY;
   Double_t        mu_tag_PZ;
   Double_t        mu_tag_M;
   Int_t           mu_tag_ID;
   Double_t        mu_tag_PIDe;
   Double_t        mu_tag_PIDmu;
   Double_t        mu_tag_PIDK;
   Double_t        mu_tag_PIDp;
   Double_t        mu_tag_ProbNNe;
   Double_t        mu_tag_ProbNNk;
   Double_t        mu_tag_ProbNNp;
   Double_t        mu_tag_ProbNNpi;
   Double_t        mu_tag_ProbNNmu;
   Double_t        mu_tag_ProbNNghost;
   Bool_t          mu_tag_hasMuon;
   Bool_t          mu_tag_isMuon;
   Bool_t          mu_tag_hasRich;
   Bool_t          mu_tag_hasCalo;
   Bool_t          mu_tag_L0Global_Dec;
   Bool_t          mu_tag_L0Global_TIS;
   Bool_t          mu_tag_L0Global_TOS;
   Bool_t          mu_tag_Hlt1Global_Dec;
   Bool_t          mu_tag_Hlt1Global_TIS;
   Bool_t          mu_tag_Hlt1Global_TOS;
   Bool_t          mu_tag_Hlt1Phys_Dec;
   Bool_t          mu_tag_Hlt1Phys_TIS;
   Bool_t          mu_tag_Hlt1Phys_TOS;
   Bool_t          mu_tag_Hlt2Global_Dec;
   Bool_t          mu_tag_Hlt2Global_TIS;
   Bool_t          mu_tag_Hlt2Global_TOS;
   Bool_t          mu_tag_Hlt2Phys_Dec;
   Bool_t          mu_tag_Hlt2Phys_TIS;
   Bool_t          mu_tag_Hlt2Phys_TOS;
   Bool_t          mu_tag_L0AnyDecision_Dec;
   Bool_t          mu_tag_L0AnyDecision_TIS;
   Bool_t          mu_tag_L0AnyDecision_TOS;
   Bool_t          mu_tag_L0MuonDecision_Dec;
   Bool_t          mu_tag_L0MuonDecision_TIS;
   Bool_t          mu_tag_L0MuonDecision_TOS;
   Bool_t          mu_tag_L0GlobalDecision_Dec;
   Bool_t          mu_tag_L0GlobalDecision_TIS;
   Bool_t          mu_tag_L0GlobalDecision_TOS;
   Bool_t          mu_tag_Hlt1TrackMVADecision_Dec;
   Bool_t          mu_tag_Hlt1TrackMVADecision_TIS;
   Bool_t          mu_tag_Hlt1TrackMVADecision_TOS;
   Bool_t          mu_tag_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          mu_tag_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          mu_tag_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          mu_tag_Hlt1SingleMuonHighPTDecision_Dec;
   Bool_t          mu_tag_Hlt1SingleMuonHighPTDecision_TIS;
   Bool_t          mu_tag_Hlt1SingleMuonHighPTDecision_TOS;
   Bool_t          mu_tag_Hlt1TrackAllL0Decision_Dec;
   Bool_t          mu_tag_Hlt1TrackAllL0Decision_TIS;
   Bool_t          mu_tag_Hlt1TrackAllL0Decision_TOS;
   Bool_t          mu_tag_Hlt1TrackMuonDecision_Dec;
   Bool_t          mu_tag_Hlt1TrackMuonDecision_TIS;
   Bool_t          mu_tag_Hlt1TrackMuonDecision_TOS;
   Bool_t          mu_tag_Hlt1TrackMuonMVADecision_Dec;
   Bool_t          mu_tag_Hlt1TrackMuonMVADecision_TIS;
   Bool_t          mu_tag_Hlt1TrackMuonMVADecision_TOS;
   Bool_t          mu_tag_Hlt1GlobalDecision_Dec;
   Bool_t          mu_tag_Hlt1GlobalDecision_TIS;
   Bool_t          mu_tag_Hlt1GlobalDecision_TOS;
   Bool_t          mu_tag_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          mu_tag_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          mu_tag_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          mu_tag_Hlt1DiMuonLowMassDecision_Dec;
   Bool_t          mu_tag_Hlt1DiMuonLowMassDecision_TIS;
   Bool_t          mu_tag_Hlt1DiMuonLowMassDecision_TOS;
   Bool_t          mu_tag_Hlt1SingleMuonNoIPDecision_Dec;
   Bool_t          mu_tag_Hlt1SingleMuonNoIPDecision_TIS;
   Bool_t          mu_tag_Hlt1SingleMuonNoIPDecision_TOS;
   Bool_t          mu_tag_Hlt2Topo2BodyDecision_Dec;
   Bool_t          mu_tag_Hlt2Topo2BodyDecision_TIS;
   Bool_t          mu_tag_Hlt2Topo2BodyDecision_TOS;
   Bool_t          mu_tag_Hlt2Topo3BodyDecision_Dec;
   Bool_t          mu_tag_Hlt2Topo3BodyDecision_TIS;
   Bool_t          mu_tag_Hlt2Topo3BodyDecision_TOS;
   Bool_t          mu_tag_Hlt2Topo4BodyDecision_Dec;
   Bool_t          mu_tag_Hlt2Topo4BodyDecision_TIS;
   Bool_t          mu_tag_Hlt2Topo4BodyDecision_TOS;
   Bool_t          mu_tag_Hlt2TopoMu2BodyDecision_Dec;
   Bool_t          mu_tag_Hlt2TopoMu2BodyDecision_TIS;
   Bool_t          mu_tag_Hlt2TopoMu2BodyDecision_TOS;
   Bool_t          mu_tag_Hlt2TopoMu3BodyDecision_Dec;
   Bool_t          mu_tag_Hlt2TopoMu3BodyDecision_TIS;
   Bool_t          mu_tag_Hlt2TopoMu3BodyDecision_TOS;
   Bool_t          mu_tag_Hlt2TopoMu4BodyDecision_Dec;
   Bool_t          mu_tag_Hlt2TopoMu4BodyDecision_TIS;
   Bool_t          mu_tag_Hlt2TopoMu4BodyDecision_TOS;
   Bool_t          mu_tag_Hlt2GlobalDecision_Dec;
   Bool_t          mu_tag_Hlt2GlobalDecision_TIS;
   Bool_t          mu_tag_Hlt2GlobalDecision_TOS;
   Bool_t          mu_tag_Hlt2SingleMuonDecision_Dec;
   Bool_t          mu_tag_Hlt2SingleMuonDecision_TIS;
   Bool_t          mu_tag_Hlt2SingleMuonDecision_TOS;
   Double_t        mu_tag_BPVIPCHI2;
   Double_t        mu_tag_Charge;
   Double_t        mu_tag_DLLK;
   Double_t        mu_tag_DLLe;
   Double_t        mu_tag_DLLmu;
   Double_t        mu_tag_DLLp;
   Double_t        mu_tag_DLLpi;
   Double_t        mu_tag_ETA;
   Double_t        mu_tag_INACCMUON;
   Double_t        mu_tag_PHI;
   Double_t        mu_tag_PID;
   Double_t        mu_tag_TRCHI2DOF;
   Double_t        mu_tag_nShared;
   Int_t           mu_tag_TRACK_Type;
   Int_t           mu_tag_TRACK_Key;
   Double_t        mu_tag_TRACK_CHI2NDOF;
   Double_t        mu_tag_TRACK_PCHI2;
   Double_t        mu_tag_TRACK_MatchCHI2;
   Double_t        mu_tag_TRACK_GhostProb;
   Double_t        mu_tag_TRACK_CloneDist;
   Double_t        mu_tag_TRACK_Likelihood;
   Double_t        mu_probe_CosTheta;
   Double_t        mu_probe_OWNPV_X;
   Double_t        mu_probe_OWNPV_Y;
   Double_t        mu_probe_OWNPV_Z;
   Double_t        mu_probe_OWNPV_XERR;
   Double_t        mu_probe_OWNPV_YERR;
   Double_t        mu_probe_OWNPV_ZERR;
   Double_t        mu_probe_OWNPV_CHI2;
   Int_t           mu_probe_OWNPV_NDOF;
   Float_t         mu_probe_OWNPV_COV_[3][3];
   Double_t        mu_probe_IP_OWNPV;
   Double_t        mu_probe_IPCHI2_OWNPV;
   Double_t        mu_probe_ORIVX_X;
   Double_t        mu_probe_ORIVX_Y;
   Double_t        mu_probe_ORIVX_Z;
   Double_t        mu_probe_ORIVX_XERR;
   Double_t        mu_probe_ORIVX_YERR;
   Double_t        mu_probe_ORIVX_ZERR;
   Double_t        mu_probe_ORIVX_CHI2;
   Int_t           mu_probe_ORIVX_NDOF;
   Float_t         mu_probe_ORIVX_COV_[3][3];
   Double_t        mu_probe_P;
   Double_t        mu_probe_PT;
   Double_t        mu_probe_PE;
   Double_t        mu_probe_PX;
   Double_t        mu_probe_PY;
   Double_t        mu_probe_PZ;
   Double_t        mu_probe_M;
   Int_t           mu_probe_ID;
   Double_t        mu_probe_PIDe;
   Double_t        mu_probe_PIDmu;
   Double_t        mu_probe_PIDK;
   Double_t        mu_probe_PIDp;
   Double_t        mu_probe_ProbNNe;
   Double_t        mu_probe_ProbNNk;
   Double_t        mu_probe_ProbNNp;
   Double_t        mu_probe_ProbNNpi;
   Double_t        mu_probe_ProbNNmu;
   Double_t        mu_probe_ProbNNghost;
   Bool_t          mu_probe_hasMuon;
   Bool_t          mu_probe_isMuon;
   Bool_t          mu_probe_hasRich;
   Bool_t          mu_probe_hasCalo;
   Bool_t          mu_probe_L0Global_Dec;
   Bool_t          mu_probe_L0Global_TIS;
   Bool_t          mu_probe_L0Global_TOS;
   Bool_t          mu_probe_Hlt1Global_Dec;
   Bool_t          mu_probe_Hlt1Global_TIS;
   Bool_t          mu_probe_Hlt1Global_TOS;
   Bool_t          mu_probe_Hlt1Phys_Dec;
   Bool_t          mu_probe_Hlt1Phys_TIS;
   Bool_t          mu_probe_Hlt1Phys_TOS;
   Bool_t          mu_probe_Hlt2Global_Dec;
   Bool_t          mu_probe_Hlt2Global_TIS;
   Bool_t          mu_probe_Hlt2Global_TOS;
   Bool_t          mu_probe_Hlt2Phys_Dec;
   Bool_t          mu_probe_Hlt2Phys_TIS;
   Bool_t          mu_probe_Hlt2Phys_TOS;
   Bool_t          mu_probe_L0AnyDecision_Dec;
   Bool_t          mu_probe_L0AnyDecision_TIS;
   Bool_t          mu_probe_L0AnyDecision_TOS;
   Bool_t          mu_probe_L0MuonDecision_Dec;
   Bool_t          mu_probe_L0MuonDecision_TIS;
   Bool_t          mu_probe_L0MuonDecision_TOS;
   Bool_t          mu_probe_L0GlobalDecision_Dec;
   Bool_t          mu_probe_L0GlobalDecision_TIS;
   Bool_t          mu_probe_L0GlobalDecision_TOS;
   Bool_t          mu_probe_Hlt1TrackMVADecision_Dec;
   Bool_t          mu_probe_Hlt1TrackMVADecision_TIS;
   Bool_t          mu_probe_Hlt1TrackMVADecision_TOS;
   Bool_t          mu_probe_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          mu_probe_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          mu_probe_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          mu_probe_Hlt1SingleMuonHighPTDecision_Dec;
   Bool_t          mu_probe_Hlt1SingleMuonHighPTDecision_TIS;
   Bool_t          mu_probe_Hlt1SingleMuonHighPTDecision_TOS;
   Bool_t          mu_probe_Hlt1TrackAllL0Decision_Dec;
   Bool_t          mu_probe_Hlt1TrackAllL0Decision_TIS;
   Bool_t          mu_probe_Hlt1TrackAllL0Decision_TOS;
   Bool_t          mu_probe_Hlt1TrackMuonDecision_Dec;
   Bool_t          mu_probe_Hlt1TrackMuonDecision_TIS;
   Bool_t          mu_probe_Hlt1TrackMuonDecision_TOS;
   Bool_t          mu_probe_Hlt1TrackMuonMVADecision_Dec;
   Bool_t          mu_probe_Hlt1TrackMuonMVADecision_TIS;
   Bool_t          mu_probe_Hlt1TrackMuonMVADecision_TOS;
   Bool_t          mu_probe_Hlt1GlobalDecision_Dec;
   Bool_t          mu_probe_Hlt1GlobalDecision_TIS;
   Bool_t          mu_probe_Hlt1GlobalDecision_TOS;
   Bool_t          mu_probe_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          mu_probe_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          mu_probe_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          mu_probe_Hlt1DiMuonLowMassDecision_Dec;
   Bool_t          mu_probe_Hlt1DiMuonLowMassDecision_TIS;
   Bool_t          mu_probe_Hlt1DiMuonLowMassDecision_TOS;
   Bool_t          mu_probe_Hlt1SingleMuonNoIPDecision_Dec;
   Bool_t          mu_probe_Hlt1SingleMuonNoIPDecision_TIS;
   Bool_t          mu_probe_Hlt1SingleMuonNoIPDecision_TOS;
   Bool_t          mu_probe_Hlt2Topo2BodyDecision_Dec;
   Bool_t          mu_probe_Hlt2Topo2BodyDecision_TIS;
   Bool_t          mu_probe_Hlt2Topo2BodyDecision_TOS;
   Bool_t          mu_probe_Hlt2Topo3BodyDecision_Dec;
   Bool_t          mu_probe_Hlt2Topo3BodyDecision_TIS;
   Bool_t          mu_probe_Hlt2Topo3BodyDecision_TOS;
   Bool_t          mu_probe_Hlt2Topo4BodyDecision_Dec;
   Bool_t          mu_probe_Hlt2Topo4BodyDecision_TIS;
   Bool_t          mu_probe_Hlt2Topo4BodyDecision_TOS;
   Bool_t          mu_probe_Hlt2TopoMu2BodyDecision_Dec;
   Bool_t          mu_probe_Hlt2TopoMu2BodyDecision_TIS;
   Bool_t          mu_probe_Hlt2TopoMu2BodyDecision_TOS;
   Bool_t          mu_probe_Hlt2TopoMu3BodyDecision_Dec;
   Bool_t          mu_probe_Hlt2TopoMu3BodyDecision_TIS;
   Bool_t          mu_probe_Hlt2TopoMu3BodyDecision_TOS;
   Bool_t          mu_probe_Hlt2TopoMu4BodyDecision_Dec;
   Bool_t          mu_probe_Hlt2TopoMu4BodyDecision_TIS;
   Bool_t          mu_probe_Hlt2TopoMu4BodyDecision_TOS;
   Bool_t          mu_probe_Hlt2GlobalDecision_Dec;
   Bool_t          mu_probe_Hlt2GlobalDecision_TIS;
   Bool_t          mu_probe_Hlt2GlobalDecision_TOS;
   Bool_t          mu_probe_Hlt2SingleMuonDecision_Dec;
   Bool_t          mu_probe_Hlt2SingleMuonDecision_TIS;
   Bool_t          mu_probe_Hlt2SingleMuonDecision_TOS;
   Double_t        mu_probe_BPVIPCHI2;
   Double_t        mu_probe_Charge;
   Double_t        mu_probe_DLLK;
   Double_t        mu_probe_DLLe;
   Double_t        mu_probe_DLLmu;
   Double_t        mu_probe_DLLp;
   Double_t        mu_probe_DLLpi;
   Double_t        mu_probe_ETA;
   Double_t        mu_probe_INACCMUON;
   Double_t        mu_probe_PHI;
   Double_t        mu_probe_PID;
   Double_t        mu_probe_TRCHI2DOF;
   Double_t        mu_probe_nShared;
   Int_t           mu_probe_TRACK_Type;
   Int_t           mu_probe_TRACK_Key;
   Double_t        mu_probe_TRACK_CHI2NDOF;
   Double_t        mu_probe_TRACK_PCHI2;
   Double_t        mu_probe_TRACK_MatchCHI2;
   Double_t        mu_probe_TRACK_GhostProb;
   Double_t        mu_probe_TRACK_CloneDist;
   Double_t        mu_probe_TRACK_Likelihood;
   Double_t        K_CosTheta;
   Double_t        K_OWNPV_X;
   Double_t        K_OWNPV_Y;
   Double_t        K_OWNPV_Z;
   Double_t        K_OWNPV_XERR;
   Double_t        K_OWNPV_YERR;
   Double_t        K_OWNPV_ZERR;
   Double_t        K_OWNPV_CHI2;
   Int_t           K_OWNPV_NDOF;
   Float_t         K_OWNPV_COV_[3][3];
   Double_t        K_IP_OWNPV;
   Double_t        K_IPCHI2_OWNPV;
   Double_t        K_ORIVX_X;
   Double_t        K_ORIVX_Y;
   Double_t        K_ORIVX_Z;
   Double_t        K_ORIVX_XERR;
   Double_t        K_ORIVX_YERR;
   Double_t        K_ORIVX_ZERR;
   Double_t        K_ORIVX_CHI2;
   Int_t           K_ORIVX_NDOF;
   Float_t         K_ORIVX_COV_[3][3];
   Double_t        K_P;
   Double_t        K_PT;
   Double_t        K_PE;
   Double_t        K_PX;
   Double_t        K_PY;
   Double_t        K_PZ;
   Double_t        K_M;
   Int_t           K_ID;
   Double_t        K_PIDe;
   Double_t        K_PIDmu;
   Double_t        K_PIDK;
   Double_t        K_PIDp;
   Double_t        K_ProbNNe;
   Double_t        K_ProbNNk;
   Double_t        K_ProbNNp;
   Double_t        K_ProbNNpi;
   Double_t        K_ProbNNmu;
   Double_t        K_ProbNNghost;
   Bool_t          K_hasMuon;
   Bool_t          K_isMuon;
   Bool_t          K_hasRich;
   Bool_t          K_hasCalo;
   Bool_t          K_L0Global_Dec;
   Bool_t          K_L0Global_TIS;
   Bool_t          K_L0Global_TOS;
   Bool_t          K_Hlt1Global_Dec;
   Bool_t          K_Hlt1Global_TIS;
   Bool_t          K_Hlt1Global_TOS;
   Bool_t          K_Hlt1Phys_Dec;
   Bool_t          K_Hlt1Phys_TIS;
   Bool_t          K_Hlt1Phys_TOS;
   Bool_t          K_Hlt2Global_Dec;
   Bool_t          K_Hlt2Global_TIS;
   Bool_t          K_Hlt2Global_TOS;
   Bool_t          K_Hlt2Phys_Dec;
   Bool_t          K_Hlt2Phys_TIS;
   Bool_t          K_Hlt2Phys_TOS;
   Bool_t          K_L0AnyDecision_Dec;
   Bool_t          K_L0AnyDecision_TIS;
   Bool_t          K_L0AnyDecision_TOS;
   Bool_t          K_L0MuonDecision_Dec;
   Bool_t          K_L0MuonDecision_TIS;
   Bool_t          K_L0MuonDecision_TOS;
   Bool_t          K_L0GlobalDecision_Dec;
   Bool_t          K_L0GlobalDecision_TIS;
   Bool_t          K_L0GlobalDecision_TOS;
   Bool_t          K_Hlt1TrackMVADecision_Dec;
   Bool_t          K_Hlt1TrackMVADecision_TIS;
   Bool_t          K_Hlt1TrackMVADecision_TOS;
   Bool_t          K_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          K_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          K_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          K_Hlt1SingleMuonHighPTDecision_Dec;
   Bool_t          K_Hlt1SingleMuonHighPTDecision_TIS;
   Bool_t          K_Hlt1SingleMuonHighPTDecision_TOS;
   Bool_t          K_Hlt1TrackAllL0Decision_Dec;
   Bool_t          K_Hlt1TrackAllL0Decision_TIS;
   Bool_t          K_Hlt1TrackAllL0Decision_TOS;
   Bool_t          K_Hlt1TrackMuonDecision_Dec;
   Bool_t          K_Hlt1TrackMuonDecision_TIS;
   Bool_t          K_Hlt1TrackMuonDecision_TOS;
   Bool_t          K_Hlt1TrackMuonMVADecision_Dec;
   Bool_t          K_Hlt1TrackMuonMVADecision_TIS;
   Bool_t          K_Hlt1TrackMuonMVADecision_TOS;
   Bool_t          K_Hlt1GlobalDecision_Dec;
   Bool_t          K_Hlt1GlobalDecision_TIS;
   Bool_t          K_Hlt1GlobalDecision_TOS;
   Bool_t          K_Hlt1DiMuonHighMassDecision_Dec;
   Bool_t          K_Hlt1DiMuonHighMassDecision_TIS;
   Bool_t          K_Hlt1DiMuonHighMassDecision_TOS;
   Bool_t          K_Hlt1DiMuonLowMassDecision_Dec;
   Bool_t          K_Hlt1DiMuonLowMassDecision_TIS;
   Bool_t          K_Hlt1DiMuonLowMassDecision_TOS;
   Bool_t          K_Hlt1SingleMuonNoIPDecision_Dec;
   Bool_t          K_Hlt1SingleMuonNoIPDecision_TIS;
   Bool_t          K_Hlt1SingleMuonNoIPDecision_TOS;
   Bool_t          K_Hlt2Topo2BodyDecision_Dec;
   Bool_t          K_Hlt2Topo2BodyDecision_TIS;
   Bool_t          K_Hlt2Topo2BodyDecision_TOS;
   Bool_t          K_Hlt2Topo3BodyDecision_Dec;
   Bool_t          K_Hlt2Topo3BodyDecision_TIS;
   Bool_t          K_Hlt2Topo3BodyDecision_TOS;
   Bool_t          K_Hlt2Topo4BodyDecision_Dec;
   Bool_t          K_Hlt2Topo4BodyDecision_TIS;
   Bool_t          K_Hlt2Topo4BodyDecision_TOS;
   Bool_t          K_Hlt2TopoMu2BodyDecision_Dec;
   Bool_t          K_Hlt2TopoMu2BodyDecision_TIS;
   Bool_t          K_Hlt2TopoMu2BodyDecision_TOS;
   Bool_t          K_Hlt2TopoMu3BodyDecision_Dec;
   Bool_t          K_Hlt2TopoMu3BodyDecision_TIS;
   Bool_t          K_Hlt2TopoMu3BodyDecision_TOS;
   Bool_t          K_Hlt2TopoMu4BodyDecision_Dec;
   Bool_t          K_Hlt2TopoMu4BodyDecision_TIS;
   Bool_t          K_Hlt2TopoMu4BodyDecision_TOS;
   Bool_t          K_Hlt2GlobalDecision_Dec;
   Bool_t          K_Hlt2GlobalDecision_TIS;
   Bool_t          K_Hlt2GlobalDecision_TOS;
   Bool_t          K_Hlt2SingleMuonDecision_Dec;
   Bool_t          K_Hlt2SingleMuonDecision_TIS;
   Bool_t          K_Hlt2SingleMuonDecision_TOS;
   Int_t           K_TRACK_Type;
   Int_t           K_TRACK_Key;
   Double_t        K_TRACK_CHI2;
   Int_t           K_TRACK_NDOF;
   Double_t        K_TRACK_CHI2NDOF;
   Double_t        K_TRACK_PCHI2;
   Double_t        K_TRACK_VeloCHI2NDOF;
   Double_t        K_TRACK_TCHI2NDOF;
   Double_t        K_TRACK_VELO_UTID;
   Double_t        K_TRACK_TT_UTID;
   Double_t        K_TRACK_IT_UTID;
   Double_t        K_TRACK_OT_UTID;
   Double_t        K_TRACK_VP_UTID;
   Double_t        K_TRACK_UT_UTID;
   Double_t        K_TRACK_FT_UTID;
   Int_t           K_TRACK_nVeloHits;
   Int_t           K_TRACK_nVeloRHits;
   Int_t           K_TRACK_nVeloPhiHits;
   Int_t           K_TRACK_nVeloPileUpHits;
   Int_t           K_TRACK_nTTHits;
   Int_t           K_TRACK_nITHits;
   Int_t           K_TRACK_nOTHits;
   Int_t           K_TRACK_nVPHits;
   Int_t           K_TRACK_nUTHits;
   Int_t           K_TRACK_nFTHits;
   Double_t        K_TRACK_FirstMeasurementX;
   Double_t        K_TRACK_FirstMeasurementY;
   Double_t        K_TRACK_FirstMeasurementZ;
   Int_t           K_TRACK_History;
   Double_t        K_TRACK_MatchCHI2;
   Double_t        K_TRACK_GhostProb;
   Double_t        K_TRACK_CloneDist;
   Double_t        K_TRACK_Likelihood;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;

   // List of branches
   TBranch        *b_B_ENDVERTEX_X;   //!
   TBranch        *b_B_ENDVERTEX_Y;   //!
   TBranch        *b_B_ENDVERTEX_Z;   //!
   TBranch        *b_B_ENDVERTEX_XERR;   //!
   TBranch        *b_B_ENDVERTEX_YERR;   //!
   TBranch        *b_B_ENDVERTEX_ZERR;   //!
   TBranch        *b_B_ENDVERTEX_CHI2;   //!
   TBranch        *b_B_ENDVERTEX_NDOF;   //!
   TBranch        *b_B_ENDVERTEX_COV_;   //!
   TBranch        *b_B_OWNPV_X;   //!
   TBranch        *b_B_OWNPV_Y;   //!
   TBranch        *b_B_OWNPV_Z;   //!
   TBranch        *b_B_OWNPV_XERR;   //!
   TBranch        *b_B_OWNPV_YERR;   //!
   TBranch        *b_B_OWNPV_ZERR;   //!
   TBranch        *b_B_OWNPV_CHI2;   //!
   TBranch        *b_B_OWNPV_NDOF;   //!
   TBranch        *b_B_OWNPV_COV_;   //!
   TBranch        *b_B_IP_OWNPV;   //!
   TBranch        *b_B_IPCHI2_OWNPV;   //!
   TBranch        *b_B_FD_OWNPV;   //!
   TBranch        *b_B_FDCHI2_OWNPV;   //!
   TBranch        *b_B_DIRA_OWNPV;   //!
   TBranch        *b_B_P;   //!
   TBranch        *b_B_PT;   //!
   TBranch        *b_B_PE;   //!
   TBranch        *b_B_PX;   //!
   TBranch        *b_B_PY;   //!
   TBranch        *b_B_PZ;   //!
   TBranch        *b_B_MM;   //!
   TBranch        *b_B_MMERR;   //!
   TBranch        *b_B_M;   //!
   TBranch        *b_B_ID;   //!
   TBranch        *b_B_TAU;   //!
   TBranch        *b_B_TAUERR;   //!
   TBranch        *b_B_TAUCHI2;   //!
   TBranch        *b_B_L0Global_Dec;   //!
   TBranch        *b_B_L0Global_TIS;   //!
   TBranch        *b_B_L0Global_TOS;   //!
   TBranch        *b_B_Hlt1Global_Dec;   //!
   TBranch        *b_B_Hlt1Global_TIS;   //!
   TBranch        *b_B_Hlt1Global_TOS;   //!
   TBranch        *b_B_Hlt1Phys_Dec;   //!
   TBranch        *b_B_Hlt1Phys_TIS;   //!
   TBranch        *b_B_Hlt1Phys_TOS;   //!
   TBranch        *b_B_Hlt2Global_Dec;   //!
   TBranch        *b_B_Hlt2Global_TIS;   //!
   TBranch        *b_B_Hlt2Global_TOS;   //!
   TBranch        *b_B_Hlt2Phys_Dec;   //!
   TBranch        *b_B_Hlt2Phys_TIS;   //!
   TBranch        *b_B_Hlt2Phys_TOS;   //!
   TBranch        *b_B_L0AnyDecision_Dec;   //!
   TBranch        *b_B_L0AnyDecision_TIS;   //!
   TBranch        *b_B_L0AnyDecision_TOS;   //!
   TBranch        *b_B_L0MuonDecision_Dec;   //!
   TBranch        *b_B_L0MuonDecision_TIS;   //!
   TBranch        *b_B_L0MuonDecision_TOS;   //!
   TBranch        *b_B_L0GlobalDecision_Dec;   //!
   TBranch        *b_B_L0GlobalDecision_TIS;   //!
   TBranch        *b_B_L0GlobalDecision_TOS;   //!
   TBranch        *b_B_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_B_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_B_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_B_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_B_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_B_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_B_Hlt1SingleMuonHighPTDecision_Dec;   //!
   TBranch        *b_B_Hlt1SingleMuonHighPTDecision_TIS;   //!
   TBranch        *b_B_Hlt1SingleMuonHighPTDecision_TOS;   //!
   TBranch        *b_B_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_B_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_B_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_B_Hlt1TrackMuonDecision_Dec;   //!
   TBranch        *b_B_Hlt1TrackMuonDecision_TIS;   //!
   TBranch        *b_B_Hlt1TrackMuonDecision_TOS;   //!
   TBranch        *b_B_Hlt1TrackMuonMVADecision_Dec;   //!
   TBranch        *b_B_Hlt1TrackMuonMVADecision_TIS;   //!
   TBranch        *b_B_Hlt1TrackMuonMVADecision_TOS;   //!
   TBranch        *b_B_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_B_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_B_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_B_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_B_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_B_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_B_Hlt1DiMuonLowMassDecision_Dec;   //!
   TBranch        *b_B_Hlt1DiMuonLowMassDecision_TIS;   //!
   TBranch        *b_B_Hlt1DiMuonLowMassDecision_TOS;   //!
   TBranch        *b_B_Hlt1SingleMuonNoIPDecision_Dec;   //!
   TBranch        *b_B_Hlt1SingleMuonNoIPDecision_TIS;   //!
   TBranch        *b_B_Hlt1SingleMuonNoIPDecision_TOS;   //!
   TBranch        *b_B_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2TopoMu2BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2TopoMu2BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2TopoMu2BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2TopoMu3BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2TopoMu3BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2TopoMu3BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2TopoMu4BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2TopoMu4BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2TopoMu4BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_B_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_B_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_B_Hlt2SingleMuonDecision_Dec;   //!
   TBranch        *b_B_Hlt2SingleMuonDecision_TIS;   //!
   TBranch        *b_B_Hlt2SingleMuonDecision_TOS;   //!
   TBranch        *b_JPsi_CosTheta;   //!
   TBranch        *b_JPsi_ENDVERTEX_X;   //!
   TBranch        *b_JPsi_ENDVERTEX_Y;   //!
   TBranch        *b_JPsi_ENDVERTEX_Z;   //!
   TBranch        *b_JPsi_ENDVERTEX_XERR;   //!
   TBranch        *b_JPsi_ENDVERTEX_YERR;   //!
   TBranch        *b_JPsi_ENDVERTEX_ZERR;   //!
   TBranch        *b_JPsi_ENDVERTEX_CHI2;   //!
   TBranch        *b_JPsi_ENDVERTEX_NDOF;   //!
   TBranch        *b_JPsi_ENDVERTEX_COV_;   //!
   TBranch        *b_JPsi_OWNPV_X;   //!
   TBranch        *b_JPsi_OWNPV_Y;   //!
   TBranch        *b_JPsi_OWNPV_Z;   //!
   TBranch        *b_JPsi_OWNPV_XERR;   //!
   TBranch        *b_JPsi_OWNPV_YERR;   //!
   TBranch        *b_JPsi_OWNPV_ZERR;   //!
   TBranch        *b_JPsi_OWNPV_CHI2;   //!
   TBranch        *b_JPsi_OWNPV_NDOF;   //!
   TBranch        *b_JPsi_OWNPV_COV_;   //!
   TBranch        *b_JPsi_IP_OWNPV;   //!
   TBranch        *b_JPsi_IPCHI2_OWNPV;   //!
   TBranch        *b_JPsi_FD_OWNPV;   //!
   TBranch        *b_JPsi_FDCHI2_OWNPV;   //!
   TBranch        *b_JPsi_DIRA_OWNPV;   //!
   TBranch        *b_JPsi_ORIVX_X;   //!
   TBranch        *b_JPsi_ORIVX_Y;   //!
   TBranch        *b_JPsi_ORIVX_Z;   //!
   TBranch        *b_JPsi_ORIVX_XERR;   //!
   TBranch        *b_JPsi_ORIVX_YERR;   //!
   TBranch        *b_JPsi_ORIVX_ZERR;   //!
   TBranch        *b_JPsi_ORIVX_CHI2;   //!
   TBranch        *b_JPsi_ORIVX_NDOF;   //!
   TBranch        *b_JPsi_ORIVX_COV_;   //!
   TBranch        *b_JPsi_FD_ORIVX;   //!
   TBranch        *b_JPsi_FDCHI2_ORIVX;   //!
   TBranch        *b_JPsi_DIRA_ORIVX;   //!
   TBranch        *b_JPsi_P;   //!
   TBranch        *b_JPsi_PT;   //!
   TBranch        *b_JPsi_PE;   //!
   TBranch        *b_JPsi_PX;   //!
   TBranch        *b_JPsi_PY;   //!
   TBranch        *b_JPsi_PZ;   //!
   TBranch        *b_JPsi_MM;   //!
   TBranch        *b_JPsi_MMERR;   //!
   TBranch        *b_JPsi_M;   //!
   TBranch        *b_JPsi_ID;   //!
   TBranch        *b_JPsi_TAU;   //!
   TBranch        *b_JPsi_TAUERR;   //!
   TBranch        *b_JPsi_TAUCHI2;   //!
   TBranch        *b_JPsi_L0Global_Dec;   //!
   TBranch        *b_JPsi_L0Global_TIS;   //!
   TBranch        *b_JPsi_L0Global_TOS;   //!
   TBranch        *b_JPsi_Hlt1Global_Dec;   //!
   TBranch        *b_JPsi_Hlt1Global_TIS;   //!
   TBranch        *b_JPsi_Hlt1Global_TOS;   //!
   TBranch        *b_JPsi_Hlt1Phys_Dec;   //!
   TBranch        *b_JPsi_Hlt1Phys_TIS;   //!
   TBranch        *b_JPsi_Hlt1Phys_TOS;   //!
   TBranch        *b_JPsi_Hlt2Global_Dec;   //!
   TBranch        *b_JPsi_Hlt2Global_TIS;   //!
   TBranch        *b_JPsi_Hlt2Global_TOS;   //!
   TBranch        *b_JPsi_Hlt2Phys_Dec;   //!
   TBranch        *b_JPsi_Hlt2Phys_TIS;   //!
   TBranch        *b_JPsi_Hlt2Phys_TOS;   //!
   TBranch        *b_JPsi_L0AnyDecision_Dec;   //!
   TBranch        *b_JPsi_L0AnyDecision_TIS;   //!
   TBranch        *b_JPsi_L0AnyDecision_TOS;   //!
   TBranch        *b_JPsi_L0MuonDecision_Dec;   //!
   TBranch        *b_JPsi_L0MuonDecision_TIS;   //!
   TBranch        *b_JPsi_L0MuonDecision_TOS;   //!
   TBranch        *b_JPsi_L0GlobalDecision_Dec;   //!
   TBranch        *b_JPsi_L0GlobalDecision_TIS;   //!
   TBranch        *b_JPsi_L0GlobalDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1SingleMuonHighPTDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1SingleMuonHighPTDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1SingleMuonHighPTDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_JPsi_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_JPsi_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_JPsi_Hlt1TrackMuonDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1TrackMuonDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1TrackMuonDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1TrackMuonMVADecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1TrackMuonMVADecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1TrackMuonMVADecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1DiMuonLowMassDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1DiMuonLowMassDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1DiMuonLowMassDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt1SingleMuonNoIPDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt1SingleMuonNoIPDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt1SingleMuonNoIPDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt2TopoMu2BodyDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt2TopoMu2BodyDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt2TopoMu2BodyDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt2TopoMu3BodyDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt2TopoMu3BodyDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt2TopoMu3BodyDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt2TopoMu4BodyDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt2TopoMu4BodyDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt2TopoMu4BodyDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_JPsi_Hlt2SingleMuonDecision_Dec;   //!
   TBranch        *b_JPsi_Hlt2SingleMuonDecision_TIS;   //!
   TBranch        *b_JPsi_Hlt2SingleMuonDecision_TOS;   //!
   TBranch        *b_mu_tag_CosTheta;   //!
   TBranch        *b_mu_tag_OWNPV_X;   //!
   TBranch        *b_mu_tag_OWNPV_Y;   //!
   TBranch        *b_mu_tag_OWNPV_Z;   //!
   TBranch        *b_mu_tag_OWNPV_XERR;   //!
   TBranch        *b_mu_tag_OWNPV_YERR;   //!
   TBranch        *b_mu_tag_OWNPV_ZERR;   //!
   TBranch        *b_mu_tag_OWNPV_CHI2;   //!
   TBranch        *b_mu_tag_OWNPV_NDOF;   //!
   TBranch        *b_mu_tag_OWNPV_COV_;   //!
   TBranch        *b_mu_tag_IP_OWNPV;   //!
   TBranch        *b_mu_tag_IPCHI2_OWNPV;   //!
   TBranch        *b_mu_tag_ORIVX_X;   //!
   TBranch        *b_mu_tag_ORIVX_Y;   //!
   TBranch        *b_mu_tag_ORIVX_Z;   //!
   TBranch        *b_mu_tag_ORIVX_XERR;   //!
   TBranch        *b_mu_tag_ORIVX_YERR;   //!
   TBranch        *b_mu_tag_ORIVX_ZERR;   //!
   TBranch        *b_mu_tag_ORIVX_CHI2;   //!
   TBranch        *b_mu_tag_ORIVX_NDOF;   //!
   TBranch        *b_mu_tag_ORIVX_COV_;   //!
   TBranch        *b_mu_tag_P;   //!
   TBranch        *b_mu_tag_PT;   //!
   TBranch        *b_mu_tag_PE;   //!
   TBranch        *b_mu_tag_PX;   //!
   TBranch        *b_mu_tag_PY;   //!
   TBranch        *b_mu_tag_PZ;   //!
   TBranch        *b_mu_tag_M;   //!
   TBranch        *b_mu_tag_ID;   //!
   TBranch        *b_mu_tag_PIDe;   //!
   TBranch        *b_mu_tag_PIDmu;   //!
   TBranch        *b_mu_tag_PIDK;   //!
   TBranch        *b_mu_tag_PIDp;   //!
   TBranch        *b_mu_tag_ProbNNe;   //!
   TBranch        *b_mu_tag_ProbNNk;   //!
   TBranch        *b_mu_tag_ProbNNp;   //!
   TBranch        *b_mu_tag_ProbNNpi;   //!
   TBranch        *b_mu_tag_ProbNNmu;   //!
   TBranch        *b_mu_tag_ProbNNghost;   //!
   TBranch        *b_mu_tag_hasMuon;   //!
   TBranch        *b_mu_tag_isMuon;   //!
   TBranch        *b_mu_tag_hasRich;   //!
   TBranch        *b_mu_tag_hasCalo;   //!
   TBranch        *b_mu_tag_L0Global_Dec;   //!
   TBranch        *b_mu_tag_L0Global_TIS;   //!
   TBranch        *b_mu_tag_L0Global_TOS;   //!
   TBranch        *b_mu_tag_Hlt1Global_Dec;   //!
   TBranch        *b_mu_tag_Hlt1Global_TIS;   //!
   TBranch        *b_mu_tag_Hlt1Global_TOS;   //!
   TBranch        *b_mu_tag_Hlt1Phys_Dec;   //!
   TBranch        *b_mu_tag_Hlt1Phys_TIS;   //!
   TBranch        *b_mu_tag_Hlt1Phys_TOS;   //!
   TBranch        *b_mu_tag_Hlt2Global_Dec;   //!
   TBranch        *b_mu_tag_Hlt2Global_TIS;   //!
   TBranch        *b_mu_tag_Hlt2Global_TOS;   //!
   TBranch        *b_mu_tag_Hlt2Phys_Dec;   //!
   TBranch        *b_mu_tag_Hlt2Phys_TIS;   //!
   TBranch        *b_mu_tag_Hlt2Phys_TOS;   //!
   TBranch        *b_mu_tag_L0AnyDecision_Dec;   //!
   TBranch        *b_mu_tag_L0AnyDecision_TIS;   //!
   TBranch        *b_mu_tag_L0AnyDecision_TOS;   //!
   TBranch        *b_mu_tag_L0MuonDecision_Dec;   //!
   TBranch        *b_mu_tag_L0MuonDecision_TIS;   //!
   TBranch        *b_mu_tag_L0MuonDecision_TOS;   //!
   TBranch        *b_mu_tag_L0GlobalDecision_Dec;   //!
   TBranch        *b_mu_tag_L0GlobalDecision_TIS;   //!
   TBranch        *b_mu_tag_L0GlobalDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1SingleMuonHighPTDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1SingleMuonHighPTDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1SingleMuonHighPTDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1TrackMuonDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1TrackMuonDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1TrackMuonDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1TrackMuonMVADecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1TrackMuonMVADecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1TrackMuonMVADecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1DiMuonLowMassDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1DiMuonLowMassDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1DiMuonLowMassDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt1SingleMuonNoIPDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt1SingleMuonNoIPDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt1SingleMuonNoIPDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu2BodyDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu2BodyDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu2BodyDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu3BodyDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu3BodyDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu3BodyDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu4BodyDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu4BodyDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt2TopoMu4BodyDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_mu_tag_Hlt2SingleMuonDecision_Dec;   //!
   TBranch        *b_mu_tag_Hlt2SingleMuonDecision_TIS;   //!
   TBranch        *b_mu_tag_Hlt2SingleMuonDecision_TOS;   //!
   TBranch        *b_mu_tag_BPVIPCHI2;   //!
   TBranch        *b_mu_tag_Charge;   //!
   TBranch        *b_mu_tag_DLLK;   //!
   TBranch        *b_mu_tag_DLLe;   //!
   TBranch        *b_mu_tag_DLLmu;   //!
   TBranch        *b_mu_tag_DLLp;   //!
   TBranch        *b_mu_tag_DLLpi;   //!
   TBranch        *b_mu_tag_ETA;   //!
   TBranch        *b_mu_tag_INACCMUON;   //!
   TBranch        *b_mu_tag_PHI;   //!
   TBranch        *b_mu_tag_PID;   //!
   TBranch        *b_mu_tag_TRCHI2DOF;   //!
   TBranch        *b_mu_tag_nShared;   //!
   TBranch        *b_mu_tag_TRACK_Type;   //!
   TBranch        *b_mu_tag_TRACK_Key;   //!
   TBranch        *b_mu_tag_TRACK_CHI2NDOF;   //!
   TBranch        *b_mu_tag_TRACK_PCHI2;   //!
   TBranch        *b_mu_tag_TRACK_MatchCHI2;   //!
   TBranch        *b_mu_tag_TRACK_GhostProb;   //!
   TBranch        *b_mu_tag_TRACK_CloneDist;   //!
   TBranch        *b_mu_tag_TRACK_Likelihood;   //!
   TBranch        *b_mu_probe_CosTheta;   //!
   TBranch        *b_mu_probe_OWNPV_X;   //!
   TBranch        *b_mu_probe_OWNPV_Y;   //!
   TBranch        *b_mu_probe_OWNPV_Z;   //!
   TBranch        *b_mu_probe_OWNPV_XERR;   //!
   TBranch        *b_mu_probe_OWNPV_YERR;   //!
   TBranch        *b_mu_probe_OWNPV_ZERR;   //!
   TBranch        *b_mu_probe_OWNPV_CHI2;   //!
   TBranch        *b_mu_probe_OWNPV_NDOF;   //!
   TBranch        *b_mu_probe_OWNPV_COV_;   //!
   TBranch        *b_mu_probe_IP_OWNPV;   //!
   TBranch        *b_mu_probe_IPCHI2_OWNPV;   //!
   TBranch        *b_mu_probe_ORIVX_X;   //!
   TBranch        *b_mu_probe_ORIVX_Y;   //!
   TBranch        *b_mu_probe_ORIVX_Z;   //!
   TBranch        *b_mu_probe_ORIVX_XERR;   //!
   TBranch        *b_mu_probe_ORIVX_YERR;   //!
   TBranch        *b_mu_probe_ORIVX_ZERR;   //!
   TBranch        *b_mu_probe_ORIVX_CHI2;   //!
   TBranch        *b_mu_probe_ORIVX_NDOF;   //!
   TBranch        *b_mu_probe_ORIVX_COV_;   //!
   TBranch        *b_mu_probe_P;   //!
   TBranch        *b_mu_probe_PT;   //!
   TBranch        *b_mu_probe_PE;   //!
   TBranch        *b_mu_probe_PX;   //!
   TBranch        *b_mu_probe_PY;   //!
   TBranch        *b_mu_probe_PZ;   //!
   TBranch        *b_mu_probe_M;   //!
   TBranch        *b_mu_probe_ID;   //!
   TBranch        *b_mu_probe_PIDe;   //!
   TBranch        *b_mu_probe_PIDmu;   //!
   TBranch        *b_mu_probe_PIDK;   //!
   TBranch        *b_mu_probe_PIDp;   //!
   TBranch        *b_mu_probe_ProbNNe;   //!
   TBranch        *b_mu_probe_ProbNNk;   //!
   TBranch        *b_mu_probe_ProbNNp;   //!
   TBranch        *b_mu_probe_ProbNNpi;   //!
   TBranch        *b_mu_probe_ProbNNmu;   //!
   TBranch        *b_mu_probe_ProbNNghost;   //!
   TBranch        *b_mu_probe_hasMuon;   //!
   TBranch        *b_mu_probe_isMuon;   //!
   TBranch        *b_mu_probe_hasRich;   //!
   TBranch        *b_mu_probe_hasCalo;   //!
   TBranch        *b_mu_probe_L0Global_Dec;   //!
   TBranch        *b_mu_probe_L0Global_TIS;   //!
   TBranch        *b_mu_probe_L0Global_TOS;   //!
   TBranch        *b_mu_probe_Hlt1Global_Dec;   //!
   TBranch        *b_mu_probe_Hlt1Global_TIS;   //!
   TBranch        *b_mu_probe_Hlt1Global_TOS;   //!
   TBranch        *b_mu_probe_Hlt1Phys_Dec;   //!
   TBranch        *b_mu_probe_Hlt1Phys_TIS;   //!
   TBranch        *b_mu_probe_Hlt1Phys_TOS;   //!
   TBranch        *b_mu_probe_Hlt2Global_Dec;   //!
   TBranch        *b_mu_probe_Hlt2Global_TIS;   //!
   TBranch        *b_mu_probe_Hlt2Global_TOS;   //!
   TBranch        *b_mu_probe_Hlt2Phys_Dec;   //!
   TBranch        *b_mu_probe_Hlt2Phys_TIS;   //!
   TBranch        *b_mu_probe_Hlt2Phys_TOS;   //!
   TBranch        *b_mu_probe_L0AnyDecision_Dec;   //!
   TBranch        *b_mu_probe_L0AnyDecision_TIS;   //!
   TBranch        *b_mu_probe_L0AnyDecision_TOS;   //!
   TBranch        *b_mu_probe_L0MuonDecision_Dec;   //!
   TBranch        *b_mu_probe_L0MuonDecision_TIS;   //!
   TBranch        *b_mu_probe_L0MuonDecision_TOS;   //!
   TBranch        *b_mu_probe_L0GlobalDecision_Dec;   //!
   TBranch        *b_mu_probe_L0GlobalDecision_TIS;   //!
   TBranch        *b_mu_probe_L0GlobalDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1SingleMuonHighPTDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1SingleMuonHighPTDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1SingleMuonHighPTDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1TrackMuonDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1TrackMuonDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1TrackMuonDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1TrackMuonMVADecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1TrackMuonMVADecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1TrackMuonMVADecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1DiMuonLowMassDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1DiMuonLowMassDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1DiMuonLowMassDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt1SingleMuonNoIPDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt1SingleMuonNoIPDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt1SingleMuonNoIPDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu2BodyDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu2BodyDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu2BodyDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu3BodyDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu3BodyDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu3BodyDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu4BodyDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu4BodyDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt2TopoMu4BodyDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_mu_probe_Hlt2SingleMuonDecision_Dec;   //!
   TBranch        *b_mu_probe_Hlt2SingleMuonDecision_TIS;   //!
   TBranch        *b_mu_probe_Hlt2SingleMuonDecision_TOS;   //!
   TBranch        *b_mu_probe_BPVIPCHI2;   //!
   TBranch        *b_mu_probe_Charge;   //!
   TBranch        *b_mu_probe_DLLK;   //!
   TBranch        *b_mu_probe_DLLe;   //!
   TBranch        *b_mu_probe_DLLmu;   //!
   TBranch        *b_mu_probe_DLLp;   //!
   TBranch        *b_mu_probe_DLLpi;   //!
   TBranch        *b_mu_probe_ETA;   //!
   TBranch        *b_mu_probe_INACCMUON;   //!
   TBranch        *b_mu_probe_PHI;   //!
   TBranch        *b_mu_probe_PID;   //!
   TBranch        *b_mu_probe_TRCHI2DOF;   //!
   TBranch        *b_mu_probe_nShared;   //!
   TBranch        *b_mu_probe_TRACK_Type;   //!
   TBranch        *b_mu_probe_TRACK_Key;   //!
   TBranch        *b_mu_probe_TRACK_CHI2NDOF;   //!
   TBranch        *b_mu_probe_TRACK_PCHI2;   //!
   TBranch        *b_mu_probe_TRACK_MatchCHI2;   //!
   TBranch        *b_mu_probe_TRACK_GhostProb;   //!
   TBranch        *b_mu_probe_TRACK_CloneDist;   //!
   TBranch        *b_mu_probe_TRACK_Likelihood;   //!
   TBranch        *b_K_CosTheta;   //!
   TBranch        *b_K_OWNPV_X;   //!
   TBranch        *b_K_OWNPV_Y;   //!
   TBranch        *b_K_OWNPV_Z;   //!
   TBranch        *b_K_OWNPV_XERR;   //!
   TBranch        *b_K_OWNPV_YERR;   //!
   TBranch        *b_K_OWNPV_ZERR;   //!
   TBranch        *b_K_OWNPV_CHI2;   //!
   TBranch        *b_K_OWNPV_NDOF;   //!
   TBranch        *b_K_OWNPV_COV_;   //!
   TBranch        *b_K_IP_OWNPV;   //!
   TBranch        *b_K_IPCHI2_OWNPV;   //!
   TBranch        *b_K_ORIVX_X;   //!
   TBranch        *b_K_ORIVX_Y;   //!
   TBranch        *b_K_ORIVX_Z;   //!
   TBranch        *b_K_ORIVX_XERR;   //!
   TBranch        *b_K_ORIVX_YERR;   //!
   TBranch        *b_K_ORIVX_ZERR;   //!
   TBranch        *b_K_ORIVX_CHI2;   //!
   TBranch        *b_K_ORIVX_NDOF;   //!
   TBranch        *b_K_ORIVX_COV_;   //!
   TBranch        *b_K_P;   //!
   TBranch        *b_K_PT;   //!
   TBranch        *b_K_PE;   //!
   TBranch        *b_K_PX;   //!
   TBranch        *b_K_PY;   //!
   TBranch        *b_K_PZ;   //!
   TBranch        *b_K_M;   //!
   TBranch        *b_K_ID;   //!
   TBranch        *b_K_PIDe;   //!
   TBranch        *b_K_PIDmu;   //!
   TBranch        *b_K_PIDK;   //!
   TBranch        *b_K_PIDp;   //!
   TBranch        *b_K_ProbNNe;   //!
   TBranch        *b_K_ProbNNk;   //!
   TBranch        *b_K_ProbNNp;   //!
   TBranch        *b_K_ProbNNpi;   //!
   TBranch        *b_K_ProbNNmu;   //!
   TBranch        *b_K_ProbNNghost;   //!
   TBranch        *b_K_hasMuon;   //!
   TBranch        *b_K_isMuon;   //!
   TBranch        *b_K_hasRich;   //!
   TBranch        *b_K_hasCalo;   //!
   TBranch        *b_K_L0Global_Dec;   //!
   TBranch        *b_K_L0Global_TIS;   //!
   TBranch        *b_K_L0Global_TOS;   //!
   TBranch        *b_K_Hlt1Global_Dec;   //!
   TBranch        *b_K_Hlt1Global_TIS;   //!
   TBranch        *b_K_Hlt1Global_TOS;   //!
   TBranch        *b_K_Hlt1Phys_Dec;   //!
   TBranch        *b_K_Hlt1Phys_TIS;   //!
   TBranch        *b_K_Hlt1Phys_TOS;   //!
   TBranch        *b_K_Hlt2Global_Dec;   //!
   TBranch        *b_K_Hlt2Global_TIS;   //!
   TBranch        *b_K_Hlt2Global_TOS;   //!
   TBranch        *b_K_Hlt2Phys_Dec;   //!
   TBranch        *b_K_Hlt2Phys_TIS;   //!
   TBranch        *b_K_Hlt2Phys_TOS;   //!
   TBranch        *b_K_L0AnyDecision_Dec;   //!
   TBranch        *b_K_L0AnyDecision_TIS;   //!
   TBranch        *b_K_L0AnyDecision_TOS;   //!
   TBranch        *b_K_L0MuonDecision_Dec;   //!
   TBranch        *b_K_L0MuonDecision_TIS;   //!
   TBranch        *b_K_L0MuonDecision_TOS;   //!
   TBranch        *b_K_L0GlobalDecision_Dec;   //!
   TBranch        *b_K_L0GlobalDecision_TIS;   //!
   TBranch        *b_K_L0GlobalDecision_TOS;   //!
   TBranch        *b_K_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_K_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_K_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_K_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_K_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_K_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_K_Hlt1SingleMuonHighPTDecision_Dec;   //!
   TBranch        *b_K_Hlt1SingleMuonHighPTDecision_TIS;   //!
   TBranch        *b_K_Hlt1SingleMuonHighPTDecision_TOS;   //!
   TBranch        *b_K_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_K_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_K_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_K_Hlt1TrackMuonDecision_Dec;   //!
   TBranch        *b_K_Hlt1TrackMuonDecision_TIS;   //!
   TBranch        *b_K_Hlt1TrackMuonDecision_TOS;   //!
   TBranch        *b_K_Hlt1TrackMuonMVADecision_Dec;   //!
   TBranch        *b_K_Hlt1TrackMuonMVADecision_TIS;   //!
   TBranch        *b_K_Hlt1TrackMuonMVADecision_TOS;   //!
   TBranch        *b_K_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_K_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_K_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_K_Hlt1DiMuonHighMassDecision_Dec;   //!
   TBranch        *b_K_Hlt1DiMuonHighMassDecision_TIS;   //!
   TBranch        *b_K_Hlt1DiMuonHighMassDecision_TOS;   //!
   TBranch        *b_K_Hlt1DiMuonLowMassDecision_Dec;   //!
   TBranch        *b_K_Hlt1DiMuonLowMassDecision_TIS;   //!
   TBranch        *b_K_Hlt1DiMuonLowMassDecision_TOS;   //!
   TBranch        *b_K_Hlt1SingleMuonNoIPDecision_Dec;   //!
   TBranch        *b_K_Hlt1SingleMuonNoIPDecision_TIS;   //!
   TBranch        *b_K_Hlt1SingleMuonNoIPDecision_TOS;   //!
   TBranch        *b_K_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_K_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_K_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_K_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_K_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_K_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_K_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_K_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_K_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_K_Hlt2TopoMu2BodyDecision_Dec;   //!
   TBranch        *b_K_Hlt2TopoMu2BodyDecision_TIS;   //!
   TBranch        *b_K_Hlt2TopoMu2BodyDecision_TOS;   //!
   TBranch        *b_K_Hlt2TopoMu3BodyDecision_Dec;   //!
   TBranch        *b_K_Hlt2TopoMu3BodyDecision_TIS;   //!
   TBranch        *b_K_Hlt2TopoMu3BodyDecision_TOS;   //!
   TBranch        *b_K_Hlt2TopoMu4BodyDecision_Dec;   //!
   TBranch        *b_K_Hlt2TopoMu4BodyDecision_TIS;   //!
   TBranch        *b_K_Hlt2TopoMu4BodyDecision_TOS;   //!
   TBranch        *b_K_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_K_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_K_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_K_Hlt2SingleMuonDecision_Dec;   //!
   TBranch        *b_K_Hlt2SingleMuonDecision_TIS;   //!
   TBranch        *b_K_Hlt2SingleMuonDecision_TOS;   //!
   TBranch        *b_K_TRACK_Type;   //!
   TBranch        *b_K_TRACK_Key;   //!
   TBranch        *b_K_TRACK_CHI2;   //!
   TBranch        *b_K_TRACK_NDOF;   //!
   TBranch        *b_K_TRACK_CHI2NDOF;   //!
   TBranch        *b_K_TRACK_PCHI2;   //!
   TBranch        *b_K_TRACK_VeloCHI2NDOF;   //!
   TBranch        *b_K_TRACK_TCHI2NDOF;   //!
   TBranch        *b_K_TRACK_VELO_UTID;   //!
   TBranch        *b_K_TRACK_TT_UTID;   //!
   TBranch        *b_K_TRACK_IT_UTID;   //!
   TBranch        *b_K_TRACK_OT_UTID;   //!
   TBranch        *b_K_TRACK_VP_UTID;   //!
   TBranch        *b_K_TRACK_UT_UTID;   //!
   TBranch        *b_K_TRACK_FT_UTID;   //!
   TBranch        *b_K_TRACK_nVeloHits;   //!
   TBranch        *b_K_TRACK_nVeloRHits;   //!
   TBranch        *b_K_TRACK_nVeloPhiHits;   //!
   TBranch        *b_K_TRACK_nVeloPileUpHits;   //!
   TBranch        *b_K_TRACK_nTTHits;   //!
   TBranch        *b_K_TRACK_nITHits;   //!
   TBranch        *b_K_TRACK_nOTHits;   //!
   TBranch        *b_K_TRACK_nVPHits;   //!
   TBranch        *b_K_TRACK_nUTHits;   //!
   TBranch        *b_K_TRACK_nFTHits;   //!
   TBranch        *b_K_TRACK_FirstMeasurementX;   //!
   TBranch        *b_K_TRACK_FirstMeasurementY;   //!
   TBranch        *b_K_TRACK_FirstMeasurementZ;   //!
   TBranch        *b_K_TRACK_History;   //!
   TBranch        *b_K_TRACK_MatchCHI2;   //!
   TBranch        *b_K_TRACK_GhostProb;   //!
   TBranch        *b_K_TRACK_CloneDist;   //!
   TBranch        *b_K_TRACK_Likelihood;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!

   B2JPsiK_selection(TTree *tree=0);
   virtual ~B2JPsiK_selection();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual bool     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef B2JPsiK_selection_cxx
B2JPsiK_selection::B2JPsiK_selection(TTree *tree) : nfout("test.root"), ntag("mu_plus"), nprobe("mu_minus"), fChain(0) 
{
  
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
// ************ (commented because relative path dependent) ***************
   /* if (tree == 0) { */
   /*    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("input_tuples/B2JPsiK_NegMuonTag.root"); */
   /*    if (!f || !f->IsOpen()) { */
   /*       f = new TFile("input_tuples/B2JPsiK_NegMuonTag.root"); */
   /*    } */
   /*    TDirectory * dir = (TDirectory*)f->Get("input_tuples/B2JPsiK_NegMuonTag.root:/tuplemumu"); */
   /*    dir->GetObject("MyTuple",tree); */

   /* } */
   Init(tree);
}

B2JPsiK_selection::~B2JPsiK_selection()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

bool B2JPsiK_selection::SetChain(TTree *tree, bool plus_is_tag)
{
  if (plus_is_tag){
    ntag = "mu_plus";
    nprobe = "mu_minus";
  } else {
    ntag = "mu_minus";
    nprobe = "mu_plus";
  }
    
  if (!tree) return false;
  return Init(tree);
}




Int_t B2JPsiK_selection::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t B2JPsiK_selection::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

bool B2JPsiK_selection::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return false;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("B_ENDVERTEX_X", &B_ENDVERTEX_X, &b_B_ENDVERTEX_X);
   fChain->SetBranchAddress("B_ENDVERTEX_Y", &B_ENDVERTEX_Y, &b_B_ENDVERTEX_Y);
   fChain->SetBranchAddress("B_ENDVERTEX_Z", &B_ENDVERTEX_Z, &b_B_ENDVERTEX_Z);
   fChain->SetBranchAddress("B_ENDVERTEX_XERR", &B_ENDVERTEX_XERR, &b_B_ENDVERTEX_XERR);
   fChain->SetBranchAddress("B_ENDVERTEX_YERR", &B_ENDVERTEX_YERR, &b_B_ENDVERTEX_YERR);
   fChain->SetBranchAddress("B_ENDVERTEX_ZERR", &B_ENDVERTEX_ZERR, &b_B_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("B_ENDVERTEX_CHI2", &B_ENDVERTEX_CHI2, &b_B_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("B_ENDVERTEX_NDOF", &B_ENDVERTEX_NDOF, &b_B_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("B_ENDVERTEX_COV_", B_ENDVERTEX_COV_, &b_B_ENDVERTEX_COV_);
   fChain->SetBranchAddress("B_OWNPV_X", &B_OWNPV_X, &b_B_OWNPV_X);
   fChain->SetBranchAddress("B_OWNPV_Y", &B_OWNPV_Y, &b_B_OWNPV_Y);
   fChain->SetBranchAddress("B_OWNPV_Z", &B_OWNPV_Z, &b_B_OWNPV_Z);
   fChain->SetBranchAddress("B_OWNPV_XERR", &B_OWNPV_XERR, &b_B_OWNPV_XERR);
   fChain->SetBranchAddress("B_OWNPV_YERR", &B_OWNPV_YERR, &b_B_OWNPV_YERR);
   fChain->SetBranchAddress("B_OWNPV_ZERR", &B_OWNPV_ZERR, &b_B_OWNPV_ZERR);
   fChain->SetBranchAddress("B_OWNPV_CHI2", &B_OWNPV_CHI2, &b_B_OWNPV_CHI2);
   fChain->SetBranchAddress("B_OWNPV_NDOF", &B_OWNPV_NDOF, &b_B_OWNPV_NDOF);
   fChain->SetBranchAddress("B_OWNPV_COV_", B_OWNPV_COV_, &b_B_OWNPV_COV_);
   fChain->SetBranchAddress("B_IP_OWNPV", &B_IP_OWNPV, &b_B_IP_OWNPV);
   fChain->SetBranchAddress("B_IPCHI2_OWNPV", &B_IPCHI2_OWNPV, &b_B_IPCHI2_OWNPV);
   fChain->SetBranchAddress("B_FD_OWNPV", &B_FD_OWNPV, &b_B_FD_OWNPV);
   fChain->SetBranchAddress("B_FDCHI2_OWNPV", &B_FDCHI2_OWNPV, &b_B_FDCHI2_OWNPV);
   fChain->SetBranchAddress("B_DIRA_OWNPV", &B_DIRA_OWNPV, &b_B_DIRA_OWNPV);
   fChain->SetBranchAddress("B_P", &B_P, &b_B_P);
   fChain->SetBranchAddress("B_PT", &B_PT, &b_B_PT);
   fChain->SetBranchAddress("B_PE", &B_PE, &b_B_PE);
   fChain->SetBranchAddress("B_PX", &B_PX, &b_B_PX);
   fChain->SetBranchAddress("B_PY", &B_PY, &b_B_PY);
   fChain->SetBranchAddress("B_PZ", &B_PZ, &b_B_PZ);
   fChain->SetBranchAddress("B_MM", &B_MM, &b_B_MM);
   fChain->SetBranchAddress("B_MMERR", &B_MMERR, &b_B_MMERR);
   fChain->SetBranchAddress("B_M", &B_M, &b_B_M);
   fChain->SetBranchAddress("B_ID", &B_ID, &b_B_ID);
   fChain->SetBranchAddress("B_TAU", &B_TAU, &b_B_TAU);
   fChain->SetBranchAddress("B_TAUERR", &B_TAUERR, &b_B_TAUERR);
   fChain->SetBranchAddress("B_TAUCHI2", &B_TAUCHI2, &b_B_TAUCHI2);
   fChain->SetBranchAddress("B_L0Global_Dec", &B_L0Global_Dec, &b_B_L0Global_Dec);
   fChain->SetBranchAddress("B_L0Global_TIS", &B_L0Global_TIS, &b_B_L0Global_TIS);
   fChain->SetBranchAddress("B_L0Global_TOS", &B_L0Global_TOS, &b_B_L0Global_TOS);
   fChain->SetBranchAddress("B_Hlt1Global_Dec", &B_Hlt1Global_Dec, &b_B_Hlt1Global_Dec);
   fChain->SetBranchAddress("B_Hlt1Global_TIS", &B_Hlt1Global_TIS, &b_B_Hlt1Global_TIS);
   fChain->SetBranchAddress("B_Hlt1Global_TOS", &B_Hlt1Global_TOS, &b_B_Hlt1Global_TOS);
   fChain->SetBranchAddress("B_Hlt1Phys_Dec", &B_Hlt1Phys_Dec, &b_B_Hlt1Phys_Dec);
   fChain->SetBranchAddress("B_Hlt1Phys_TIS", &B_Hlt1Phys_TIS, &b_B_Hlt1Phys_TIS);
   fChain->SetBranchAddress("B_Hlt1Phys_TOS", &B_Hlt1Phys_TOS, &b_B_Hlt1Phys_TOS);
   fChain->SetBranchAddress("B_Hlt2Global_Dec", &B_Hlt2Global_Dec, &b_B_Hlt2Global_Dec);
   fChain->SetBranchAddress("B_Hlt2Global_TIS", &B_Hlt2Global_TIS, &b_B_Hlt2Global_TIS);
   fChain->SetBranchAddress("B_Hlt2Global_TOS", &B_Hlt2Global_TOS, &b_B_Hlt2Global_TOS);
   fChain->SetBranchAddress("B_Hlt2Phys_Dec", &B_Hlt2Phys_Dec, &b_B_Hlt2Phys_Dec);
   fChain->SetBranchAddress("B_Hlt2Phys_TIS", &B_Hlt2Phys_TIS, &b_B_Hlt2Phys_TIS);
   fChain->SetBranchAddress("B_Hlt2Phys_TOS", &B_Hlt2Phys_TOS, &b_B_Hlt2Phys_TOS);
   fChain->SetBranchAddress("B_L0AnyDecision_Dec", &B_L0AnyDecision_Dec, &b_B_L0AnyDecision_Dec);
   fChain->SetBranchAddress("B_L0AnyDecision_TIS", &B_L0AnyDecision_TIS, &b_B_L0AnyDecision_TIS);
   fChain->SetBranchAddress("B_L0AnyDecision_TOS", &B_L0AnyDecision_TOS, &b_B_L0AnyDecision_TOS);
   fChain->SetBranchAddress("B_L0MuonDecision_Dec", &B_L0MuonDecision_Dec, &b_B_L0MuonDecision_Dec);
   fChain->SetBranchAddress("B_L0MuonDecision_TIS", &B_L0MuonDecision_TIS, &b_B_L0MuonDecision_TIS);
   fChain->SetBranchAddress("B_L0MuonDecision_TOS", &B_L0MuonDecision_TOS, &b_B_L0MuonDecision_TOS);
   fChain->SetBranchAddress("B_L0GlobalDecision_Dec", &B_L0GlobalDecision_Dec, &b_B_L0GlobalDecision_Dec);
   fChain->SetBranchAddress("B_L0GlobalDecision_TIS", &B_L0GlobalDecision_TIS, &b_B_L0GlobalDecision_TIS);
   fChain->SetBranchAddress("B_L0GlobalDecision_TOS", &B_L0GlobalDecision_TOS, &b_B_L0GlobalDecision_TOS);
   fChain->SetBranchAddress("B_Hlt1TrackMVADecision_Dec", &B_Hlt1TrackMVADecision_Dec, &b_B_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("B_Hlt1TrackMVADecision_TIS", &B_Hlt1TrackMVADecision_TIS, &b_B_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("B_Hlt1TrackMVADecision_TOS", &B_Hlt1TrackMVADecision_TOS, &b_B_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("B_Hlt1TwoTrackMVADecision_Dec", &B_Hlt1TwoTrackMVADecision_Dec, &b_B_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("B_Hlt1TwoTrackMVADecision_TIS", &B_Hlt1TwoTrackMVADecision_TIS, &b_B_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("B_Hlt1TwoTrackMVADecision_TOS", &B_Hlt1TwoTrackMVADecision_TOS, &b_B_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("B_Hlt1SingleMuonHighPTDecision_Dec", &B_Hlt1SingleMuonHighPTDecision_Dec, &b_B_Hlt1SingleMuonHighPTDecision_Dec);
   fChain->SetBranchAddress("B_Hlt1SingleMuonHighPTDecision_TIS", &B_Hlt1SingleMuonHighPTDecision_TIS, &b_B_Hlt1SingleMuonHighPTDecision_TIS);
   fChain->SetBranchAddress("B_Hlt1SingleMuonHighPTDecision_TOS", &B_Hlt1SingleMuonHighPTDecision_TOS, &b_B_Hlt1SingleMuonHighPTDecision_TOS);
   fChain->SetBranchAddress("B_Hlt1TrackAllL0Decision_Dec", &B_Hlt1TrackAllL0Decision_Dec, &b_B_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("B_Hlt1TrackAllL0Decision_TIS", &B_Hlt1TrackAllL0Decision_TIS, &b_B_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("B_Hlt1TrackAllL0Decision_TOS", &B_Hlt1TrackAllL0Decision_TOS, &b_B_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("B_Hlt1TrackMuonDecision_Dec", &B_Hlt1TrackMuonDecision_Dec, &b_B_Hlt1TrackMuonDecision_Dec);
   fChain->SetBranchAddress("B_Hlt1TrackMuonDecision_TIS", &B_Hlt1TrackMuonDecision_TIS, &b_B_Hlt1TrackMuonDecision_TIS);
   fChain->SetBranchAddress("B_Hlt1TrackMuonDecision_TOS", &B_Hlt1TrackMuonDecision_TOS, &b_B_Hlt1TrackMuonDecision_TOS);
   fChain->SetBranchAddress("B_Hlt1TrackMuonMVADecision_Dec", &B_Hlt1TrackMuonMVADecision_Dec, &b_B_Hlt1TrackMuonMVADecision_Dec);
   fChain->SetBranchAddress("B_Hlt1TrackMuonMVADecision_TIS", &B_Hlt1TrackMuonMVADecision_TIS, &b_B_Hlt1TrackMuonMVADecision_TIS);
   fChain->SetBranchAddress("B_Hlt1TrackMuonMVADecision_TOS", &B_Hlt1TrackMuonMVADecision_TOS, &b_B_Hlt1TrackMuonMVADecision_TOS);
   fChain->SetBranchAddress("B_Hlt1GlobalDecision_Dec", &B_Hlt1GlobalDecision_Dec, &b_B_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("B_Hlt1GlobalDecision_TIS", &B_Hlt1GlobalDecision_TIS, &b_B_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("B_Hlt1GlobalDecision_TOS", &B_Hlt1GlobalDecision_TOS, &b_B_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("B_Hlt1DiMuonHighMassDecision_Dec", &B_Hlt1DiMuonHighMassDecision_Dec, &b_B_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("B_Hlt1DiMuonHighMassDecision_TIS", &B_Hlt1DiMuonHighMassDecision_TIS, &b_B_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("B_Hlt1DiMuonHighMassDecision_TOS", &B_Hlt1DiMuonHighMassDecision_TOS, &b_B_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("B_Hlt1DiMuonLowMassDecision_Dec", &B_Hlt1DiMuonLowMassDecision_Dec, &b_B_Hlt1DiMuonLowMassDecision_Dec);
   fChain->SetBranchAddress("B_Hlt1DiMuonLowMassDecision_TIS", &B_Hlt1DiMuonLowMassDecision_TIS, &b_B_Hlt1DiMuonLowMassDecision_TIS);
   fChain->SetBranchAddress("B_Hlt1DiMuonLowMassDecision_TOS", &B_Hlt1DiMuonLowMassDecision_TOS, &b_B_Hlt1DiMuonLowMassDecision_TOS);
   fChain->SetBranchAddress("B_Hlt1SingleMuonNoIPDecision_Dec", &B_Hlt1SingleMuonNoIPDecision_Dec, &b_B_Hlt1SingleMuonNoIPDecision_Dec);
   fChain->SetBranchAddress("B_Hlt1SingleMuonNoIPDecision_TIS", &B_Hlt1SingleMuonNoIPDecision_TIS, &b_B_Hlt1SingleMuonNoIPDecision_TIS);
   fChain->SetBranchAddress("B_Hlt1SingleMuonNoIPDecision_TOS", &B_Hlt1SingleMuonNoIPDecision_TOS, &b_B_Hlt1SingleMuonNoIPDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyDecision_Dec", &B_Hlt2Topo2BodyDecision_Dec, &b_B_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyDecision_TIS", &B_Hlt2Topo2BodyDecision_TIS, &b_B_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyDecision_TOS", &B_Hlt2Topo2BodyDecision_TOS, &b_B_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyDecision_Dec", &B_Hlt2Topo3BodyDecision_Dec, &b_B_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyDecision_TIS", &B_Hlt2Topo3BodyDecision_TIS, &b_B_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyDecision_TOS", &B_Hlt2Topo3BodyDecision_TOS, &b_B_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyDecision_Dec", &B_Hlt2Topo4BodyDecision_Dec, &b_B_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyDecision_TIS", &B_Hlt2Topo4BodyDecision_TIS, &b_B_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyDecision_TOS", &B_Hlt2Topo4BodyDecision_TOS, &b_B_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2TopoMu2BodyDecision_Dec", &B_Hlt2TopoMu2BodyDecision_Dec, &b_B_Hlt2TopoMu2BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2TopoMu2BodyDecision_TIS", &B_Hlt2TopoMu2BodyDecision_TIS, &b_B_Hlt2TopoMu2BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2TopoMu2BodyDecision_TOS", &B_Hlt2TopoMu2BodyDecision_TOS, &b_B_Hlt2TopoMu2BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2TopoMu3BodyDecision_Dec", &B_Hlt2TopoMu3BodyDecision_Dec, &b_B_Hlt2TopoMu3BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2TopoMu3BodyDecision_TIS", &B_Hlt2TopoMu3BodyDecision_TIS, &b_B_Hlt2TopoMu3BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2TopoMu3BodyDecision_TOS", &B_Hlt2TopoMu3BodyDecision_TOS, &b_B_Hlt2TopoMu3BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2TopoMu4BodyDecision_Dec", &B_Hlt2TopoMu4BodyDecision_Dec, &b_B_Hlt2TopoMu4BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2TopoMu4BodyDecision_TIS", &B_Hlt2TopoMu4BodyDecision_TIS, &b_B_Hlt2TopoMu4BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2TopoMu4BodyDecision_TOS", &B_Hlt2TopoMu4BodyDecision_TOS, &b_B_Hlt2TopoMu4BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2GlobalDecision_Dec", &B_Hlt2GlobalDecision_Dec, &b_B_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2GlobalDecision_TIS", &B_Hlt2GlobalDecision_TIS, &b_B_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2GlobalDecision_TOS", &B_Hlt2GlobalDecision_TOS, &b_B_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2SingleMuonDecision_Dec", &B_Hlt2SingleMuonDecision_Dec, &b_B_Hlt2SingleMuonDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2SingleMuonDecision_TIS", &B_Hlt2SingleMuonDecision_TIS, &b_B_Hlt2SingleMuonDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2SingleMuonDecision_TOS", &B_Hlt2SingleMuonDecision_TOS, &b_B_Hlt2SingleMuonDecision_TOS);
   fChain->SetBranchAddress("JPsi_CosTheta", &JPsi_CosTheta, &b_JPsi_CosTheta);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_X", &JPsi_ENDVERTEX_X, &b_JPsi_ENDVERTEX_X);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_Y", &JPsi_ENDVERTEX_Y, &b_JPsi_ENDVERTEX_Y);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_Z", &JPsi_ENDVERTEX_Z, &b_JPsi_ENDVERTEX_Z);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_XERR", &JPsi_ENDVERTEX_XERR, &b_JPsi_ENDVERTEX_XERR);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_YERR", &JPsi_ENDVERTEX_YERR, &b_JPsi_ENDVERTEX_YERR);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_ZERR", &JPsi_ENDVERTEX_ZERR, &b_JPsi_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_CHI2", &JPsi_ENDVERTEX_CHI2, &b_JPsi_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_NDOF", &JPsi_ENDVERTEX_NDOF, &b_JPsi_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("JPsi_ENDVERTEX_COV_", JPsi_ENDVERTEX_COV_, &b_JPsi_ENDVERTEX_COV_);
   fChain->SetBranchAddress("JPsi_OWNPV_X", &JPsi_OWNPV_X, &b_JPsi_OWNPV_X);
   fChain->SetBranchAddress("JPsi_OWNPV_Y", &JPsi_OWNPV_Y, &b_JPsi_OWNPV_Y);
   fChain->SetBranchAddress("JPsi_OWNPV_Z", &JPsi_OWNPV_Z, &b_JPsi_OWNPV_Z);
   fChain->SetBranchAddress("JPsi_OWNPV_XERR", &JPsi_OWNPV_XERR, &b_JPsi_OWNPV_XERR);
   fChain->SetBranchAddress("JPsi_OWNPV_YERR", &JPsi_OWNPV_YERR, &b_JPsi_OWNPV_YERR);
   fChain->SetBranchAddress("JPsi_OWNPV_ZERR", &JPsi_OWNPV_ZERR, &b_JPsi_OWNPV_ZERR);
   fChain->SetBranchAddress("JPsi_OWNPV_CHI2", &JPsi_OWNPV_CHI2, &b_JPsi_OWNPV_CHI2);
   fChain->SetBranchAddress("JPsi_OWNPV_NDOF", &JPsi_OWNPV_NDOF, &b_JPsi_OWNPV_NDOF);
   fChain->SetBranchAddress("JPsi_OWNPV_COV_", JPsi_OWNPV_COV_, &b_JPsi_OWNPV_COV_);
   fChain->SetBranchAddress("JPsi_IP_OWNPV", &JPsi_IP_OWNPV, &b_JPsi_IP_OWNPV);
   fChain->SetBranchAddress("JPsi_IPCHI2_OWNPV", &JPsi_IPCHI2_OWNPV, &b_JPsi_IPCHI2_OWNPV);
   fChain->SetBranchAddress("JPsi_FD_OWNPV", &JPsi_FD_OWNPV, &b_JPsi_FD_OWNPV);
   fChain->SetBranchAddress("JPsi_FDCHI2_OWNPV", &JPsi_FDCHI2_OWNPV, &b_JPsi_FDCHI2_OWNPV);
   fChain->SetBranchAddress("JPsi_DIRA_OWNPV", &JPsi_DIRA_OWNPV, &b_JPsi_DIRA_OWNPV);
   fChain->SetBranchAddress("JPsi_ORIVX_X", &JPsi_ORIVX_X, &b_JPsi_ORIVX_X);
   fChain->SetBranchAddress("JPsi_ORIVX_Y", &JPsi_ORIVX_Y, &b_JPsi_ORIVX_Y);
   fChain->SetBranchAddress("JPsi_ORIVX_Z", &JPsi_ORIVX_Z, &b_JPsi_ORIVX_Z);
   fChain->SetBranchAddress("JPsi_ORIVX_XERR", &JPsi_ORIVX_XERR, &b_JPsi_ORIVX_XERR);
   fChain->SetBranchAddress("JPsi_ORIVX_YERR", &JPsi_ORIVX_YERR, &b_JPsi_ORIVX_YERR);
   fChain->SetBranchAddress("JPsi_ORIVX_ZERR", &JPsi_ORIVX_ZERR, &b_JPsi_ORIVX_ZERR);
   fChain->SetBranchAddress("JPsi_ORIVX_CHI2", &JPsi_ORIVX_CHI2, &b_JPsi_ORIVX_CHI2);
   fChain->SetBranchAddress("JPsi_ORIVX_NDOF", &JPsi_ORIVX_NDOF, &b_JPsi_ORIVX_NDOF);
   fChain->SetBranchAddress("JPsi_ORIVX_COV_", JPsi_ORIVX_COV_, &b_JPsi_ORIVX_COV_);
   fChain->SetBranchAddress("JPsi_FD_ORIVX", &JPsi_FD_ORIVX, &b_JPsi_FD_ORIVX);
   fChain->SetBranchAddress("JPsi_FDCHI2_ORIVX", &JPsi_FDCHI2_ORIVX, &b_JPsi_FDCHI2_ORIVX);
   fChain->SetBranchAddress("JPsi_DIRA_ORIVX", &JPsi_DIRA_ORIVX, &b_JPsi_DIRA_ORIVX);
   fChain->SetBranchAddress("JPsi_P", &JPsi_P, &b_JPsi_P);
   fChain->SetBranchAddress("JPsi_PT", &JPsi_PT, &b_JPsi_PT);
   fChain->SetBranchAddress("JPsi_PE", &JPsi_PE, &b_JPsi_PE);
   fChain->SetBranchAddress("JPsi_PX", &JPsi_PX, &b_JPsi_PX);
   fChain->SetBranchAddress("JPsi_PY", &JPsi_PY, &b_JPsi_PY);
   fChain->SetBranchAddress("JPsi_PZ", &JPsi_PZ, &b_JPsi_PZ);
   fChain->SetBranchAddress("JPsi_MM", &JPsi_MM, &b_JPsi_MM);
   fChain->SetBranchAddress("JPsi_MMERR", &JPsi_MMERR, &b_JPsi_MMERR);
   fChain->SetBranchAddress("JPsi_M", &JPsi_M, &b_JPsi_M);
   fChain->SetBranchAddress("JPsi_ID", &JPsi_ID, &b_JPsi_ID);
   fChain->SetBranchAddress("JPsi_TAU", &JPsi_TAU, &b_JPsi_TAU);
   fChain->SetBranchAddress("JPsi_TAUERR", &JPsi_TAUERR, &b_JPsi_TAUERR);
   fChain->SetBranchAddress("JPsi_TAUCHI2", &JPsi_TAUCHI2, &b_JPsi_TAUCHI2);
   fChain->SetBranchAddress("JPsi_L0Global_Dec", &JPsi_L0Global_Dec, &b_JPsi_L0Global_Dec);
   fChain->SetBranchAddress("JPsi_L0Global_TIS", &JPsi_L0Global_TIS, &b_JPsi_L0Global_TIS);
   fChain->SetBranchAddress("JPsi_L0Global_TOS", &JPsi_L0Global_TOS, &b_JPsi_L0Global_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1Global_Dec", &JPsi_Hlt1Global_Dec, &b_JPsi_Hlt1Global_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1Global_TIS", &JPsi_Hlt1Global_TIS, &b_JPsi_Hlt1Global_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1Global_TOS", &JPsi_Hlt1Global_TOS, &b_JPsi_Hlt1Global_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1Phys_Dec", &JPsi_Hlt1Phys_Dec, &b_JPsi_Hlt1Phys_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1Phys_TIS", &JPsi_Hlt1Phys_TIS, &b_JPsi_Hlt1Phys_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1Phys_TOS", &JPsi_Hlt1Phys_TOS, &b_JPsi_Hlt1Phys_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2Global_Dec", &JPsi_Hlt2Global_Dec, &b_JPsi_Hlt2Global_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2Global_TIS", &JPsi_Hlt2Global_TIS, &b_JPsi_Hlt2Global_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2Global_TOS", &JPsi_Hlt2Global_TOS, &b_JPsi_Hlt2Global_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2Phys_Dec", &JPsi_Hlt2Phys_Dec, &b_JPsi_Hlt2Phys_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2Phys_TIS", &JPsi_Hlt2Phys_TIS, &b_JPsi_Hlt2Phys_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2Phys_TOS", &JPsi_Hlt2Phys_TOS, &b_JPsi_Hlt2Phys_TOS);
   fChain->SetBranchAddress("JPsi_L0AnyDecision_Dec", &JPsi_L0AnyDecision_Dec, &b_JPsi_L0AnyDecision_Dec);
   fChain->SetBranchAddress("JPsi_L0AnyDecision_TIS", &JPsi_L0AnyDecision_TIS, &b_JPsi_L0AnyDecision_TIS);
   fChain->SetBranchAddress("JPsi_L0AnyDecision_TOS", &JPsi_L0AnyDecision_TOS, &b_JPsi_L0AnyDecision_TOS);
   fChain->SetBranchAddress("JPsi_L0MuonDecision_Dec", &JPsi_L0MuonDecision_Dec, &b_JPsi_L0MuonDecision_Dec);
   fChain->SetBranchAddress("JPsi_L0MuonDecision_TIS", &JPsi_L0MuonDecision_TIS, &b_JPsi_L0MuonDecision_TIS);
   fChain->SetBranchAddress("JPsi_L0MuonDecision_TOS", &JPsi_L0MuonDecision_TOS, &b_JPsi_L0MuonDecision_TOS);
   fChain->SetBranchAddress("JPsi_L0GlobalDecision_Dec", &JPsi_L0GlobalDecision_Dec, &b_JPsi_L0GlobalDecision_Dec);
   fChain->SetBranchAddress("JPsi_L0GlobalDecision_TIS", &JPsi_L0GlobalDecision_TIS, &b_JPsi_L0GlobalDecision_TIS);
   fChain->SetBranchAddress("JPsi_L0GlobalDecision_TOS", &JPsi_L0GlobalDecision_TOS, &b_JPsi_L0GlobalDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMVADecision_Dec", &JPsi_Hlt1TrackMVADecision_Dec, &b_JPsi_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMVADecision_TIS", &JPsi_Hlt1TrackMVADecision_TIS, &b_JPsi_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMVADecision_TOS", &JPsi_Hlt1TrackMVADecision_TOS, &b_JPsi_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1TwoTrackMVADecision_Dec", &JPsi_Hlt1TwoTrackMVADecision_Dec, &b_JPsi_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1TwoTrackMVADecision_TIS", &JPsi_Hlt1TwoTrackMVADecision_TIS, &b_JPsi_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1TwoTrackMVADecision_TOS", &JPsi_Hlt1TwoTrackMVADecision_TOS, &b_JPsi_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1SingleMuonHighPTDecision_Dec", &JPsi_Hlt1SingleMuonHighPTDecision_Dec, &b_JPsi_Hlt1SingleMuonHighPTDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1SingleMuonHighPTDecision_TIS", &JPsi_Hlt1SingleMuonHighPTDecision_TIS, &b_JPsi_Hlt1SingleMuonHighPTDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1SingleMuonHighPTDecision_TOS", &JPsi_Hlt1SingleMuonHighPTDecision_TOS, &b_JPsi_Hlt1SingleMuonHighPTDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1TrackAllL0Decision_Dec", &JPsi_Hlt1TrackAllL0Decision_Dec, &b_JPsi_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1TrackAllL0Decision_TIS", &JPsi_Hlt1TrackAllL0Decision_TIS, &b_JPsi_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1TrackAllL0Decision_TOS", &JPsi_Hlt1TrackAllL0Decision_TOS, &b_JPsi_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMuonDecision_Dec", &JPsi_Hlt1TrackMuonDecision_Dec, &b_JPsi_Hlt1TrackMuonDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMuonDecision_TIS", &JPsi_Hlt1TrackMuonDecision_TIS, &b_JPsi_Hlt1TrackMuonDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMuonDecision_TOS", &JPsi_Hlt1TrackMuonDecision_TOS, &b_JPsi_Hlt1TrackMuonDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMuonMVADecision_Dec", &JPsi_Hlt1TrackMuonMVADecision_Dec, &b_JPsi_Hlt1TrackMuonMVADecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMuonMVADecision_TIS", &JPsi_Hlt1TrackMuonMVADecision_TIS, &b_JPsi_Hlt1TrackMuonMVADecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1TrackMuonMVADecision_TOS", &JPsi_Hlt1TrackMuonMVADecision_TOS, &b_JPsi_Hlt1TrackMuonMVADecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1GlobalDecision_Dec", &JPsi_Hlt1GlobalDecision_Dec, &b_JPsi_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1GlobalDecision_TIS", &JPsi_Hlt1GlobalDecision_TIS, &b_JPsi_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1GlobalDecision_TOS", &JPsi_Hlt1GlobalDecision_TOS, &b_JPsi_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1DiMuonHighMassDecision_Dec", &JPsi_Hlt1DiMuonHighMassDecision_Dec, &b_JPsi_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1DiMuonHighMassDecision_TIS", &JPsi_Hlt1DiMuonHighMassDecision_TIS, &b_JPsi_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1DiMuonHighMassDecision_TOS", &JPsi_Hlt1DiMuonHighMassDecision_TOS, &b_JPsi_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1DiMuonLowMassDecision_Dec", &JPsi_Hlt1DiMuonLowMassDecision_Dec, &b_JPsi_Hlt1DiMuonLowMassDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1DiMuonLowMassDecision_TIS", &JPsi_Hlt1DiMuonLowMassDecision_TIS, &b_JPsi_Hlt1DiMuonLowMassDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1DiMuonLowMassDecision_TOS", &JPsi_Hlt1DiMuonLowMassDecision_TOS, &b_JPsi_Hlt1DiMuonLowMassDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt1SingleMuonNoIPDecision_Dec", &JPsi_Hlt1SingleMuonNoIPDecision_Dec, &b_JPsi_Hlt1SingleMuonNoIPDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt1SingleMuonNoIPDecision_TIS", &JPsi_Hlt1SingleMuonNoIPDecision_TIS, &b_JPsi_Hlt1SingleMuonNoIPDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt1SingleMuonNoIPDecision_TOS", &JPsi_Hlt1SingleMuonNoIPDecision_TOS, &b_JPsi_Hlt1SingleMuonNoIPDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2Topo2BodyDecision_Dec", &JPsi_Hlt2Topo2BodyDecision_Dec, &b_JPsi_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2Topo2BodyDecision_TIS", &JPsi_Hlt2Topo2BodyDecision_TIS, &b_JPsi_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2Topo2BodyDecision_TOS", &JPsi_Hlt2Topo2BodyDecision_TOS, &b_JPsi_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2Topo3BodyDecision_Dec", &JPsi_Hlt2Topo3BodyDecision_Dec, &b_JPsi_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2Topo3BodyDecision_TIS", &JPsi_Hlt2Topo3BodyDecision_TIS, &b_JPsi_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2Topo3BodyDecision_TOS", &JPsi_Hlt2Topo3BodyDecision_TOS, &b_JPsi_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2Topo4BodyDecision_Dec", &JPsi_Hlt2Topo4BodyDecision_Dec, &b_JPsi_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2Topo4BodyDecision_TIS", &JPsi_Hlt2Topo4BodyDecision_TIS, &b_JPsi_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2Topo4BodyDecision_TOS", &JPsi_Hlt2Topo4BodyDecision_TOS, &b_JPsi_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu2BodyDecision_Dec", &JPsi_Hlt2TopoMu2BodyDecision_Dec, &b_JPsi_Hlt2TopoMu2BodyDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu2BodyDecision_TIS", &JPsi_Hlt2TopoMu2BodyDecision_TIS, &b_JPsi_Hlt2TopoMu2BodyDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu2BodyDecision_TOS", &JPsi_Hlt2TopoMu2BodyDecision_TOS, &b_JPsi_Hlt2TopoMu2BodyDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu3BodyDecision_Dec", &JPsi_Hlt2TopoMu3BodyDecision_Dec, &b_JPsi_Hlt2TopoMu3BodyDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu3BodyDecision_TIS", &JPsi_Hlt2TopoMu3BodyDecision_TIS, &b_JPsi_Hlt2TopoMu3BodyDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu3BodyDecision_TOS", &JPsi_Hlt2TopoMu3BodyDecision_TOS, &b_JPsi_Hlt2TopoMu3BodyDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu4BodyDecision_Dec", &JPsi_Hlt2TopoMu4BodyDecision_Dec, &b_JPsi_Hlt2TopoMu4BodyDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu4BodyDecision_TIS", &JPsi_Hlt2TopoMu4BodyDecision_TIS, &b_JPsi_Hlt2TopoMu4BodyDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2TopoMu4BodyDecision_TOS", &JPsi_Hlt2TopoMu4BodyDecision_TOS, &b_JPsi_Hlt2TopoMu4BodyDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2GlobalDecision_Dec", &JPsi_Hlt2GlobalDecision_Dec, &b_JPsi_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2GlobalDecision_TIS", &JPsi_Hlt2GlobalDecision_TIS, &b_JPsi_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2GlobalDecision_TOS", &JPsi_Hlt2GlobalDecision_TOS, &b_JPsi_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("JPsi_Hlt2SingleMuonDecision_Dec", &JPsi_Hlt2SingleMuonDecision_Dec, &b_JPsi_Hlt2SingleMuonDecision_Dec);
   fChain->SetBranchAddress("JPsi_Hlt2SingleMuonDecision_TIS", &JPsi_Hlt2SingleMuonDecision_TIS, &b_JPsi_Hlt2SingleMuonDecision_TIS);
   fChain->SetBranchAddress("JPsi_Hlt2SingleMuonDecision_TOS", &JPsi_Hlt2SingleMuonDecision_TOS, &b_JPsi_Hlt2SingleMuonDecision_TOS);
   fChain->SetBranchAddress(ntag+"_CosTheta", &mu_tag_CosTheta, &b_mu_tag_CosTheta);
   fChain->SetBranchAddress(ntag+"_OWNPV_X", &mu_tag_OWNPV_X, &b_mu_tag_OWNPV_X);
   fChain->SetBranchAddress(ntag+"_OWNPV_Y", &mu_tag_OWNPV_Y, &b_mu_tag_OWNPV_Y);
   fChain->SetBranchAddress(ntag+"_OWNPV_Z", &mu_tag_OWNPV_Z, &b_mu_tag_OWNPV_Z);
   fChain->SetBranchAddress(ntag+"_OWNPV_XERR", &mu_tag_OWNPV_XERR, &b_mu_tag_OWNPV_XERR);
   fChain->SetBranchAddress(ntag+"_OWNPV_YERR", &mu_tag_OWNPV_YERR, &b_mu_tag_OWNPV_YERR);
   fChain->SetBranchAddress(ntag+"_OWNPV_ZERR", &mu_tag_OWNPV_ZERR, &b_mu_tag_OWNPV_ZERR);
   fChain->SetBranchAddress(ntag+"_OWNPV_CHI2", &mu_tag_OWNPV_CHI2, &b_mu_tag_OWNPV_CHI2);
   fChain->SetBranchAddress(ntag+"_OWNPV_NDOF", &mu_tag_OWNPV_NDOF, &b_mu_tag_OWNPV_NDOF);
   fChain->SetBranchAddress(ntag+"_OWNPV_COV_", mu_tag_OWNPV_COV_, &b_mu_tag_OWNPV_COV_);
   fChain->SetBranchAddress(ntag+"_IP_OWNPV", &mu_tag_IP_OWNPV, &b_mu_tag_IP_OWNPV);
   fChain->SetBranchAddress(ntag+"_IPCHI2_OWNPV", &mu_tag_IPCHI2_OWNPV, &b_mu_tag_IPCHI2_OWNPV);
   fChain->SetBranchAddress(ntag+"_ORIVX_X", &mu_tag_ORIVX_X, &b_mu_tag_ORIVX_X);
   fChain->SetBranchAddress(ntag+"_ORIVX_Y", &mu_tag_ORIVX_Y, &b_mu_tag_ORIVX_Y);
   fChain->SetBranchAddress(ntag+"_ORIVX_Z", &mu_tag_ORIVX_Z, &b_mu_tag_ORIVX_Z);
   fChain->SetBranchAddress(ntag+"_ORIVX_XERR", &mu_tag_ORIVX_XERR, &b_mu_tag_ORIVX_XERR);
   fChain->SetBranchAddress(ntag+"_ORIVX_YERR", &mu_tag_ORIVX_YERR, &b_mu_tag_ORIVX_YERR);
   fChain->SetBranchAddress(ntag+"_ORIVX_ZERR", &mu_tag_ORIVX_ZERR, &b_mu_tag_ORIVX_ZERR);
   fChain->SetBranchAddress(ntag+"_ORIVX_CHI2", &mu_tag_ORIVX_CHI2, &b_mu_tag_ORIVX_CHI2);
   fChain->SetBranchAddress(ntag+"_ORIVX_NDOF", &mu_tag_ORIVX_NDOF, &b_mu_tag_ORIVX_NDOF);
   fChain->SetBranchAddress(ntag+"_ORIVX_COV_", mu_tag_ORIVX_COV_, &b_mu_tag_ORIVX_COV_);
   fChain->SetBranchAddress(ntag+"_P", &mu_tag_P, &b_mu_tag_P);
   fChain->SetBranchAddress(ntag+"_PT", &mu_tag_PT, &b_mu_tag_PT);
   fChain->SetBranchAddress(ntag+"_PE", &mu_tag_PE, &b_mu_tag_PE);
   fChain->SetBranchAddress(ntag+"_PX", &mu_tag_PX, &b_mu_tag_PX);
   fChain->SetBranchAddress(ntag+"_PY", &mu_tag_PY, &b_mu_tag_PY);
   fChain->SetBranchAddress(ntag+"_PZ", &mu_tag_PZ, &b_mu_tag_PZ);
   fChain->SetBranchAddress(ntag+"_M", &mu_tag_M, &b_mu_tag_M);
   fChain->SetBranchAddress(ntag+"_ID", &mu_tag_ID, &b_mu_tag_ID);
   fChain->SetBranchAddress(ntag+"_PIDe", &mu_tag_PIDe, &b_mu_tag_PIDe);
   fChain->SetBranchAddress(ntag+"_PIDmu", &mu_tag_PIDmu, &b_mu_tag_PIDmu);
   fChain->SetBranchAddress(ntag+"_PIDK", &mu_tag_PIDK, &b_mu_tag_PIDK);
   fChain->SetBranchAddress(ntag+"_PIDp", &mu_tag_PIDp, &b_mu_tag_PIDp);
   fChain->SetBranchAddress(ntag+"_ProbNNe", &mu_tag_ProbNNe, &b_mu_tag_ProbNNe);
   fChain->SetBranchAddress(ntag+"_ProbNNk", &mu_tag_ProbNNk, &b_mu_tag_ProbNNk);
   fChain->SetBranchAddress(ntag+"_ProbNNp", &mu_tag_ProbNNp, &b_mu_tag_ProbNNp);
   fChain->SetBranchAddress(ntag+"_ProbNNpi", &mu_tag_ProbNNpi, &b_mu_tag_ProbNNpi);
   fChain->SetBranchAddress(ntag+"_ProbNNmu", &mu_tag_ProbNNmu, &b_mu_tag_ProbNNmu);
   fChain->SetBranchAddress(ntag+"_ProbNNghost", &mu_tag_ProbNNghost, &b_mu_tag_ProbNNghost);
   fChain->SetBranchAddress(ntag+"_hasMuon", &mu_tag_hasMuon, &b_mu_tag_hasMuon);
   fChain->SetBranchAddress(ntag+"_isMuon", &mu_tag_isMuon, &b_mu_tag_isMuon);
   fChain->SetBranchAddress(ntag+"_hasRich", &mu_tag_hasRich, &b_mu_tag_hasRich);
   fChain->SetBranchAddress(ntag+"_hasCalo", &mu_tag_hasCalo, &b_mu_tag_hasCalo);
   fChain->SetBranchAddress(ntag+"_L0Global_Dec", &mu_tag_L0Global_Dec, &b_mu_tag_L0Global_Dec);
   fChain->SetBranchAddress(ntag+"_L0Global_TIS", &mu_tag_L0Global_TIS, &b_mu_tag_L0Global_TIS);
   fChain->SetBranchAddress(ntag+"_L0Global_TOS", &mu_tag_L0Global_TOS, &b_mu_tag_L0Global_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1Global_Dec", &mu_tag_Hlt1Global_Dec, &b_mu_tag_Hlt1Global_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1Global_TIS", &mu_tag_Hlt1Global_TIS, &b_mu_tag_Hlt1Global_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1Global_TOS", &mu_tag_Hlt1Global_TOS, &b_mu_tag_Hlt1Global_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1Phys_Dec", &mu_tag_Hlt1Phys_Dec, &b_mu_tag_Hlt1Phys_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1Phys_TIS", &mu_tag_Hlt1Phys_TIS, &b_mu_tag_Hlt1Phys_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1Phys_TOS", &mu_tag_Hlt1Phys_TOS, &b_mu_tag_Hlt1Phys_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2Global_Dec", &mu_tag_Hlt2Global_Dec, &b_mu_tag_Hlt2Global_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2Global_TIS", &mu_tag_Hlt2Global_TIS, &b_mu_tag_Hlt2Global_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2Global_TOS", &mu_tag_Hlt2Global_TOS, &b_mu_tag_Hlt2Global_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2Phys_Dec", &mu_tag_Hlt2Phys_Dec, &b_mu_tag_Hlt2Phys_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2Phys_TIS", &mu_tag_Hlt2Phys_TIS, &b_mu_tag_Hlt2Phys_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2Phys_TOS", &mu_tag_Hlt2Phys_TOS, &b_mu_tag_Hlt2Phys_TOS);
   fChain->SetBranchAddress(ntag+"_L0AnyDecision_Dec", &mu_tag_L0AnyDecision_Dec, &b_mu_tag_L0AnyDecision_Dec);
   fChain->SetBranchAddress(ntag+"_L0AnyDecision_TIS", &mu_tag_L0AnyDecision_TIS, &b_mu_tag_L0AnyDecision_TIS);
   fChain->SetBranchAddress(ntag+"_L0AnyDecision_TOS", &mu_tag_L0AnyDecision_TOS, &b_mu_tag_L0AnyDecision_TOS);
   fChain->SetBranchAddress(ntag+"_L0MuonDecision_Dec", &mu_tag_L0MuonDecision_Dec, &b_mu_tag_L0MuonDecision_Dec);
   fChain->SetBranchAddress(ntag+"_L0MuonDecision_TIS", &mu_tag_L0MuonDecision_TIS, &b_mu_tag_L0MuonDecision_TIS);
   fChain->SetBranchAddress(ntag+"_L0MuonDecision_TOS", &mu_tag_L0MuonDecision_TOS, &b_mu_tag_L0MuonDecision_TOS);
   fChain->SetBranchAddress(ntag+"_L0GlobalDecision_Dec", &mu_tag_L0GlobalDecision_Dec, &b_mu_tag_L0GlobalDecision_Dec);
   fChain->SetBranchAddress(ntag+"_L0GlobalDecision_TIS", &mu_tag_L0GlobalDecision_TIS, &b_mu_tag_L0GlobalDecision_TIS);
   fChain->SetBranchAddress(ntag+"_L0GlobalDecision_TOS", &mu_tag_L0GlobalDecision_TOS, &b_mu_tag_L0GlobalDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMVADecision_Dec", &mu_tag_Hlt1TrackMVADecision_Dec, &b_mu_tag_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMVADecision_TIS", &mu_tag_Hlt1TrackMVADecision_TIS, &b_mu_tag_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMVADecision_TOS", &mu_tag_Hlt1TrackMVADecision_TOS, &b_mu_tag_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1TwoTrackMVADecision_Dec", &mu_tag_Hlt1TwoTrackMVADecision_Dec, &b_mu_tag_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1TwoTrackMVADecision_TIS", &mu_tag_Hlt1TwoTrackMVADecision_TIS, &b_mu_tag_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1TwoTrackMVADecision_TOS", &mu_tag_Hlt1TwoTrackMVADecision_TOS, &b_mu_tag_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1SingleMuonHighPTDecision_Dec", &mu_tag_Hlt1SingleMuonHighPTDecision_Dec, &b_mu_tag_Hlt1SingleMuonHighPTDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1SingleMuonHighPTDecision_TIS", &mu_tag_Hlt1SingleMuonHighPTDecision_TIS, &b_mu_tag_Hlt1SingleMuonHighPTDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1SingleMuonHighPTDecision_TOS", &mu_tag_Hlt1SingleMuonHighPTDecision_TOS, &b_mu_tag_Hlt1SingleMuonHighPTDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackAllL0Decision_Dec", &mu_tag_Hlt1TrackAllL0Decision_Dec, &b_mu_tag_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackAllL0Decision_TIS", &mu_tag_Hlt1TrackAllL0Decision_TIS, &b_mu_tag_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackAllL0Decision_TOS", &mu_tag_Hlt1TrackAllL0Decision_TOS, &b_mu_tag_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMuonDecision_Dec", &mu_tag_Hlt1TrackMuonDecision_Dec, &b_mu_tag_Hlt1TrackMuonDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMuonDecision_TIS", &mu_tag_Hlt1TrackMuonDecision_TIS, &b_mu_tag_Hlt1TrackMuonDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMuonDecision_TOS", &mu_tag_Hlt1TrackMuonDecision_TOS, &b_mu_tag_Hlt1TrackMuonDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMuonMVADecision_Dec", &mu_tag_Hlt1TrackMuonMVADecision_Dec, &b_mu_tag_Hlt1TrackMuonMVADecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMuonMVADecision_TIS", &mu_tag_Hlt1TrackMuonMVADecision_TIS, &b_mu_tag_Hlt1TrackMuonMVADecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1TrackMuonMVADecision_TOS", &mu_tag_Hlt1TrackMuonMVADecision_TOS, &b_mu_tag_Hlt1TrackMuonMVADecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1GlobalDecision_Dec", &mu_tag_Hlt1GlobalDecision_Dec, &b_mu_tag_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1GlobalDecision_TIS", &mu_tag_Hlt1GlobalDecision_TIS, &b_mu_tag_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1GlobalDecision_TOS", &mu_tag_Hlt1GlobalDecision_TOS, &b_mu_tag_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1DiMuonHighMassDecision_Dec", &mu_tag_Hlt1DiMuonHighMassDecision_Dec, &b_mu_tag_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1DiMuonHighMassDecision_TIS", &mu_tag_Hlt1DiMuonHighMassDecision_TIS, &b_mu_tag_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1DiMuonHighMassDecision_TOS", &mu_tag_Hlt1DiMuonHighMassDecision_TOS, &b_mu_tag_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1DiMuonLowMassDecision_Dec", &mu_tag_Hlt1DiMuonLowMassDecision_Dec, &b_mu_tag_Hlt1DiMuonLowMassDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1DiMuonLowMassDecision_TIS", &mu_tag_Hlt1DiMuonLowMassDecision_TIS, &b_mu_tag_Hlt1DiMuonLowMassDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1DiMuonLowMassDecision_TOS", &mu_tag_Hlt1DiMuonLowMassDecision_TOS, &b_mu_tag_Hlt1DiMuonLowMassDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt1SingleMuonNoIPDecision_Dec", &mu_tag_Hlt1SingleMuonNoIPDecision_Dec, &b_mu_tag_Hlt1SingleMuonNoIPDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt1SingleMuonNoIPDecision_TIS", &mu_tag_Hlt1SingleMuonNoIPDecision_TIS, &b_mu_tag_Hlt1SingleMuonNoIPDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt1SingleMuonNoIPDecision_TOS", &mu_tag_Hlt1SingleMuonNoIPDecision_TOS, &b_mu_tag_Hlt1SingleMuonNoIPDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo2BodyDecision_Dec", &mu_tag_Hlt2Topo2BodyDecision_Dec, &b_mu_tag_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo2BodyDecision_TIS", &mu_tag_Hlt2Topo2BodyDecision_TIS, &b_mu_tag_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo2BodyDecision_TOS", &mu_tag_Hlt2Topo2BodyDecision_TOS, &b_mu_tag_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo3BodyDecision_Dec", &mu_tag_Hlt2Topo3BodyDecision_Dec, &b_mu_tag_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo3BodyDecision_TIS", &mu_tag_Hlt2Topo3BodyDecision_TIS, &b_mu_tag_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo3BodyDecision_TOS", &mu_tag_Hlt2Topo3BodyDecision_TOS, &b_mu_tag_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo4BodyDecision_Dec", &mu_tag_Hlt2Topo4BodyDecision_Dec, &b_mu_tag_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo4BodyDecision_TIS", &mu_tag_Hlt2Topo4BodyDecision_TIS, &b_mu_tag_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2Topo4BodyDecision_TOS", &mu_tag_Hlt2Topo4BodyDecision_TOS, &b_mu_tag_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu2BodyDecision_Dec", &mu_tag_Hlt2TopoMu2BodyDecision_Dec, &b_mu_tag_Hlt2TopoMu2BodyDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu2BodyDecision_TIS", &mu_tag_Hlt2TopoMu2BodyDecision_TIS, &b_mu_tag_Hlt2TopoMu2BodyDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu2BodyDecision_TOS", &mu_tag_Hlt2TopoMu2BodyDecision_TOS, &b_mu_tag_Hlt2TopoMu2BodyDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu3BodyDecision_Dec", &mu_tag_Hlt2TopoMu3BodyDecision_Dec, &b_mu_tag_Hlt2TopoMu3BodyDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu3BodyDecision_TIS", &mu_tag_Hlt2TopoMu3BodyDecision_TIS, &b_mu_tag_Hlt2TopoMu3BodyDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu3BodyDecision_TOS", &mu_tag_Hlt2TopoMu3BodyDecision_TOS, &b_mu_tag_Hlt2TopoMu3BodyDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu4BodyDecision_Dec", &mu_tag_Hlt2TopoMu4BodyDecision_Dec, &b_mu_tag_Hlt2TopoMu4BodyDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu4BodyDecision_TIS", &mu_tag_Hlt2TopoMu4BodyDecision_TIS, &b_mu_tag_Hlt2TopoMu4BodyDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2TopoMu4BodyDecision_TOS", &mu_tag_Hlt2TopoMu4BodyDecision_TOS, &b_mu_tag_Hlt2TopoMu4BodyDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2GlobalDecision_Dec", &mu_tag_Hlt2GlobalDecision_Dec, &b_mu_tag_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2GlobalDecision_TIS", &mu_tag_Hlt2GlobalDecision_TIS, &b_mu_tag_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2GlobalDecision_TOS", &mu_tag_Hlt2GlobalDecision_TOS, &b_mu_tag_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress(ntag+"_Hlt2SingleMuonDecision_Dec", &mu_tag_Hlt2SingleMuonDecision_Dec, &b_mu_tag_Hlt2SingleMuonDecision_Dec);
   fChain->SetBranchAddress(ntag+"_Hlt2SingleMuonDecision_TIS", &mu_tag_Hlt2SingleMuonDecision_TIS, &b_mu_tag_Hlt2SingleMuonDecision_TIS);
   fChain->SetBranchAddress(ntag+"_Hlt2SingleMuonDecision_TOS", &mu_tag_Hlt2SingleMuonDecision_TOS, &b_mu_tag_Hlt2SingleMuonDecision_TOS);
   fChain->SetBranchAddress(ntag+"_BPVIPCHI2", &mu_tag_BPVIPCHI2, &b_mu_tag_BPVIPCHI2);
   fChain->SetBranchAddress(ntag+"_Charge", &mu_tag_Charge, &b_mu_tag_Charge);
   fChain->SetBranchAddress(ntag+"_DLLK", &mu_tag_DLLK, &b_mu_tag_DLLK);
   fChain->SetBranchAddress(ntag+"_DLLe", &mu_tag_DLLe, &b_mu_tag_DLLe);
   fChain->SetBranchAddress(ntag+"_DLLmu", &mu_tag_DLLmu, &b_mu_tag_DLLmu);
   fChain->SetBranchAddress(ntag+"_DLLp", &mu_tag_DLLp, &b_mu_tag_DLLp);
   fChain->SetBranchAddress(ntag+"_DLLpi", &mu_tag_DLLpi, &b_mu_tag_DLLpi);
   fChain->SetBranchAddress(ntag+"_ETA", &mu_tag_ETA, &b_mu_tag_ETA);
   fChain->SetBranchAddress(ntag+"_INACCMUON", &mu_tag_INACCMUON, &b_mu_tag_INACCMUON);
   fChain->SetBranchAddress(ntag+"_PHI", &mu_tag_PHI, &b_mu_tag_PHI);
   fChain->SetBranchAddress(ntag+"_PID", &mu_tag_PID, &b_mu_tag_PID);
   fChain->SetBranchAddress(ntag+"_TRCHI2DOF", &mu_tag_TRCHI2DOF, &b_mu_tag_TRCHI2DOF);
   fChain->SetBranchAddress(ntag+"_nShared", &mu_tag_nShared, &b_mu_tag_nShared);
   fChain->SetBranchAddress(ntag+"_TRACK_Type", &mu_tag_TRACK_Type, &b_mu_tag_TRACK_Type);
   fChain->SetBranchAddress(ntag+"_TRACK_Key", &mu_tag_TRACK_Key, &b_mu_tag_TRACK_Key);
   fChain->SetBranchAddress(ntag+"_TRACK_CHI2NDOF", &mu_tag_TRACK_CHI2NDOF, &b_mu_tag_TRACK_CHI2NDOF);
   fChain->SetBranchAddress(ntag+"_TRACK_PCHI2", &mu_tag_TRACK_PCHI2, &b_mu_tag_TRACK_PCHI2);
   fChain->SetBranchAddress(ntag+"_TRACK_MatchCHI2", &mu_tag_TRACK_MatchCHI2, &b_mu_tag_TRACK_MatchCHI2);
   fChain->SetBranchAddress(ntag+"_TRACK_GhostProb", &mu_tag_TRACK_GhostProb, &b_mu_tag_TRACK_GhostProb);
   fChain->SetBranchAddress(ntag+"_TRACK_CloneDist", &mu_tag_TRACK_CloneDist, &b_mu_tag_TRACK_CloneDist);
   fChain->SetBranchAddress(ntag+"_TRACK_Likelihood", &mu_tag_TRACK_Likelihood, &b_mu_tag_TRACK_Likelihood);
   fChain->SetBranchAddress(nprobe+"_CosTheta", &mu_probe_CosTheta, &b_mu_probe_CosTheta);
   fChain->SetBranchAddress(nprobe+"_OWNPV_X", &mu_probe_OWNPV_X, &b_mu_probe_OWNPV_X);
   fChain->SetBranchAddress(nprobe+"_OWNPV_Y", &mu_probe_OWNPV_Y, &b_mu_probe_OWNPV_Y);
   fChain->SetBranchAddress(nprobe+"_OWNPV_Z", &mu_probe_OWNPV_Z, &b_mu_probe_OWNPV_Z);
   fChain->SetBranchAddress(nprobe+"_OWNPV_XERR", &mu_probe_OWNPV_XERR, &b_mu_probe_OWNPV_XERR);
   fChain->SetBranchAddress(nprobe+"_OWNPV_YERR", &mu_probe_OWNPV_YERR, &b_mu_probe_OWNPV_YERR);
   fChain->SetBranchAddress(nprobe+"_OWNPV_ZERR", &mu_probe_OWNPV_ZERR, &b_mu_probe_OWNPV_ZERR);
   fChain->SetBranchAddress(nprobe+"_OWNPV_CHI2", &mu_probe_OWNPV_CHI2, &b_mu_probe_OWNPV_CHI2);
   fChain->SetBranchAddress(nprobe+"_OWNPV_NDOF", &mu_probe_OWNPV_NDOF, &b_mu_probe_OWNPV_NDOF);
   fChain->SetBranchAddress(nprobe+"_OWNPV_COV_", mu_probe_OWNPV_COV_, &b_mu_probe_OWNPV_COV_);
   fChain->SetBranchAddress(nprobe+"_IP_OWNPV", &mu_probe_IP_OWNPV, &b_mu_probe_IP_OWNPV);
   fChain->SetBranchAddress(nprobe+"_IPCHI2_OWNPV", &mu_probe_IPCHI2_OWNPV, &b_mu_probe_IPCHI2_OWNPV);
   fChain->SetBranchAddress(nprobe+"_ORIVX_X", &mu_probe_ORIVX_X, &b_mu_probe_ORIVX_X);
   fChain->SetBranchAddress(nprobe+"_ORIVX_Y", &mu_probe_ORIVX_Y, &b_mu_probe_ORIVX_Y);
   fChain->SetBranchAddress(nprobe+"_ORIVX_Z", &mu_probe_ORIVX_Z, &b_mu_probe_ORIVX_Z);
   fChain->SetBranchAddress(nprobe+"_ORIVX_XERR", &mu_probe_ORIVX_XERR, &b_mu_probe_ORIVX_XERR);
   fChain->SetBranchAddress(nprobe+"_ORIVX_YERR", &mu_probe_ORIVX_YERR, &b_mu_probe_ORIVX_YERR);
   fChain->SetBranchAddress(nprobe+"_ORIVX_ZERR", &mu_probe_ORIVX_ZERR, &b_mu_probe_ORIVX_ZERR);
   fChain->SetBranchAddress(nprobe+"_ORIVX_CHI2", &mu_probe_ORIVX_CHI2, &b_mu_probe_ORIVX_CHI2);
   fChain->SetBranchAddress(nprobe+"_ORIVX_NDOF", &mu_probe_ORIVX_NDOF, &b_mu_probe_ORIVX_NDOF);
   fChain->SetBranchAddress(nprobe+"_ORIVX_COV_", mu_probe_ORIVX_COV_, &b_mu_probe_ORIVX_COV_);
   fChain->SetBranchAddress(nprobe+"_P", &mu_probe_P, &b_mu_probe_P);
   fChain->SetBranchAddress(nprobe+"_PT", &mu_probe_PT, &b_mu_probe_PT);
   fChain->SetBranchAddress(nprobe+"_PE", &mu_probe_PE, &b_mu_probe_PE);
   fChain->SetBranchAddress(nprobe+"_PX", &mu_probe_PX, &b_mu_probe_PX);
   fChain->SetBranchAddress(nprobe+"_PY", &mu_probe_PY, &b_mu_probe_PY);
   fChain->SetBranchAddress(nprobe+"_PZ", &mu_probe_PZ, &b_mu_probe_PZ);
   fChain->SetBranchAddress(nprobe+"_M", &mu_probe_M, &b_mu_probe_M);
   fChain->SetBranchAddress(nprobe+"_ID", &mu_probe_ID, &b_mu_probe_ID);
   fChain->SetBranchAddress(nprobe+"_PIDe", &mu_probe_PIDe, &b_mu_probe_PIDe);
   fChain->SetBranchAddress(nprobe+"_PIDmu", &mu_probe_PIDmu, &b_mu_probe_PIDmu);
   fChain->SetBranchAddress(nprobe+"_PIDK", &mu_probe_PIDK, &b_mu_probe_PIDK);
   fChain->SetBranchAddress(nprobe+"_PIDp", &mu_probe_PIDp, &b_mu_probe_PIDp);
   fChain->SetBranchAddress(nprobe+"_ProbNNe", &mu_probe_ProbNNe, &b_mu_probe_ProbNNe);
   fChain->SetBranchAddress(nprobe+"_ProbNNk", &mu_probe_ProbNNk, &b_mu_probe_ProbNNk);
   fChain->SetBranchAddress(nprobe+"_ProbNNp", &mu_probe_ProbNNp, &b_mu_probe_ProbNNp);
   fChain->SetBranchAddress(nprobe+"_ProbNNpi", &mu_probe_ProbNNpi, &b_mu_probe_ProbNNpi);
   fChain->SetBranchAddress(nprobe+"_ProbNNmu", &mu_probe_ProbNNmu, &b_mu_probe_ProbNNmu);
   fChain->SetBranchAddress(nprobe+"_ProbNNghost", &mu_probe_ProbNNghost, &b_mu_probe_ProbNNghost);
   fChain->SetBranchAddress(nprobe+"_hasMuon", &mu_probe_hasMuon, &b_mu_probe_hasMuon);
   fChain->SetBranchAddress(nprobe+"_isMuon", &mu_probe_isMuon, &b_mu_probe_isMuon);
   fChain->SetBranchAddress(nprobe+"_hasRich", &mu_probe_hasRich, &b_mu_probe_hasRich);
   fChain->SetBranchAddress(nprobe+"_hasCalo", &mu_probe_hasCalo, &b_mu_probe_hasCalo);
   fChain->SetBranchAddress(nprobe+"_L0Global_Dec", &mu_probe_L0Global_Dec, &b_mu_probe_L0Global_Dec);
   fChain->SetBranchAddress(nprobe+"_L0Global_TIS", &mu_probe_L0Global_TIS, &b_mu_probe_L0Global_TIS);
   fChain->SetBranchAddress(nprobe+"_L0Global_TOS", &mu_probe_L0Global_TOS, &b_mu_probe_L0Global_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1Global_Dec", &mu_probe_Hlt1Global_Dec, &b_mu_probe_Hlt1Global_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1Global_TIS", &mu_probe_Hlt1Global_TIS, &b_mu_probe_Hlt1Global_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1Global_TOS", &mu_probe_Hlt1Global_TOS, &b_mu_probe_Hlt1Global_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1Phys_Dec", &mu_probe_Hlt1Phys_Dec, &b_mu_probe_Hlt1Phys_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1Phys_TIS", &mu_probe_Hlt1Phys_TIS, &b_mu_probe_Hlt1Phys_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1Phys_TOS", &mu_probe_Hlt1Phys_TOS, &b_mu_probe_Hlt1Phys_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Global_Dec", &mu_probe_Hlt2Global_Dec, &b_mu_probe_Hlt2Global_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2Global_TIS", &mu_probe_Hlt2Global_TIS, &b_mu_probe_Hlt2Global_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Global_TOS", &mu_probe_Hlt2Global_TOS, &b_mu_probe_Hlt2Global_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Phys_Dec", &mu_probe_Hlt2Phys_Dec, &b_mu_probe_Hlt2Phys_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2Phys_TIS", &mu_probe_Hlt2Phys_TIS, &b_mu_probe_Hlt2Phys_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Phys_TOS", &mu_probe_Hlt2Phys_TOS, &b_mu_probe_Hlt2Phys_TOS);
   fChain->SetBranchAddress(nprobe+"_L0AnyDecision_Dec", &mu_probe_L0AnyDecision_Dec, &b_mu_probe_L0AnyDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_L0AnyDecision_TIS", &mu_probe_L0AnyDecision_TIS, &b_mu_probe_L0AnyDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_L0AnyDecision_TOS", &mu_probe_L0AnyDecision_TOS, &b_mu_probe_L0AnyDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_L0MuonDecision_Dec", &mu_probe_L0MuonDecision_Dec, &b_mu_probe_L0MuonDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_L0MuonDecision_TIS", &mu_probe_L0MuonDecision_TIS, &b_mu_probe_L0MuonDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_L0MuonDecision_TOS", &mu_probe_L0MuonDecision_TOS, &b_mu_probe_L0MuonDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_L0GlobalDecision_Dec", &mu_probe_L0GlobalDecision_Dec, &b_mu_probe_L0GlobalDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_L0GlobalDecision_TIS", &mu_probe_L0GlobalDecision_TIS, &b_mu_probe_L0GlobalDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_L0GlobalDecision_TOS", &mu_probe_L0GlobalDecision_TOS, &b_mu_probe_L0GlobalDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMVADecision_Dec", &mu_probe_Hlt1TrackMVADecision_Dec, &b_mu_probe_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMVADecision_TIS", &mu_probe_Hlt1TrackMVADecision_TIS, &b_mu_probe_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMVADecision_TOS", &mu_probe_Hlt1TrackMVADecision_TOS, &b_mu_probe_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TwoTrackMVADecision_Dec", &mu_probe_Hlt1TwoTrackMVADecision_Dec, &b_mu_probe_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1TwoTrackMVADecision_TIS", &mu_probe_Hlt1TwoTrackMVADecision_TIS, &b_mu_probe_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TwoTrackMVADecision_TOS", &mu_probe_Hlt1TwoTrackMVADecision_TOS, &b_mu_probe_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1SingleMuonHighPTDecision_Dec", &mu_probe_Hlt1SingleMuonHighPTDecision_Dec, &b_mu_probe_Hlt1SingleMuonHighPTDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1SingleMuonHighPTDecision_TIS", &mu_probe_Hlt1SingleMuonHighPTDecision_TIS, &b_mu_probe_Hlt1SingleMuonHighPTDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1SingleMuonHighPTDecision_TOS", &mu_probe_Hlt1SingleMuonHighPTDecision_TOS, &b_mu_probe_Hlt1SingleMuonHighPTDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackAllL0Decision_Dec", &mu_probe_Hlt1TrackAllL0Decision_Dec, &b_mu_probe_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackAllL0Decision_TIS", &mu_probe_Hlt1TrackAllL0Decision_TIS, &b_mu_probe_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackAllL0Decision_TOS", &mu_probe_Hlt1TrackAllL0Decision_TOS, &b_mu_probe_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMuonDecision_Dec", &mu_probe_Hlt1TrackMuonDecision_Dec, &b_mu_probe_Hlt1TrackMuonDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMuonDecision_TIS", &mu_probe_Hlt1TrackMuonDecision_TIS, &b_mu_probe_Hlt1TrackMuonDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMuonDecision_TOS", &mu_probe_Hlt1TrackMuonDecision_TOS, &b_mu_probe_Hlt1TrackMuonDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMuonMVADecision_Dec", &mu_probe_Hlt1TrackMuonMVADecision_Dec, &b_mu_probe_Hlt1TrackMuonMVADecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMuonMVADecision_TIS", &mu_probe_Hlt1TrackMuonMVADecision_TIS, &b_mu_probe_Hlt1TrackMuonMVADecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1TrackMuonMVADecision_TOS", &mu_probe_Hlt1TrackMuonMVADecision_TOS, &b_mu_probe_Hlt1TrackMuonMVADecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1GlobalDecision_Dec", &mu_probe_Hlt1GlobalDecision_Dec, &b_mu_probe_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1GlobalDecision_TIS", &mu_probe_Hlt1GlobalDecision_TIS, &b_mu_probe_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1GlobalDecision_TOS", &mu_probe_Hlt1GlobalDecision_TOS, &b_mu_probe_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1DiMuonHighMassDecision_Dec", &mu_probe_Hlt1DiMuonHighMassDecision_Dec, &b_mu_probe_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1DiMuonHighMassDecision_TIS", &mu_probe_Hlt1DiMuonHighMassDecision_TIS, &b_mu_probe_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1DiMuonHighMassDecision_TOS", &mu_probe_Hlt1DiMuonHighMassDecision_TOS, &b_mu_probe_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1DiMuonLowMassDecision_Dec", &mu_probe_Hlt1DiMuonLowMassDecision_Dec, &b_mu_probe_Hlt1DiMuonLowMassDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1DiMuonLowMassDecision_TIS", &mu_probe_Hlt1DiMuonLowMassDecision_TIS, &b_mu_probe_Hlt1DiMuonLowMassDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1DiMuonLowMassDecision_TOS", &mu_probe_Hlt1DiMuonLowMassDecision_TOS, &b_mu_probe_Hlt1DiMuonLowMassDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt1SingleMuonNoIPDecision_Dec", &mu_probe_Hlt1SingleMuonNoIPDecision_Dec, &b_mu_probe_Hlt1SingleMuonNoIPDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt1SingleMuonNoIPDecision_TIS", &mu_probe_Hlt1SingleMuonNoIPDecision_TIS, &b_mu_probe_Hlt1SingleMuonNoIPDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt1SingleMuonNoIPDecision_TOS", &mu_probe_Hlt1SingleMuonNoIPDecision_TOS, &b_mu_probe_Hlt1SingleMuonNoIPDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo2BodyDecision_Dec", &mu_probe_Hlt2Topo2BodyDecision_Dec, &b_mu_probe_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo2BodyDecision_TIS", &mu_probe_Hlt2Topo2BodyDecision_TIS, &b_mu_probe_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo2BodyDecision_TOS", &mu_probe_Hlt2Topo2BodyDecision_TOS, &b_mu_probe_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo3BodyDecision_Dec", &mu_probe_Hlt2Topo3BodyDecision_Dec, &b_mu_probe_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo3BodyDecision_TIS", &mu_probe_Hlt2Topo3BodyDecision_TIS, &b_mu_probe_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo3BodyDecision_TOS", &mu_probe_Hlt2Topo3BodyDecision_TOS, &b_mu_probe_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo4BodyDecision_Dec", &mu_probe_Hlt2Topo4BodyDecision_Dec, &b_mu_probe_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo4BodyDecision_TIS", &mu_probe_Hlt2Topo4BodyDecision_TIS, &b_mu_probe_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2Topo4BodyDecision_TOS", &mu_probe_Hlt2Topo4BodyDecision_TOS, &b_mu_probe_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu2BodyDecision_Dec", &mu_probe_Hlt2TopoMu2BodyDecision_Dec, &b_mu_probe_Hlt2TopoMu2BodyDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu2BodyDecision_TIS", &mu_probe_Hlt2TopoMu2BodyDecision_TIS, &b_mu_probe_Hlt2TopoMu2BodyDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu2BodyDecision_TOS", &mu_probe_Hlt2TopoMu2BodyDecision_TOS, &b_mu_probe_Hlt2TopoMu2BodyDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu3BodyDecision_Dec", &mu_probe_Hlt2TopoMu3BodyDecision_Dec, &b_mu_probe_Hlt2TopoMu3BodyDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu3BodyDecision_TIS", &mu_probe_Hlt2TopoMu3BodyDecision_TIS, &b_mu_probe_Hlt2TopoMu3BodyDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu3BodyDecision_TOS", &mu_probe_Hlt2TopoMu3BodyDecision_TOS, &b_mu_probe_Hlt2TopoMu3BodyDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu4BodyDecision_Dec", &mu_probe_Hlt2TopoMu4BodyDecision_Dec, &b_mu_probe_Hlt2TopoMu4BodyDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu4BodyDecision_TIS", &mu_probe_Hlt2TopoMu4BodyDecision_TIS, &b_mu_probe_Hlt2TopoMu4BodyDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2TopoMu4BodyDecision_TOS", &mu_probe_Hlt2TopoMu4BodyDecision_TOS, &b_mu_probe_Hlt2TopoMu4BodyDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2GlobalDecision_Dec", &mu_probe_Hlt2GlobalDecision_Dec, &b_mu_probe_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2GlobalDecision_TIS", &mu_probe_Hlt2GlobalDecision_TIS, &b_mu_probe_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2GlobalDecision_TOS", &mu_probe_Hlt2GlobalDecision_TOS, &b_mu_probe_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_Hlt2SingleMuonDecision_Dec", &mu_probe_Hlt2SingleMuonDecision_Dec, &b_mu_probe_Hlt2SingleMuonDecision_Dec);
   fChain->SetBranchAddress(nprobe+"_Hlt2SingleMuonDecision_TIS", &mu_probe_Hlt2SingleMuonDecision_TIS, &b_mu_probe_Hlt2SingleMuonDecision_TIS);
   fChain->SetBranchAddress(nprobe+"_Hlt2SingleMuonDecision_TOS", &mu_probe_Hlt2SingleMuonDecision_TOS, &b_mu_probe_Hlt2SingleMuonDecision_TOS);
   fChain->SetBranchAddress(nprobe+"_BPVIPCHI2", &mu_probe_BPVIPCHI2, &b_mu_probe_BPVIPCHI2);
   fChain->SetBranchAddress(nprobe+"_Charge", &mu_probe_Charge, &b_mu_probe_Charge);
   fChain->SetBranchAddress(nprobe+"_DLLK", &mu_probe_DLLK, &b_mu_probe_DLLK);
   fChain->SetBranchAddress(nprobe+"_DLLe", &mu_probe_DLLe, &b_mu_probe_DLLe);
   fChain->SetBranchAddress(nprobe+"_DLLmu", &mu_probe_DLLmu, &b_mu_probe_DLLmu);
   fChain->SetBranchAddress(nprobe+"_DLLp", &mu_probe_DLLp, &b_mu_probe_DLLp);
   fChain->SetBranchAddress(nprobe+"_DLLpi", &mu_probe_DLLpi, &b_mu_probe_DLLpi);
   fChain->SetBranchAddress(nprobe+"_ETA", &mu_probe_ETA, &b_mu_probe_ETA);
   fChain->SetBranchAddress(nprobe+"_INACCMUON", &mu_probe_INACCMUON, &b_mu_probe_INACCMUON);
   fChain->SetBranchAddress(nprobe+"_PHI", &mu_probe_PHI, &b_mu_probe_PHI);
   fChain->SetBranchAddress(nprobe+"_PID", &mu_probe_PID, &b_mu_probe_PID);
   fChain->SetBranchAddress(nprobe+"_TRCHI2DOF", &mu_probe_TRCHI2DOF, &b_mu_probe_TRCHI2DOF);
   fChain->SetBranchAddress(nprobe+"_nShared", &mu_probe_nShared, &b_mu_probe_nShared);
   fChain->SetBranchAddress(nprobe+"_TRACK_Type", &mu_probe_TRACK_Type, &b_mu_probe_TRACK_Type);
   fChain->SetBranchAddress(nprobe+"_TRACK_Key", &mu_probe_TRACK_Key, &b_mu_probe_TRACK_Key);
   fChain->SetBranchAddress(nprobe+"_TRACK_CHI2NDOF", &mu_probe_TRACK_CHI2NDOF, &b_mu_probe_TRACK_CHI2NDOF);
   fChain->SetBranchAddress(nprobe+"_TRACK_PCHI2", &mu_probe_TRACK_PCHI2, &b_mu_probe_TRACK_PCHI2);
   fChain->SetBranchAddress(nprobe+"_TRACK_MatchCHI2", &mu_probe_TRACK_MatchCHI2, &b_mu_probe_TRACK_MatchCHI2);
   fChain->SetBranchAddress(nprobe+"_TRACK_GhostProb", &mu_probe_TRACK_GhostProb, &b_mu_probe_TRACK_GhostProb);
   fChain->SetBranchAddress(nprobe+"_TRACK_CloneDist", &mu_probe_TRACK_CloneDist, &b_mu_probe_TRACK_CloneDist);
   fChain->SetBranchAddress(nprobe+"_TRACK_Likelihood", &mu_probe_TRACK_Likelihood, &b_mu_probe_TRACK_Likelihood);
   fChain->SetBranchAddress("K_CosTheta", &K_CosTheta, &b_K_CosTheta);
   fChain->SetBranchAddress("K_OWNPV_X", &K_OWNPV_X, &b_K_OWNPV_X);
   fChain->SetBranchAddress("K_OWNPV_Y", &K_OWNPV_Y, &b_K_OWNPV_Y);
   fChain->SetBranchAddress("K_OWNPV_Z", &K_OWNPV_Z, &b_K_OWNPV_Z);
   fChain->SetBranchAddress("K_OWNPV_XERR", &K_OWNPV_XERR, &b_K_OWNPV_XERR);
   fChain->SetBranchAddress("K_OWNPV_YERR", &K_OWNPV_YERR, &b_K_OWNPV_YERR);
   fChain->SetBranchAddress("K_OWNPV_ZERR", &K_OWNPV_ZERR, &b_K_OWNPV_ZERR);
   fChain->SetBranchAddress("K_OWNPV_CHI2", &K_OWNPV_CHI2, &b_K_OWNPV_CHI2);
   fChain->SetBranchAddress("K_OWNPV_NDOF", &K_OWNPV_NDOF, &b_K_OWNPV_NDOF);
   fChain->SetBranchAddress("K_OWNPV_COV_", K_OWNPV_COV_, &b_K_OWNPV_COV_);
   fChain->SetBranchAddress("K_IP_OWNPV", &K_IP_OWNPV, &b_K_IP_OWNPV);
   fChain->SetBranchAddress("K_IPCHI2_OWNPV", &K_IPCHI2_OWNPV, &b_K_IPCHI2_OWNPV);
   fChain->SetBranchAddress("K_ORIVX_X", &K_ORIVX_X, &b_K_ORIVX_X);
   fChain->SetBranchAddress("K_ORIVX_Y", &K_ORIVX_Y, &b_K_ORIVX_Y);
   fChain->SetBranchAddress("K_ORIVX_Z", &K_ORIVX_Z, &b_K_ORIVX_Z);
   fChain->SetBranchAddress("K_ORIVX_XERR", &K_ORIVX_XERR, &b_K_ORIVX_XERR);
   fChain->SetBranchAddress("K_ORIVX_YERR", &K_ORIVX_YERR, &b_K_ORIVX_YERR);
   fChain->SetBranchAddress("K_ORIVX_ZERR", &K_ORIVX_ZERR, &b_K_ORIVX_ZERR);
   fChain->SetBranchAddress("K_ORIVX_CHI2", &K_ORIVX_CHI2, &b_K_ORIVX_CHI2);
   fChain->SetBranchAddress("K_ORIVX_NDOF", &K_ORIVX_NDOF, &b_K_ORIVX_NDOF);
   fChain->SetBranchAddress("K_ORIVX_COV_", K_ORIVX_COV_, &b_K_ORIVX_COV_);
   fChain->SetBranchAddress("K_P", &K_P, &b_K_P);
   fChain->SetBranchAddress("K_PT", &K_PT, &b_K_PT);
   fChain->SetBranchAddress("K_PE", &K_PE, &b_K_PE);
   fChain->SetBranchAddress("K_PX", &K_PX, &b_K_PX);
   fChain->SetBranchAddress("K_PY", &K_PY, &b_K_PY);
   fChain->SetBranchAddress("K_PZ", &K_PZ, &b_K_PZ);
   fChain->SetBranchAddress("K_M", &K_M, &b_K_M);
   fChain->SetBranchAddress("K_ID", &K_ID, &b_K_ID);
   fChain->SetBranchAddress("K_PIDe", &K_PIDe, &b_K_PIDe);
   fChain->SetBranchAddress("K_PIDmu", &K_PIDmu, &b_K_PIDmu);
   fChain->SetBranchAddress("K_PIDK", &K_PIDK, &b_K_PIDK);
   fChain->SetBranchAddress("K_PIDp", &K_PIDp, &b_K_PIDp);
   fChain->SetBranchAddress("K_ProbNNe", &K_ProbNNe, &b_K_ProbNNe);
   fChain->SetBranchAddress("K_ProbNNk", &K_ProbNNk, &b_K_ProbNNk);
   fChain->SetBranchAddress("K_ProbNNp", &K_ProbNNp, &b_K_ProbNNp);
   fChain->SetBranchAddress("K_ProbNNpi", &K_ProbNNpi, &b_K_ProbNNpi);
   fChain->SetBranchAddress("K_ProbNNmu", &K_ProbNNmu, &b_K_ProbNNmu);
   fChain->SetBranchAddress("K_ProbNNghost", &K_ProbNNghost, &b_K_ProbNNghost);
   fChain->SetBranchAddress("K_hasMuon", &K_hasMuon, &b_K_hasMuon);
   fChain->SetBranchAddress("K_isMuon", &K_isMuon, &b_K_isMuon);
   fChain->SetBranchAddress("K_hasRich", &K_hasRich, &b_K_hasRich);
   fChain->SetBranchAddress("K_hasCalo", &K_hasCalo, &b_K_hasCalo);
   fChain->SetBranchAddress("K_L0Global_Dec", &K_L0Global_Dec, &b_K_L0Global_Dec);
   fChain->SetBranchAddress("K_L0Global_TIS", &K_L0Global_TIS, &b_K_L0Global_TIS);
   fChain->SetBranchAddress("K_L0Global_TOS", &K_L0Global_TOS, &b_K_L0Global_TOS);
   fChain->SetBranchAddress("K_Hlt1Global_Dec", &K_Hlt1Global_Dec, &b_K_Hlt1Global_Dec);
   fChain->SetBranchAddress("K_Hlt1Global_TIS", &K_Hlt1Global_TIS, &b_K_Hlt1Global_TIS);
   fChain->SetBranchAddress("K_Hlt1Global_TOS", &K_Hlt1Global_TOS, &b_K_Hlt1Global_TOS);
   fChain->SetBranchAddress("K_Hlt1Phys_Dec", &K_Hlt1Phys_Dec, &b_K_Hlt1Phys_Dec);
   fChain->SetBranchAddress("K_Hlt1Phys_TIS", &K_Hlt1Phys_TIS, &b_K_Hlt1Phys_TIS);
   fChain->SetBranchAddress("K_Hlt1Phys_TOS", &K_Hlt1Phys_TOS, &b_K_Hlt1Phys_TOS);
   fChain->SetBranchAddress("K_Hlt2Global_Dec", &K_Hlt2Global_Dec, &b_K_Hlt2Global_Dec);
   fChain->SetBranchAddress("K_Hlt2Global_TIS", &K_Hlt2Global_TIS, &b_K_Hlt2Global_TIS);
   fChain->SetBranchAddress("K_Hlt2Global_TOS", &K_Hlt2Global_TOS, &b_K_Hlt2Global_TOS);
   fChain->SetBranchAddress("K_Hlt2Phys_Dec", &K_Hlt2Phys_Dec, &b_K_Hlt2Phys_Dec);
   fChain->SetBranchAddress("K_Hlt2Phys_TIS", &K_Hlt2Phys_TIS, &b_K_Hlt2Phys_TIS);
   fChain->SetBranchAddress("K_Hlt2Phys_TOS", &K_Hlt2Phys_TOS, &b_K_Hlt2Phys_TOS);
   fChain->SetBranchAddress("K_L0AnyDecision_Dec", &K_L0AnyDecision_Dec, &b_K_L0AnyDecision_Dec);
   fChain->SetBranchAddress("K_L0AnyDecision_TIS", &K_L0AnyDecision_TIS, &b_K_L0AnyDecision_TIS);
   fChain->SetBranchAddress("K_L0AnyDecision_TOS", &K_L0AnyDecision_TOS, &b_K_L0AnyDecision_TOS);
   fChain->SetBranchAddress("K_L0MuonDecision_Dec", &K_L0MuonDecision_Dec, &b_K_L0MuonDecision_Dec);
   fChain->SetBranchAddress("K_L0MuonDecision_TIS", &K_L0MuonDecision_TIS, &b_K_L0MuonDecision_TIS);
   fChain->SetBranchAddress("K_L0MuonDecision_TOS", &K_L0MuonDecision_TOS, &b_K_L0MuonDecision_TOS);
   fChain->SetBranchAddress("K_L0GlobalDecision_Dec", &K_L0GlobalDecision_Dec, &b_K_L0GlobalDecision_Dec);
   fChain->SetBranchAddress("K_L0GlobalDecision_TIS", &K_L0GlobalDecision_TIS, &b_K_L0GlobalDecision_TIS);
   fChain->SetBranchAddress("K_L0GlobalDecision_TOS", &K_L0GlobalDecision_TOS, &b_K_L0GlobalDecision_TOS);
   fChain->SetBranchAddress("K_Hlt1TrackMVADecision_Dec", &K_Hlt1TrackMVADecision_Dec, &b_K_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("K_Hlt1TrackMVADecision_TIS", &K_Hlt1TrackMVADecision_TIS, &b_K_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("K_Hlt1TrackMVADecision_TOS", &K_Hlt1TrackMVADecision_TOS, &b_K_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("K_Hlt1TwoTrackMVADecision_Dec", &K_Hlt1TwoTrackMVADecision_Dec, &b_K_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("K_Hlt1TwoTrackMVADecision_TIS", &K_Hlt1TwoTrackMVADecision_TIS, &b_K_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("K_Hlt1TwoTrackMVADecision_TOS", &K_Hlt1TwoTrackMVADecision_TOS, &b_K_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("K_Hlt1SingleMuonHighPTDecision_Dec", &K_Hlt1SingleMuonHighPTDecision_Dec, &b_K_Hlt1SingleMuonHighPTDecision_Dec);
   fChain->SetBranchAddress("K_Hlt1SingleMuonHighPTDecision_TIS", &K_Hlt1SingleMuonHighPTDecision_TIS, &b_K_Hlt1SingleMuonHighPTDecision_TIS);
   fChain->SetBranchAddress("K_Hlt1SingleMuonHighPTDecision_TOS", &K_Hlt1SingleMuonHighPTDecision_TOS, &b_K_Hlt1SingleMuonHighPTDecision_TOS);
   fChain->SetBranchAddress("K_Hlt1TrackAllL0Decision_Dec", &K_Hlt1TrackAllL0Decision_Dec, &b_K_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("K_Hlt1TrackAllL0Decision_TIS", &K_Hlt1TrackAllL0Decision_TIS, &b_K_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("K_Hlt1TrackAllL0Decision_TOS", &K_Hlt1TrackAllL0Decision_TOS, &b_K_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("K_Hlt1TrackMuonDecision_Dec", &K_Hlt1TrackMuonDecision_Dec, &b_K_Hlt1TrackMuonDecision_Dec);
   fChain->SetBranchAddress("K_Hlt1TrackMuonDecision_TIS", &K_Hlt1TrackMuonDecision_TIS, &b_K_Hlt1TrackMuonDecision_TIS);
   fChain->SetBranchAddress("K_Hlt1TrackMuonDecision_TOS", &K_Hlt1TrackMuonDecision_TOS, &b_K_Hlt1TrackMuonDecision_TOS);
   fChain->SetBranchAddress("K_Hlt1TrackMuonMVADecision_Dec", &K_Hlt1TrackMuonMVADecision_Dec, &b_K_Hlt1TrackMuonMVADecision_Dec);
   fChain->SetBranchAddress("K_Hlt1TrackMuonMVADecision_TIS", &K_Hlt1TrackMuonMVADecision_TIS, &b_K_Hlt1TrackMuonMVADecision_TIS);
   fChain->SetBranchAddress("K_Hlt1TrackMuonMVADecision_TOS", &K_Hlt1TrackMuonMVADecision_TOS, &b_K_Hlt1TrackMuonMVADecision_TOS);
   fChain->SetBranchAddress("K_Hlt1GlobalDecision_Dec", &K_Hlt1GlobalDecision_Dec, &b_K_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("K_Hlt1GlobalDecision_TIS", &K_Hlt1GlobalDecision_TIS, &b_K_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("K_Hlt1GlobalDecision_TOS", &K_Hlt1GlobalDecision_TOS, &b_K_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("K_Hlt1DiMuonHighMassDecision_Dec", &K_Hlt1DiMuonHighMassDecision_Dec, &b_K_Hlt1DiMuonHighMassDecision_Dec);
   fChain->SetBranchAddress("K_Hlt1DiMuonHighMassDecision_TIS", &K_Hlt1DiMuonHighMassDecision_TIS, &b_K_Hlt1DiMuonHighMassDecision_TIS);
   fChain->SetBranchAddress("K_Hlt1DiMuonHighMassDecision_TOS", &K_Hlt1DiMuonHighMassDecision_TOS, &b_K_Hlt1DiMuonHighMassDecision_TOS);
   fChain->SetBranchAddress("K_Hlt1DiMuonLowMassDecision_Dec", &K_Hlt1DiMuonLowMassDecision_Dec, &b_K_Hlt1DiMuonLowMassDecision_Dec);
   fChain->SetBranchAddress("K_Hlt1DiMuonLowMassDecision_TIS", &K_Hlt1DiMuonLowMassDecision_TIS, &b_K_Hlt1DiMuonLowMassDecision_TIS);
   fChain->SetBranchAddress("K_Hlt1DiMuonLowMassDecision_TOS", &K_Hlt1DiMuonLowMassDecision_TOS, &b_K_Hlt1DiMuonLowMassDecision_TOS);
   fChain->SetBranchAddress("K_Hlt1SingleMuonNoIPDecision_Dec", &K_Hlt1SingleMuonNoIPDecision_Dec, &b_K_Hlt1SingleMuonNoIPDecision_Dec);
   fChain->SetBranchAddress("K_Hlt1SingleMuonNoIPDecision_TIS", &K_Hlt1SingleMuonNoIPDecision_TIS, &b_K_Hlt1SingleMuonNoIPDecision_TIS);
   fChain->SetBranchAddress("K_Hlt1SingleMuonNoIPDecision_TOS", &K_Hlt1SingleMuonNoIPDecision_TOS, &b_K_Hlt1SingleMuonNoIPDecision_TOS);
   fChain->SetBranchAddress("K_Hlt2Topo2BodyDecision_Dec", &K_Hlt2Topo2BodyDecision_Dec, &b_K_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("K_Hlt2Topo2BodyDecision_TIS", &K_Hlt2Topo2BodyDecision_TIS, &b_K_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("K_Hlt2Topo2BodyDecision_TOS", &K_Hlt2Topo2BodyDecision_TOS, &b_K_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("K_Hlt2Topo3BodyDecision_Dec", &K_Hlt2Topo3BodyDecision_Dec, &b_K_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("K_Hlt2Topo3BodyDecision_TIS", &K_Hlt2Topo3BodyDecision_TIS, &b_K_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("K_Hlt2Topo3BodyDecision_TOS", &K_Hlt2Topo3BodyDecision_TOS, &b_K_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("K_Hlt2Topo4BodyDecision_Dec", &K_Hlt2Topo4BodyDecision_Dec, &b_K_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("K_Hlt2Topo4BodyDecision_TIS", &K_Hlt2Topo4BodyDecision_TIS, &b_K_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("K_Hlt2Topo4BodyDecision_TOS", &K_Hlt2Topo4BodyDecision_TOS, &b_K_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("K_Hlt2TopoMu2BodyDecision_Dec", &K_Hlt2TopoMu2BodyDecision_Dec, &b_K_Hlt2TopoMu2BodyDecision_Dec);
   fChain->SetBranchAddress("K_Hlt2TopoMu2BodyDecision_TIS", &K_Hlt2TopoMu2BodyDecision_TIS, &b_K_Hlt2TopoMu2BodyDecision_TIS);
   fChain->SetBranchAddress("K_Hlt2TopoMu2BodyDecision_TOS", &K_Hlt2TopoMu2BodyDecision_TOS, &b_K_Hlt2TopoMu2BodyDecision_TOS);
   fChain->SetBranchAddress("K_Hlt2TopoMu3BodyDecision_Dec", &K_Hlt2TopoMu3BodyDecision_Dec, &b_K_Hlt2TopoMu3BodyDecision_Dec);
   fChain->SetBranchAddress("K_Hlt2TopoMu3BodyDecision_TIS", &K_Hlt2TopoMu3BodyDecision_TIS, &b_K_Hlt2TopoMu3BodyDecision_TIS);
   fChain->SetBranchAddress("K_Hlt2TopoMu3BodyDecision_TOS", &K_Hlt2TopoMu3BodyDecision_TOS, &b_K_Hlt2TopoMu3BodyDecision_TOS);
   fChain->SetBranchAddress("K_Hlt2TopoMu4BodyDecision_Dec", &K_Hlt2TopoMu4BodyDecision_Dec, &b_K_Hlt2TopoMu4BodyDecision_Dec);
   fChain->SetBranchAddress("K_Hlt2TopoMu4BodyDecision_TIS", &K_Hlt2TopoMu4BodyDecision_TIS, &b_K_Hlt2TopoMu4BodyDecision_TIS);
   fChain->SetBranchAddress("K_Hlt2TopoMu4BodyDecision_TOS", &K_Hlt2TopoMu4BodyDecision_TOS, &b_K_Hlt2TopoMu4BodyDecision_TOS);
   fChain->SetBranchAddress("K_Hlt2GlobalDecision_Dec", &K_Hlt2GlobalDecision_Dec, &b_K_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("K_Hlt2GlobalDecision_TIS", &K_Hlt2GlobalDecision_TIS, &b_K_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("K_Hlt2GlobalDecision_TOS", &K_Hlt2GlobalDecision_TOS, &b_K_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("K_Hlt2SingleMuonDecision_Dec", &K_Hlt2SingleMuonDecision_Dec, &b_K_Hlt2SingleMuonDecision_Dec);
   fChain->SetBranchAddress("K_Hlt2SingleMuonDecision_TIS", &K_Hlt2SingleMuonDecision_TIS, &b_K_Hlt2SingleMuonDecision_TIS);
   fChain->SetBranchAddress("K_Hlt2SingleMuonDecision_TOS", &K_Hlt2SingleMuonDecision_TOS, &b_K_Hlt2SingleMuonDecision_TOS);
   fChain->SetBranchAddress("K_TRACK_Type", &K_TRACK_Type, &b_K_TRACK_Type);
   fChain->SetBranchAddress("K_TRACK_Key", &K_TRACK_Key, &b_K_TRACK_Key);
   fChain->SetBranchAddress("K_TRACK_CHI2", &K_TRACK_CHI2, &b_K_TRACK_CHI2);
   fChain->SetBranchAddress("K_TRACK_NDOF", &K_TRACK_NDOF, &b_K_TRACK_NDOF);
   fChain->SetBranchAddress("K_TRACK_CHI2NDOF", &K_TRACK_CHI2NDOF, &b_K_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("K_TRACK_PCHI2", &K_TRACK_PCHI2, &b_K_TRACK_PCHI2);
   fChain->SetBranchAddress("K_TRACK_VeloCHI2NDOF", &K_TRACK_VeloCHI2NDOF, &b_K_TRACK_VeloCHI2NDOF);
   fChain->SetBranchAddress("K_TRACK_TCHI2NDOF", &K_TRACK_TCHI2NDOF, &b_K_TRACK_TCHI2NDOF);
   fChain->SetBranchAddress("K_TRACK_VELO_UTID", &K_TRACK_VELO_UTID, &b_K_TRACK_VELO_UTID);
   fChain->SetBranchAddress("K_TRACK_TT_UTID", &K_TRACK_TT_UTID, &b_K_TRACK_TT_UTID);
   fChain->SetBranchAddress("K_TRACK_IT_UTID", &K_TRACK_IT_UTID, &b_K_TRACK_IT_UTID);
   fChain->SetBranchAddress("K_TRACK_OT_UTID", &K_TRACK_OT_UTID, &b_K_TRACK_OT_UTID);
   fChain->SetBranchAddress("K_TRACK_VP_UTID", &K_TRACK_VP_UTID, &b_K_TRACK_VP_UTID);
   fChain->SetBranchAddress("K_TRACK_UT_UTID", &K_TRACK_UT_UTID, &b_K_TRACK_UT_UTID);
   fChain->SetBranchAddress("K_TRACK_FT_UTID", &K_TRACK_FT_UTID, &b_K_TRACK_FT_UTID);
   fChain->SetBranchAddress("K_TRACK_nVeloHits", &K_TRACK_nVeloHits, &b_K_TRACK_nVeloHits);
   fChain->SetBranchAddress("K_TRACK_nVeloRHits", &K_TRACK_nVeloRHits, &b_K_TRACK_nVeloRHits);
   fChain->SetBranchAddress("K_TRACK_nVeloPhiHits", &K_TRACK_nVeloPhiHits, &b_K_TRACK_nVeloPhiHits);
   fChain->SetBranchAddress("K_TRACK_nVeloPileUpHits", &K_TRACK_nVeloPileUpHits, &b_K_TRACK_nVeloPileUpHits);
   fChain->SetBranchAddress("K_TRACK_nTTHits", &K_TRACK_nTTHits, &b_K_TRACK_nTTHits);
   fChain->SetBranchAddress("K_TRACK_nITHits", &K_TRACK_nITHits, &b_K_TRACK_nITHits);
   fChain->SetBranchAddress("K_TRACK_nOTHits", &K_TRACK_nOTHits, &b_K_TRACK_nOTHits);
   fChain->SetBranchAddress("K_TRACK_nVPHits", &K_TRACK_nVPHits, &b_K_TRACK_nVPHits);
   fChain->SetBranchAddress("K_TRACK_nUTHits", &K_TRACK_nUTHits, &b_K_TRACK_nUTHits);
   fChain->SetBranchAddress("K_TRACK_nFTHits", &K_TRACK_nFTHits, &b_K_TRACK_nFTHits);
   fChain->SetBranchAddress("K_TRACK_FirstMeasurementX", &K_TRACK_FirstMeasurementX, &b_K_TRACK_FirstMeasurementX);
   fChain->SetBranchAddress("K_TRACK_FirstMeasurementY", &K_TRACK_FirstMeasurementY, &b_K_TRACK_FirstMeasurementY);
   fChain->SetBranchAddress("K_TRACK_FirstMeasurementZ", &K_TRACK_FirstMeasurementZ, &b_K_TRACK_FirstMeasurementZ);
   fChain->SetBranchAddress("K_TRACK_History", &K_TRACK_History, &b_K_TRACK_History);
   fChain->SetBranchAddress("K_TRACK_MatchCHI2", &K_TRACK_MatchCHI2, &b_K_TRACK_MatchCHI2);
   fChain->SetBranchAddress("K_TRACK_GhostProb", &K_TRACK_GhostProb, &b_K_TRACK_GhostProb);
   fChain->SetBranchAddress("K_TRACK_CloneDist", &K_TRACK_CloneDist, &b_K_TRACK_CloneDist);
   fChain->SetBranchAddress("K_TRACK_Likelihood", &K_TRACK_Likelihood, &b_K_TRACK_Likelihood);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVX", PVX, &b_PVX);
   fChain->SetBranchAddress("PVY", PVY, &b_PVY);
   fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
   fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
   fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
   fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
   fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
   fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
   fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   Notify();

   return true;
}

Bool_t B2JPsiK_selection::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void B2JPsiK_selection::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t B2JPsiK_selection::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
return 1;
}
#endif // #ifdef B2JPsiK_selection_cxx
