//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jan  2 12:26:49 2019 by ROOT version 6.06/02
// from TTree JpsiCalib_combined/JpsiCalib_combined
// found on file: ../../../tuples/tagANDprobe/trees/Jpsi_Mu_MagDown_Strip21_0.root
//////////////////////////////////////////////////////////

#ifndef Bu2JpsiK_Jpsi2MuMu_h
#define Bu2JpsiK_Jpsi2MuMu_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TString.h"
// Header file for the classes stored in the TTree if any.

class Bu2JpsiK_Jpsi2MuMu {
public :
  TString nfout;
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           Charge_idx;
   Char_t          Charge_lbl;
   Double_t        nsig_sw;
   Double_t        nTracks;
   Double_t        nSPDHits;
   Double_t        runNumber;
   Double_t        nRich1Hits;
   Double_t        nRich2Hits;
   Double_t        nPVs;
   Double_t        Mu_CombDLLK;
   Double_t        Mu_CombDLLp;
   Double_t        Mu_CombDLLmu;
   Double_t        Mu_CombDLLe;
   Double_t        Mu_V2ProbNNK;
   Double_t        Mu_V2ProbNNpi;
   Double_t        Mu_V2ProbNNp;
   Double_t        Mu_V2ProbNNmu;
   Double_t        Mu_V2ProbNNe;
   Double_t        Mu_V3ProbNNK;
   Double_t        Mu_V3ProbNNpi;
   Double_t        Mu_V3ProbNNp;
   Double_t        Mu_V3ProbNNmu;
   Double_t        Mu_V3ProbNNe;
   Double_t        Mu_V2ProbNNghost;
   Double_t        Mu_V3ProbNNghost;
   Double_t        Mu_P;
   Double_t        Mu_PT;
   Double_t        Mu_Eta;
   Double_t        Mu_Phi;
   Double_t        Mu_trackcharge;
   Double_t        Mu_InMuonAcc;
   Double_t        Mu_IsMuon;
   Double_t        Mu_IsMuonLoose;
   Double_t        Mu_nShared;
   Double_t        Mu_RICHThreshold_pi;
   Double_t        Mu_RICHThreshold_K;
   Double_t        Mu_RICHThreshold_p;
   Double_t        Mu_RICHThreshold_mu;
   Double_t        Mu_RICHThreshold_e;
   Double_t        Mu_RICHAerogelUsed;
   Double_t        Mu_RICH1GasUsed;
   Double_t        Mu_RICH2GasUsed;
   Double_t        Mu_hasRich;
   Double_t        Mu_Probe_TIS;
   Double_t        Mu_Tag_TOS;

   // List of branches
   TBranch        *b_Charge_idx;   //!
   TBranch        *b_Charge_lbl;   //!
   TBranch        *b_nsig_sw;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_Mu_CombDLLK;   //!
   TBranch        *b_Mu_CombDLLp;   //!
   TBranch        *b_Mu_CombDLLmu;   //!
   TBranch        *b_Mu_CombDLLe;   //!
   TBranch        *b_Mu_V2ProbNNK;   //!
   TBranch        *b_Mu_V2ProbNNpi;   //!
   TBranch        *b_Mu_V2ProbNNp;   //!
   TBranch        *b_Mu_V2ProbNNmu;   //!
   TBranch        *b_Mu_V2ProbNNe;   //!
   TBranch        *b_Mu_V3ProbNNK;   //!
   TBranch        *b_Mu_V3ProbNNpi;   //!
   TBranch        *b_Mu_V3ProbNNp;   //!
   TBranch        *b_Mu_V3ProbNNmu;   //!
   TBranch        *b_Mu_V3ProbNNe;   //!
   TBranch        *b_Mu_V2ProbNNghost;   //!
   TBranch        *b_Mu_V3ProbNNghost;   //!
   TBranch        *b_Mu_P;   //!
   TBranch        *b_Mu_PT;   //!
   TBranch        *b_Mu_Eta;   //!
   TBranch        *b_Mu_Phi;   //!
   TBranch        *b_Mu_trackcharge;   //!
   TBranch        *b_Mu_InMuonAcc;   //!
   TBranch        *b_Mu_IsMuon;   //!
   TBranch        *b_Mu_IsMuonLoose;   //!
   TBranch        *b_Mu_nShared;   //!
   TBranch        *b_Mu_RICHThreshold_pi;   //!
   TBranch        *b_Mu_RICHThreshold_K;   //!
   TBranch        *b_Mu_RICHThreshold_p;   //!
   TBranch        *b_Mu_RICHThreshold_mu;   //!
   TBranch        *b_Mu_RICHThreshold_e;   //!
   TBranch        *b_Mu_RICHAerogelUsed;   //!
   TBranch        *b_Mu_RICH1GasUsed;   //!
   TBranch        *b_Mu_RICH2GasUsed;   //!
   TBranch        *b_Mu_hasRich;   //!
   TBranch        *b_Mu_Probe_TIS;   //!
   TBranch        *b_Mu_Tag_TOS;   //!

   Bu2JpsiK_Jpsi2MuMu(TTree *tree=0);
   virtual ~Bu2JpsiK_Jpsi2MuMu();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Bu2JpsiK_Jpsi2MuMu_cxx
Bu2JpsiK_Jpsi2MuMu::Bu2JpsiK_Jpsi2MuMu(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
/*
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../../../tuples/tagANDprobe/trees/Jpsi_Mu_MagDown_Strip21_0.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../../../tuples/tagANDprobe/trees/Jpsi_Mu_MagDown_Strip21_0.root");
      }
      f->GetObject("JpsiCalib_combined",tree);

   }
*/
   Init(tree);
}

Bu2JpsiK_Jpsi2MuMu::~Bu2JpsiK_Jpsi2MuMu()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Bu2JpsiK_Jpsi2MuMu::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Bu2JpsiK_Jpsi2MuMu::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Bu2JpsiK_Jpsi2MuMu::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Charge_idx", &Charge_idx, &b_Charge_idx);
   fChain->SetBranchAddress("Charge_lbl", &Charge_lbl, &b_Charge_lbl);
   fChain->SetBranchAddress("nsig_sw", &nsig_sw, &b_nsig_sw);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("Mu_CombDLLK", &Mu_CombDLLK, &b_Mu_CombDLLK);
   fChain->SetBranchAddress("Mu_CombDLLp", &Mu_CombDLLp, &b_Mu_CombDLLp);
   fChain->SetBranchAddress("Mu_CombDLLmu", &Mu_CombDLLmu, &b_Mu_CombDLLmu);
   fChain->SetBranchAddress("Mu_CombDLLe", &Mu_CombDLLe, &b_Mu_CombDLLe);
   fChain->SetBranchAddress("Mu_V2ProbNNK", &Mu_V2ProbNNK, &b_Mu_V2ProbNNK);
   fChain->SetBranchAddress("Mu_V2ProbNNpi", &Mu_V2ProbNNpi, &b_Mu_V2ProbNNpi);
   fChain->SetBranchAddress("Mu_V2ProbNNp", &Mu_V2ProbNNp, &b_Mu_V2ProbNNp);
   fChain->SetBranchAddress("Mu_V2ProbNNmu", &Mu_V2ProbNNmu, &b_Mu_V2ProbNNmu);
   fChain->SetBranchAddress("Mu_V2ProbNNe", &Mu_V2ProbNNe, &b_Mu_V2ProbNNe);
   fChain->SetBranchAddress("Mu_V3ProbNNK", &Mu_V3ProbNNK, &b_Mu_V3ProbNNK);
   fChain->SetBranchAddress("Mu_V3ProbNNpi", &Mu_V3ProbNNpi, &b_Mu_V3ProbNNpi);
   fChain->SetBranchAddress("Mu_V3ProbNNp", &Mu_V3ProbNNp, &b_Mu_V3ProbNNp);
   fChain->SetBranchAddress("Mu_V3ProbNNmu", &Mu_V3ProbNNmu, &b_Mu_V3ProbNNmu);
   fChain->SetBranchAddress("Mu_V3ProbNNe", &Mu_V3ProbNNe, &b_Mu_V3ProbNNe);
   fChain->SetBranchAddress("Mu_V2ProbNNghost", &Mu_V2ProbNNghost, &b_Mu_V2ProbNNghost);
   fChain->SetBranchAddress("Mu_V3ProbNNghost", &Mu_V3ProbNNghost, &b_Mu_V3ProbNNghost);
   fChain->SetBranchAddress("Mu_P", &Mu_P, &b_Mu_P);
   fChain->SetBranchAddress("Mu_PT", &Mu_PT, &b_Mu_PT);
   fChain->SetBranchAddress("Mu_Eta", &Mu_Eta, &b_Mu_Eta);
   fChain->SetBranchAddress("Mu_Phi", &Mu_Phi, &b_Mu_Phi);
   fChain->SetBranchAddress("Mu_trackcharge", &Mu_trackcharge, &b_Mu_trackcharge);
   fChain->SetBranchAddress("Mu_InMuonAcc", &Mu_InMuonAcc, &b_Mu_InMuonAcc);
   fChain->SetBranchAddress("Mu_IsMuon", &Mu_IsMuon, &b_Mu_IsMuon);
   fChain->SetBranchAddress("Mu_IsMuonLoose", &Mu_IsMuonLoose, &b_Mu_IsMuonLoose);
   fChain->SetBranchAddress("Mu_nShared", &Mu_nShared, &b_Mu_nShared);
   fChain->SetBranchAddress("Mu_RICHThreshold_pi", &Mu_RICHThreshold_pi, &b_Mu_RICHThreshold_pi);
   fChain->SetBranchAddress("Mu_RICHThreshold_K", &Mu_RICHThreshold_K, &b_Mu_RICHThreshold_K);
   fChain->SetBranchAddress("Mu_RICHThreshold_p", &Mu_RICHThreshold_p, &b_Mu_RICHThreshold_p);
   fChain->SetBranchAddress("Mu_RICHThreshold_mu", &Mu_RICHThreshold_mu, &b_Mu_RICHThreshold_mu);
   fChain->SetBranchAddress("Mu_RICHThreshold_e", &Mu_RICHThreshold_e, &b_Mu_RICHThreshold_e);
   fChain->SetBranchAddress("Mu_RICHAerogelUsed", &Mu_RICHAerogelUsed, &b_Mu_RICHAerogelUsed);
   fChain->SetBranchAddress("Mu_RICH1GasUsed", &Mu_RICH1GasUsed, &b_Mu_RICH1GasUsed);
   fChain->SetBranchAddress("Mu_RICH2GasUsed", &Mu_RICH2GasUsed, &b_Mu_RICH2GasUsed);
   fChain->SetBranchAddress("Mu_hasRich", &Mu_hasRich, &b_Mu_hasRich);
   fChain->SetBranchAddress("Mu_Probe_TIS", &Mu_Probe_TIS, &b_Mu_Probe_TIS);
   fChain->SetBranchAddress("Mu_Tag_TOS", &Mu_Tag_TOS, &b_Mu_Tag_TOS);
   Notify();
}

Bool_t Bu2JpsiK_Jpsi2MuMu::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Bu2JpsiK_Jpsi2MuMu::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Bu2JpsiK_Jpsi2MuMu::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Bu2JpsiK_Jpsi2MuMu_cxx
