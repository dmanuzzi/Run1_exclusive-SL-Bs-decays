import sys
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/effPIDmu/rew')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/effPIDmu/getEff')
from f_reweigh2D_PIDmu import f_reweigh2D_PIDmu
from f_getEffPIDmu import f_getEffPIDmu

def f_getAB(years, modes, pols, pythias, cats, bins, inpathMC, outpath, tag, CSsPIDCalib):
    
    ### run reweight
    f_reweigh2D_PIDmu(tag, modes, years, cats, bins, pythias, pols, inpathMC, CSsPIDCalib)
    ### getEff 
    resultsA = {}
    resultsB = {}
    nfout = outpath+'/results_%s_effPIDmu.txt'%(tag)
    fout = open(nfout, 'w')
    fout.close()
    resultsA, resultsB = f_getEffPIDmu(CSsPIDCalib, nfout, tag, years, pols, cats, bins)
    return resultsA, resultsB
