"""
Run this script with: 
lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python reweight2D_merged.py
"""
import os, sys
import root_numpy
import pandas
from hep_ml import reweight
import numpy as np
from rootpy.io import root_open
from ROOT import *
from shutil import copyfile
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/srcEff')
from reweighter2D import myReweighter



#calibrations samples
pathJpsi = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/tagANDprobe/'
CSs = {'2011' : {'Up' : {'nfin' : pathJpsi+'DataPIDCalib-2011-Bu2JpsiKUp_MuTagTOS.root', 'df' : None, 'df_w' : None}, 
                 'Dw' : {'nfin' : pathJpsi+'DataPIDCalib-2011-Bu2JpsiKDw_MuTagTOS.root', 'df' : None, 'df_w' : None}
                 },
       '2012' : {'Up' : {'nfin' : pathJpsi+'DataPIDCalib-2012-Bu2JpsiKUp_MuTagTOS.root', 'df' : None, 'df_w' : None},
                 'Dw' : {'nfin' : pathJpsi+'DataPIDCalib-2012-Bu2JpsiKDw_MuTagTOS.root', 'df' : None, 'df_w' : None}
                 }
       }

#copying the calibration TTree from the backup ones
#and import them in pandas
for year in CSs.keys():
    for pol in CSs[year].keys():
        print 'cp '+CSs[year][pol]['nfin']
        copyfile(CSs[year][pol]['nfin']+'.bak', CSs[year][pol]['nfin'])        
        print 'Reading dataset '+CSs[year][pol]['nfin']
        input_CS = root_numpy.root2array(CSs[year][pol]['nfin'], treename='DecayTree', branches=['Mu_PT','Mu_Eta'])
        input_CS = pandas.DataFrame(input_CS)
        CSs[year][pol]['df'] = input_CS
        input_CSw= root_numpy.root2array(CSs[year][pol]['nfin'], treename='DecayTree', branches=['nsig_sw'])
        input_CSw= input_CSw['nsig_sw']
        CSs[year][pol]['df_w'] = input_CSw

#MC samples
pathMC='/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC/'
#Bins = [0,800,1100,1400,1700,2000,4500]
Bins = [0, 4500]
configs = [
#    ( ['BuKpipi'], ['2011','2012'], ['Up', 'Dw'], ['Pythia6', 'Pythia8'], [0],       pathMC ),
    ( ['BdKpipi'], ['2011'], ['Up', 'Dw'], ['Pythia6', 'Pythia8'], [1,2], pathMC, 'BdKpipi_2011' , Bins),
    ( ['BdKpipi'], ['2012'], ['Up', 'Dw'], ['Pythia6', 'Pythia8'], [1,2], pathMC, 'BdKpipi_2012' , Bins),
    ( ['BdKKpi'],  ['2012'], ['Up', 'Dw'], ['Pythia6', 'Pythia8'], [1,2], pathMC, 'BdKKpi_2012'  , Bins),
    ( ['BsSignal'],['2012'], ['Up', 'Dw'], ['Pythia6', 'Pythia8'], [1,2], pathMC, 'BsSignal_2012', Bins)
    ]

#reweigh the calibration samples according to all the MC samples 
for modes, years, pols, pythias, cats, path_MC, tag, bins in configs:
    Nbins = len(bins)-1
    nfinMCs   = []
    nfinCSs   = []
    input_CSs = []
    input_CSws= []
    for year in years:
        for pol in pols:
            nfinCSs.append(CSs[year][pol]['nfin'])
            input_CSs.append(CSs[year][pol]['df'])
            input_CSws.append(CSs[year][pol]['df_w'])
            for pythia in pythias:
                for mode in modes:
                    nfinMC = path_MC+'/noTrigg_noPIDmu/{mode}/Mag{pol}/MC-{year}-{mode}{pol}-noTrigg_noPIDmu_{pythia}Unfiltered_0.root'
                    nfinMCs.append( nfinMC.format(mode=mode, pol=pol, year=year, pythia=pythia) )

    for cat in cats:
        nw='w_PIDmu_sigCat%d_%s'%(cat,tag)
        MCcut = '(D_peak)'
        if 'Bs' in tag: MCcut = '(Ds_peak)'
        if cat :
            MCcut += ' && (signalCategory==%d)'%(cat)
        for i in range(0,Nbins):
            nwBinned = nw+'_bin%d'%(i+1)
            print nwBinned
            MCcutBinned = MCcut+' && (%f < D_Ptrans && D_Ptrans < %f)'%(bins[i], bins[i+1])
            myReweighter(nfinCSs, input_CSs, nfinMCs, nw=nwBinned,
                         ntCSin = 'DecayTree', input_CSws=input_CSws, 
                         ntMCin='DMutuple', mySel=MCcutBinned, 
                         columns_MC = ['Mu_PT','Mu_ETA'], binning=[12,6])

                        
