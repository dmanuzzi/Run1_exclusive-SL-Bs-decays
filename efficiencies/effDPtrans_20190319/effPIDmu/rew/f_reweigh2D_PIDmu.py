import os, sys
import root_numpy
import pandas
from hep_ml import reweight
import numpy as np
from rootpy.io import root_open
from ROOT import *
from shutil import copyfile
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/srcEff')
from reweighter2D import myReweighter


def f_prepareCSsPIDCalib(pathJpsi_PIDCalib='/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/tagANDprobe/', copyFromBackup=True, initDF=True):
    #calibrations samples
    CSs_PIDCalib = {'2011' : {'Up' : {'nfin' : pathJpsi_PIDCalib+'DataPIDCalib-2011-Bu2JpsiKUp_MuTagTOS.root', 'df' : None, 'df_w' : None}, 
                              'Dw' : {'nfin' : pathJpsi_PIDCalib+'DataPIDCalib-2011-Bu2JpsiKDw_MuTagTOS.root', 'df' : None, 'df_w' : None}
                              },
                    '2012' : {'Up' : {'nfin' : pathJpsi_PIDCalib+'DataPIDCalib-2012-Bu2JpsiKUp_MuTagTOS.root', 'df' : None, 'df_w' : None},
                              'Dw' : {'nfin' : pathJpsi_PIDCalib+'DataPIDCalib-2012-Bu2JpsiKDw_MuTagTOS.root', 'df' : None, 'df_w' : None}
                              }
                    }

     #copying the calibration TTree from the backup ones
     #and import them in pandas
    for year in CSs_PIDCalib.keys():
        for pol in CSs_PIDCalib[year].keys():
            if copyFromBackup:
                print 'cp '+CSs_PIDCalib[year][pol]['nfin']
                copyfile(CSs_PIDCalib[year][pol]['nfin']+'.bak', CSs_PIDCalib[year][pol]['nfin'])        
            if initDF:
                print 'Reading dataset '+CSs_PIDCalib[year][pol]['nfin']
                input_CS = root_numpy.root2array(CSs_PIDCalib[year][pol]['nfin'], treename='DecayTree', branches=['Mu_PT','Mu_Eta'])
                input_CS = pandas.DataFrame(input_CS)
                CSs_PIDCalib[year][pol]['df'] = input_CS
                input_CSw= root_numpy.root2array(CSs_PIDCalib[year][pol]['nfin'], treename='DecayTree', branches=['nsig_sw'])
                input_CSw= input_CSw['nsig_sw']
                CSs_PIDCalib[year][pol]['df_w'] = input_CSw
                print year, pol, 'N_CS = ', len(input_CS), 'N_CSw = ', len(input_CSw)
    return CSs_PIDCalib

#reweigh the calibration samples according to all the MC samples 
def f_reweigh2D_PIDmu(tag, modes, years, cats, bins,
                      pythias, pols, path_MC, CSs_PIDCalib):
    Nbins = len(bins)-1
    nfinMCs   = []
    nfinCSs_PIDCalib   = []
    input_CSs_PIDCalib = []
    input_CSws= []
    for year in years:
        for pol in pols:
            nfinCSs_PIDCalib.append(CSs_PIDCalib[year][pol]['nfin'])
            input_CSs_PIDCalib.append(CSs_PIDCalib[year][pol]['df'])
            input_CSws.append(CSs_PIDCalib[year][pol]['df_w'])
            for pythia in pythias:
                for mode in modes:
                    nfinMC = path_MC+'/noTrigg_noPIDmu/{mode}/Mag{pol}/MC-{year}-{mode}{pol}-noTrigg_noPIDmu_{pythia}Unfiltered_0.root'
                    nfinMCs.append( nfinMC.format(mode=mode, pol=pol, year=year, pythia=pythia) )

    for cat in cats:
        nw='w_PIDmu_sigCat%d_%s'%(cat,tag)
        MCcut = '(D_peak)'
        if 'Bs' in tag: MCcut = '(Ds_peak)'
        if cat :
            MCcut += ' && (signalCategory==%d)'%(cat)
        nbins = ['bin%d'%i for i in range(1,len(bins)) ]
        nbins += ['tot']
        i = 0
        for nbin in nbins:
            nwBinned = nw+'_'+nbin
            print nwBinned
            MCcutBinned = MCcut
            if nbin != 'tot':
                MCcutBinned = MCcut+' && (%f < D_Ptrans && D_Ptrans < %f)'%(bins[i], bins[i+1])
            myReweighter(nfinCSs_PIDCalib, input_CSs_PIDCalib, nfinMCs, nw=nwBinned,
                         ntCSin = 'DecayTree', input_CSws=input_CSws, 
                         ntMCin='DMutuple', mySel=MCcutBinned, nwMC='w_FF_corr',
                         columns_MC = ['Mu_PT','Mu_ETA'], binning=[12,6])
            i= i+1
                        
