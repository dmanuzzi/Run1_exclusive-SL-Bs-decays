#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "TMath.h"
#include <fstream>
#include "TFile.h"
using namespace std;
using namespace TMath;
double getEff_PIDmu(TTree*tin, TString nw, TString nfout, float *results){
  
  tin->SetBranchStatus("*",0);
  double sw=1;
  tin->SetBranchStatus("nsig_sw", 1);
  tin->SetBranchAddress("nsig_sw", &sw);

  double w=1., we=1.;
  if (nw != "none"){
      tin->SetBranchStatus(nw, 1);
      tin->SetBranchAddress(nw, &w);
  }
  

  double PIDmu=0;
  double IsMuon=1;
  tin->SetBranchStatus("Mu_CombDLLmu",1);
  tin->SetBranchStatus("Mu_IsMuon",1);
  tin->SetBranchAddress("Mu_CombDLLmu", &PIDmu);
  tin->SetBranchAddress("Mu_IsMuon", &IsMuon);
   
  double npass=0., nfail=0., npasserr=0., nfailerr=0.;
  bool cut = false;
  for (int i=0; i<tin->GetEntries(); ++i){
    //    if (i>700000) break;
    tin->GetEntry(i);
    cut = ((bool)IsMuon && (PIDmu>0));
    if (cut){
      npass += w*sw;
      npasserr+=w*sw*w*sw;
    }
    else {
      nfail += w*sw;
      nfailerr += w*sw*w*sw; 
    }
  }

  double eff = npass/(npass+nfail);
  npasserr = Sqrt(npasserr);
  nfailerr = Sqrt(nfailerr);
  double efferr = Sqrt(Power(npass*nfailerr,2)+Power(nfail*npasserr,2))/Power(npass+nfail,2);
  cout << "--------------------------------------------\n";
  cout << "   "  <<  nw << endl;
  cout << " --- eff    =   " << eff      << "               efferr =             " << efferr<< endl;
  cout << "     pass   =   " << npass   <<  "               fail   =             " << nfail   << endl;
  cout << "     passerr=   " << npasserr<<  "               failerr=             " << nfailerr<<endl;
  cout << "getEntries  =   " << tin->GetEntries() << endl;
  tin->SetBranchStatus("*",1);

  ofstream pout(nfout, std::ios_base::app);
  pout << nw << " : eff = " << eff << " \\pm " << efferr << endl; 
  results[0] = eff;
  results[1] = efferr;
  results[2] = npass;
  results[3] = nfail;
  results[4] = npasserr;
  results[5] = nfailerr;
  return eff;
}



double getCov_PIDmu(TTree *tin, TString nw_x, TString nw_y, float *results,  TString nfout){

  tin->SetBranchStatus("*",0);

  double sw=1;
  tin->SetBranchStatus("nsig_sw", 1);
  tin->SetBranchAddress("nsig_sw", &sw);
  
  double x=1., y=1.;
  double *px = &x;
  double *py = &y;
  if (nw_x == nw_y)  py = &x;
  if ((nw_x != "none") && (nw_y != "none")){
    tin->SetBranchStatus(nw_x, 1);
    tin->SetBranchAddress(nw_x, px);
    tin->SetBranchStatus(nw_y, 1);
    tin->SetBranchAddress(nw_y, py);
  }

  double PIDmu=0;
  double IsMuon=1;
  tin->SetBranchStatus("Mu_CombDLLmu",1);
  tin->SetBranchStatus("Mu_IsMuon",1);
  tin->SetBranchAddress("Mu_CombDLLmu", &PIDmu);
  tin->SetBranchAddress("Mu_IsMuon", &IsMuon);

  double sxy0=0.; //fail
  double sxy1=0.; //pass
  double npass_x=0., nfail_x=0.;
  double npass_y=0., nfail_y=0.;
  double npass_x_err = 0., nfail_x_err=0.;
  double npass_y_err = 0., nfail_y_err=0.;
  bool cut = false;
  for (int i=0; i<tin->GetEntries(); ++i){
    tin->GetEntry(i);
    cut = ((bool)IsMuon && (PIDmu>0));
    //    cout << "sw,px,py,  "<< sw <<"  "<< *px <<"  " <<*py << endl;
    if (cut){
      npass_x += (*px)*sw;
      npass_y += (*py)*sw;
      npass_x_err += (*px)*sw*(*px)*sw;
      npass_y_err += (*py)*sw*(*py)*sw;

      sxy1 += (*px)*(*py)*sw*sw;
      
    }
    else {
      nfail_x += (*px)*sw;
      nfail_y += (*py)*sw;
      nfail_x_err += (*px)*sw*(*px)*sw;
      nfail_y_err += (*py)*sw*(*py)*sw;

      sxy0 += (*px)*(*py)*sw*sw;
    }
  }
  npass_x_err = TMath::Sqrt(npass_x_err);
  npass_y_err = TMath::Sqrt(npass_y_err);
  nfail_x_err = TMath::Sqrt(nfail_x_err);
  nfail_y_err = TMath::Sqrt(nfail_y_err);

  double eff_x = npass_x/(npass_x+nfail_x);
  double eff_y = npass_y/(npass_y+nfail_y);

  double cov_pass = sxy1;
  double cov_fail = sxy0;

  double cor_pass = cov_pass/(npass_x_err*npass_y_err);
  double cor_fail = cov_fail/(nfail_x_err*nfail_y_err);
  results[0] = eff_x;
  results[1] = eff_y;
  results[2] = cov_pass;
  results[3] = cov_fail;

  TString sp_x = "";
  for (int i=0;i<nw_x.Length();++i)
    sp_x.Append(" ");
  TString sp_y = "";
  for (int i=0;i<nw_y.Length();++i)
    sp_y.Append(" ");

  cout << "----"+nw_x+"    "+sp_y+"              eff1  =         " << eff_x <<  endl;
  cout << "----"+nw_y+"    "+sp_x+"              eff2  =         " << eff_y <<  endl;
  cout << "----"+nw_x+" vs "+nw_y+"         cov_pass   =         " << cov_pass << endl;
  cout << "----"+nw_x+" vs "+nw_y+"         cov_fail   =         " << cov_fail << endl;
  cout << "----"+nw_x+" vs "+nw_y+"         cor_pass   =         " << cor_pass << endl;
  cout << "----"+nw_x+" vs "+nw_y+"         cor_fail   =         " << cor_fail << endl;
  tin->SetBranchStatus("*",1);

  ofstream pout(nfout, std::ios_base::app);
  pout << nw_x << " vs " << nw_y << " --> eff1 = " << eff_x << "   eff2 = " << eff_y << "    cov_pass = " << cov_pass << "   cov_fail = " << cov_fail <<  endl; 


  return 1;
}












int getEffPIDmuBinned(){
  TString pathIN = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/tagANDprobe/";
  TChain *tin_CS_2011 = new TChain("DecayTree");
  tin_CS_2011->Add(pathIN+"DataPIDCalib-2011-Bu2JpsiKUp_MuTagTOS.root");
  tin_CS_2011->Add(pathIN+"DataPIDCalib-2011-Bu2JpsiKDw_MuTagTOS.root");
  TChain *tin_CS_2012 = new TChain("DecayTree");
  tin_CS_2012->Add(pathIN+"DataPIDCalib-2012-Bu2JpsiKUp_MuTagTOS.root");
  tin_CS_2012->Add(pathIN+"DataPIDCalib-2012-Bu2JpsiKDw_MuTagTOS.root");
  TChain* tin_CS[2];
  tin_CS[0] = tin_CS_2011;
  tin_CS[1] = tin_CS_2012;
  

  cout << "================================================================\n";
  cout << "*     Eff: PIDmu        (Tag & Probe)                          *\n";
  cout << "================================================================\n";

  TString nfout = "AllEffPIDmuBinned.txt";
  TFile fout(nfout, "RECREATE");
  fout.Close();

  TString mode[]  = {"BdKpipi", "BdKKpi", "BsSignal"};
  TString year[]  = {"2011", "2012"};
  float results[2];
  for (int i=0; i<3;++i){                  // loop on modes
    for (int j=0; j<2; ++j){ // loop on years
      if (!mode[i].Contains("BdKpipi") && year[j].Contains("2011")) continue;
      for (int m=1; m<=1; ++m){        // loop on D_Ptrans bins
	if (mode[i].Contains("BdKpipi") || mode[i].Contains("BdKKpi") || mode[i].Contains("BsSignal")){
	  for (int l=1; l<=2; ++l){        // loop on signal categories
	    TString nw = TString::Format("w_PIDmu_sigCat%d_%s_%s_bin%d", l, mode[i].Data(), year[j].Data(),m); 
	    getEff_PIDmu(tin_CS[j], nw, nfout, results);
	    cout << "====> eff = " << results[0] << " +/- " << results[1] << endl;
	  }
	} else {
	  TString nw = TString::Format("w_PIDmu_sigCat0_%s_%s_bin%d", mode[i].Data(), year[j].Data(), m); 
	  getEff_PIDmu(tin_CS[j], nw, nfout, results);
	  cout << "====> eff = " << results[0] << " +/- " << results[1] << endl;
	}
      }
    }
  }
  
  return 1;
}
