#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "TMath.h"
#include <fstream>
#include "TFile.h"
using namespace std;
using namespace TMath;
double getEff(TTree*tin, TString nw){

  tin->SetBranchStatus("*",0);
  double sw=1;
  tin->SetBranchStatus("nsig_sw", 1);
  tin->SetBranchAddress("nsig_sw", &sw);

  double w=1., we=1.;
  if (nw != "none"){
      tin->SetBranchStatus(nw, 1);
      tin->SetBranchAddress(nw, &w);
  }
  

  double PIDmu=0;
  double IsMuon=1;
  tin->SetBranchStatus("Mu_CombDLLmu",1);
  tin->SetBranchStatus("Mu_IsMuon",1);
  tin->SetBranchAddress("Mu_CombDLLmu", &PIDmu);
  tin->SetBranchAddress("Mu_IsMuon", &IsMuon);
   
  double npass=0., nfail=0., npasserr=0., nfailerr=0.;
  bool cut = false;
  for (int i=0; i<tin->GetEntries(); ++i){
    tin->GetEntry(i);
    cut = ((bool)IsMuon && (PIDmu>0));
    if (cut){
      npass += w*sw;
      npasserr+=w*sw*w*sw;
    }
    else {
      nfail += w*sw;
      nfailerr += w*sw*w*sw; 
    }
  }

  double eff = npass/(npass+nfail);
  npasserr = Sqrt(npasserr);
  nfailerr = Sqrt(nfailerr);
  double efferr = Sqrt(Power(npass*nfailerr,2)+Power(nfail*npasserr,2))/Power(npass+nfail,2);
  cout << "--------------------------------------------\n";
  cout << "   "  <<  nw << endl;
  cout << " --- eff    =   " << eff      << "               efferr =             " << efferr<< endl;
  cout << "     pass   =   " << npass   <<  "               fail   =             " << nfail   << endl;
  cout << "     passerr=   " << npasserr<<  "               failerr=             " << nfailerr<<endl;
  cout << "getEntries  =   " << tin->GetEntries() << endl;
  tin->SetBranchStatus("*",1);
  
  return eff;
}

double getCorrelation(TTree*tin, 
		      TString nw_x, TString nw_y, 
		      TString cprobe, int whichCuts, int sigCat=0){
  tin->SetBranchStatus("*",0);
  double x=1., y=1.;
  if ((nw_x != "none") && (nw_y != "none")){
      tin->SetBranchStatus(nw_x, 1);
      tin->SetBranchAddress(nw_x, &x);
      tin->SetBranchStatus(nw_y, 1);
      tin->SetBranchAddress(nw_y, &y);
  }
  
  int scat=0;
  if (sigCat!=0){
    tin->SetBranchStatus("signalCategory", 1);
    tin->SetBranchAddress("signalCategory", &scat);
  }

  double PIDmu=0;
  bool IsMuon=true;
  if (whichCuts>=1){
    tin->SetBranchStatus(cprobe+"_PIDmu",1);
    tin->SetBranchStatus(cprobe+"_isMuon",1);
    tin->SetBranchAddress(cprobe+"_PIDmu", &PIDmu);
    tin->SetBranchAddress(cprobe+"_isMuon", &IsMuon);
  } 
  bool L0=true;
  if (whichCuts>=2){
    tin->SetBranchStatus (cprobe+"_L0MuonDecision_TOS", 1);
    tin->SetBranchAddress(cprobe+"_L0MuonDecision_TOS", &L0);
  }
  bool   Hlt1TrackMuonDecision=true;
  double Mu_PT(0.), Mu_P(0.), Mu_ProbNNghost(0.), Mu_IPChi2(0.);
  if (whichCuts>=3){
    tin->SetBranchStatus (cprobe+"_Hlt1TrackMuonDecision_TOS", 1);
    tin->SetBranchAddress(cprobe+"_Hlt1TrackMuonDecision_TOS", &Hlt1TrackMuonDecision);
    tin->SetBranchStatus (cprobe+"_P", 1);
    tin->SetBranchAddress(cprobe+"_P", &Mu_P);
    tin->SetBranchStatus (cprobe+"_PT", 1);
    tin->SetBranchAddress(cprobe+"_PT", &Mu_PT);
    tin->SetBranchStatus (cprobe+"_ProbNNghost", 1);
    tin->SetBranchAddress(cprobe+"_ProbNNghost", &Mu_ProbNNghost);
    tin->SetBranchStatus (cprobe+"_IPCHI2_OWNPV", 1);
    tin->SetBranchAddress(cprobe+"_IPCHI2_OWNPV", &Mu_IPChi2);
  }


  double sxy0=0., sx20=0., sy20=0.; //fail
  double sxy1=0., sx21=0., sy21=0.; //pass    
  double npass_x=0., nfail_x=0., npasserr_x=0., nfailerr_x=0.;
  double npass_y=0., nfail_y=0., npasserr_y=0., nfailerr_y=0.;

  bool cut = false;
  for (int i=0; i<tin->GetEntries(); ++i){
    tin->GetEntry(i);
    if (scat == sigCat) {
      if (whichCuts==4){
	cut = (IsMuon && (PIDmu>0) && L0 && Hlt1TrackMuonDecision && (Mu_P>6000) && (Mu_PT>1100) && (Mu_ProbNNghost<0.2) && (Mu_IPChi2>35));
      } else {
	if (whichCuts>=1) cut = (IsMuon && (PIDmu>0));
	if (whichCuts>=2){
	  if (cut) cut = L0;
	  else continue;
	}
	if (whichCuts>=3){
	  if (cut) cut = Hlt1TrackMuonDecision && (Mu_P>6000) && (Mu_PT>1100) && (Mu_ProbNNghost<0.2) && (Mu_IPChi2>35);
	  else continue;
	}
      }
      if (cut){
	npass_x +=x;
	npass_y +=y;

	sxy1+= x*y;
	sx21+= x*x;
	sy21+= y*y;
      }
      else {
	nfail_x +=x;
	nfail_y +=y;

	sxy0+= x*y;
	sx20+= x*x;
	sy20+= y*y;
      }
    }
  }

  double eff_x = npass_x/(npass_x+nfail_x);
  npasserr_x = Sqrt(sx21);
  nfailerr_x = Sqrt(sx20);
  double efferr_x = Sqrt(Power(npass_x*nfailerr_x,2)+Power(nfail_x*npasserr_x,2))/Power(npass_x+nfail_x,2);
  double eff_y = npass_y/(npass_y+nfail_y);
  npasserr_y = Sqrt(sy21);
  nfailerr_y = Sqrt(sy20);
  double efferr_y = Sqrt(Power(npass_y*nfailerr_y,2)+Power(nfail_y*npasserr_y,2))/Power(npass_y+nfail_y,2);

  double rho_pass = sxy1/TMath::Sqrt(sx21*sy21);
  double rho_fail = sxy0/TMath::Sqrt(sx20*sy20);

  double ratio = eff_x/eff_y;
  double ratioerr_1 = ratio/Sqrt((npass_x+nfail_x)*(npass_y+nfail_y));
  double ratioerr_2 = (npass_y+nfail_y)/(npass_x+nfail_x)*( Power(nfail_x*npasserr_x/npass_x,2)+Power(nfailerr_x,2) );
  double ratioerr_3 = (npass_x+nfail_x)/(npass_y+nfail_y)*( Power(nfail_y*npasserr_y/npass_y,2)+Power(nfailerr_y,2) );
  double ratioerr_4 = 2*rho_pass*npasserr_x*npasserr_y*nfail_x*nfail_y/(npass_x*npass_y);
  double ratioerr_5 = 2*rho_fail*nfailerr_x*nfailerr_y;
  double ratioerr = ratioerr_1*Sqrt( ratioerr_2+ratioerr_3-ratioerr_4-ratioerr_5 );
  
  TString sp_x = "";
  for (int i=0;i<nw_x.Length();++i)
    sp_x.Append(" ");
  TString sp_y = "";
  for (int i=0;i<nw_y.Length();++i)
    sp_y.Append(" ");

  cout << "----"+nw_x+"    "+sp_y+"              eff   =         " << eff_x << "        +/-      " << efferr_x <<  endl;
  cout << "----"+nw_y+"    "+sp_x+"              eff   =         " << eff_y << "        +/-      " << efferr_y <<  endl;
  cout << "----"+nw_x+" vs "+nw_y+"         rho_pass   =         " << rho_pass << endl;
  cout << "----"+nw_x+" vs "+nw_y+"         rho_fail   =         " << rho_fail << endl;
  cout << "----"+sp_x+"    "+sp_y+" efficiency ratio   =         " << ratio << "        +/-      " << ratioerr << endl;
  tin->SetBranchStatus("*",1);
  
  return rho_pass;
}





int getEff_tagANDprobe(){
  //  TString pathIN = "/afs/cern.ch/user/d/dmanuzzi/Bs2Dmunu/Bs2DsBrRatio/selection/effMU/selected_tuples/weighted2Dall/";
  TString pathIN = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/tagANDprobe/";
  TChain *tin_CS_2011_up = new TChain("DecayTree");
  tin_CS_2011_up->Add(pathIN+"DataPIDCalib-2011-Bu2JpsiKUp_MuTagTOS.root");
  TChain *tin_CS_2011_dw = new TChain("DecayTree");
  tin_CS_2011_dw->Add(pathIN+"DataPIDCalib-2011-Bu2JpsiKDw_MuTagTOS.root");
  TChain *tin_CS_2012_up = new TChain("DecayTree");
  tin_CS_2012_up->Add(pathIN+"DataPIDCalib-2012-Bu2JpsiKUp_MuTagTOS.root");
  TChain *tin_CS_2012_dw = new TChain("DecayTree");
  tin_CS_2012_dw->Add(pathIN+"DataPIDCalib-2012-Bu2JpsiKDw_MuTagTOS.root");
  TChain *tin_CS[2][2];
  tin_CS[0][0] = tin_CS_2011_up;
  tin_CS[0][1] = tin_CS_2011_dw;
  tin_CS[1][0] = tin_CS_2012_up;
  tin_CS[1][1] = tin_CS_2012_dw;
  

  cout << "================================================================\n";
  cout << "*     Eff: PIDmu        (Tag & Probe)                          *\n";
  cout << "================================================================\n";

  TString nfout = "AllEffPIDmu.txt";
  TFile fout(nfout, "RECREATE");
  fout.Close();
  ostream pout(nfout, std::ios_base::app);
  TString mode[]  = {"BdKpipi", "BuKpipi"};
  TString year[]  = {"2011", "2012"};
  TString pol[]   = {"Up","Dw"};
  TString pythia[]= {"Pythia6", "Pyhiat8"};

  for (int i=0; i<2;++i){                  // loop on modes
    for (int j=0; j<2; ++j){               // loop on years
      for (int h=0; h<2; ++h){             // loop on pols
	for (int k=0; k<2; ++k){           // loop on pythias
	  if (mode[i].Contains("BdKpipi")){
	    for (l=1; l<=4; ++l){
	      TString nw = TString::Format("w_SigCat%d_%s_%s_%s_%s", l, mode[i], year[j], pol[h], pythia[k]); 
	      getEff(tin_CS[j][h], nw);
	    }
	  } else {
	    TString nw = TString::Format("w_SigCat0_%s_%s_%s_%s",  l, mode[i], year[j], pol[h], pythia[k]); 
	    getEff(tin_CS[j][h], nw);
	  }
	}
      }
    }
  }
  /*
  cout << " ============= BsSignal Cat1 2012 ============================================================== \n";
  getEff(tin_CS_2012_dw, "w_SigCat1_noTrigg_noPIDmu_BsSignal_2012_Dw_Pythia6");
  getEff(tin_CS_2012_dw, "w_SigCat1_noTrigg_noPIDmu_BsSignal_2012_Dw_Pythia8");
  getEff(tin_CS_2012_up, "w_SigCat1_noTrigg_noPIDmu_BsSignal_2012_Up_Pythia6");
  getEff(tin_CS_2012_up, "w_SigCat1_noTrigg_noPIDmu_BsSignal_2012_Up_Pythia8");
  cout << " ============= BsSignal Cat2 2012 ============================================================== \n";
  getEff(tin_CS_2012_dw, "w_SigCat2_noTrigg_noPIDmu_BsSignal_2012_Dw_Pythia6");
  getEff(tin_CS_2012_dw, "w_SigCat2_noTrigg_noPIDmu_BsSignal_2012_Dw_Pythia8");
  getEff(tin_CS_2012_up, "w_SigCat2_noTrigg_noPIDmu_BsSignal_2012_Up_Pythia6");
  getEff(tin_CS_2012_up, "w_SigCat2_noTrigg_noPIDmu_BsSignal_2012_Up_Pythia8");

  cout << " ============= BdKKpi Cat1 2012 ============================================================== \n";
  getEff(tin_CS_2012_dw, "w_SigCat1_noTrigg_noPIDmu_BdKKpi_2012_Dw_Pythia6");
  getEff(tin_CS_2012_dw, "w_SigCat1_noTrigg_noPIDmu_BdKKpi_2012_Dw_Pythia8");
  getEff(tin_CS_2012_up, "w_SigCat1_noTrigg_noPIDmu_BdKKpi_2012_Up_Pythia6");
  getEff(tin_CS_2012_up, "w_SigCat1_noTrigg_noPIDmu_BdKKpi_2012_Up_Pythia8");
  cout << " ============= BdKKpi Cat2 2012 ============================================================== \n";
  getEff(tin_CS_2012_dw, "w_SigCat2_noTrigg_noPIDmu_BdKKpi_2012_Dw_Pythia6");
  getEff(tin_CS_2012_dw, "w_SigCat2_noTrigg_noPIDmu_BdKKpi_2012_Dw_Pythia8");
  getEff(tin_CS_2012_up, "w_SigCat2_noTrigg_noPIDmu_BdKKpi_2012_Up_Pythia6");
  getEff(tin_CS_2012_up, "w_SigCat2_noTrigg_noPIDmu_BdKKpi_2012_Up_Pythia8");
 
  cout << " ============= BdKpipi Cat1 2011 ============================================================== \n";
  getEff(tin_CS_2011_dw, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2011_Dw_Pythia6");
  getEff(tin_CS_2011_dw, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2011_Dw_Pythia8");
  getEff(tin_CS_2011_up, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2011_Up_Pythia6");
  getEff(tin_CS_2011_up, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2011_Up_Pythia8");
  cout << " ============= BdKpipi Cat2 2011 ============================================================== \n";
  getEff(tin_CS_2011_dw, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2011_Dw_Pythia6");
  getEff(tin_CS_2011_dw, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2011_Dw_Pythia8");
  getEff(tin_CS_2011_up, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2011_Up_Pythia6");
  getEff(tin_CS_2011_up, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2011_Up_Pythia8");
  cout << " ============= BdKpipi Cat1 2012 ============================================================== \n";
  getEff(tin_CS_2012_dw, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2012_Dw_Pythia6");
  getEff(tin_CS_2012_dw, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2012_Dw_Pythia8");
  getEff(tin_CS_2012_up, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2012_Up_Pythia6");
  getEff(tin_CS_2012_up, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2012_Up_Pythia8");
  cout << " ============= BdKpipi Cat2 2012 ============================================================== \n";
  getEff(tin_CS_2012_dw, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2012_Dw_Pythia6");
  getEff(tin_CS_2012_dw, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2012_Dw_Pythia8");
  getEff(tin_CS_2012_up, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2012_Up_Pythia6");
  getEff(tin_CS_2012_up, "w_SigCat1_noTrigg_noPIDmu_BdKpipi_2012_Up_Pythia8");
  */  



  //  getEff(tin_CS_pos, "w_SigCat1_BsSignalUp_"+whichEff, "mu_minus",whichCuts);
  /*
  cout << " ============= Bs Cat2  -  "+whichEff+" ============== \n";
  getEff(tin_CS_neg, "w_SigCat2_BsSignal_"+whichEff, "mu_plus", whichCuts);
  //getEff(tin_CS_pos, "w_SigCat2_"+whichEff+"_BsSignal", "mu_minus",whichCuts);
  
  cout << " ============= Bd Cat1  -  "+whichEff+" ============== \n";
  getEff(tin_CS_neg, "w_SigCat1_BdKKpi_"+whichEff, "mu_plus", whichCuts);
  //  getEff(tin_CS_pos, "w_SigCat1_"+whichEff+"_BdKKpi", "mu_minus",whichCuts);

  cout << " ============= Bd Cat2  -  "+whichEff+" ============== \n";
  getEff(tin_CS_neg, "w_SigCat2_BdKKpi_"+whichEff, "mu_plus", whichCuts);
  //  getEff(tin_CS_pos, "w_SigCat2_"+whichEff+"_BdKKpi", "mu_minus",whichCuts);
  */

  /*
  getCorrelation(tin_CS_neg,
		 "w_SigCat1_BsSignal_"+whichEff, "w_SigCat2_BsSignal_"+whichEff,
		 "mu_plus", whichCuts);		 
  getCorrelation(tin_CS_pos, 
		 "w_SigCat1_BsSignal_"+whichEff, "w_SigCat2_BsSignal_"+whichEff,
   		 "mu_minus",whichCuts);
  cout << "----------------------------------------------------------------------------\n";
  getCorrelation(tin_CS_neg,
		 "w_SigCat1_BdKKpi_"+whichEff, "w_SigCat2_BdKKpi_"+whichEff,
		 "mu_plus", whichCuts);
  getCorrelation(tin_CS_pos,
		 "w_SigCat1_BdKKpi_"+whichEff, "w_SigCat2_BdKKpi_"+whichEff,
   		 "mu_minus", whichCuts);
  cout << "----------------------------------------------------------------------------\n";
  getCorrelation(tin_CS_neg,
		 "w_SigCat1_BsSignal_"+whichEff, "w_SigCat1_BdKKpi_"+whichEff,
		 "mu_plus", whichCuts); 
   getCorrelation(tin_CS_pos,
		 "w_SigCat1_BsSignal_"+whichEff, "w_SigCat1_BdKKpi_"+whichEff,
   		 "mu_minus", whichCuts); 
  cout << "----------------------------------------------------------------------------\n";
  getCorrelation(tin_CS_neg,
		 "w_SigCat2_BsSignal_"+whichEff, "w_SigCat2_BdKKpi_"+whichEff,
		 "mu_plus", whichCuts); 
   getCorrelation(tin_CS_pos,
		 "w_SigCat2_BsSignal_"+whichEff, "w_SigCat2_BdKKpi_"+whichEff,
   		 "mu_minus", whichCuts); 
  */
  
  return 1;
}
