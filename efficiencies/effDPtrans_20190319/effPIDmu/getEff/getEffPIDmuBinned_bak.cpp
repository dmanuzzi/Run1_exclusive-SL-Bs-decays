#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "TMath.h"
#include <fstream>
#include "TFile.h"
using namespace std;
using namespace TMath;
double getEff(TTree*tin, TString nw, TString nfout){
  
  tin->SetBranchStatus("*",0);
  double sw=1;
  tin->SetBranchStatus("nsig_sw", 1);
  tin->SetBranchAddress("nsig_sw", &sw);

  double w=1., we=1.;
  if (nw != "none"){
      tin->SetBranchStatus(nw, 1);
      tin->SetBranchAddress(nw, &w);
  }
  

  double PIDmu=0;
  double IsMuon=1;
  tin->SetBranchStatus("Mu_CombDLLmu",1);
  tin->SetBranchStatus("Mu_IsMuon",1);
  tin->SetBranchAddress("Mu_CombDLLmu", &PIDmu);
  tin->SetBranchAddress("Mu_IsMuon", &IsMuon);
   
  double npass=0., nfail=0., npasserr=0., nfailerr=0.;
  bool cut = false;
  for (int i=0; i<tin->GetEntries(); ++i){
    if (i>700000) break;
    tin->GetEntry(i);
    cut = ((bool)IsMuon && (PIDmu>0));
    if (cut){
      npass += w*sw;
      npasserr+=w*sw*w*sw;
    }
    else {
      nfail += w*sw;
      nfailerr += w*sw*w*sw; 
    }
  }

  double eff = npass/(npass+nfail);
  npasserr = Sqrt(npasserr);
  nfailerr = Sqrt(nfailerr);
  double efferr = Sqrt(Power(npass*nfailerr,2)+Power(nfail*npasserr,2))/Power(npass+nfail,2);
  cout << "--------------------------------------------\n";
  cout << "   "  <<  nw << endl;
  cout << " --- eff    =   " << eff      << "               efferr =             " << efferr<< endl;
  cout << "     pass   =   " << npass   <<  "               fail   =             " << nfail   << endl;
  cout << "     passerr=   " << npasserr<<  "               failerr=             " << nfailerr<<endl;
  cout << "getEntries  =   " << tin->GetEntries() << endl;
  tin->SetBranchStatus("*",1);

  ofstream pout(nfout, std::ios_base::app);
  pout << nw << " : eff = " << eff << " \\pm " << efferr << endl; 
  return eff;
}




int getEffPIDmuBinned(){
  //  TString pathIN = "/afs/cern.ch/user/d/dmanuzzi/Bs2Dmunu/Bs2DsBrRatio/selection/effMU/selected_tuples/weighted2Dall/";
  TString pathIN = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/tagANDprobe/";
  TChain *tin_CS_2011_up = new TChain("DecayTree");
  tin_CS_2011_up->Add(pathIN+"DataPIDCalib-2011-Bu2JpsiKUp_MuTagTOS.root");
  TChain *tin_CS_2011_dw = new TChain("DecayTree");
  tin_CS_2011_dw->Add(pathIN+"DataPIDCalib-2011-Bu2JpsiKDw_MuTagTOS.root");
  TChain *tin_CS_2012_up = new TChain("DecayTree");
  tin_CS_2012_up->Add(pathIN+"DataPIDCalib-2012-Bu2JpsiKUp_MuTagTOS.root");
  TChain *tin_CS_2012_dw = new TChain("DecayTree");
  tin_CS_2012_dw->Add(pathIN+"DataPIDCalib-2012-Bu2JpsiKDw_MuTagTOS.root");
  TChain *tin_CS[2][2];
  tin_CS[0][0] = tin_CS_2011_up;
  tin_CS[0][1] = tin_CS_2011_dw;
  tin_CS[1][0] = tin_CS_2012_up;
  tin_CS[1][1] = tin_CS_2012_dw;
  

  cout << "================================================================\n";
  cout << "*     Eff: PIDmu        (Tag & Probe)                          *\n";
  cout << "================================================================\n";

  TString nfout = "AllEffPIDmuBinned.txt";
  TFile fout(nfout, "RECREATE");
  fout.Close();

  TString mode[]  = {"BdKpipi", "BuKpipi"};
  TString year[]  = {"2011", "2012"};
  TString pol[]   = {"Up","Dw"};
  TString pythia[]= {"Pythia6", "Pythia8"};

  for (int i=0; i<2;++i){                  // loop on modes
    for (int j=0; j<2; ++j){               // loop on years
      for (int h=0; h<2; ++h){             // loop on pols
	for (int k=0; k<2; ++k){           // loop on pythias
	  for (int m=1; m<=6; ++m){        // loop on D_Ptrans bins
	    if (mode[i].Contains("BdKpipi")){
	      for (int l=1; l<=4; ++l){        // loop on signal categories
		TString nw = TString::Format("w_PIDmu_sigCat%d_%s_%s_%s_%s_bin%d", l, mode[i].Data(), year[j].Data(), pol[h].Data(), pythia[k].Data(), m); 
		getEff(tin_CS[j][h], nw, nfout);
	      }
	    } else {
	      TString nw = TString::Format("w_PIDmu_sigCat0_%s_%s_%s_%s_bin%d", mode[i].Data(), year[j].Data(), pol[h].Data(), pythia[k].Data(), m); 
	      getEff(tin_CS[j][h], nw, nfout);
	    }
	  }
	}
      }
    }
  }
  
  return 1;
}
