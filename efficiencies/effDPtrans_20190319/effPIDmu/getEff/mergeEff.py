
from ROOT import TChain, gROOT, TFile
import array as arr

gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/srcEff/getEff_D_Ptrans.cpp++')
getHist    = __import__('ROOT.getHist'   ,globals(),locals(),['getHist'])
createFout = __import__('ROOT.createFout',globals(),locals(),['createFout'])

def f_getEntries(mode, year, pol, pythia, cats, inPath, nfout):
    nfin = inPath+'noTrigg_noPIDmu/{mode}/Mag{pol}/MC-{year}-{mode}{pol}-noTrigg_noPIDmu_{pythia}Unfiltered_0.root'
    nfin = nfin.format(mode=mode, pol=pol, year=year, pythia=pythia)
    tin = TChain('DMutuple')
    tin.Add(nfin)
    tmp_cats = arr.array('i', cats)
    TAG = '%s_%s_%sUnfiltered_Mag%s'%(year,mode,pythia,pol)
    nw = "w_FF_corr"
    getHist(tin, len(cats), tmp_cats,  nfout, 'h_entries_'+TAG,  nw)
    f = TFile(nfout, 'READ')
    results = {}
    for cat in cats:
        results[cat] = []
        h = f.Get('h_entries_'+TAG+'_%d'%cat)
        Nbins = h.GetNbinsX()
        for i in range(1,Nbins+1):
            results[cat].append( h.GetBinContent(i) )
    return results

def f_getSavedEffPIDmu(mode, year, pol, pythia, cats):
    fin = open('AllEffPIDmuBinned.txt', 'r').read()
    ls_fin = fin.split('\n')[:-1]
    results = {}
    for cat in cats:
        results[cat] = [0,0,0,0,0,0]
        for i in range(0,6):
            for l in ls_fin:
                if (mode in l) and (year in l) and (pol in l) and (pythia in l) and ('sigCat%d'%(cat) in l) and ('bin%d'%(i+1) in l):
                    results[cat][i] = (float(l.split(' ')[-3:][0]))
    return results

def f_mergeEff(modes, years, pols, pythias, cats, inPath):
    print  modes, years, pols, pythias, cats
    num = {}
    den = {}
    eff = {}
    for cat in cats:
        num[cat] = [0,0,0,0,0,0]
        den[cat] = [0,0,0,0,0,0]
        eff[cat] = [0,0,0,0,0,0]
    for mode in modes:
        for year in years:
            for pol in pols:
                for pythia in pythias:
                    entries = f_getEntries(mode, year, pol, pythia, cats, inPath, nfout)
                    #print 'entries', entries
                    eff_tmp = f_getSavedEffPIDmu(mode, year, pol, pythia, cats)
                    #print 'eff_tmp', eff_tmp
                    for cat in cats:
                        for i in range(0,6):
                            num[cat][i] += entries[cat][i]*eff_tmp[cat][i]
                            den[cat][i] += entries[cat][i]
    for cat in cats:
        print '--------- SignalCategory == %d ----------'%(cat)
        for i in range(0,6):
            eff[cat][i] = float(num[cat][i])/float(den[cat][i])
        print 'Merged eff =    ', eff[cat]

    return eff








nfout = 'EffPIDmu-Kpipi-Merged.root'
createFout(nfout, 'RECREATE')
Source = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC_new/'
configs = [
    ( ['BuKpipi'], ['2011','2012'], ['Up', 'Dw'], ['Pythia6', 'Pythia8'], [0],       Source ),
    ( ['BdKpipi'], ['2011','2012'], ['Up', 'Dw'], ['Pythia6', 'Pythia8'], [1,2,3,4], Source )
    ]

for config in configs:
    modes,years,pols,pythias,cats,source = config
    f_mergeEff(modes,years,pols,pythias,cats,source)
    print '=============================================================='
