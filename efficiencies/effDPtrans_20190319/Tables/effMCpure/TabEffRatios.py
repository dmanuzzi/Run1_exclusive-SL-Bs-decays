import json
def f_printEffRatiosMC(nfin):
    fin = open(nfin,'r')
    din = json.loads(fin.read())
    fin.close()

    nbins = din.keys()
    tags = din[nbins[0]].keys()
    cats = din[nbins[0]][tags[0]].keys()
    tags = ['BdKpipi11', 'BdKpipi12', 'BdKKpi', 'BsSignal']
    cat = ''
    if 'cat1' in nfin:
        cat = '1'
    elif 'cat2' in nfin:
        cat = '2'
    nbins = ['bin%d'%(i) for i in range(1,len(nbins))] + ['tot']
    #print tags
    #print nbins
    #print cats
    print '\\begin{table}'
    print '\\centering'
    print '\\begin{tabular}{cc|ccccccc}'
    legend = ' &  '
    for nbin in nbins:
        legend += ' & \\textbf{%s}'%(nbin)
    legend += '  \\\\'
    print legend
    for tag2 in tags:
        print '\\hline'
        for tag1 in tags:
            line = '%s\\_i/%s\\_tot & cat=%s'%(tag1,tag2,cat)
            for nbin in nbins:
                r = din[nbin][tag1][cat][0]/din['tot'][tag2][cat][0]
                r_err = r*((din[nbin][tag1][cat][1]/din[nbin][tag1][cat][0])**2+(din['tot'][tag2][cat][1]/din['tot'][tag2][cat][0])**2)**(0.5)
                line += ' & $ %.3f \\pm %.3f $'%(r,r_err)
            line += '\\\\'
            print line
    print '\\hline'
    print '\\end{tabular}'
    print '\\caption{Eff. Ratios by MC only, cat=%s}'%(cat)
    print '\\end{table}'

f_printEffRatiosMC('effMconly_cat1.txt')
f_printEffRatiosMC('effMconly_cat2.txt')
