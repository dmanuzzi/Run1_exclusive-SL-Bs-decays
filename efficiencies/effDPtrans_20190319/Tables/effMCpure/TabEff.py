import json

def f_printTableEff(tag, cat):
    fin = open('yieldsMConly_%s.txt'%(tag), 'r')
    d = json.loads(fin.read())
    fin.close()
    cutstep = ['Ngen', 'noTrigg_noPID_noOffL', 'noTrigg_noPID', 'noTrigg_noPIDmu', 'noTrigg', 'noHLT' ,'noHLT2', 'FinalSelection']
#    cutstep = ['Ngen','noTrigg_noPID', 'noTrigg_noPIDmu']
    nbins = ['bin%d'%(i) for i in range(1,7)] + ['tot']  
    print '% ===== ', tag, '  sigCat = %s'%(cat)
    print '\\begin{table}'
    print '\\begin{tabular}{l|cccccccc}'
    print '\\hline'
    print ' & $\\epsilon_{P_{\perp}(D)}$&$\\epsilon_{0} (\%)$ & $\\epsilon_{Kin} (\%)$ & $\\epsilon_{PIDhad} (\%)$ & $\\epsilon_{PIDmu} (\%)$ & $\\epsilon_{L0} (\%)$ & $\\epsilon_{Hlt1} (\%)$ & $\\epsilon_{Hlt2} (\%)$  \\\\'
    print '\\hline'
    for nbin in nbins:
        line = ' '+nbin+' '
        effDPtrans = float(d['Ngen'][cat][nbin]['Ngen']) /  float(d['Ngen'][cat]['tot']['Ngen'])
        effDPtrans_err = (effDPtrans*(1-effDPtrans)/d['Ngen'][cat]['tot']['Ngen'])**(0.5)
        line += '& $%.4f \\pm %.4f$ '%(effDPtrans*100, effDPtrans_err*100)
        for i in range(0, len(cutstep)-1):
            Y_den = d[cutstep[i]][cat][nbin]
            if i == 0 :
                Y_den = d[cutstep[i]][cat][nbin]['Ngen']
            Y_num = d[cutstep[i+1]][cat][nbin]
            Eff = float(Y_num)/float(Y_den)
            Eff_err = (Eff*(1.0-Eff)/Y_den)**(0.5)
            if   i==0: line += '& $%.4f \\pm %.4f$ '%(Eff*100, Eff_err*100)
            elif i in [1,2,3,4,5]: line += '& $%.2f \\pm %.2f$ '%(Eff*100, Eff_err*100)
            elif i == 6: line += '& $%.1f \\pm %.1f$ '%(Eff*100, Eff_err*100)
        line +='\\\\'
        print line
    print '\\hline'
    print '\\end{tabular}'
    print '\\caption{%s, sigCat=%s}'%(tag,cat)
    print '\\end{table}'
    print '\n\n'
#####################################################

#f_printTableEff('BdKpipi12', '1')
'''
f_printTableEff('BdKpipi11', '2')
f_printTableEff('BdKpipi12', '1')
f_printTableEff('BdKpipi12', '2')
f_printTableEff('BdKKpi', '1')
f_printTableEff('BdKKpi', '2')
f_printTableEff('BsSignal', '1')
f_printTableEff('BsSignal', '2')
'''

#####################################################
#####################################################

def f_printTableEffStep(inputs, cutstepIn, cutstepOut, caption, nfout=False):
    ds = {}
    for tag,cats in inputs:
        #print tag, cats
#        fin = open('yieldsMConly_%s_20190226.txt'%(tag), 'r')
        fin = open('20190311_noPidCorr_forPIDmu/yieldsMConly_%s.txt'%(tag), 'r')
        ds[tag] = json.loads(fin.read())
        fin.close()
    dout = {}
    nbins = ['bin%d'%(i) for i in range(1,7)] + ['tot']  
    cs = ''
    l0 = ' '
    for tag, cats in inputs:
        for cat in cats:
            cs += 'c'
            l0 += ' & %s Cat=%s $(\%s)$'%(tag, cat,'%')
    l0 += ' \\\\'
    print '% ===== ', inputs, cutstepIn, cutstepOut 
    print '\\begin{table}'
    print '\\begin{tabular}{l|%s}'%(cs)
    print '\\hline'
    print l0
    print '\\hline'
    for nbin in nbins:
        dout[nbin] = {}
        line = ' '+nbin+' '
        for tag, cats in inputs:
            dout[nbin][tag] = {}
            for cat in cats:
                if cutstepOut == 'effDPtrans':
                    effDPtrans = ds[tag]['Ngen'][cat][nbin]['effDPtransCut']/ds[tag]['Ngen'][cat]['tot']['effDPtransCut']
                    effDPtrans_err = ds[tag]['Ngen'][cat][nbin]['effDPtransCut_err']/ ds[tag]['Ngen'][cat]['tot']['effDPtransCut']
                    line += '& $%.2f \\pm %.2f$ '%(effDPtrans*100, effDPtrans_err*100)
                    dout[nbin][tag][cat] = (effDPtrans, effDPtrans_err) 

                else:
                    Y_den_err = 0
                    if cutstepIn != 'effDPtrans' :
                        Y_den = ds[tag][cutstepIn][cat][nbin]
                    else: 
                        Y_den = ds[tag]['Ngen'][cat][nbin]['Ngen']
                        Y_den_err = ds[tag]['Ngen'][cat][nbin]['Ngen_err']
                    Y_num = ds[tag][cutstepOut][cat][nbin]
                    Eff = float(Y_num)/float(Y_den)
                    Eff_err = (Eff*(1.0-Eff)/Y_den)**(0.5)
                    if cutstepOut in ['noTrigg_noPID_noOffL']:
                        Eff_err2 = Eff*((Y_den_err/Y_den)**2+1.0/Y_num)**(0.5)
                        line += '& $%.3f \\pm %.3f~(%.3f)$ '%(Eff*100,Eff_err2*100, Eff_err*100)
                    elif cutstepOut in ['noTrigg_noPID', 'noTrigg_noPIDmu', 'noTrigg']:
                        line += '& $%.2f \\pm %.2f$ '%(Eff*100, Eff_err*100)
                    elif (cutstepIn in 'effDPtrans') and (cutstepOut in 'FinalSelection'):
                        Eff_err2 = Eff*((Y_den_err/Y_den)**2+1.0/Y_num)**(0.5)
                        line += '& $%.4f \\pm %.4f~(%.4f)$ '%(Eff*100,Eff_err2*100, Eff_err*100)
                    elif cutstepOut in ['noHLT', 'noHLT2', 'FinalSelection']:
                        line += '& $%.1f \\pm %.1f$ '%(Eff*100, Eff_err*100)
                    dout[nbin][tag][cat] = (Eff, Eff_err) 

        line +='\\\\'
        print line
    print '\\hline'
    print '\\end{tabular}'
    print '\\caption{%s}'%(caption)
    print '\\end{table}'
    print '\n\n'
    if nfout:
        fout = open(nfout, 'w')
        fout.write(json.dumps(dout))
        fout.close()

inputs1 = [
    ('BdKpipi11',('1')),
    ('BdKpipi12',('1')),
    ('BdKKpi'   ,('1')),
    ('BsSignal' ,('1')),
    ]
inputs2 = [
    ('BdKpipi11',('2')),
    ('BdKpipi12',('2')),
    ('BdKKpi'   ,('2')),
    ('BsSignal' ,('2')),
    ]

if 0:
    f_printTableEffStep(inputs1,'Ngen', 'effDPtrans', 'Eff. $N^{gen}_{tot} \\to N^{gen}_i$')
    f_printTableEffStep(inputs2,'Ngen', 'effDPtrans', 'Eff. $N^{gen}_{tot} \\to N^{gen}_i$')
    f_printTableEffStep(inputs1,'effDPtrans', 'noTrigg_noPID_noOffL', 'Eff. $N^{gen}_i \\to N_i^{TM}$')
    f_printTableEffStep(inputs2,'effDPtrans', 'noTrigg_noPID_noOffL', 'Eff. $N^{gen}_i \\to N_i^{TM}$')
    f_printTableEffStep(inputs1,'noTrigg_noPID_noOffL', 'noTrigg_noPID', 'Eff. $N_i^{TM} \\to N_i^{Kin}$')
    f_printTableEffStep(inputs2,'noTrigg_noPID_noOffL', 'noTrigg_noPID', 'Eff. $N_i^{TM} \\to N_i^{Kin}$')
    f_printTableEffStep(inputs1,'noTrigg_noPID', 'noTrigg_noPIDmu',  'Eff. $N_i^{Kin} \\to N_i^{PIDhad}$')
    f_printTableEffStep(inputs2,'noTrigg_noPID', 'noTrigg_noPIDmu',  'Eff. $N_i^{Kin} \\to N_i^{PIDhad}$')
    f_printTableEffStep(inputs1,'noTrigg_noPIDmu', 'noTrigg', 'Eff. $N_i^{PIDhad} \\to N_i^{PIDmu}$')
    f_printTableEffStep(inputs2,'noTrigg_noPIDmu', 'noTrigg', 'Eff. $N_i^{PIDhad} \\to N_i^{PIDmu}$')
    f_printTableEffStep(inputs1,'noTrigg', 'noHLT', 'Eff. $N_i^{PIDmu} \\to N_i^{L0}$')
    f_printTableEffStep(inputs2,'noTrigg', 'noHLT', 'Eff. $N_i^{PIDmu} \\to N_i^{L0}$')
    f_printTableEffStep(inputs1,'noHLT', 'noHLT2', 'Eff. $N_i^{L0} \\to N_i^{Hlt1}$')
    f_printTableEffStep(inputs2,'noHLT', 'noHLT2', 'Eff. $N_i^{L0} \\to N_i^{Hlt1}$')
    f_printTableEffStep(inputs1,'noHLT2', 'FinalSelection', 'Eff. $N_i^{Hlt1} \\to N_i^{pass}$')
    f_printTableEffStep(inputs2,'noHLT2', 'FinalSelection', 'Eff. $N_i^{Hlt1} \\to N_i^{pass}$')
    f_printTableEffStep(inputs1,'effDPtrans', 'FinalSelection', 'Eff. $N_i^{gen} \\to N_i^{pass}$', nfout='effMconly_cat1.txt')
    f_printTableEffStep(inputs2,'effDPtrans', 'FinalSelection', 'Eff. $N_i^{gen} \\to N_i^{pass}$', nfout='effMconly_cat2.txt')

if 0:
    f_printTableEffStep(inputs1,'noTrigg', 'noHLT2', 'Eff. $N_i^{PIDmu} \\to N_i^{Hlt1}$')
    f_printTableEffStep(inputs2,'noTrigg', 'noHLT2', 'Eff. $N_i^{PIDmi} \\to N_i^{Hlt1}$')
    f_printTableEffStep(inputs1,'noTrigg_noPIDmu', 'noHLT2', 'Eff. $N_i^{PIDhad} \\to N_i^{Hlt1}$')
    f_printTableEffStep(inputs2,'noTrigg_noPIDmu', 'noHLT2', 'Eff. $N_i^{PIDhad} \\to N_i^{Hlt1}$')

f_printTableEffStep(inputs1,'noTrigg_noPIDmu', 'noTrigg', 'Eff. $N_i^{PIDhad} \\to N_i^{PIDmu}$, noPIDCorr')
f_printTableEffStep(inputs2,'noTrigg_noPIDmu', 'noTrigg', 'Eff. $N_i^{PIDhad} \\to N_i^{PIDmu}$, noPIDCorr')


def f_printTableEffGenLev(inputs):
    ds = {}
    for tag,cats in inputs:
        #print tag, cats
        fin = open('yieldsMConly_%s.txt'%(tag), 'r')
        ds[tag] = json.loads(fin.read())
        fin.close()
    
    print '% ===== ', inputs,  
    print '\\begin{table}'
    print '\\begin{tabular}{l|cccc}'
    print '\\hline'
    print ' mode & $\\epsilon_{GenLevCut}$  \\\\'
    print '\\hline'
    for tag, cats in inputs:
        #Ngen = ds[tag]['Ngen'][cats[0]]['tot']['Ngen']
        #Ngen_err = ds[tag]['Ngen'][cats[0]]['tot']['Ngen_err']
        EffGenLev = ds[tag]['Ngen'][cats[0]]['tot']['effGenLevCut']
        EffGenLev_err = ds[tag]['Ngen'][cats[0]]['tot']['effGenLevCut_err']
        #Nbk = Ngen*EffGenLev
        line = '%s & $%.4f \\pm %.4f$ \\\\'%(tag,EffGenLev, EffGenLev_err)
        print line
    print '\\hline'
    print '\\end{tabular}'
    print '\\caption{Eff. of the generator level cuts}'
    print '\\end{table}'
    print '\n\n'

f_printTableEffGenLev(inputs1)
f_printTableEffGenLev(inputs2)
