import json

def f_printTableY(tag, cat):
    fin = open('yieldsMConly_%s.txt'%(tag), 'r')
    d = json.loads(fin.read())
    fin.close()
    cutstep = ['Ngen', 'noTrigg_noPID_noOffL', 'noTrigg_noPID', 'noTrigg_noPIDmu', 'noTrigg', 'noHLT' ,'noHLT2', 'FinalSelection']
    nbins = ['bin%d'%(i) for i in range(1,7)] + ['tot']  
    print '% ===== ', tag, '  sigCat = %s'%(cat)
    print '\\begin{table}'
    print '\\begin{tabular}{l|rrrrrrrr}'
    print '\\hline'
    print ' & $N_{gen}$ & $N_{TM}$ & $N_{Kin}$ & $N_{PIDhad}$ & $N_{PIDmu}$ & $N_{L0}$ & $N_{HLT1}$ & $N^{pass}$ \\\\'
    print '\\hline'
    for nbin in nbins:
        Y0 = d[cutstep[0]][cat][nbin]['Ngen']
        Y1 = d[cutstep[1]][cat][nbin]
        Y2 = d[cutstep[2]][cat][nbin]
        Y3 = d[cutstep[3]][cat][nbin]
        Y4 = d[cutstep[4]][cat][nbin]
        Y5 = d[cutstep[5]][cat][nbin]
        Y6 = d[cutstep[6]][cat][nbin]
        Y7 = d[cutstep[7]][cat][nbin]
        line = '%s & $%.0f$ & $%.0f$ & $%.0f$ & $%.0f$ & $%.0f$ & $%.0f$ & $%.0f$ & $%.0f$\\\\'%(nbin, Y0,Y1,Y2,Y3,Y4,Y5,Y6, Y7)
        print line
    print '\\hline'
    print '\\end{tabular}'
    print '\\caption{%s, sigCat=%s}'%(tag,cat)
    print '\\end{table}'
    print '\n\n'
#####################################################
f_printTableY('BdKpipi11', '1')
f_printTableY('BdKpipi11', '2')
f_printTableY('BdKpipi12', '1')
f_printTableY('BdKpipi12', '2')
f_printTableY('BdKKpi', '1')
f_printTableY('BdKKpi', '2')
f_printTableY('BsSignal', '1')
f_printTableY('BsSignal', '2')
