import json

def f_printTab_K_AB_CD(inputs, caption='', AB=False, CD=False, K=False):
    ds = {}
    fin = open('AllVars_20190314_MagUp_Pythia8.txt', 'r')
    ds = json.loads(fin.read())
    fin.close()
    ds.keys()
    nbins = ['bin%d'%(i) for i in range(1,7)] + ['tot']  
    cs = ''
    l0 = ' '
    for tag, cats in inputs:
        for cat in cats:
            cs += 'c'
            l0 += ' & %s Cat=%s $(\%s)$'%(tag, cat,'%')
    l0 += ' \\\\'
    print '% ===== ', inputs, AB, CD
    print '\\begin{table}'
    print '\\centering'
    print '\\begin{tabular}{l|%s}'%(cs)
    print '\\hline'
    print l0
    print '\\hline'
    for nbin in nbins:
        line = ' '+nbin+' '
        for tag, cats in inputs:
            for cat in cats:
                eff = 1
                eff_err = 0
                if K:
                    eff = ds[tag]['K'][cat][nbin]['K']
                    eff_err = ds[tag]['K'][cat][nbin]['K_err']
                if AB:
                    A = ds[tag]['A'][cat][nbin]['p']
                    B = ds[tag]['B'][cat][nbin]['f']
                    Aerr= ds[tag]['A'][cat][nbin]['p_err']
                    Berr= ds[tag]['B'][cat][nbin]['f_err']
                    effAB = A/(A+B) 
                    effAB_err = ((B*Aerr/(A+B)**2)**2 + (A*Berr/(A+B)**2)**2)**(0.5)
                    eff_box = eff
                    eff = eff_box*effAB
                    eff_err = eff*((eff_err/eff_box)**2+(effAB_err/effAB)**2)**(0.5)
                if CD:
                    C = ds[tag]['C'][cat][nbin]['p']
                    D = ds[tag]['D'][cat][nbin]['f']
                    Cerr = ds[tag]['C'][cat][nbin]['p_err']
                    Derr = ds[tag]['D'][cat][nbin]['f_err']
                    effCD = C/(C+D)
                    effCD_err = ( (D*Cerr/(C+D)**2)**2 + (C*Derr/(C+D)**2)**2)**(0.5)
                    eff_box = eff
                    eff = eff_box*effCD
                    eff_err = eff*((eff_err/eff_box)**2+(effCD_err/effCD)**2)**(0.5)
                if K:
                    line += '& $%.4f \\pm %.4f$ '%(eff*100, eff_err*100)
                else:
                    line += '& $%.2f \\pm %.2f$ '%(eff*100, eff_err*100)
        line +='\\\\'
        print line
    print '\\hline'
    print '\\end{tabular}'
    print '\\caption{%s}'%(caption)
    print '\\end{table}'
    print '\n\n'

inputs1 = [
    ('BdKpipi11', ('1')),
    ('BdKpipi12', ('1')),
    ('BdKKpi', ('1')),
    ('BsSignal', ('1'))
]
inputs2 = [
    ('BdKpipi11', ('2')),
    ('BdKpipi12', ('2')),
    ('BdKKpi', ('2')),
    ('BsSignal', ('2'))
]
if 1:
    f_printTab_K_AB_CD(inputs1, 'Tag\&Probe, EffTRIGGmu', AB=False, CD=True)
    f_printTab_K_AB_CD(inputs2, 'Tag\&Probe, EffTRIGGmu', AB=False, CD=True)

if 1:
    f_printTab_K_AB_CD(inputs1, 'K, i.e. Eff. by MC', AB=False, CD=False, K=True)
    f_printTab_K_AB_CD(inputs2, 'K, i.e. Eff. by MC', AB=False, CD=False, K=True)
if 1:
    f_printTab_K_AB_CD(inputs1, 'Total Eff.', AB=False, CD=True, K=True)
    f_printTab_K_AB_CD(inputs2, 'Total Eff.', AB=False, CD=True, K=True)
