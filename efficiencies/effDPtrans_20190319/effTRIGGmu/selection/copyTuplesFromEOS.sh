#!/bin/bash

for sample in 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_1.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_1.root' 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_2.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_2.root' 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_3.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_3.root' 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_4.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_4.root' 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_5.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_5.root' 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_6.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_6.root' 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_7.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_7.root' 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_8.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_8.root' 'Dimuon_strip21_MagDown_CharmoniumBottomonium_Lines_9.root'  'Dimuon_strip21_MagUp_CharmoniumBottomonium_Lines_9.root'; do
    
    path='root://eoslhcb.cern.ch//eos/lhcb/wg/RD/BsMuMu/ntuple_reproduction/data/stripping21/Charmonium_Bottomonium_ntuples'
   xrdcp $path/$sample /storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/original/$sample
done




for sample in 'Dimuon_strip21r1_MagDown_CharmoniumBottomonium_Lines_1.root'  'Dimuon_strip21r1_MagUp_CharmoniumBottomonium_Lines_1.root' 'Dimuon_strip21r1_MagDown_CharmoniumBottomonium_Lines_2.root'  'Dimuon_strip21r1_MagUp_CharmoniumBottomonium_Lines_2.root' 'Dimuon_strip21r1_MagDown_CharmoniumBottomonium_Lines_3.root'  'Dimuon_strip21r1_MagUp_CharmoniumBottomonium_Lines_3.root' 'Dimuon_strip21r1_MagDown_CharmoniumBottomonium_Lines_4.root'  'Dimuon_strip21r1_MagUp_CharmoniumBottomonium_Lines_4.root' 'Dimuon_strip21r1_MagDown_CharmoniumBottomonium_Lines_5.root'  'Dimuon_strip21r1_MagUp_CharmoniumBottomonium_Lines_5.root'; do

    path='root://eoslhcb.cern.ch//eos/lhcb/wg/RD/BsMuMu/ntuple_reproduction/data/stripping21r1/Charmonium_Bottomonium_ntuples'
    xrdcp $path/$sample /storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/original/$sample

done