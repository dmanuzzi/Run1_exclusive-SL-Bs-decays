mode = 'Data'
#mode = 'MC'
pathOUT = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/selected/'
import array as arr

####################################
####################################
####################################
import os
from ROOT import gROOT
gROOT.ProcessLine('.L ./performSelection.cpp++')
performSelection = __import__('ROOT.performSelection',globals(),locals(),['performSelection'])

#Set I/O name according to MC or Data mode
nfin  = ''
ntin  = '' 
years = ''
nfout = ''
if mode == 'MC':
    nfin = 'root://eoslhcb.cern.ch//eos/lhcb/wg/RD/BsMuMu/ntuple_reproduction/mc/B2JpsiK/MC{Y}_B2JpsiK_{polarity}_*.root'
    ntin = 'B2JpsiKTuple/DecayTree'
    years = [('2011','11',2), ('2012','12',4)]
    nfout = pathOUT+'MC-{year}-JPsiMuMu{pol}.root.bak'
elif mode == 'Data':
    nfin = 'root://eoslhcb.cern.ch//eos/lhcb/wg/RD/BsMuMu/ntuple_reproduction_July2016/data/{Y}/BsMuMu_data_Norm_{Y}_{polarity}*.root'
    ntin = 'B2JpsiKTuple/DecayTree'
    years = [('2011', 'S21r1',0),('2012', 'S21',0)]
    nfout = pathOUT+'Data-{year}-JPsiMuMu{pol}.root.bak'

# Run the selection
for pol, polarity in [('Up','MagUp'), ('Dw','MagDown')]:
    for year,Y,I in years:
        nfin_tmp = nfin.format(Y=Y, polarity=polarity)
        nfout_tmp= nfout.format(year=year,pol=pol) 
        performSelection(nfin_tmp,I, ntin, nfout_tmp)

#Merge the selection outputs
if mode == 'MC':
    os.system('hadd -f {path}/MC-2011-JPsiMuMu.root.bak {path}/MC-2011-JPsiMuMuDw.root.bak {path}/MC-2011-JPsiMuMuUp.root.bak'.format(path=pathOUT))
    os.system('hadd -f {path}/MC-2012-JPsiMuMu.root.bak {path}/MC-2012-JPsiMuMuDw.root.bak {path}/MC-2012-JPsiMuMuUp.root.bak'.format(path=pathOUT))
    os.system('hadd -f {path}/MC-JPsiMuMu.root.bak      {path}/MC-2011-JPsiMuMu.root.bak   {path}/MC-2012-JPsiMuMu.root.bak'.format(path=pathOUT))
elif mode == 'Data':
    os.system('hadd -f {path}/Data-2011-JPsiMuMu.root.bak    {path}/Data-2011-JPsiMuMuDw.root.bak   {path}/Data-2011-JPsiMuMuUp.root.bak'.format(path=pathOUT))
    os.system('hadd -f {path}/Data-2012-JPsiMuMu.root.bak    {path}/Data-2012-JPsiMuMuDw.root.bak   {path}/Data-2012-JPsiMuMuUp.root.bak'.format(path=pathOUT))
    os.system('hadd -f {path}/Data-JPsiMuMu.root.bak    {path}/Data-2011-JPsiMuMu.root.bak   {path}/Data-2012-JPsiMuMu.root.bak'.format(path=pathOUT))

