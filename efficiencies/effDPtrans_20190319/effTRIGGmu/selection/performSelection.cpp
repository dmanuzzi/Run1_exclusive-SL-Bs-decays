#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TH1D.h"
#include "TChain.h"
#include <iostream>
#include "TRandom3.h"
using namespace std;
int performSelection(TString nfin, int Nfin, TString ntin, TString nfout){
  TChain *tin = new TChain(ntin);
  cout << nfin << endl;
  if (Nfin){
    for (int i=1; i<=Nfin;++i){
      TString nin = nfin;
      nin.ReplaceAll("*", TString::Format("%d",i));
      tin->Add(nin);
      cout << nin << endl;
    }
  } else
    tin->Add(nfin);

  tin->SetBranchStatus("*",1);
  tin->SetBranchStatus("mu*_eta",1);
  tin->SetBranchStatus("mu*_PT",1);
  tin->SetBranchStatus("mu*_NShared",1);
  tin->SetBranchStatus("mu*_L0MuonDecision_T*S",1);
  tin->SetBranchStatus("mu*_Hlt1TrackMuonDecision_T*S",1); 
  tin->SetBranchStatus("mu*_Hlt1TrackAllL0Decision_T*S",1); 
  tin->SetBranchStatus("mu*_Hlt1SingleMuonHighPTDecision_T*S",1); 
  tin->SetBranchStatus("mu*_PIDmu",1);
  tin->SetBranchStatus("mu*_isMuon",1); 
  tin->SetBranchStatus("mu*_ProbNNmu",1); 
  tin->SetBranchStatus("mu*_IPCHI2_OWNPV",1); 
  tin->SetBranchStatus("mu*_IP*",1); 
  tin->SetBranchStatus("mu*_Hlt2*Topo*_T*S",1); 
  tin->SetBranchStatus("J_psi_1S_M", 1);
  tin->SetBranchStatus("B_M", 1);
  tin->SetBranchStatus("runNumber",1);
  tin->SetBranchStatus("eventNumber",1);
  tin->SetBranchStatus("nSPDHits",1);
  // tin->SetBranchStatus("L0Global",1);
  // tin->SetBranchStatus("Hlt1Global",1);
  // tin->SetBranchStatus("Hlt2Global",1);
  
  // tin->SetBranchStatus("*TRUEID",1);
  // tin->SetBranchStatus("*MOTHER*",1);
  // tin->SetBranchStatus("*BKGCAT*",1);


  Bool_t mu_L0MuonDecision_TOS[2], mu_Hlt1TrackAllL0Decision_TOS[2],  mu_Hlt1TrackMuonDecision_TOS[2],  mu_Hlt1SingleMuonHighPTDecision_TOS[2], mu_isMuon[2];
  Bool_t mu_Hlt2Topo2BodyBDTDecision_TOS[2], mu_Hlt2Topo3BodyBDTDecision_TOS[2];
  Bool_t mu_Hlt2Topo2BodyBDTDecision_TIS[2], mu_Hlt2Topo3BodyBDTDecision_TIS[2];
  Double_t mu_PIDmu[2], mu_IPCHI2_OWNPV[2], mu_PT[2], mu_ETA[2], mu_IP_OWNPV[2];
  Double_t J_psi_1S_M(0.), B_M(0.);
  Int_t  mu_NShared[2], nSPDHits(0);
  UInt_t runNumber = 0;
  ULong64_t eventNumber = 0;
  TString ncharge[2] = {"minus", "plus"};
  for (int j=0; j<2; ++j){
    tin->SetBranchAddress("mu"+ncharge[j]+"_PT", &(mu_PT[j]));
    tin->SetBranchAddress("mu"+ncharge[j]+"_eta", &(mu_ETA[j]));
    tin->SetBranchAddress("mu"+ncharge[j]+"_L0MuonDecision_TOS", &(mu_L0MuonDecision_TOS[j])); 
    tin->SetBranchAddress("mu"+ncharge[j]+"_Hlt1TrackMuonDecision_TOS", &(mu_Hlt1TrackMuonDecision_TOS[j])); 
    tin->SetBranchAddress("mu"+ncharge[j]+"_Hlt1TrackAllL0Decision_TOS", &(mu_Hlt1TrackAllL0Decision_TOS[j])); 
    tin->SetBranchAddress("mu"+ncharge[j]+"_Hlt1SingleMuonHighPTDecision_TOS", &(mu_Hlt1SingleMuonHighPTDecision_TOS[j])); 
    tin->SetBranchAddress("mu"+ncharge[j]+"_PIDmu", &(mu_PIDmu[j]));
    tin->SetBranchAddress("mu"+ncharge[j]+"_isMuon", &(mu_isMuon[j])); 
    tin->SetBranchAddress("mu"+ncharge[j]+"_IPCHI2_OWNPV", &(mu_IPCHI2_OWNPV[j])); 
    tin->SetBranchAddress("mu"+ncharge[j]+"_IP_OWNPV", &(mu_IP_OWNPV[j])); 
    tin->SetBranchAddress("mu"+ncharge[j]+"_NShared", &(mu_NShared[j]));
    tin->SetBranchAddress("mu"+ncharge[j]+"_Hlt2Topo2BodyBBDTDecision_TOS", &(mu_Hlt2Topo2BodyBDTDecision_TOS[j]));
    tin->SetBranchAddress("mu"+ncharge[j]+"_Hlt2Topo3BodyBBDTDecision_TOS", &(mu_Hlt2Topo3BodyBDTDecision_TOS[j]));
    tin->SetBranchAddress("mu"+ncharge[j]+"_Hlt2Topo2BodyBBDTDecision_TIS", &(mu_Hlt2Topo2BodyBDTDecision_TIS[j]));
    tin->SetBranchAddress("mu"+ncharge[j]+"_Hlt2Topo3BodyBBDTDecision_TIS", &(mu_Hlt2Topo3BodyBDTDecision_TIS[j]));

  }
  tin->SetBranchAddress("J_psi_1S_M", &J_psi_1S_M);
  tin->SetBranchAddress("B_M", &B_M);
  // tin->SetBranchAddress("runNumber", &runNumber);
  // tin->SetBranchAddress("eventNumber", &eventNumber);
  // tin->SetBranchAddress("nSPDHits", &nSPDHits);
  //int B_BKGCAT,  J_psi_1S_BKGCAT;
  //tin->SetBranchAddress("J_psi_1S_BKGCAT", &J_psi_1S_BKGCAT);
  //tin->SetBranchAddress("B_BKGCAT", &B_BKGCAT);


  TFile fout(nfout, "RECREATE");
  TTree *tout = tin->CloneTree(0);
  int nentries = tin->GetEntries();
  int ientry=0;
  //  TTree *tout = new TTree("DecayTree", "DecayTree");
  Bool_t TPmu_L0MuonDecision_TOS[2], TPmu_Hlt1TrackAllL0Decision_TOS[2], TPmu_Hlt1TrackMuonDecision_TOS[2],  TPmu_Hlt1SingleMuonHighPTDecision_TOS[2], TPmu_isMuon[2];
  Bool_t TPmu_Hlt2Topo2BodyBDTDecision_TOS[2], TPmu_Hlt2Topo3BodyBDTDecision_TOS[2];
  Bool_t TPmu_Hlt2Topo2BodyBDTDecision_TIS[2], TPmu_Hlt2Topo3BodyBDTDecision_TIS[2];
  Double_t TPmu_PIDmu[2], TPmu_IPCHI2_OWNPV[2], TPmu_PT[2], TPmu_ETA[2], TPmu_IP_OWNPV[2];
  Double_t TPJ_psi_1S_M(0.), TPB_M(0.);
  Int_t  TPmu_NShared[2], TPnSPDHits(0);
  UInt_t TPrunNumber = 0;
  ULong64_t TPeventNumber = 0;
  Int_t whichProbe = 0, whichTag = 0;
  TString ncat[2] = {"Tag", "Probe"};
  for (int j=0; j<2; ++j){
    tout->Branch("mu"+ncat[j]+"_PT", &(TPmu_PT[j]));
    tout->Branch("mu"+ncat[j]+"_eta", &(TPmu_ETA[j]));
    tout->Branch("mu"+ncat[j]+"_L0MuonDecision_TOS", &(TPmu_L0MuonDecision_TOS[j])); 
    tout->Branch("mu"+ncat[j]+"_Hlt1TrackMuonDecision_TOS", &(TPmu_Hlt1TrackMuonDecision_TOS[j])); 
    tout->Branch("mu"+ncat[j]+"_Hlt1TrackAllL0Decision_TOS", &(TPmu_Hlt1TrackAllL0Decision_TOS[j])); 
    tout->Branch("mu"+ncat[j]+"_Hlt1SingleMuonHighPTDecision_TOS", &(TPmu_Hlt1SingleMuonHighPTDecision_TOS[j])); 
    tout->Branch("mu"+ncat[j]+"_PIDmu", &(TPmu_PIDmu[j]));
    tout->Branch("mu"+ncat[j]+"_isMuon", &(TPmu_isMuon[j])); 
    tout->Branch("mu"+ncat[j]+"_IPCHI2_OWNPV", &(TPmu_IPCHI2_OWNPV[j])); 
    tout->Branch("mu"+ncat[j]+"_IP_OWNPV", &(TPmu_IP_OWNPV[j])); 
    tout->Branch("mu"+ncat[j]+"_NShared", &(TPmu_NShared[j]));
    tout->Branch("mu"+ncat[j]+"_Hlt2Topo2BodyBBDTDecision_TOS", &(TPmu_Hlt2Topo2BodyBDTDecision_TOS[j]));
    tout->Branch("mu"+ncat[j]+"_Hlt2Topo3BodyBBDTDecision_TOS", &(TPmu_Hlt2Topo3BodyBDTDecision_TOS[j]));
    tout->Branch("mu"+ncat[j]+"_Hlt2Topo2BodyBBDTDecision_TIS", &(TPmu_Hlt2Topo2BodyBDTDecision_TIS[j]));
    tout->Branch("mu"+ncat[j]+"_Hlt2Topo3BodyBBDTDecision_TIS", &(TPmu_Hlt2Topo3BodyBDTDecision_TIS[j]));
  }
  /*
  tout->Branch("J_psi_1S_M", &TPJ_psi_1S_M);
  tout->Branch("B_M", &TPB_M);
  tout->Branch("runNumber", &TPrunNumber);
  tout->Branch("eventNumber", &TPeventNumber);
  tout->Branch("nSPDHits", &TPeventNumber);
  */
  tout->Branch("whichProbe", &whichProbe);
  tout->Branch("whichTag", &whichTag);



  int cProbe(-1), cTag(-1);
  TH1D hout("h_JPsi_M", "h_JPsi_M", 100, 3030, 3170);
  TRandom3 rand;
  for (ientry=0; ientry<nentries; ++ientry){
    //    if (ientry>(double)nentries/20) break;
    if (ientry%10000==0){
      cout << "\r at entry " << ientry << " of " << nentries;
      cout.flush();
    }
    tin->GetEntry(ientry);
    
    if (rand.Rndm() > 0.5){
      cProbe = 0; //muminus
      cTag = 1; //muplus
    } else {
      cProbe = 1; //muplus
      cTag = 0; //muminus
    }
    int TP[2] = {cTag, cProbe};
    //    if (B_BKGCAT != 0) continue;
    //    if (J_psi_1S_BKGCAT != 0) continue;  
    if ( mu_PT[cProbe]      <  1000 ) continue;
    if ( mu_PT[cProbe]      > 14000 ) continue;
    if ( mu_ETA[cProbe]     <     2 ) continue;
    if ( mu_ETA[cProbe]     >   4.7 ) continue;
    if ( mu_PT[cTag]      <  1000 ) continue;
    if ( mu_PT[cTag]      > 14000 ) continue;
    if ( mu_ETA[cTag]     <     2 ) continue;
    if ( mu_ETA[cTag]     >   4.7 ) continue;
    
    if ( mu_NShared[cTag]   !=0 ) continue;
    if ( mu_NShared[cProbe] !=0 ) continue;

    if (B_M < 5240 || B_M > 5320) continue;
    // if (!(L0Global==1 && Hlt1Global==1 && Hlt2Global==1)) continue;
    hout.Fill(J_psi_1S_M);
    //    if (J_psi_1S_M < 3080 || J_psi_1S_M > 3120) continue;
    for (int j=0; j<2; ++j){
      TPmu_PT[j]      = mu_PT[TP[j]];
      TPmu_ETA[TP[j]] = mu_ETA[TP[j]];
      TPmu_L0MuonDecision_TOS[TP[j]]               = mu_L0MuonDecision_TOS[TP[j]]; 
      TPmu_Hlt1TrackMuonDecision_TOS[TP[j]]        = mu_Hlt1TrackMuonDecision_TOS[TP[j]]; 
      TPmu_Hlt1TrackAllL0Decision_TOS[TP[j]]       = mu_Hlt1TrackAllL0Decision_TOS[TP[j]]; 
      TPmu_Hlt1SingleMuonHighPTDecision_TOS[TP[j]] = mu_Hlt1SingleMuonHighPTDecision_TOS[TP[j]]; 
      TPmu_PIDmu[TP[j]] = mu_PIDmu[TP[j]];
      TPmu_isMuon[TP[j]]= mu_isMuon[TP[j]]; 
      TPmu_IPCHI2_OWNPV[TP[j]]= mu_IPCHI2_OWNPV[TP[j]]; 
      TPmu_IP_OWNPV[TP[j]]    = mu_IP_OWNPV[TP[j]]; 
      TPmu_NShared[TP[j]]     =  mu_NShared[TP[j]];
      TPmu_Hlt2Topo2BodyBDTDecision_TOS[TP[j]]= mu_Hlt2Topo2BodyBDTDecision_TOS[TP[j]];
      TPmu_Hlt2Topo3BodyBDTDecision_TOS[TP[j]]= mu_Hlt2Topo3BodyBDTDecision_TOS[TP[j]];
      TPmu_Hlt2Topo2BodyBDTDecision_TIS[TP[j]]= mu_Hlt2Topo2BodyBDTDecision_TIS[TP[j]];
      TPmu_Hlt2Topo3BodyBDTDecision_TIS[TP[j]]= mu_Hlt2Topo3BodyBDTDecision_TIS[TP[j]];
    }
    // TPJ_psi_1S_M=J_psi_1S_M;
    // TPB_M = B_M;
    // TPrunNumber = runNumber;
    // TPeventNumber = eventNumber;
    whichProbe = cProbe;
    whichTag = cTag;
    tout->Fill();
  }
  cout << "\r at entry " << ientry << " of " << nentries << endl;
  fout.WriteTObject(&hout, hout.GetName(),  "overwrite");
  fout.WriteTObject( tout, tout->GetName(), "overwrite");
  fout.Close();
  return 1;
}
