#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "TMath.h"
#include <fstream>
#include "TFile.h"
using namespace std;
using namespace TMath;
double getEff(TTree*tin, TString nw, TString nfout, TString probe="plus"){
  
  tin->SetBranchStatus("*",0);

  /*
  double sw=1;
  tin->SetBranchStatus("nsig_sw", 1);
  tin->SetBranchAddress("nsig_sw", &sw);
  */

  double w=1.;
  if (nw != "none"){
      tin->SetBranchStatus(nw, 1);
      tin->SetBranchAddress(nw, &w);
  }
  Bool_t muProbe_L0MuonDecision_TOS(false), muProbe_Hlt1TrackAllL0Decision_TOS(false), muProbe_Hlt1TrackMuonDecision_TOS(false), muProbe_Hlt1SingleMuonHighPTDecision_TOS(false);
  tin->SetBranchStatus("mu"+probe+"_L0MuonDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_L0MuonDecision_TOS", &muProbe_L0MuonDecision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1TrackMuonDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1TrackMuonDecision_TOS", &muProbe_Hlt1TrackMuonDecision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1TrackAllL0Decision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1TrackAllL0Decision_TOS", &muProbe_Hlt1TrackAllL0Decision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS", &muProbe_Hlt1SingleMuonHighPTDecision_TOS);
   
  double npass=0., nfail=0., npasserr=0., nfailerr=0.;
  bool cut = false;
  for (int i=0; i<tin->GetEntries(); ++i){
    tin->GetEntry(i);
    cut = (muProbe_L0MuonDecision_TOS && (muProbe_Hlt1TrackMuonDecision_TOS || muProbe_Hlt1TrackAllL0Decision_TOS || muProbe_Hlt1SingleMuonHighPTDecision_TOS));
    //cut = muProbe_L0MuonDecision_TOS;
    //if (!cut) continue;
    //cut = muProbe_Hlt1TrackMuonDecision_TOS || muProbe_Hlt1TrackAllL0Decision_TOS || muProbe_Hlt1SingleMuonHighPTDecision_TOS;
    if (cut){
      npass += w;
      npasserr+=w*w;
    }
    else {
      nfail += w;
      nfailerr += w*w; 
    }
  }

  double eff = npass/(npass+nfail);
  npasserr = Sqrt(npasserr);
  nfailerr = Sqrt(nfailerr);
  double efferr = Sqrt(Power(npass*nfailerr,2)+Power(nfail*npasserr,2))/Power(npass+nfail,2);
  cout << "--------------------------------------------\n";
  cout << "   "  <<  nw << endl;
  cout << " --- eff    =   " << eff      << "               efferr =             " << efferr<< endl;
  cout << "     pass   =   " << npass   <<  "               fail   =             " << nfail   << endl;
  cout << "     passerr=   " << npasserr<<  "               failerr=             " << nfailerr<<endl;
  cout << "getEntries  =   " << tin->GetEntries() << endl;
  tin->SetBranchStatus("*",1);

  ofstream pout(nfout, std::ios_base::app);
  pout << nw << " : eff = " << eff << " \\pm " << efferr << endl; 
  return eff;
}




int getEffTRIGGmuBinned_allmerged(){
  TString pathIN = "/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/selected/";
  TChain *tin_CS = new TChain("DecayTree");
  //tin_CS->Add(pathIN+"Data-JPsiMuMu.root");
  tin_CS->Add(pathIN+"MC-JPsiMuMu.root");

  cout << "================================================================\n";
  cout << "*     Eff: TRIGmu        (Tag & Probe)                          *\n";
  cout << "================================================================\n";

  TString nfout = "AllEffTRIGGmuBinned_allmerged.txt";
  TFile fout(nfout, "RECREATE");
  fout.Close();

  TString mode[]  = {"BdKpipi", "BuKpipi"};
  //TString mode[]  = {"BsSignal", "BdKKpi"};
  
  for (int i=0; i<2;++i){                  // loop on modes
    for (int m=1; m<=6; ++m){        // loop on D_Ptrans bins
      if (mode[i].Contains("BdKpipi") ||mode[i].Contains("BsSignal") ||mode[i].Contains("BdKKpi")){
	for (int l=1; l<=2; ++l){        // loop on signal categories
	  TString nw = TString::Format("w_TRIGGmu_sigCat%d_%s_bin%d", l, mode[i].Data(), m); 
	  getEff(tin_CS, nw, nfout);
	}
      } else {
	TString nw = TString::Format("w_TRIGGmu_sigCat0_%s_bin%d", mode[i].Data(), m); 
	getEff(tin_CS, nw, nfout);
      }
    }
  }
  return 1;
}
