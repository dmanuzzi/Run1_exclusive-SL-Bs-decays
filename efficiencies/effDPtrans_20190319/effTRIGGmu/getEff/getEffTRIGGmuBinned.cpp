#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "TMath.h"
#include <fstream>
#include "TFile.h"
using namespace std;
using namespace TMath;
double getEff_TRIGGmu(TTree *tin, TTree *tinW, TString nw, TString nfout, float *results){
  
  tin->SetBranchStatus("*",0);
  tinW->SetBranchStatus("*",0);

  double w=1., we=1.;
  if (nw != "none"){
      tinW->SetBranchStatus(nw, 1);
      tinW->SetBranchAddress(nw, &w);
  }
  //  tin->Print(nw+"*");
  TString probe = "plus";
  Bool_t muProbe_L0MuonDecision_TOS(false), muProbe_Hlt1TrackAllL0Decision_TOS(false), muProbe_Hlt1TrackMuonDecision_TOS(false), muProbe_Hlt1SingleMuonHighPTDecision_TOS(false);
  tin->SetBranchStatus("mu"+probe+"_L0MuonDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_L0MuonDecision_TOS", &muProbe_L0MuonDecision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1TrackMuonDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1TrackMuonDecision_TOS", &muProbe_Hlt1TrackMuonDecision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1TrackAllL0Decision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1TrackAllL0Decision_TOS", &muProbe_Hlt1TrackAllL0Decision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS", &muProbe_Hlt1SingleMuonHighPTDecision_TOS);
   
  double npass=0., nfail=0., npasserr=0., nfailerr=0.;
  bool cut = false;
  for (int i=0; i<tin->GetEntries(); ++i){
    tin->GetEntry(i);
    tinW->GetEntry(i);
    cut = (muProbe_L0MuonDecision_TOS && (muProbe_Hlt1TrackMuonDecision_TOS || muProbe_Hlt1TrackAllL0Decision_TOS || muProbe_Hlt1SingleMuonHighPTDecision_TOS));
    if (cut){
      npass += w;
      npasserr+=w*w;
    }
    else {
      nfail += w;
      nfailerr += w*w; 
    }
  }

  double eff = npass/(npass+nfail);
  npasserr = Sqrt(npasserr);
  nfailerr = Sqrt(nfailerr);
  double efferr = Sqrt(Power(npass*nfailerr,2)+Power(nfail*npasserr,2))/Power(npass+nfail,2);
  cout << "--------------------------------------------\n";
  cout << "   "  <<  nw << endl;
  cout << " --- eff    =   " << eff      << "               efferr =             " << efferr<< endl;
  cout << "     pass   =   " << npass   <<  "               fail   =             " << nfail   << endl;
  cout << "     passerr=   " << npasserr<<  "               failerr=             " << nfailerr<<endl;
  cout << "getEntries  =   " << tin->GetEntries() << endl;
  tin->SetBranchStatus("*",1);

  ofstream pout(nfout, std::ios_base::app);
  pout << nw << " : eff = " << eff << " \\pm " << efferr << "     npass = " << npass << "      nfail = " << nfail << endl; 
  results[0] = eff;
  results[1] = efferr;
  results[2] = npass;
  results[3] = nfail;
  results[4] = npasserr;
  results[5] = nfailerr;
  return eff;
}


double getCov_TRIGGmu(TTree *tin, TTree *tinX, TTree *tinY, TString nw_x, TString nw_y, float *results,  TString nfout){

  tin->SetBranchStatus("*",0);
  tinX->SetBranchStatus("*",0);
  tinY->SetBranchStatus("*",0);
  
  double x=1., y=1.;
  double *px = &x;
  double *py = &y;
  if (nw_x == nw_y)  py = &x;
  if ((nw_x != "none") && (nw_y != "none")){
    tinX->SetBranchStatus(nw_x, 1);
    tinX->SetBranchAddress(nw_x, px);
    tinY->SetBranchStatus(nw_y, 1);
    tinY->SetBranchAddress(nw_y, py);
  }
  TString probe = "plus";
  Bool_t muProbe_L0MuonDecision_TOS(false), muProbe_Hlt1TrackAllL0Decision_TOS(false), muProbe_Hlt1TrackMuonDecision_TOS(false), muProbe_Hlt1SingleMuonHighPTDecision_TOS(false);
  tin->SetBranchStatus("mu"+probe+"_L0MuonDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_L0MuonDecision_TOS", &muProbe_L0MuonDecision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1TrackMuonDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1TrackMuonDecision_TOS", &muProbe_Hlt1TrackMuonDecision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1TrackAllL0Decision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1TrackAllL0Decision_TOS", &muProbe_Hlt1TrackAllL0Decision_TOS);
  tin->SetBranchStatus("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS",1);
  tin->SetBranchAddress("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS", &muProbe_Hlt1SingleMuonHighPTDecision_TOS);


  double sxy0=0.; //fail
  double sxy1=0.; //pass
  double npass_x=0., nfail_x=0.;
  double npass_y=0., nfail_y=0.;
  double npass_x_err = 0., nfail_x_err = 0.;
  double npass_y_err = 0., nfail_y_err = 0.;
  bool cut = false;
  for (int i=0; i<tin->GetEntries(); ++i){
    cout << "\r" << i;
    tin->GetEntry(i);
    tinX->GetEntry(i);
    tinY->GetEntry(i);
    cut = (muProbe_L0MuonDecision_TOS && (muProbe_Hlt1TrackMuonDecision_TOS || muProbe_Hlt1TrackAllL0Decision_TOS || muProbe_Hlt1SingleMuonHighPTDecision_TOS));
    if (cut){
      npass_x += (*px);
      npass_y += (*py);
      npass_x_err += (*px)*(*px);
      npass_y_err += (*py)*(*py);
      sxy1 += (*px)*(*py);
    }
    else {
      nfail_x += (*px);
      nfail_y += (*py);
      nfail_x_err += (*px)*(*px);
      nfail_y_err += (*py)*(*py);

      sxy0 += (*px)*(*py);
    }
  }
  cout << endl;
  
  double eff_x = npass_x/(npass_x+nfail_x);
  double eff_y = npass_y/(npass_y+nfail_y);
  npass_x_err = TMath::Sqrt(npass_x_err);
  npass_y_err = TMath::Sqrt(npass_y_err);
  nfail_x_err = TMath::Sqrt(nfail_x_err);
  nfail_y_err = TMath::Sqrt(nfail_y_err);
  double cov_pass = sxy1;
  double cov_fail = sxy0;
  double cor_pass = cov_pass/(npass_x_err*npass_y_err);
  double cor_fail = cov_fail/(nfail_x_err*nfail_y_err);

  results[0] = eff_x;
  results[1] = eff_y;
  results[2] = cov_pass;
  results[3] = cov_fail;
  
  TString sp_x = "";
  for (int i=0;i<nw_x.Length();++i)
    sp_x.Append(" ");
  TString sp_y = "";
  for (int i=0;i<nw_y.Length();++i)
    sp_y.Append(" ");

  cout << "----"+nw_x+"    "+sp_y+"              eff1  =         " << eff_x <<  endl;
  cout << "----"+nw_y+"    "+sp_x+"              eff2  =         " << eff_y <<  endl;
  cout << "----"+nw_x+" vs "+nw_y+"         cov_pass   =         " << cov_pass << endl;
  cout << "----"+nw_x+" vs "+nw_y+"         cov_fail   =         " << cov_fail << endl;
  cout << "----"+nw_x+" vs "+nw_y+"         cor_pass   =         " << cor_pass << endl;
  cout << "----"+nw_x+" vs "+nw_y+"         cor_fail   =         " << cor_fail << endl;
  tin->SetBranchStatus("*",1);

  ofstream pout(nfout, std::ios_base::app);
  pout << nw_x << " vs " << nw_y << " --> eff1 = " << eff_x << "   eff2 = " << eff_y << "    cov_pass = " << cov_pass << "   cov_fail = " << cov_fail << endl; 


  return 1;
}







int getEffTRIGGmuBinned(){
  /*
  TString pathIN = "/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/selected/";
  TChain *tin_CS_2011 = new TChain("DecayTree");
  // tin_CS_2011->Add(pathIN+"MC-2011-JPsiMuMuUp.root");
  // tin_CS_2011->Add(pathIN+"MC-2011-JPsiMuMuDw.root");
  tin_CS_2011->Add(pathIN+"Data-JPsiMuMuUp.root");
  tin_CS_2011->Add(pathIN+"Data-JPsiMuMuDw.root");
  TChain *tin_CS_2012 = new TChain("DecayTree");
  // tin_CS_2012->Add(pathIN+"MC-2012-JPsiMuMuUp.root");
  // tin_CS_2012->Add(pathIN+"MC-2012-JPsiMuMuDw.root");
  tin_CS_2012->Add(pathIN+"Data-JPsiMuMuUp.root");
  tin_CS_2012->Add(pathIN+"Data-JPsiMuMuDw.root");

  TChain* tin_CS[2];
  tin_CS[0] = tin_CS_2011;
  tin_CS[1] = tin_CS_2012;
  

  cout << "================================================================\n";
  cout << "*     Eff: TRIGmu        (Tag & Probe)                          *\n";
  cout << "================================================================\n";

  TString nfout = "AllEffTRIGGmuBinned.txt";
  TFile fout(nfout, "RECREATE");
  fout.Close();

  //  TString mode[]  = {"BdKpipi", "BuKpipi"};
  TString mode[]  = {"BdKpipi", "BdKKpi", "BsSignal"};
  TString year[]  = {"2011", "2012"};
  float results[2]= {0.,0.};
  for (int i=0; i<3;++i){                  // loop on modes
    for (int j=0; j<2; ++j){               // loop on years
      if (mode[i] != "BdKpipi" && year[j] == "2011") continue;
      for (int m=1; m<=6; ++m){        // loop on D_Ptrans bins
	if (mode[i].Contains("BsSignal") || mode[i].Contains("BdKKpi") || mode[i].Contains("BdKpipi")){
	  for (int l=1; l<=2; ++l){        // loop on signal categories
	    TString nw = TString::Format("w_TRIGGmu_sigCat%d_%s_%s_bin%d", l, mode[i].Data(), year[j].Data(), m); 
	    getEff_TRIGGmu(tin_CS[j], nw, nfout, results);
	    cout << "======> eff = " << results[0] << "+/-" << results[1] << endl;
	  }
	} else {
	  TString nw = TString::Format("w_TRIGGmu_sigCat0_%s_%s_bin%d", mode[i].Data(), year[j].Data(), m); 
	  getEff_TRIGGmu(tin_CS[j], nw, nfout, results);
	  cout << "======> eff = " << results[0] << "+/-" << results[1] << endl;
	}
      }
    }
  }
  */  
  return 1;
}
