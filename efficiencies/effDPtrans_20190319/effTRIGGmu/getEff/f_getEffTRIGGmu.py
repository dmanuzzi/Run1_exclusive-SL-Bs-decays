from ROOT import TChain, gROOT
import array as arr
gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/getEff/getEffTRIGGmuBinned.cpp++')
getEff_TRIGGmu    = __import__('ROOT.getEff_TRIGGmu'   ,globals(),locals(),['getEff_TRIGGmu'])
def f_getEffTRIGGmu(CSs, nfout, tag, years, pols, cats, bins):
    results = {}
    tin_CS = TChain('DecayTree')
    for year in years:
        for pol in pols:
            tin_CS.Add(CSs[year][pol]['nfin'])
    
    print '***********************************************'
    print '*  Eff: TRIGGmu      (Tag & Probe)            *'
    print '***********************************************'
    resultsP = {}
    resultsF = {}
    for cat in cats:
        resultsP[cat] = {}
        resultsF[cat] = {}
        nbins =  ['bin%d'%i for i in range(1, len(bins))]
        nbins += ['tot']
        for nbin in nbins:
            nw = 'w_TRIGGmu_sigCat%d_%s_%s'%(cat, tag, nbin)
            res = arr.array('f', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            tin_w = TChain("DecayTree")
            for year in years:
                for pol in pols:
                    nFriend = CSs[year][pol]['nfin'].replace('.root','_'+nw+'.root')
                    tin_w.Add(nFriend)
            getEff_TRIGGmu(tin_CS, tin_w, nw, nfout, res)
            print res[2], res[3]
            resultsP[cat][nbin] = {'p' : res[2], 'p_err' : res[4]}
            resultsF[cat][nbin] = {'f' : res[3], 'f_err' : res[5]}
        
    return resultsP, resultsF
