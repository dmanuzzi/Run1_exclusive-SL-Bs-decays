#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "TMath.h"
#include <fstream>
#include "TFile.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"
#include "plot2Hratio.cpp" 
using namespace std;
using namespace TMath;


double effCurve(){
  TH1::SetDefaultSumw2();
  TString pathin = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/B2JpsiK_mumu/selected/";
  TChain *tCS = new TChain("DecayTree");
  //  tCS->Add(pathin+"Data-2011-JPsiMuMuDw.root");
  //  tCS->Add(pathin+"Data-2011-JPsiMuMuUp.root");
  tCS->Add(pathin+"Data-2012-JPsiMuMuDw.root");
  tCS->Add(pathin+"Data-2012-JPsiMuMuUp.root");

  tCS->SetBranchStatus("*",0);
  TString probe = "plus";
  double Mu_PT=0.,Mu_ETA=0.;
  tCS->SetBranchStatus("mu"+probe+"_PT",1);
  tCS->SetBranchAddress("mu"+probe+"_PT", &Mu_PT);
  tCS->SetBranchStatus("mu"+probe+"_eta",1);
  tCS->SetBranchAddress("mu"+probe+"_eta", &Mu_ETA);
  Bool_t muProbe_L0MuonDecision_TOS(false), muProbe_Hlt1TrackAllL0Decision_TOS(false), muProbe_Hlt1TrackMuonDecision_TOS(false), muProbe_Hlt1SingleMuonHighPTDecision_TOS(false);
  tCS->SetBranchStatus("mu"+probe+"_L0MuonDecision_TOS",1);
  tCS->SetBranchAddress("mu"+probe+"_L0MuonDecision_TOS", &muProbe_L0MuonDecision_TOS);
  tCS->SetBranchStatus("mu"+probe+"_Hlt1TrackMuonDecision_TOS",1);
  tCS->SetBranchAddress("mu"+probe+"_Hlt1TrackMuonDecision_TOS", &muProbe_Hlt1TrackMuonDecision_TOS);
  tCS->SetBranchStatus("mu"+probe+"_Hlt1TrackAllL0Decision_TOS",1);
  tCS->SetBranchAddress("mu"+probe+"_Hlt1TrackAllL0Decision_TOS", &muProbe_Hlt1TrackAllL0Decision_TOS);
  tCS->SetBranchStatus("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS",1);
  tCS->SetBranchAddress("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS", &muProbe_Hlt1SingleMuonHighPTDecision_TOS);
  bool cut = false;
  TH1D *hPT_pass = new TH1D("hPT_pass", "hPT_pass", 12, 250, 14000);
  TH1D *hETA_pass= new TH1D("hETA_pass", "hETA_pass", 6,1.5,5.5);
  TH2D *h2D_pass = new TH2D("h2D_pass", "h2D_pass", 12,250,14000, 6,1.5,5.5);
  TH1D *hPT_den = new TH1D("hPT_den", "hPT_den", 12, 250, 14000);
  TH1D *hETA_den= new TH1D("hETA_den", "hETA_den", 6,1.5,5.5);
  TH2D *h2D_den = new TH2D("h2D_den", "h2D_den", 12,250,14000, 6,1.5,5.5); 
  for (int i=0; i<tCS->GetEntries(); ++i){
    tCS->GetEntry(i);
    hPT_den->Fill(Mu_PT);
    hETA_den->Fill(Mu_ETA);
    h2D_den->Fill(Mu_PT, Mu_ETA);
    cut = (muProbe_L0MuonDecision_TOS && (muProbe_Hlt1TrackMuonDecision_TOS || muProbe_Hlt1TrackAllL0Decision_TOS || muProbe_Hlt1SingleMuonHighPTDecision_TOS));
    if (cut){
      hPT_pass->Fill(Mu_PT);
      hETA_pass->Fill(Mu_ETA);
      h2D_pass->Fill(Mu_PT, Mu_ETA);
    }
  }
  TH1D *hPT = new TH1D("hPT", "Efficiency(p_{T}(#mu))", 12, 250, 14000);
  TH1D *hETA= new TH1D("hETA", "Efficiency(#eta(#mu))", 6,1.5,5.5);
  TH2D *h2D = new TH2D("h2D", "Data: Efficiency(p_{T}(#mu), #eta(#mu))", 12,250,14000, 6,1.5,5.5);
  TH2D *eh2D = new TH2D("eh2D", "Data: errors on Efficiency(p_{T}(#mu), #eta(#mu))", 12,250,14000, 6,1.5,5.5);
  hPT->Divide(hPT_pass, hPT_den, 1, 1, "B");
  hPT->SetMinimum(0.);
  hPT->SetMaximum(1.05);
  hPT->GetYaxis()->SetTitle("Eff.");
  hPT->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  hETA->Divide(hETA_pass, hETA_den, 1, 1, "B");
  hETA->SetMinimum(0.);
  hETA->SetMaximum(1.05);
  hETA->GetYaxis()->SetTitle("Eff.");
  hETA->GetXaxis()->SetTitle("#eta(#mu)");
  h2D->Divide(h2D_pass, h2D_den, 1, 1, "B");
  h2D->SetMinimum(0.);
  h2D->SetMaximum(1.);
  h2D->GetYaxis()->SetTitle("#eta(#mu)");
  h2D->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  eh2D->GetYaxis()->SetTitle("#eta(#mu)");
  eh2D->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  //  eh2D->SetMaximum(0.6);
  for (int  i=1; i<=12;++i){
    for (int j=1; j<=6;++j){
      eh2D->SetBinContent(i,j, h2D->GetBinError(i,j));
    }
  }
  //====================================================================
  //====================================================================
  TChain *tMC = new TChain("DecayTree");
  //  tMC->Add(pathin+"Data-2011-JPsiMuMuDw.root");
  //  tMC->Add(pathin+"Data-2011-JPsiMuMuUp.root");
  tMC->Add(pathin+"MC-2012-JPsiMuMuDw.root.bak");
  tMC->Add(pathin+"MC-2012-JPsiMuMuUp.root.bak");

  tMC->SetBranchStatus("*",0);
  tMC->SetBranchStatus("mu"+probe+"_PT",1);
  tMC->SetBranchAddress("mu"+probe+"_PT", &Mu_PT);
  tMC->SetBranchStatus("mu"+probe+"_eta",1);
  tMC->SetBranchAddress("mu"+probe+"_eta", &Mu_ETA);
  tMC->SetBranchStatus("mu"+probe+"_L0MuonDecision_TOS",1);
  tMC->SetBranchAddress("mu"+probe+"_L0MuonDecision_TOS", &muProbe_L0MuonDecision_TOS);
  tMC->SetBranchStatus("mu"+probe+"_Hlt1TrackMuonDecision_TOS",1);
  tMC->SetBranchAddress("mu"+probe+"_Hlt1TrackMuonDecision_TOS", &muProbe_Hlt1TrackMuonDecision_TOS);
  tMC->SetBranchStatus("mu"+probe+"_Hlt1TrackAllL0Decision_TOS",1);
  tMC->SetBranchAddress("mu"+probe+"_Hlt1TrackAllL0Decision_TOS", &muProbe_Hlt1TrackAllL0Decision_TOS);
  tMC->SetBranchStatus("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS",1);
  tMC->SetBranchAddress("mu"+probe+"_Hlt1SingleMuonHighPTDecision_TOS", &muProbe_Hlt1SingleMuonHighPTDecision_TOS);
  TH1D *hMCPT_pass = new TH1D("hMCPT_pass", "hMCPT_pass", 12, 250, 14000);
  TH1D *hMCETA_pass= new TH1D("hMCETA_pass", "hMCETA_pass", 6,1.5,5.5);
  TH2D *hMC2D_pass = new TH2D("hMC2D_pass", "hMC2D_pass", 12,250,14000, 6,1.5,5.5);
  TH1D *hMCPT_den = new TH1D("hMCPT_den", "hMCPT_den", 12, 250, 14000);
  TH1D *hMCETA_den= new TH1D("hMCETA_den", "hMCETA_den", 6,1.5,5.5);
  TH2D *hMC2D_den = new TH2D("hMC2D_den", "hMC2D_den", 12,250,14000, 6,1.5,5.5); 
  for (int i=0; i<tMC->GetEntries(); ++i){
    tMC->GetEntry(i);
    hMCPT_den->Fill(Mu_PT);
    hMCETA_den->Fill(Mu_ETA);
    hMC2D_den->Fill(Mu_PT, Mu_ETA);
    cut = (muProbe_L0MuonDecision_TOS && (muProbe_Hlt1TrackMuonDecision_TOS || muProbe_Hlt1TrackAllL0Decision_TOS || muProbe_Hlt1SingleMuonHighPTDecision_TOS));
    if (cut){
      hMCPT_pass->Fill(Mu_PT);
      hMCETA_pass->Fill(Mu_ETA);
      hMC2D_pass->Fill(Mu_PT, Mu_ETA);
    }
  }
  TH1D *hMCPT = new TH1D("hMCPT", "Efficiency(p_{T}(#mu))", 12, 250, 14000);
  TH1D *hMCETA= new TH1D("hMCETA", "Efficiency(#eta(#mu))", 6,1.5,5.5);
  TH2D *hMC2D = new TH2D("hMC2D", "MC: Efficiency(p_{T}(#mu), #eta(#mu))", 12,250,14000, 6,1.5,5.5);
  TH2D *ehMC2D = new TH2D("ehMC2D", "MC: errors on Efficiency(p_{T}(#mu), #eta(#mu))", 12,250,14000, 6,1.5,5.5);
  hMCPT->Divide(hMCPT_pass, hMCPT_den, 1, 1, "B");
  hMCPT->SetMinimum(0.);
  hMCPT->SetMaximum(1.05);
  hMCPT->GetYaxis()->SetTitle("Eff.");
  hMCPT->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  hMCETA->Divide(hMCETA_pass, hMCETA_den, 1, 1, "B");
  hMCETA->SetMinimum(0.);
  hMCETA->SetMaximum(1.05);
  hMCETA->GetYaxis()->SetTitle("Eff.");
  hMCETA->GetXaxis()->SetTitle("#eta(#mu)");
  hMC2D->Divide(hMC2D_pass, hMC2D_den, 1, 1, "B");
  hMC2D->GetYaxis()->SetTitle("#eta(#mu)");
  hMC2D->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  h2D->SetMinimum(0.);
  h2D->SetMaximum(1.);
  ehMC2D->GetYaxis()->SetTitle("#eta(#mu)");
  ehMC2D->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  //  ehMC2D->SetMaximum(0.6);
  for (int  i=1; i<=12;++i){
    for (int j=1; j<=6;++j){
      ehMC2D->SetBinContent(i,j, hMC2D->GetBinError(i,j));
    }
  }
  //====================================================================
  //====================================================================
  plot2Hratio(hPT, hMCPT, "Mu_PT", "effCurve.root", "B2JPsiK_2012");
  plot2Hratio(hETA, hMCETA, "Mu_ETA", "effCurve.root", "B2JPsiK_2012");
  //====================================================================
  //====================================================================
  //====================================================================
  //====================================================================
  gStyle->SetOptStat(0);
  TCanvas *c1 = new TCanvas("c1", "c1", 950,700);
  gStyle->SetPaintTextFormat(".4f");
  h2D->Draw("COLORZ");
  eh2D->SetLineColor(kRed);
  eh2D->Draw("BOX SAME");
  h2D->Draw("TEXT SAME");
  TCanvas *c1bis = new TCanvas("c1bis", "c1bis", 950,700);
  eh2D->Draw("COLORZ TEXT");
  TCanvas *c1MC = new TCanvas("c1MC", "c1MC", 950,700);
  hMC2D->Draw("COLORZ");
  ehMC2D->SetLineColor(kRed);
  ehMC2D->Draw("BOX SAME");
  hMC2D->Draw("TEXT SAME");
  TCanvas *c1bisMC = new TCanvas("c1bisMC", "c1bisMC", 950,700);
  ehMC2D->Draw("COLORZ TEXT");
  /*
  TCanvas *c2 = new TCanvas("c2", "c2", 800,700);
  hPT->Draw();
  hMCPT->SetLineColor(kRed);
  hMCPT->Draw("same");
  TCanvas *c3 = new TCanvas("c3", "c3", 800,700);
  hETA->Draw();
  hMCETA->SetLineColor(kRed);
  hMCETA->Draw("same");
  */
  return 1;
}





