from ROOT import TFile,TChain, TH2D, TH1D, TCanvas, gROOT
gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans/effTRIGGmu/checks/overlapping/overlappingTEST.cpp++')
overlappingTEST  = __import__('ROOT.overlappingTEST'   ,globals(),locals(),['overlappingTEST'])
nfout = 'overlappingTESTS.root'
fout = TFile.Open(nfout, 'recreate')
fout.Close()

pathCS = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/B2JpsiK_mumu/selected/'
tCS = TChain("DecayTree")
tCS.Add(pathCS+"Data-2012-JPsiMuMuDw.root")
tCS.Add(pathCS+"Data-2012-JPsiMuMuUp.root")

pathMC = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC/noTrigg/BdKpipi/"
tMC = TChain("DMutuple")
tMC.Add(pathMC+"/MagUp/MC-2012-BdKpipiUp-noTrigg_Pythia8Unfiltered_0.root")
tMC.Add(pathMC+"/MagUp/MC-2012-BdKpipiUp-noTrigg_Pythia6Unfiltered_0.root")
tMC.Add(pathMC+"/MagDw/MC-2012-BdKpipiDw-noTrigg_Pythia8Unfiltered_0.root")
tMC.Add(pathMC+"/MagDw/MC-2012-BdKpipiDw-noTrigg_Pythia6Unfiltered_0.root")


cats = ['1','2']
bins = [0.0,800.0,1100.0,1400.0,1700.0,2000.0,4500.0]

for cat in cats:
    for i in range(0, len(bins)-1):
        tag = 'cat%s_bin%d'%(cat, i+1)
        overlappingTEST(tCS, tMC, int(cat), bins[i], bins[i+1], nfout, tag)
        

