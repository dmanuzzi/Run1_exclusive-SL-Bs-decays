#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "TMath.h"
#include <fstream>
#include "TFile.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"
#include "plot2Hratio.cpp" 
using namespace std;
using namespace TMath;


double overlapping(){
  TH1::SetDefaultSumw2();
  TString pathin = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/B2JpsiK_mumu/selected/";
  TChain *tCS = new TChain("DecayTree");
  tCS->Add(pathin+"Data-2012-JPsiMuMuDw.root");
  tCS->Add(pathin+"Data-2012-JPsiMuMuUp.root");

  tCS->SetBranchStatus("*",0);
  TString probe = "plus";
  double Mu_PT=0.,Mu_ETA=0.;
  tCS->SetBranchStatus("mu"+probe+"_PT",1);
  tCS->SetBranchAddress("mu"+probe+"_PT", &Mu_PT);
  tCS->SetBranchStatus("mu"+probe+"_eta",1);
  tCS->SetBranchAddress("mu"+probe+"_eta", &Mu_ETA);
  TH1D *hPT = new TH1D("hPT", "p_{T}(#mu)", 100, 1000, 14000);
  TH1D *hETA= new TH1D("hETA", "#eta(#mu)", 25,2,4.7);
  TH2D *h2D = new TH2D("h2D", "Data: p_{T}(#mu), #eta(#mu)", 36,1000,14000, 18,2,4.7);
  hPT->SetMinimum(0.);
  hPT->SetMaximum(1.05);
  hPT->GetYaxis()->SetTitle("pdf(p_{T}(#mu))");
  hPT->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  hETA->SetMinimum(0.);
  hETA->SetMaximum(1.05);
  hETA->GetYaxis()->SetTitle("pdf(#eta(#mu))");
  hETA->GetXaxis()->SetTitle("#eta(#mu)");
  h2D->SetMinimum(0.);
  h2D->SetMaximum(1.);
  h2D->GetYaxis()->SetTitle("#eta(#mu)");
  h2D->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  for (int i=0; i<tCS->GetEntries(); ++i){
    tCS->GetEntry(i);
    hPT->Fill(Mu_PT);
    hETA->Fill(Mu_ETA);
    h2D->Fill(Mu_PT, Mu_ETA);
  }
  //====================================================================
  //====================================================================
  TChain *tMC = new TChain("DMutuple");
  pathin = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC/noTrigg/BdKpipi/";
  tMC->Add(pathin+"/MagUp/MC-2012-BdKpipiUp-noTrigg_Pythia8Unfiltered_0.root");
  tMC->Add(pathin+"/MagUp/MC-2012-BdKpipiUp-noTrigg_Pythia6Unfiltered_0.root");
  tMC->Add(pathin+"/MagDw/MC-2012-BdKpipiDw-noTrigg_Pythia8Unfiltered_0.root");
  tMC->Add(pathin+"/MagDw/MC-2012-BdKpipiDw-noTrigg_Pythia6Unfiltered_0.root");
  tMC->SetBranchStatus("*",0);
  tMC->SetBranchStatus("Mu_PT",1);
  tMC->SetBranchAddress("Mu_PT", &Mu_PT);
  tMC->SetBranchStatus("Mu_ETA",1);
  tMC->SetBranchAddress("Mu_ETA", &Mu_ETA);
  TH1D *hMCPT = new TH1D("hMCPT", "p_{T}(#mu)", 100, 1000, 14000);
  TH1D *hMCETA= new TH1D("hMCETA", "#eta(#mu)", 25,2,4.7);
  TH2D *hMC2D = new TH2D("hMC2D", "MC: p_{T}(#mu), #eta(#mu)", 36,1000,14000, 18,2,4.7);
  for (int i=0; i<tMC->GetEntries(); ++i){
    tMC->GetEntry(i);
    hMCPT->Fill(Mu_PT);
    hMCETA->Fill(Mu_ETA);
    hMC2D->Fill(Mu_PT, Mu_ETA);
  }
  hMCPT->SetMinimum(0.);
  hMCPT->SetMaximum(1.05);
  hMCPT->GetYaxis()->SetTitle("pdf(p_{T}(#mu))");
  hMCPT->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  hMCETA->SetMinimum(0.);
  hMCETA->SetMaximum(1.05);
  hMCETA->GetYaxis()->SetTitle("pdf(#eta(#mu))");
  hMCETA->GetXaxis()->SetTitle("#eta(#mu)");
  hMC2D->GetYaxis()->SetTitle("#eta(#mu)");
  hMC2D->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  h2D->SetMinimum(0.);
  h2D->SetMaximum(1.);
  int a=0, b=0, c=0;
  for(int i=1; i<=36;++i){
    for(int j=1; j<=18;++j){
      if (h2D->GetBinContent(i,j)==0 && hMC2D->GetBinContent(i,j)>5){
	++c;
	cout << i << "    " << j << "   Data: " << h2D->GetBinContent(i,j) << "    MC: " <<  hMC2D->GetBinContent(i,j) << endl;
      }
      if (h2D->GetBinContent(i,j)<10 && h2D->GetBinContent(i,j)==0) ++a;
      if (hMC2D->GetBinContent(i,j)<10 && hMC2D->GetBinContent(i,j)==0) ++b;
    }
  }
  h2D->Print();
  cout << " risky bins: " << c << endl;
  cout << "#data<10: " << a << endl;
  cout << "#MC< 10: " << b << endl;
  //====================================================================
  hPT->Scale(1./hPT->Integral());
  hMCPT->Scale(1./hMCPT->Integral());
  hETA->Scale(1./hETA->Integral());
  hMCETA->Scale(1./hMCETA->Integral());
  h2D->Scale(1./h2D->Integral());
  hMC2D->Scale(1./hMC2D->Integral());
  //====================================================================
  //====================================================================
  plot2Hratio(hPT, hMCPT, "Mu_PT", "effCurve.root", "B2JPIsiK_vs_BdKpipi2012");
  plot2Hratio(hETA, hMCETA, "Mu_ETA", "effCurve.root", "B2JPIsiK_vs_BdKpipi2012");
  //====================================================================
  //====================================================================
  //====================================================================
  //====================================================================
  gStyle->SetOptStat(0);
  TCanvas *c1 = new TCanvas("c1", "c1", 950,700);
  gStyle->SetPaintTextFormat(".4f");
  h2D->Draw("COLORZ");
  hMC2D->Draw("BOX SAME");
  //h2D->Draw("TEXT SAME");

  TCanvas *c1MC = new TCanvas("c1MC", "c1MC", 950,700);
  hMC2D->Draw("COLORZ");
  h2D->Draw("BOX SAME");
  //hMC2D->Draw("TEXT SAME");
  return 1;
}





