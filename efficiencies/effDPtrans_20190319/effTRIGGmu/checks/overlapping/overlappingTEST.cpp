#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "TMath.h"
#include <fstream>
#include "TFile.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"
#include "../plot2Hratio.cpp" 
using namespace std;
using namespace TMath;


double overlappingTEST(TChain *tCS, TChain *tMC, int cat, double D_PtransMIN, double D_PtransMAX, TString nfout, TString tag){
  TH1::SetDefaultSumw2();

  tCS->SetBranchStatus("*",0);
  TString probe = "plus";
  double Mu_PT=0.,Mu_ETA=0.;
  tCS->SetBranchStatus("mu"+probe+"_PT",1);
  tCS->SetBranchAddress("mu"+probe+"_PT", &Mu_PT);
  tCS->SetBranchStatus("mu"+probe+"_eta",1);
  tCS->SetBranchAddress("mu"+probe+"_eta", &Mu_ETA);
  TH1D *hPT = new TH1D("hPT_"+tag, "p_{T}(#mu)", 100, 1000, 14000);
  TH1D *hETA= new TH1D("hETA_"+tag, "#eta(#mu)", 25,2,4.7);
  TH2D *h2D = new TH2D("h2D_"+tag, "Data: p_{T}(#mu), #eta(#mu)", 36,1000,14000, 18,2,4.7);
  hPT->SetMinimum(0.);
  hPT->SetMaximum(1.05);
  hPT->GetYaxis()->SetTitle("pdf(p_{T}(#mu))");
  hPT->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  hETA->SetMinimum(0.);
  hETA->SetMaximum(1.05);
  hETA->GetYaxis()->SetTitle("pdf(#eta(#mu))");
  hETA->GetXaxis()->SetTitle("#eta(#mu)");
  h2D->SetMinimum(0.);
  h2D->SetMaximum(1.);
  h2D->GetYaxis()->SetTitle("#eta(#mu)");
  h2D->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  h2D->SetLineColor(kRed);
  for (int i=0; i<tCS->GetEntries(); ++i){
    tCS->GetEntry(i);
    hPT->Fill(Mu_PT);
    hETA->Fill(Mu_ETA);
    h2D->Fill(Mu_PT, Mu_ETA);
  }
  h2D->Print();

  //====================================================================
  //====================================================================
  tMC->SetBranchStatus("*",0);
  tMC->SetBranchStatus("Mu_PT",1);
  tMC->SetBranchAddress("Mu_PT", &Mu_PT);
  tMC->SetBranchStatus("Mu_ETA",1);
  tMC->SetBranchAddress("Mu_ETA", &Mu_ETA);
  double D_Ptrans=0., w_FF=1;
  int sigCat=0;
  bool D_peak=false;
  tMC->SetBranchStatus("signalCategory",1);
  tMC->SetBranchAddress("signalCategory",&sigCat);
  tMC->SetBranchStatus("D_Ptrans",1);
  tMC->SetBranchAddress("D_Ptrans",&D_Ptrans);
  tMC->SetBranchStatus("w_FF_corr",1);
  tMC->SetBranchAddress("w_FF_corr",&w_FF);
  tMC->SetBranchStatus("D_peak",1);
  tMC->SetBranchAddress("D_peak",&D_peak);

  TH1D *hMCPT = new TH1D("hMCPT_"+tag, "p_{T}(#mu)", 100, 1000, 14000);
  TH1D *hMCETA= new TH1D("hMCETA_"+tag, "#eta(#mu)", 25,2,4.7);
  TH2D *hMC2D = new TH2D("hMC2D_"+tag, "MC: p_{T}(#mu), #eta(#mu)", 36,1000,14000, 18,2,4.7);
  for (int i=0; i<tMC->GetEntries(); ++i){
    tMC->GetEntry(i);
    if (sigCat != cat) continue;
    if (D_Ptrans < D_PtransMIN) continue;
    if (D_Ptrans > D_PtransMAX) continue;
    if (!D_peak) continue;
    hMCPT->Fill(Mu_PT, w_FF);
    hMCETA->Fill(Mu_ETA, w_FF);
    hMC2D->Fill(Mu_PT, Mu_ETA, w_FF);
  }
  hMC2D->Print();
  hMCPT->SetMinimum(0.);
  hMCPT->SetMaximum(1.05);
  hMCPT->GetYaxis()->SetTitle("pdf(p_{T}(#mu))");
  hMCPT->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  hMCETA->SetMinimum(0.);
  hMCETA->SetMaximum(1.05);
  hMCETA->GetYaxis()->SetTitle("pdf(#eta(#mu))");
  hMCETA->GetXaxis()->SetTitle("#eta(#mu)");
  hMC2D->GetYaxis()->SetTitle("#eta(#mu)");
  hMC2D->GetXaxis()->SetTitle("p_{T}(#mu)  [MeV/c]");
  hMC2D->SetMinimum(0.);
  hMC2D->SetMaximum(1.);
  hMC2D->SetLineColor(kRed);
  int a=0, b=0, c=0;
  for(int i=1; i<=36;++i){
    for(int j=1; j<=18;++j){
      if (h2D->GetBinContent(i,j)==0 && hMC2D->GetBinContent(i,j)>5){
	++c;
	cout << i << "    " << j << "   Data: " << h2D->GetBinContent(i,j) << "    MC: " <<  hMC2D->GetBinContent(i,j) << endl;
      }
      if (h2D->GetBinContent(i,j)<10 && h2D->GetBinContent(i,j)==0) ++a;
      if (hMC2D->GetBinContent(i,j)<10 && hMC2D->GetBinContent(i,j)==0) ++b;
    }
  }
  cout << " risky bins: " << c << endl;
  cout << "#data<10: " << a << endl;
  cout << "#MC< 10: " << b << endl;
  //====================================================================
  hPT->Scale(1./hPT->Integral());
  hMCPT->Scale(1./hMCPT->Integral());
  hETA->Scale(1./hETA->Integral());
  hMCETA->Scale(1./hMCETA->Integral());
  h2D->Scale(1./h2D->Integral());
  hMC2D->Scale(1./hMC2D->Integral());
  //====================================================================
  //====================================================================
  plot2Hratio(hPT, hMCPT, "Mu_PT", nfout, tag, false, "./plots/");
  plot2Hratio(hETA, hMCETA, "Mu_ETA", nfout, tag, false, "./plots/");
  //====================================================================
  //====================================================================
  //====================================================================
  //====================================================================
  gStyle->SetOptStat(0);
  TCanvas *c1 = new TCanvas("canv_DataCOL_"+tag, "canv_DataCOL_"+tag, 950,700);
  gStyle->SetPaintTextFormat(".4f");
  h2D->Draw("COLORZ");
  hMC2D->Draw("BOX SAME");
  //h2D->Draw("TEXT SAME");

  TCanvas *c1MC = new TCanvas("canv_MCCOL_"+tag, "canv_MCCOL_"+tag, 950,700);
  hMC2D->Draw("COLORZ");
  h2D->Draw("BOX SAME");
  //hMC2D->Draw("TEXT SAME");

  TFile fout(nfout, "APPEND");
  fout.WriteTObject(c1, c1->GetName(), "overwrite");
  fout.WriteTObject(c1MC, c1MC->GetName(), "overwrite");
  fout.Close();
  c1->SaveAs("./plots/"+TString(c1->GetName())+".pdf");
  c1MC->SaveAs("./plots/"+TString(c1MC->GetName())+".pdf");
  c1->SaveAs("./plots/"+TString(c1->GetName())+".png");
  c1MC->SaveAs("./plots/"+TString(c1MC->GetName())+".png");
  return 1;
}





