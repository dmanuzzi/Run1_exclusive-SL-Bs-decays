import os
from ROOT import TH1D, TCanvas
import array as arr
DPbins = arr.array('f', [0,800,1100,1400,1700,2000,4500])
r12 = TH1D("effRatios2012","Eff. T&P/C&C cat1 2012 (L0+HLT1, Data)",6, DPbins)
r11 = TH1D("effRatios2011","Eff. T&P/C&C cat1 2011 (L0+HLT1, Data)",6, DPbins)
h12TP = TH1D("eff2012TP","Eff. T&P cat1 2012 (L0+HLT1, Data)",6, DPbins)
h11TP = TH1D("eff2011TP","Eff. T&P cat1 2011 (L0+HLT1, Data)",6, DPbins)
h12CC = TH1D("eff2012CC","Eff. C&C cat1 2012 (L0+HLT1, Data)",6, DPbins)
h11CC = TH1D("eff2011CC","Eff. C&C cat1 2011 (L0+HLT1, Data)",6, DPbins)

years = [
    (2011, r11, h11TP,h11CC),
    (2012, r12, h12TP,h12CC)
    ]
bins = [
    (   0, 800),
    ( 800,1100),
    (1100,1400),
    (1400,1700),
    (1700,2000),
    (2000,4500),
    ]

for year,r,hTP,hCC in years:
    i=1
    for bmin, bmax in bins:
        print '*********', year, bmin, bmax
        nfin = './logs/%d.%d.%d'%(year,bmin,bmax)
        fin = open(nfin, 'r').read()
        lines = fin.split('\n')[:-1]
        effCC=0
        eeffCC=0
        effTP=0
        eeffTP=0
        for line in lines:
            what = line.replace(' ', '').split(':')
            if 'effC&C' in what[0]:
                print '     eff C&C : %.2f '%(float(what[1])*100)
                effCC=float(what[1])
            if 'efferrC&C' in what[0]:
                print ' err eff C&C : %.2f '%(float(what[1])*100)
                eeffCC=float(what[1])
            if 'effT&P' in what[0]:
                print '     eff T&P : %.2f '%(float(what[1])*100)
                effTP=float(what[1])
            if 'efferrT&P' in what[0]:
                print ' err eff T&P : %.2f '%(float(what[1])*100)
                eeffTP=float(what[1])
        sigma = float((effCC-effTP))/(eeffCC**2+eeffTP**2)**0.5
        print '       sigma : %.1f'%(sigma)
        r.SetBinContent(i, effTP/effCC)
        r.SetBinError(i, effTP/effCC*( (eeffTP/effTP)**2+(eeffCC/effCC)**2 )**0.5)
        hTP.SetBinContent(i, effTP)
        hTP.SetBinError(i, eeffTP)
        hTP.SetMinimum(0.4)
        hTP.SetMaximum(0.6)
        hCC.SetBinContent(i, effCC)
        hCC.SetBinError(i, eeffCC)
        hCC.SetLineColor(2)
        hCC.SetMinimum(0.4)
        hCC.SetMaximum(0.6)
        i+=1
    ch = TCanvas()
    hTP.Draw()
    hCC.Draw('same')
    ch.SaveAs("eff"+str(year)+".pdf")
    cR = TCanvas()
    r.Draw()
    cR.SaveAs("effRatios"+str(year)+".pdf")
