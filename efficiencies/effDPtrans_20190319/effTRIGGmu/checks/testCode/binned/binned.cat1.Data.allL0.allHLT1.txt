********* 2011 0 800
     eff C&C : 57.95
 err eff C&C : 0.90
     eff T&P : 55.14
 err eff T&P : 0.12
       sigma : 3.1
********* 2011 800 1100
     eff C&C : 59.42
 err eff C&C : 0.74
     eff T&P : 56.24
 err eff T&P : 0.10
       sigma : 4.3
********* 2011 1100 1400
     eff C&C : 60.26
 err eff C&C : 0.59
     eff T&P : 55.90
 err eff T&P : 0.10
       sigma : 7.3
********* 2011 1400 1700
     eff C&C : 59.70
 err eff C&C : 0.50
     eff T&P : 56.14
 err eff T&P : 0.09
       sigma : 7.0
********* 2011 1700 2000
     eff C&C : 59.47
 err eff C&C : 0.48
     eff T&P : 55.52
 err eff T&P : 0.09
       sigma : 8.1
********* 2011 2000 4500
     eff C&C : 57.54
 err eff C&C : 0.47
     eff T&P : 53.11
 err eff T&P : 0.09
       sigma : 9.2
********* 2012 0 800
     eff C&C : 57.94
 err eff C&C : 0.65
     eff T&P : 57.69
 err eff T&P : 0.09
       sigma : 0.4
********* 2012 800 1100
     eff C&C : 57.82
 err eff C&C : 0.53
     eff T&P : 58.55
 err eff T&P : 0.07
       sigma : -1.4
********* 2012 1100 1400
     eff C&C : 57.55
 err eff C&C : 0.42
     eff T&P : 58.20
 err eff T&P : 0.06
       sigma : -1.5
********* 2012 1400 1700
     eff C&C : 56.99
 err eff C&C : 0.37
     eff T&P : 57.46
 err eff T&P : 0.06
       sigma : -1.3
********* 2012 1700 2000
     eff C&C : 56.43
 err eff C&C : 0.34
     eff T&P : 56.89
 err eff T&P : 0.06
       sigma : -1.3
********* 2012 2000 4500
     eff C&C : 53.30
 err eff C&C : 0.34
     eff T&P : 54.84
 err eff T&P : 0.06
       sigma : -4.4
