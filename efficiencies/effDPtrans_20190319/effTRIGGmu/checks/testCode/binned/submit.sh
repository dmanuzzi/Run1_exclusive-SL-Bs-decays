#!/bin/bash
DIR=$PWD

binMIN[0]=0
binMIN[1]=800
binMIN[2]=1100
binMIN[3]=1400
binMIN[4]=1700
binMIN[5]=2000
binMIN[6]=0

binMAX[0]=800
binMAX[1]=1100
binMAX[2]=1400
binMAX[3]=1700
binMAX[4]=2000
binMAX[5]=4500
binMAX[6]=4500

for year in '2011' '2012'; do
    for i in {0..6}; do
	echo $year'  '${binMIN[$i]}'  '${binMAX[$i]}
	echo "#!/bin/sh" > tmp.sh
	echo ". $VO_LHCB_SW_DIR/lib/LbLogin.sh" >> tmp.sh 
	echo "cd $DIR" >> tmp.sh
	echo "lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python testCodeBinned.py $year ${binMIN[$i]} ${binMAX[$i]} >> logs/$year.${binMIN[$i]}.${binMAX[$i]}" >> tmp.sh
	qsub -N $year.${binMIN[$i]}.${binMAX[$i]} -e $DIR"/err/"$year.${binMIN[$i]}.${binMAX[$i]} -o $DIR"/out/"$year.${binMIN[$i]}.${binMAX[$i]} tmp.sh
   done
done