#include <iostream>
#include "TObject.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TChain.h"
#include "TTree.h"
#include "TH1D.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TString.h"
#include "TMath.h"
#include "TLegend.h"
#include <TLorentzVector.h>
#include "TF1.h"
#include "TPaveText.h"
using namespace std;
using namespace TMath;


int plot2Hratio(TH1D* hData, TH1D* hMC, TString nvar, TString nfout, TString tag, bool inv=false, TString outdir="./"){
  TH1::SetDefaultSumw2();
  const int nbins = hData->GetNbinsX();
  const double min= hData->GetXaxis()->GetXmin();
  const double max= hData->GetXaxis()->GetXmax();

  cout << endl;
  gStyle->SetOptStat(0);

  TString nXaxis;
  
  if (inv)
    nXaxis = "#frac{1}{8}tan(atan(8) "+nvar+")";
  else
    nXaxis = nvar;
  

  //  hData->SetTitle(nvar);
  hData->SetLineColor(1);
  hData->SetFillColor(1);
  hData->SetLineWidth(2);
  hData->SetMarkerColor(1);
  hData->SetMarkerStyle(20);
  hData->SetMarkerSize(1);
  // hData->GetXaxis()->SetTitle(nXaxis);
  // hData->GetYaxis()->SetTitle("pdf("+nvar+")");

  hMC->SetLineColor(kRed);
  hMC->SetFillColor(kRed);
  hMC->SetFillStyle(1001);
  hMC->SetLineWidth(2);
  hMC->SetMarkerColor(kRed);
  hMC->SetMarkerStyle(20);
  hMC->SetMarkerSize(0.5);
  // hMC->GetXaxis()->SetTitle(nXaxis);
  // hMC->GetYaxis()->SetTitle("pdf("+nvar+")");

  double ymax[3];
  ymax[0] = hData->GetMaximum();
  ymax[1] = hMC->GetMaximum();
  ymax[2] = TMath::MaxElement(2, ymax);
  hData->SetMaximum(ymax[2]);
  hMC->SetMaximum(ymax[2]);
  hData->SetMinimum(0.);
  hMC->SetMinimum(0.);

  TH1D* ratio1 = new TH1D("ratio_MCvsData_"+tag+"_"+nvar, "ratio_MCvsData_"+tag+"_"+nvar, nbins,min,max);
  ratio1->Divide(hMC, hData);
  if (inv) {
    ratio1->SetMaximum(2.);
    ratio1->SetMinimum(0.5);
  } else {
    ratio1->SetMaximum(1.5);
    ratio1->SetMinimum(0.5);
  }


  ratio1->SetFillColor(kRed);
  ratio1->SetLineColor(kRed);
  ratio1->SetTitle(" ");
  ratio1->SetMarkerColor(kRed);
  ratio1->SetMarkerStyle(20);
  ratio1->SetMarkerSize(0.5);
  ratio1->GetXaxis()->SetTitle(nXaxis);
  ratio1->GetYaxis()->SetTitle("pdf_{MC}/pdf_{data}");
  ratio1->GetYaxis()->SetLabelSize(0.08);
  ratio1->GetYaxis()->SetTitleOffset(0.4);
  ratio1->GetYaxis()->SetTitleSize(0.1);

  TF1 *funit = new TF1("funit", "1", min, max);
  double Chi2 = 0;
  for (int i=1; i<nbins+1;++i){
    if (ratio1->GetBinError(i)<1.e-12) continue;
    Chi2 = Chi2 + (ratio1->GetBinContent(i)-1.)*(ratio1->GetBinContent(i)-1.)/(ratio1->GetBinError(i)*ratio1->GetBinError(i));
  }
  TString nChi2_1;
  nChi2_1.Form("#chi^{2} = %.1f", Chi2);
  cout << "Chi2 = " << Chi2 << endl;

  TString nndof_1;
  nndof_1.Form(",  ndof = %d", nbins);
  nChi2_1=nChi2_1+nndof_1;
  TCanvas* c1 = new TCanvas("canv_"+nvar+tag,"canv_"+nvar+tag, 900,700);
  c1->SetFillColor(kWhite);
  TPad* upperPad = new TPad("upperPad", "upperPad"+nvar, .005, .2425, .995, .995);
  TPad* lowerPad = new TPad("lowerPad", "lowerPad"+nvar, .005, .005, .995, .2425);
  lowerPad->SetGrid();
  lowerPad->Draw();
  upperPad->Draw();

  upperPad->cd();
  hData->Draw();
  hMC->Draw("same");

  TLegend *leg = new TLegend(0.68,0.73,0.9,0.9);
  leg->SetFillStyle(0);
  leg->AddEntry(hData, "Data", "lep");
  leg->AddEntry(hMC,   "MC", "lep");
  leg->Draw();

  TPaveText * tagTEX = new TPaveText(0.68,0.6925,0.9,0.6175,"NDCBR");
  tagTEX->SetFillStyle(0);
  tagTEX->SetTextSize(0.05);
  tagTEX->AddText(tag.Data());
  tagTEX->Draw("same");


  lowerPad->cd();
  funit->SetLineColor(1);
  ratio1->Draw("");
  funit->Draw("same");
  ratio1->Draw("same");

  TPaveText * title1 = new TPaveText(0.18,0.65,0.65,0.9,"NDCBR");
  title1->SetFillStyle(0);
  title1->SetTextSize(0.2);
  title1->AddText(nChi2_1.Data());
  title1->Draw("same");
  TFile* fout = TFile::Open(nfout, "UPDATE");
  //  fout->WriteTObject(hData, hData->GetName(), "Overwrite");                                                                                        
  //  fout->WriteTObject(hMC, hMC->GetName(), "Overwrite");                                                                                                  
  //  fout->WriteTObject(hMCcor, hMCcor->GetName(), "Overwrite");                                                                                            
  //  fout->WriteTObject(ratio1, ratio1->GetName(), "Overwrite");                                                                                            
  //  fout->WriteTObject(ratio2, ratio2->GetName(), "Overwrite");                                                                                            
  fout->WriteTObject(c1, c1->GetName(), "Overwrite");
  c1->SaveAs(outdir+TString(c1->GetName())+TString(".png"));
  c1->SaveAs(outdir+TString(c1->GetName())+TString(".pdf"));
  /*  
  delete ratio1;
  delete upperPad;
  delete lowerPad;
  delete c1;
  delete funit;
  delete title1;
  delete leg;
  */
  fout->Close();
  cout << "===============================================================\n";
  return 1;

}
