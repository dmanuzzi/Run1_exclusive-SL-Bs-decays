import os, sys
import root_numpy
import pandas
import numpy as np
from rootpy.io import root_open
from ROOT import *
from shutil import copyfile
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/srcEff')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/')
import pickle
from waitORgo import waitORgo
import time
pathSRCREW = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/rew/srcRew/'
pathSRCEFF = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/srcEff'
pathLOGS = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/main/logs/'
def f_prepareCSsBu2JPsiK(pathJpsi = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/B2JpsiK_mumu/selected/', copyFromBackup=True, initDF=True):
    #calibrations samples
    
    CSs = {'2011' : {'Up' : {'nfin' : pathJpsi+'Data-2011-JPsiMuMuUp.root', 'df' : None, 'df_w' : None}, 
                     'Dw' : {'nfin' : pathJpsi+'Data-2011-JPsiMuMuDw.root', 'df' : None, 'df_w' : None}
                     },
           '2012' : {'Up' : {'nfin' : pathJpsi+'Data-2012-JPsiMuMuUp.root', 'df' : None, 'df_w' : None},
                     'Dw' : {'nfin' : pathJpsi+'Data-2012-JPsiMuMuDw.root', 'df' : None, 'df_w' : None}
                     }
           }
    '''
    CSs = {'2011' : {'Up' : {'nfin' : pathJpsi+'MC-2011-JPsiMuMuUp.root', 'df' : None}, 
                     'Dw' : {'nfin' : pathJpsi+'MC-2011-JPsiMuMuDw.root', 'df' : None}
                     },
           '2012' : {'Up' : {'nfin' : pathJpsi+'MC-2012-JPsiMuMuUp.root', 'df' : None},
                     'Dw' : {'nfin' : pathJpsi+'MC-2012-JPsiMuMuDw.root', 'df' : None}
                     }
           }
           '''
     #copying the calibration TTree from the backup ones
     #and import them in pandas
    for year in CSs.keys():
        for pol in CSs[year].keys():
            if copyFromBackup:
                print 'cp '+CSs[year][pol]['nfin']
                copyfile(CSs[year][pol]['nfin']+'.bak', CSs[year][pol]['nfin'])        
            if initDF:
                print 'Reading dataset '+CSs[year][pol]['nfin']
                input_CS = root_numpy.root2array(CSs[year][pol]['nfin'], treename='DecayTree', branches=['muplus_PT','muplus_eta', 'muplus_IP_OWNPV'])
                input_CS = pandas.DataFrame(input_CS)
                CSs[year][pol]['df'] = input_CS
                print year, pol, 'N_CS = ', len(CSs[year][pol]['df']) 
    return CSs

#reweigh the calibration samples according to all the MC samples 
def f_reweigh2D_TRIGGmu(tag, modes, years, cats, bins,
                      pythias, pols, path_MC, CSs):
    nfinMCs   = []
    nfinCSs   = []
    input_CSs = []
    #print 'pols :', pols
    for year in years:
        for pol in pols:
            nfinCSs.append(CSs[year][pol]['nfin'])
            input_CSs.append(CSs[year][pol]['df'])
            for pythia in pythias:
                for mode in modes:
                    nfinMC = path_MC+'/noTrigg/{mode}/Mag{pol}/MC-{year}-{mode}{pol}-noTrigg_{pythia}Unfiltered_0.root'
                    nfinMCs.append( nfinMC.format(mode=mode, pol=pol, year=year, pythia=pythia) )
    for cat in cats:
        nw='w_TRIGGmu_sigCat%d_%s'%(cat,tag)
        MCcut = '(D_peak)'
        if 'Bs' in tag: MCcut = '(Ds_peak)'
        if cat :
            MCcut += ' && (signalCategory==%d)'%(cat)
        nbins = ['bin%d'%i for i in range(1,len(bins)) ]
        nbins += ['tot']
        i = 0
        for nbin in nbins:
            nwBinned = nw+'_'+nbin
            MCcutBinned = MCcut
            if nbin != 'tot':
                MCcutBinned = MCcut+' && (%f < D_Ptrans && D_Ptrans < %f)'%(bins[i], bins[i+1])
            #print nfinCSs
            #print nfinMCs
            setup = (nfinCSs, nfinMCs, nwBinned, 'DecayTree' , 'DMutuple', '1', MCcutBinned, 'w_FF_corr', ['muplus_PT', 'muplus_eta', 'muplus_IP_OWNPV'], ['Mu_PT','Mu_ETA', 'Mu_IP'])
            nfout = pathSRCREW+nwBinned+'.pickle'
            fout = open(nfout, 'wb')
            pickle.dump(setup, fout)
            fout.close()     
            ## submit to the queue
            ftmp = open('./tmp.sh', 'w')
            ftmp.write('#!/bin/bash \n')
            ftmp.write('. $VO_LHCB_SW_DIR/lib/LbLogin.sh \n')
            ftmp.write('cd %s \n'%(pathSRCREW))
            ftmp.write('lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python %s/reweighter_parallel.py %s \n'%(pathSRCEFF, nfout))
            ftmp.close()
            os.chmod('./tmp.sh',0755)
            command = 'qsub -N {nw} -e {path}/err/{nw}.txt -o {path}/out/{nw}.txt ./tmp.sh'.format(nw=nwBinned, path=pathLOGS)
            print 'qsub '+nwBinned
            os.system(command)
            #os.system('./tmp.sh')
            i=i+1
                        
    ### wait for all jobs to end
    while (not waitORgo()):
        time.sleep(5)
