import sys
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/rew')
sys.path.insert(0, '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/efficiencies/effDPtrans_20190319/effTRIGGmu/getEff')
from f_reweigh2D_TRIGGmu import f_reweigh2D_TRIGGmu
from f_getEffTRIGGmu import f_getEffTRIGGmu
def f_getCD(years, modes, pols, pythias, cats, bins, inpathMC, outpath, tag, CSs_Bu2JPsiK):
    ### run reweight
    f_reweigh2D_TRIGGmu(tag, modes, years, cats, bins, pythias, pols, inpathMC, CSs_Bu2JPsiK)
    ### getEff 
    resultsC = {}
    resultsD = {}
    nfout = outpath+'/results_%s_effTRIGGmu.txt'%(tag)
    fout = open(nfout , 'w')
    fout.close()
    resultsC, resultsD = f_getEffTRIGGmu(CSs_Bu2JPsiK, nfout, tag, years, pols, cats, bins)
    return resultsC, resultsD

