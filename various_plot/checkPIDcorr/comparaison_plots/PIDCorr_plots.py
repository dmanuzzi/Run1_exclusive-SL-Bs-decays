'''
Run this macro with: lb-run -c x86_64-slc6-gcc49-opt LHCb/v41r1 python PIDCorr_plots.py
'''
mode = 'BdKpipi'
Flag_nTracks_rew=False

######################################
pathIN    = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples'
nfin_Data = ''
ntin_Data = ''
nfin_MC   = ''
ntin_MC   = ''
nfout     = ''
variables = []
#### INPUTS BdKpipi ####
if mode == 'BdKpipi': 
    nfin_Data = pathIN+'/Data/noHadronicPID/Kpipi/Data-Kpipi-noHadronicPID.root'
    ntin_Data = 'DMutuple'
    nfin_MC   = pathIN+'/MC/noHadronicPID/BdKpipi/MC-BdKpipi-noHadronicPID_Unfiltered.root'
    ntin_MC   = 'DMutuple'
    if Flag_nTracks_rew:
        nfout = './plots/weighted/BdKpipi_PIDCorr_plots_20181229.root'
    else:
        nfout = './plots/unweighted/BdKpipi_PIDCorr_plots_20181229.root'
    variables = [('K_PIDK'      , 100,   -5, 100),
                 ('K_ProbNNk'   , 100,    0,   1),
                 ('Pi1_PIDK'    , 100, -100,  20),
                 ('Pi1_ProbNNpi', 100,    0,   1),
                 ('Pi2_PIDK'    , 100, -100,  20),
                 ('Pi2_ProbNNpi', 100,    0,   1),
                 ('Mu_PIDmu'    , 100,    0,  15),
                 ('Mu_ProbNNmu' , 100,    0,   1),
                 ]
#### INPUTS BdKKpi #### 
elif mode == 'BdKKpi':
    nfin_Data = pathIN+'/Data/noHadronicPID/KKpi/Data-2012-KKpi-noHadronicPID.root'
    ntin_Data = 'DMutuple'
    nfin_MC   = pathIN+'/MC/noHadronicPID/BdKKpi/MC-2012-BdKKpi-noHadronicPID_Unfiltered.root'
    ntin_MC   = 'DMutuple'
    if Flag_nTracks_rew:
        nfout = './plots/weighted/BdKKpi_PIDCorr_plots_20181229.root'
    else:
        nfout = './plots/unweighted/BdKKpi_PIDCorr_plots_20181229.root'
   
    variables = [
               #  ('K1_PIDK'     , 100,   -5, 100),
               #  ('K1_ProbNNk'  , 100,    0,   1),
                 ('K2_PIDK'     , 100,   -5, 100),
                 ('K2_ProbNNk'  , 100,    0,   1),
                 ('Pi_PIDK'     , 100, -100,  20),
                 ('Pi_ProbNNpi' , 100,    0,   1),
                 ('Mu_PIDmu'    , 100,    0,  15),
                 ('Mu_ProbNNmu' , 100,    0,   1),
                 ]
#################################################################################################################


print 'Importing libraries...'
import ROOT
import root_numpy
import pandas
ROOT.gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/various_plot/plot2Hratio.cpp+')
ROOT.gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/various_plot/plot3Hratios.cpp+')
ROOT.gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/various_plot/checkPIDcorr/comparaison_plots/nTracks_rew.cpp+')
#plot2Hratio = __import__('ROOT.plot2Hratio',globals(),locals(),['plot2Hratio'])
plot3Hratios= __import__('ROOT.plot3Hratios',globals(),locals(),['plot3Hratios'])
nTracks_rew = __import__('ROOT.nTracks_rew', globals(),locals(),['nTracks_rew'])

print 'Creating output file:', nfout
fout = ROOT.TFile.Open(nfout,'RECREATE')
fout.Close()

nfin_MC_friend = ''
nW = ''
if Flag_nTracks_rew:
    nfin_MC_friend = './tuples/MC-2012-BdKKpi_noHadronicPID_w_nTracks.root'
    print 'nTracks reweighing...'
    nTracks_rew(nfin_MC, nfin_Data,  nfin_MC_friend)
    nW = 'w_nTracks'
print 'Importing input files:'
nvars = []
nvars_corr = []
for x,bins,xmin,xmax in variables:
    nvars.append(x)
    nvars_corr.append(x+'_corr')
nvars.append('D_peak')
nvars.append('D_side')

test_selection='Mu_PIDmu<15 && Mu_PIDmu>0 && K_PIDK>-5 && K_PIDK<100 && Pi1_PIDK>-100 && Pi1_PIDK<20 && Pi2_PIDK>-100 && Pi2_PIDK<20'
test_selection='K_PIDK < 100'
print '   --> Data: ', nfin_Data
n_array_Data = root_numpy.root2array(nfin_Data, treename=ntin_Data, branches=nvars, start=0, stop=1000000)
df_Data = pandas.DataFrame(n_array_Data)
df_Data.eval('wD = D_peak*1-D_side*1')
print '   -->   MC: ', nfin_MC
n_array_MC = root_numpy.root2array(nfin_MC, treename=ntin_MC, branches=nvars+nvars_corr, start=0, stop=1000000)
df_MC = pandas.DataFrame(n_array_MC)
df_MC.eval('wD = D_peak*1-D_side*1')
if nW:
    n_array_MC_friend = root_numpy.root2array(nfin_MC_friend, treename=ntin_MC,branches = [nW], start=0, stop=1000000)
    df_MC_friend = pandas.DataFrame(n_array_MC_friend)
    df_MC = df_MC.assign(w=df_MC_friend[nW])
print 'Creating all empty histograms...'
ROOT.TH1.SetDefaultSumw2(True)
nvars_hists = []
for x, bins, xmin, xmax  in variables:
    ll = []
    ll.append( ROOT.TH1D('hData_'+x, 'hData_'+x, bins, xmin, xmax) )
    ll.append( ROOT.TH1D('hMC_'+x,   'hMC_'+x,   bins, xmin, xmax) )
    ll.append( ROOT.TH1D('hMC_'+x+'_corr',   'hMC_'+x+'_corr',   bins, xmin, xmax) )
    l =[x,ll]
    nvars_hists.append(l)

print 'Filling histograms...'
for var, hl  in nvars_hists:
    for X,wD in zip(df_Data[var], df_Data['wD']):
        hl[0].Fill(X,wD)
    if nW:
        for X,X_corr,W,wD in zip(df_MC[var], df_MC[var+'_corr'], df_MC['w'], df_MC['wD']):
            hl[1].Fill(X, W*wD)
            hl[2].Fill(X_corr, W*wD)
    else:
        for X,X_corr,wD in zip(df_MC[var], df_MC[var+'_corr'], df_MC['wD']):
            hl[1].Fill(X,wD)
            hl[2].Fill(X_corr,wD)
            
print 'Drawing histograms...'
for nvar, hl  in nvars_hists:
    hData = hl[0]
    hMC = hl[1]
    hMC_corr = hl[2]
    hData.Scale(1.0/hData.Integral())
    hMC.Scale(1.0/hMC.Integral())
    hMC_corr.Scale(1.0/hMC_corr.Integral())
#    hData.Print()
#    hMC.Print()
#    hMC_corr.Print()
    outdir = './plots/unweighted/'
    if nW:
        outdir = './plots/weighted/'
    plot3Hratios(hData, hMC, hMC_corr, nvar, nfout, '_test', False, outdir)

'''
for x, hl  in nvars_hists:
    hData = hl[0]
    hMC = hl[1]
    hData.Scale(1.0/hData.Integral())
    hMC.Scale(1.0/hMC.Integral())
    hData.Print()
    hMC.Print()
    plot2Hratio(hData, hMC, x, nfout, x+'_test', False, './plots/unweighted/')
'''
