#include <iostream>
#include "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/myRew/myRew.cpp"
using namespace std;

int nTracks_rew(TString nfin_MC, TString nfin_Data, TString nfout){
  myRew<int> R(nfin_MC, nfin_Data, "DMutuple", "nTracks", nfout, 90, 0., 450.);
  R.peakDef = "(D_M>1850 && D_M<1890)";
  R.sideDef = "((D_M>1825 && D_M<1845) || (D_M>1895 && D_M<1915))";
  R.cut = "(D_M<1915)";
  R.nW = "w_nTracks";
  R.doRew();
  return 1;
}
