#include <iostream>
#include "TObject.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TChain.h"
#include "TTree.h"
#include "TH1D.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TString.h"
#include "TMath.h"
#include "TLegend.h"
#include <TLorentzVector.h>
#include "TF1.h"
#include "TPaveText.h"
using namespace std;
using namespace TMath;

int plot3Hratios(TH1D* hData, TH1D* hMC, TH1D* hMCcor, TString nvar, TString nfout, TString tag, bool inv=false, TString outdir="./"){
  hData->Print();
  hMC->Print();
  hMCcor->Print();

  const int nbins = hData->GetNbinsX();
  const double min= hData->GetXaxis()->GetXmin();
  const double max= hData->GetXaxis()->GetXmax();

  cout << endl;
  gStyle->SetOptStat(0);

  TString nXaxis;
  if (inv)
    nXaxis = "#frac{1}{8}tan(atan(8) "+nvar+")";
  else
    nXaxis = nvar;

  hData->SetTitle(nvar);
  hData->SetLineColor(1);
  hData->SetFillColor(1);
  hData->SetLineWidth(2);
  hData->SetMarkerColor(1);
  hData->SetMarkerStyle(20);
  hData->SetMarkerSize(1);  
  hData->GetXaxis()->SetTitle(nXaxis);
  hData->GetYaxis()->SetTitle("pdf("+nvar+")");

  hMC->SetLineColor(kAzure);
  hMC->SetFillColor(kAzure);
  hMC->SetFillStyle(1001);
  hMC->SetLineWidth(2);
  hMC->SetMarkerColor(kAzure);
  hMC->SetMarkerStyle(20);
  hMC->SetMarkerSize(0.5);
  hMC->GetXaxis()->SetTitle(nXaxis);
  hMC->GetYaxis()->SetTitle("pdf("+nvar+")");

  hMCcor->SetLineColor(2);
  hMCcor->SetFillColor(2);
  hMCcor->SetFillStyle(3004);
  hMCcor->SetLineWidth(2);
  hMCcor->SetMarkerColor(2);
  hMCcor->SetMarkerStyle(20);
  hMCcor->SetMarkerSize(0.5);
  hMCcor->GetXaxis()->SetTitle(nXaxis);
  hMCcor->GetYaxis()->SetTitle("pdf("+nvar+")");

  double ymax[4];
  ymax[0] = hData->GetMaximum();
  ymax[1] = hMC->GetMaximum();
  ymax[2] = hMCcor->GetMaximum();
  ymax[3] = TMath::MaxElement(3, ymax);
  hData->SetMaximum(ymax[3]);
  hMC->SetMaximum(ymax[3]);
  hMCcor->SetMaximum(ymax[3]);
  hData->SetMinimum(0.);
  hMC->SetMinimum(0.);
  hMCcor->SetMinimum(0.);

  TH1D* ratio1 = new TH1D("ratio_MCvsData_"+nvar, "ratio_MCvsData_"+nvar, nbins,min,max);
  ratio1->Divide(hMC, hData);

  if (inv) {
    ratio1->SetMaximum(1.5);
    ratio1->SetMinimum(0.5);
  } else {
    ratio1->SetMaximum(3.);
    ratio1->SetMinimum(0.5);
  }
  ratio1->SetFillColor(kAzure);
  ratio1->SetLineColor(kAzure);
  ratio1->SetMarkerColor(4);
  ratio1->SetMarkerStyle(20);
  ratio1->SetMarkerSize(0.5);

  ratio1->SetTitle(" ");
  ratio1->GetXaxis()->SetTitle(nXaxis);
  ratio1->GetYaxis()->SetTitle("pdf_{MC}/pdf_{data}");
  ratio1->GetYaxis()->SetLabelSize(0.08);
  ratio1->GetYaxis()->SetTitleOffset(0.4);
  ratio1->GetYaxis()->SetTitleSize(0.1);

  TF1 *funit = new TF1("funit", "1", min, max);
  double Chi2 = 0;
  for (int i=1; i<nbins+1;++i){
    if (ratio1->GetBinError(i)<1.e-12) continue;
    Chi2 = Chi2 + (ratio1->GetBinContent(i)-1.)*(ratio1->GetBinContent(i)-1.)/(ratio1->GetBinError(i)*ratio1->GetBinError(i));
  }
  TString nChi2_1;
  nChi2_1.Form("#chi^{2} = %.1f", Chi2);

  TString nndof_1;
  nndof_1.Form(",  ndof = %d", nbins);
  nChi2_1=nChi2_1+nndof_1;

  TH1D* ratio2 = new TH1D("ratio_MCcorvsData_"+nvar, "ratio_MCcorvsData_"+nvar, nbins,min,max);
  ratio2->Divide(hMCcor, hData);

  if (inv) {
    ratio2->SetMaximum(1.5);
    ratio2->SetMinimum(0.5);
  } else {
    ratio2->SetMaximum(3.);
    ratio2->SetMinimum(0.5);
  }
  ratio2->SetMarkerColor(2);
  ratio2->SetMarkerStyle(20);
  ratio2->SetMarkerSize(0.5);
  ratio2->SetFillColor(2);
  ratio2->SetLineColor(2);
  ratio2->SetTitle(" ");
  ratio2->GetXaxis()->SetTitle(nXaxis);
  ratio2->GetYaxis()->SetTitle("pdf_{MCcorr}/pdf_{data}");
  ratio2->GetYaxis()->SetLabelSize(0.07);
  ratio2->GetYaxis()->SetTitleOffset(0.4);
  ratio2->GetYaxis()->SetTitleSize(0.1);

  Chi2 = 0;
  for (int i=1; i<nbins+1;++i){
    if (ratio1->GetBinError(i)<1.e-12) continue;
    Chi2 = Chi2 + (ratio2->GetBinContent(i)-1.)*(ratio2->GetBinContent(i)-1.)/(ratio2->GetBinError(i)*ratio2->GetBinError(i));
  }

  TString nChi2_2;
  nChi2_2.Form("#chi^{2} = %.1f", Chi2);

  TString nndof_2;
  nndof_2.Form(",  ndof = %d", nbins);
  nChi2_2=nChi2_2+nndof_2;


  TCanvas* c1 = new TCanvas("canv_"+nvar+tag,"canv_"+nvar+tag, 900,700);
  c1->SetFillColor(kWhite);
  TPad* upperPad = new TPad("upperPad", "upperPad"+nvar, .005, .405, .995, .995);
  TPad* mediumPad= new TPad("mediumPad","mediumPad"+nvar, .005, .205, .995, .405);
  TPad* lowerPad = new TPad("lowerPad", "lowerPad"+nvar, .005, .005, .995, .205);
  lowerPad->SetGrid();
  mediumPad->SetGrid();
  lowerPad->Draw();
  mediumPad->Draw();
  upperPad->Draw();
  upperPad->cd();
  hData->Draw();
  hMC->Draw("same");
  hMCcor->Draw("same");

  TLegend *leg = new TLegend(0.15,0.7,0.4,0.9);
  leg->AddEntry(hData, "Data", "lep");
  leg->AddEntry(hMC,   "MC", "lep");
  leg->AddEntry(hMCcor,"MC corrected", "lep");
  leg->Draw();

  mediumPad->cd();
  funit->SetLineColor(1);
  ratio1->Draw("");
  funit->Draw("same");
  ratio1->Draw("same");

  TPaveText * title1 = new TPaveText(0.28,0.7,0.55,0.85,"NDCBR");
  title1->SetFillColor(kWhite);
  title1->SetTextSize(0.20);
  title1->AddText(nChi2_1.Data());
  title1->Draw("same");

  lowerPad->cd();
  funit->SetLineColor(1);
  ratio2->Draw("");
  funit->Draw("same");
  ratio2->Draw("same");

  TPaveText * title2 = new TPaveText(0.28,0.7,0.55,0.85,"NDCBR");
  title2->SetFillColor(kWhite);
  title2->SetTextSize(0.20);
  title2->AddText(nChi2_2.Data());
  title2->Draw("same");

  TFile* fout = TFile::Open(nfout, "UPDATE");
  //  fout->WriteTObject(hData, hData->GetName(), "Overwrite");                                                                                              
  //  fout->WriteTObject(hMC, hMC->GetName(), "Overwrite");                                                                                                  
  //  fout->WriteTObject(hMCcor, hMCcor->GetName(), "Overwrite");                                                                                            
  //  fout->WriteTObject(ratio1, ratio1->GetName(), "Overwrite");                                                                                            
  //  fout->WriteTObject(ratio2, ratio2->GetName(), "Overwrite");                                                                                            
  fout->WriteTObject(c1, c1->GetName(), "Overwrite");
  c1->SaveAs(outdir+TString(c1->GetName())+TString(".png"));
  c1->SaveAs(outdir+TString(c1->GetName())+TString(".pdf"));

  delete ratio1;
  delete ratio2;
  delete upperPad;
  delete mediumPad;
  delete lowerPad;
  delete c1;

  fout->Close();
  cout << "===============================================================\n";
  return 1;
}
