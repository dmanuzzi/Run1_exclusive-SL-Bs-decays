#include <stdio.h>
#include <stdlib.h>

#include "./do_cfr.cpp"

int cfr_var_BdKpipi(TString tag = ""){
  tag = "Run1_BdKpipi_Rew_FF";
 
  TString nfData(""), nfMC(""), nw("none");
  if (tag.Contains("Run1_BdKpipi")){
    nfMC   = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC/FinalSelection/BdKpipi/MC-BdKpipi-AfterSelection_ReDecayUnfiltered.root";
    nfData = "/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/Data/FinalSelection/Kpipi/Data-Kpipi-AfterSelection.root";
  }
  if (tag.Contains("Rew_FF")){
        nw = "w_FF";
  }

  TString nfout = "./"+tag+"/"+tag+".root";
  TFile *fout = new TFile(nfout, "RECREATE");
  fout->Close();
  system("mkdir "+tag);
  //  do_cfr("B_Pperp"   , nfMC, nfData, tag, nfout, 50,      0.,    3000.,  nw);  
   
  do_cfr("Mu_PT"     , nfMC, nfData, tag, nfout, 50,      0.,   15000.,   nw);
  do_cfr("Mu_IP"     , nfMC, nfData, tag, nfout, 50,      0.,      2.5,  nw);
  do_cfr("B_FD"      , nfMC, nfData, tag, nfout,  50,      0.,     80.,   nw);
  do_cfr("B_MCORR"   , nfMC, nfData, tag, nfout,  50,   3000.,   8000.,   nw);
  do_cfr("D_FDChi2"  , nfMC, nfData, tag, nfout,  50,      0., 160000.,   nw);
  do_cfr("B_MCORRERR", nfMC, nfData, tag, nfout,  50,      0.,   2500.,   nw);
  do_cfr("D_PT"      , nfMC, nfData, tag, nfout,  50,      0.,  10000.,   nw);
  do_cfr("D_P"       , nfMC, nfData, tag, nfout,  50,      0., 250000.,   nw);
  do_cfr("D_IPChi2"  , nfMC, nfData, tag, nfout,  50,      0.,    500.,   nw);
  do_cfr("D_IP"      , nfMC, nfData, tag, nfout,  50,      0.,      1.6,  nw);
  do_cfr("D_FD"      , nfMC, nfData, tag, nfout,  50,      0.,    100.,   nw);
  do_cfr("B_FDChi2"  , nfMC, nfData, tag, nfout,  50,      0.,   4000.,   nw);
  do_cfr("B_P"       , nfMC, nfData, tag, nfout,  50,      0., 400000.,   nw);
  do_cfr("B_PT"      , nfMC, nfData, tag, nfout,  50,      0.,  25000.,   nw);
  do_cfr("log(Mu_IPChi2)",nfMC,nfData,tag, nfout, 50,      2.,     12.,   nw);  
  do_cfr("Mu_P"       , nfMC, nfData, tag, nfout, 50,      0., 250000.,   nw);
  do_cfr("nSPDHits"   , nfMC, nfData, tag, nfout, 50,     0.,    450.,   nw);
  do_cfr("nTracks"    , nfMC, nfData, tag, nfout, 50,     0.,    450.,   nw);
  
   // //   do_cfr("MKPi1"     , nfMC, nfData, tag, nfout,  50,    600.,   1800.,   nw);
   // //   do_cfr("MPi1Pi2"   , nfMC, nfData, tag, nfout,  50,    250.,   1400.,   nw);
  
 // do_cfr("D_PZ"      , nfMC, nfData, tag, nfout,  50,      0., 200000.,   nw);
   // do_cfr("D_phi"     , nfMC, nfData, tag, nfout,  50,     -3.14,    3.14, nw);
   // do_cfr("nPV"       , nfMC, nfData, tag, nfout,   9,      0.,      9.,   nw);
   // do_cfr("D_t"       , nfMC, nfData, tag, nfout,  50,     -1.,      5.,   nw);
   // do_cfr("B_t"       , nfMC, nfData, tag, nfout,  50,     -0.,      8.,   nw);
   // //   do_cfr("D_DOCAMAX" , nfMC, nfData, tag, nfout,  50,      0.,      0.3,  nw);
  
  // do_cfr("B_PZ"      , nfMC, nfData, tag, nfout,  50,      0., 300000.,   nw);
   // do_cfr("B_phi"     , nfMC, nfData, tag, nfout,  50,     -3.14,    3.14, nw);
   // do_cfr("PVXERR"    , nfMC, nfData, tag, nfout,  50,      0.,      0.05, nw);
   // do_cfr("PVYERR"    , nfMC, nfData, tag, nfout,  50,      0.,      0.05, nw);
   // do_cfr("PVZERR"    , nfMC, nfData, tag, nfout,  50,      0.,      0.25, nw);
   // do_cfr("B_VX"      , nfMC, nfData, tag, nfout,  50,     -5.,      5.,   nw);
   // do_cfr("B_VY"      , nfMC, nfData, tag, nfout,  50,     -5.,      5.,   nw);
   // do_cfr("B_VZ"      , nfMC, nfData, tag, nfout,  50,    -200,    200.,   nw);
   // do_cfr("B_VChi2"   , nfMC, nfData, tag, nfout,  50,      0.,      6.,   nw);
   // do_cfr("PVNTRACKS" , nfMC, nfData, tag, nfout,  40,      0.,    120.,   nw);
   // do_cfr("PVX"       , nfMC, nfData, tag, nfout,  50,      0.7,     1.,   nw);
   // do_cfr("PVY"       , nfMC, nfData, tag, nfout,  50,     -0.35,    0.,   nw);
   // do_cfr("PVZ"       , nfMC, nfData, tag, nfout,  50,   -150.,    150.,   nw);
   // do_cfr("D_VX"      , nfMC, nfData, tag, nfout,  50,     -7.,      7.,   nw);
   // do_cfr("D_VY"      , nfMC, nfData, tag, nfout,  50,     -7.,      7.,   nw);
   // do_cfr("D_VZ"      , nfMC, nfData, tag, nfout,  50,    -140,    220.,   nw);
  
   // do_cfr("Mu_PZ"      , nfMC, nfData, tag, nfout, 50,      0., 300000.,   nw);
   // do_cfr("Mu_phi"     , nfMC, nfData, tag, nfout, 50,     -3.14,    3.14, nw);  

   // do_cfr("B_PT"      , nfMC, nfData, tag, nfout,  50,      0.,  15000.,   nw);





  return 1;
}
