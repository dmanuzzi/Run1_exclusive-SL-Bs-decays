#include <iostream>
using namespace std;
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "../../../tools/Tools.h"
#include "../../plot2Hratio.cpp"
#include "../../../myRew/myRew.cpp"
namespace regions{
  TString peak_D = " (D_M > 1850 && D_M < 1890)";
  TString side_D = "((D_M > 1830 && D_M < 1850) || (D_M > 1890  && D_M < 1910))" ;
  TString peak_Ds= " (D_M > 1940 && D_M < 2000)";
  TString side_Ds= "((D_M > 1910 && D_M < 1940) || (D_M > 2000  && D_M < 2030))" ;
  
  TString K1_P = "(sqrt(K1_PX**2+K1_PY**2+K1_PZ**2))";
  TString K1_E = "(sqrt("+K1_P+"**2+493.677**2))";
  TString K2_P = "(sqrt(K2_PX**2+K2_PY**2+K2_PZ**2))";
  TString K2_E = "(sqrt("+K2_P+"**2+493.677**2))";
  TString MKK  = "sqrt(("+K1_E+"+"+K2_E+")**2-((K1_PX+K2_PX)**2+(K1_PY+K2_PY)**2+(K1_PZ+K2_PZ)**2))";
  TString cut_for_BsSignal = "("+MKK+">1010 && "+MKK+"<1030)";
  TString cut_for_BdKKpi   = "1";

  TString onePV = "(nPVs==1)";

  TString K_P = "(sqrt(K_PX**2+K_PY**2+K_PZ**2))";
  TString K_E = "(sqrt("+K_P+"**2+493.677**2))";
  TString Pi1_P = "(sqrt(Pi1_PX**2+Pi1_PY**2+Pi1_PZ**2))";
  TString Pi1_E = "(sqrt("+Pi1_P+"**2+139.57**2))";
  TString Pi2_P = "(sqrt(Pi2_PX**2+Pi2_PY**2+Pi2_PZ**2))";
  TString Pi2_E = "(sqrt("+Pi2_P+"**2+139.57**2))";
  TString MKPi1 = "sqrt(("+K_E+"+"+Pi1_E+")**2-((K_PX+Pi1_PX)**2+(K_PY+Pi1_PY)**2+(K_PZ+Pi1_PZ)**2))";
  TString MKPi2 = "sqrt(("+K_E+"+"+Pi2_E+")**2-((K_PX+Pi2_PX)**2+(K_PY+Pi2_PY)**2+(K_PZ+Pi2_PZ)**2))";
  TString MPi1Pi2 = "sqrt(("+Pi1_E+"+"+Pi2_E+")**2-((Pi1_PX+Pi2_PX)**2+(Pi1_PY+Pi2_PY)**2+(Pi1_PZ+Pi2_PZ)**2))";

  TString C = Form("%f", Constants::C);
  TString D_FD  = "(sqrt((D_VX-B_VX)**2+(D_VY-B_VY)**2+(D_VZ-B_VZ)**2))";
  TString D_P2  = "(D_PX**2+D_PY**2+D_PZ**2)";
  TString D_FDP = "((D_VX-B_VX)*D_PX+(D_VY-B_VY)*D_PY+(D_VZ-B_VZ)*D_PZ)";
  TString M_D = Form("%f", Mass::D);
  TString D_t = "("+M_D+"*"+D_FDP+")/("+C+"*"+D_P2+")";
  TString D_TRUETAU = "(D_TRUETAU*1000)";
  
  TString B_FD  = "(sqrt((B_VX-B_PVX)**2+(B_VY-B_PVY)**2+(B_VZ-B_PVZ)**2))";
  TString B_P2  = "(B_PX**2+B_PY**2+B_PZ**2)";
  TString B_FDP = "((B_VX-B_PVX)*B_PX+(B_VY-B_PVY)*B_PY+(B_VZ-B_PVZ)*B_PZ)";
  TString M_B0 = Form("%f", Mass::B0);
  TString B_t = "("+M_B0+"*"+B_FDP+")/("+C+"*"+B_P2+")";
  TString B_TRUETAU = "(B_TRUETAU*1000)";
  TString B_TRUEP   = "(sqrt(B_TRUEP_X**2+B_TRUEP_Y**2+B_TRUEP_Z**2))";

  TString D_PT = "sqrt(D_PX**2+D_PY**2)";
  TString D_phi = "atan2(D_PY,D_PX)";
  TString B_PT = "sqrt(B_PX**2+B_PY**2)";
  TString B_phi = "atan2(B_PY,B_PX)";
  TString Mu_phi = "atan2(Mu_PY,Mu_PX)";

}

void options(TString &nvar, TString &cutMC, TString &cutData, TString &w, TString &peak, TString &side, TString &tag);

int do_cfr(TString nvar, TString nfin_MC, TString nfin_data, TString tag, TString nfout, 
	   int nbins =0,  double min = 0.,  double max = 0. , TString nW="none"){
  TString nvar_box=nvar; 
  TString cutMC   = "(1)";
  TString cutData = "(1)";

  TString w = "(1)";
  TString peak = "";
  TString side = "";
  options(nvar, cutMC, cutData, w,  peak, side,  tag);
  TString nvar_MC = nvar;
  TString nvar_data = nvar;
 
  TH1::SetDefaultSumw2();

  TFile *fMC = TFile::Open(nfin_MC, "READ");
  TTree *tMC = (TTree*)fMC->Get("DMutuple");
  
  if (nW!="none"){
    //    tMC->AddFriend("DMutuple", "re"+nW+".root");
    w = nW;
    std::cout << nW << endl;
  }
  
  if (nbins==0){
    nbins=100.;
    min=tMC->GetMinimum(nvar);
    max=tMC->GetMaximum(nvar);
  }

  TString weightMC = "(("+peak+" && "+cutMC+")*"+w+")";
  cout << weightMC << endl;
  TH1D *h_MC = new TH1D("h_MC_"+tag, "h_MC_"+tag, nbins, min, max);
  tMC->Draw(nvar_MC+">>h_MC_"+tag,weightMC ,"goff");
  h_MC->Scale(1./h_MC->Integral());

  TFile *fdata = TFile::Open(nfin_data, "READ");  
  TTree *tdata = (TTree*)fdata->Get("DMutuple");  
  TH1D *h_data_peak = new TH1D("h_data_peak_"+tag, "h_data_peak_"+tag, nbins, min, max);
  tdata->Draw(nvar_data+">>h_data_peak_"+tag, peak+" && "+cutData,"goff");
  TH1D *h_data_side = new TH1D("h_data_side_"+tag, "h_data_side_"+tag, nbins, min, max);

  TH1D *h_data = new TH1D("h_data_"+tag, "h_data_"+tag, nbins, min, max);
  h_data->Add(h_data_peak, +1);
  if (!tag.Contains("Res")){
    tdata->Draw(nvar_data+">>h_data_side_"+tag,  side+" && "+cutData,"goff");
    h_data->Add(h_data_side, -1);
  }
  h_data->Scale(1./h_data->Integral());
  plot2Hratio(h_data, h_MC, nvar_box, nfout, "_"+tag, false, "./"+tag+"/");

  delete h_MC;
  delete h_data_peak;
  delete h_data_side;
  delete h_data;
  fMC->Close();
  fdata->Close();
  return 1;
}



void options(TString &nvar, TString &cutMC, TString &cutData, TString &w, TString &peak, TString &side, TString &tag){
  if (tag.Contains("BdKpipi") && nvar=="MKPi1" )
    nvar=regions::MKPi1;
  if (tag.Contains("BdKpipi") && nvar=="MPi1Pi2")
    nvar=regions::MPi1Pi2;
  if (nvar=="D_t")
    nvar=regions::D_t;
  if (nvar=="D_FD")
    nvar=regions::D_FD;
  if (nvar=="B_t")
    nvar=regions::B_t;
  if (nvar=="B_FD")
    nvar=regions::B_FD;
  if (nvar=="D_t-D_TRUETAU")
    nvar=regions::D_t+"-"+regions::D_TRUETAU;
  if (nvar=="B_t-B_TRUETAU")
    nvar=regions::B_t+"-"+regions::B_TRUETAU;
  if (nvar=="B_phi")
    nvar=regions::B_phi;
  if (nvar=="D_phi")
    nvar=regions::D_phi;
  if (nvar=="Mu_phi")
    nvar=regions::Mu_phi;
  if (nvar=="B_PT")
    nvar=regions::B_PT;
  if (nvar=="D_PT")
    nvar=regions::D_PT;
  if (nvar=="B_TRUEP")
    nvar=regions::B_TRUEP;
  if (tag.Contains("BsSignal") && nvar=="MKK")
    nvar=regions::MKK;
  if (tag.Contains("onePV")){
    cutMC = cutData = "(nPV==1)";
  }
  if (tag.Contains("BsSignal")){
    peak = regions::peak_Ds;
    side = regions::side_Ds;
    cutMC = cutData = regions::cut_for_BsSignal;
  }
  if (tag.Contains("BdKpipi")){
    peak = regions::peak_D;
    side = regions::side_D;
    cutMC = cutData = regions::cut_for_BdKKpi; 
  }

  TString cut = cutMC;
  return;
}





/*
int do_cfr_2D_projections(TString nvar1, TString nvar2, 
			 TString nfin_MC, TString nfin_data, TString tag, TString nfout,
			 int nbins1 =0,  double min1 = 0.,  double max1 = 0.,
			 int nbins2 =0,  double min2 = 0.,  double max2 = 0.){
  
  TString nvar1_box=nvar1, nvar2_box=nvar2;
  
  if (tag.Contains("Run1") && nvar1=="MKPi1" )
    nvar1=regions::MKPi1;
  if (tag.Contains("Run1") && nvar2=="MPi1Pi2")
    nvar2=regions::MPi1Pi2;
  
  TH1::SetDefaultSumw2();
  TFile *fMC = TFile::Open(nfin_MC, "READ");
  TTree *tMC = (TTree*)fMC->Get("DMutuple");
  if (nbins1==0){
    nbins1=100.;
    min1=tMC->GetMinimum(nvar1);
    max1=tMC->GetMaximum(nvar1);
  }
  if (nbins2==0){
    nbins2=100.;
    min2=tMC->GetMinimum(nvar2);
    max2=tMC->GetMaximum(nvar2);
  }
  TH2D *h_MC_2D = new TH2D("h_MC_2D_"+tag, "h_MC_2D_"+tag, nbins1, min1, max1, nbins2, min2, max2);
  tMC->Draw(nvar2+":"+nvar1+">>h_MC_2D_"+tag, regions::peak,"goff");
  TH1D *h_MC_x = h_MC_2D->ProjectionX();
  h_MC_x->Scale(1./h_MC_x->Integral());
  TH1D *h_MC_y = h_MC_2D->ProjectionY();
  h_MC_y->Scale(1./h_MC_y->Integral());

  TFile *fdata = TFile::Open(nfin_data, "READ");  
  TTree *tdata = (TTree*)fdata->Get("DMutuple");  
  TH2D *h_data_peak_2D = new TH2D("h_data_peak_2D_"+tag, "h_data_peak_2D_"+tag, nbins1, min1, max1, nbins2, min2, max2);
  tdata->Draw(nvar2+":"+nvar1+">>h_data_peak_2D_"+tag,  regions::peak,"goff");
  TH2D *h_data_side_2D = new TH2D("h_data_side_2D_"+tag, "h_data_side_2D_"+tag, nbins1, min1, max1, nbins2, min2, max2);
  tdata->Draw(nvar2+":"+nvar1+">>h_data_side_2D_"+tag,  regions::side,"goff");
  TH2D *h_data_2D   = new TH2D("h_data_2D_"+tag, "h_data_2D_"+tag,  nbins1, min1, max1, nbins2, min2, max2);
  h_data_2D->Add(h_data_peak_2D, +1);
  h_data_2D->Add(h_data_side_2D, -1);
  TH1D *h_data_x = h_data_2D->ProjectionX();
  h_data_x->Scale(1./h_data_x->Integral());
  TH1D *h_data_y = h_data_2D->ProjectionY();
  h_data_y->Scale(1./h_data_y->Integral());

  plot2Hratio(h_data_x, h_MC_x, nvar1_box, nfout, "_"+tag, false, "./");
  plot2Hratio(h_data_y, h_MC_y, nvar2_box, nfout, "_"+tag, false, "./");

  delete h_MC_2D;
  delete h_data_peak_2D;
  delete h_data_side_2D;
  delete h_data_2D;
  delete h_MC_x;
  delete h_data_x;
  delete h_MC_y;
  delete h_data_y;
  fMC->Close();
  fdata->Close();
  return 1;
}
*/
