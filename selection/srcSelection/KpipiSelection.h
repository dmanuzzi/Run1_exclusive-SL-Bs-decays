#ifndef KpipiSelection_h
#define KpipiSelection_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include "../rewFormFactors/FormFactorsRew.C"

class KpipiSelection {
public :
  KpipiSelection(TTree *tree=0, Bool_t ismc=false, Bool_t isbkg=false,  Bool_t isb2dd=false);
  virtual ~KpipiSelection();
   
  bool SetChain(TTree *tree, Bool_t ismc=false, Bool_t isbkg=false,  Bool_t isb2dd=false);
  bool PassTrigger();
  bool TruthMatch();
  bool PassKin();
  bool PassPID();
  void Loop();

  bool doTruthMatch;
  bool doKinCuts;
  bool doKinVeto;
  bool doPIDCuts;
  bool doPIDhadro;
  bool doPIDmu;
  bool doTriggCuts;
  bool doTriggL0;
  bool doTriggHlt1;
  bool doTriggHlt2;


  TString         outFileName;
  TString         outFileCandidates;
  Bool_t          debug;
  FormFactorsRew  rew;

  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual Bool_t   Init(TTree *tree);

  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain
  Bool_t          isMC;
  Bool_t          isBkg;
  Bool_t          isB2DD;
  Int_t           signalCategory;
  Int_t           signalCategory_old;

  // Declaration of leaf types
  Int_t           B_TRUEID;
  Int_t           B_MC_MOTHER_ID;
  Int_t           B_MC_MOTHER_KEY;
  Int_t           B_MC_GD_MOTHER_ID;
  Int_t           B_MC_GD_MOTHER_KEY;
  Int_t           B_MC_GD_GD_MOTHER_ID;
  Int_t           B_MC_GD_GD_MOTHER_KEY;
  Double_t        B_TRUEP_E;
  Double_t        B_TRUEP_X;
  Double_t        B_TRUEP_Y;
  Double_t        B_TRUEP_Z;
  Double_t        B_TRUEPT;
  Double_t        B_TRUEORIGINVERTEX_X;
  Double_t        B_TRUEORIGINVERTEX_Y;
  Double_t        B_TRUEORIGINVERTEX_Z;
  Double_t        B_TRUEENDVERTEX_X;
  Double_t        B_TRUEENDVERTEX_Y;
  Double_t        B_TRUEENDVERTEX_Z;
  Bool_t          B_TRUEISSTABLE;
  Double_t        B_TRUETAU;
  Double_t        B_DOCA;
  Double_t        B_FDChi2;
  Double_t        B_M;
  Double_t        B_PVX;
  Double_t        B_PVY;
  Double_t        B_PVZ;
  Double_t        B_PX;
  Double_t        B_PY;
  Double_t        B_PZ;
  Double_t        B_VChi2;
  Double_t        B_VX;
  Double_t        B_VY;
  Double_t        B_VZ;
  Int_t           B_BKGCAT;
  Double_t        B_SV_X;
  Double_t        B_SV_Y;
  Double_t        B_SV_Z;
  Double_t        B_SV_CHI2;
  Int_t           B_SV_NDOF;
  Double_t        B_PV_X;
  Double_t        B_PV_Y;
  Double_t        B_PV_Z;
  Double_t        B_PV_CHI2;
  Int_t           B_PV_NDOF;
  Double_t        B_MCORR;
  Double_t        B_MCORRERR;
  Double_t        B_MCORRFULLERR;
  Double_t        B_Q2SOL1;
  Double_t        B_Q2SOL2;
  Bool_t          B_L0Global_Dec;
  Bool_t          B_L0Global_TIS;
  Bool_t          B_L0Global_TOS;
  Bool_t          B_Hlt1Global_Dec;
  Bool_t          B_Hlt1Global_TIS;
  Bool_t          B_Hlt1Global_TOS;
  Bool_t          B_Hlt1Phys_Dec;
  Bool_t          B_Hlt1Phys_TIS;
  Bool_t          B_Hlt1Phys_TOS;
  Bool_t          B_Hlt2Global_Dec;
  Bool_t          B_Hlt2Global_TIS;
  Bool_t          B_Hlt2Global_TOS;
  Bool_t          B_Hlt2Phys_Dec;
  Bool_t          B_Hlt2TopoMu2BodyBBDTDecision_Dec;
  Bool_t          B_Hlt2TopoMu2BodyBBDTDecision_TIS;
  Bool_t          B_Hlt2TopoMu2BodyBBDTDecision_TOS;
  Bool_t          B_Hlt2TopoMu3BodyBBDTDecision_Dec;
  Bool_t          B_Hlt2TopoMu3BodyBBDTDecision_TIS;
  Bool_t          B_Hlt2TopoMu3BodyBBDTDecision_TOS;
  Bool_t          B_Hlt2TopoMu4BodyBBDTDecision_Dec;
  Bool_t          B_Hlt2TopoMu4BodyBBDTDecision_TIS;
  Bool_t          B_Hlt2TopoMu4BodyBBDTDecision_TOS;
  Bool_t          B_Hlt2Topo2BodyBBDTDecision_Dec;
  Bool_t          B_Hlt2Topo2BodyBBDTDecision_TIS;
  Bool_t          B_Hlt2Topo2BodyBBDTDecision_TOS;
  Bool_t          B_Hlt2Topo3BodyBBDTDecision_Dec;
  Bool_t          B_Hlt2Topo3BodyBBDTDecision_TIS;
  Bool_t          B_Hlt2Topo3BodyBBDTDecision_TOS;
  Bool_t          B_Hlt2Topo4BodyBBDTDecision_Dec;
  Bool_t          B_Hlt2Topo4BodyBBDTDecision_TIS;
  Bool_t          B_Hlt2Topo4BodyBBDTDecision_TOS;
  Int_t           D_TRUEID;
  Int_t           D_MC_MOTHER_ID;
  Int_t           D_MC_MOTHER_KEY;
  Int_t           D_MC_GD_MOTHER_ID;
  Int_t           D_MC_GD_MOTHER_KEY;
  Int_t           D_MC_GD_GD_MOTHER_ID;
  Int_t           D_MC_GD_GD_MOTHER_KEY;
  Double_t        D_TRUEP_E;
  Double_t        D_TRUEP_X;
  Double_t        D_TRUEP_Y;
  Double_t        D_TRUEP_Z;
  Double_t        D_TRUEPT;
  Double_t        D_TRUEORIGINVERTEX_X;
  Double_t        D_TRUEORIGINVERTEX_Y;
  Double_t        D_TRUEORIGINVERTEX_Z;
  Double_t        D_TRUEENDVERTEX_X;
  Double_t        D_TRUEENDVERTEX_Y;
  Double_t        D_TRUEENDVERTEX_Z;
  Bool_t          D_TRUEISSTABLE;
  Double_t        D_TRUETAU;
  Double_t        D_DOCAMAX;
  Double_t        D_FDChi2;
  Double_t        D_IP;
  Double_t        D_IPChi2;
  Double_t        D_M;
  Double_t        D_PVX;
  Double_t        D_PVY;
  Double_t        D_PVZ;
  Double_t        D_PX;
  Double_t        D_PY;
  Double_t        D_PZ;
  Double_t        D_VChi2;
  Double_t        D_VX;
  Double_t        D_VY;
  Double_t        D_VZ;
  Int_t           D_BKGCAT;
  Int_t           Pi1_TRUEID;
  Int_t           Pi1_MC_MOTHER_ID;
  Int_t           Pi1_MC_MOTHER_KEY;
  Int_t           Pi1_MC_GD_MOTHER_ID;
  Int_t           Pi1_MC_GD_MOTHER_KEY;
  Int_t           Pi1_MC_GD_GD_MOTHER_ID;
  Int_t           Pi1_MC_GD_GD_MOTHER_KEY;
  Double_t        Pi1_TRUEP_E;
  Double_t        Pi1_TRUEP_X;
  Double_t        Pi1_TRUEP_Y;
  Double_t        Pi1_TRUEP_Z;
  Double_t        Pi1_TRUEPT;
  Double_t        Pi1_TRUEORIGINVERTEX_X;
  Double_t        Pi1_TRUEORIGINVERTEX_Y;
  Double_t        Pi1_TRUEORIGINVERTEX_Z;
  Double_t        Pi1_TRUEENDVERTEX_X;
  Double_t        Pi1_TRUEENDVERTEX_Y;
  Double_t        Pi1_TRUEENDVERTEX_Z;
  Bool_t          Pi1_TRUEISSTABLE;
  Double_t        Pi1_TRUETAU;
  Double_t        Pi1_IP;
  Double_t        Pi1_IPChi2;
  Double_t        Pi1_IsMuon;
  Double_t        Pi1_PX;
  Double_t        Pi1_PY;
  Double_t        Pi1_PZ;
  Double_t        Pi1_PIDK;
  Double_t        Pi1_ProbNNe;
  Double_t        Pi1_ProbNNghost;
  Double_t        Pi1_ProbNNk;
  Double_t        Pi1_ProbNNmu;
  Double_t        Pi1_ProbNNp;
  Double_t        Pi1_ProbNNpi;
  Double_t        Pi1_Q;
  Int_t           Pi2_TRUEID;
  Int_t           Pi2_MC_MOTHER_ID;
  Int_t           Pi2_MC_MOTHER_KEY;
  Int_t           Pi2_MC_GD_MOTHER_ID;
  Int_t           Pi2_MC_GD_MOTHER_KEY;
  Int_t           Pi2_MC_GD_GD_MOTHER_ID;
  Int_t           Pi2_MC_GD_GD_MOTHER_KEY;
  Double_t        Pi2_TRUEP_E;
  Double_t        Pi2_TRUEP_X;
  Double_t        Pi2_TRUEP_Y;
  Double_t        Pi2_TRUEP_Z;
  Double_t        Pi2_TRUEPT;
  Double_t        Pi2_TRUEORIGINVERTEX_X;
  Double_t        Pi2_TRUEORIGINVERTEX_Y;
  Double_t        Pi2_TRUEORIGINVERTEX_Z;
  Double_t        Pi2_TRUEENDVERTEX_X;
  Double_t        Pi2_TRUEENDVERTEX_Y;
  Double_t        Pi2_TRUEENDVERTEX_Z;
  Bool_t          Pi2_TRUEISSTABLE;
  Double_t        Pi2_TRUETAU;
  Double_t        Pi2_IP;
  Double_t        Pi2_IPChi2;
  Double_t        Pi2_IsMuon;
  Double_t        Pi2_PX;
  Double_t        Pi2_PY;
  Double_t        Pi2_PZ;
  Double_t        Pi2_PIDK;
  Double_t        Pi2_ProbNNe;
  Double_t        Pi2_ProbNNghost;
  Double_t        Pi2_ProbNNk;
  Double_t        Pi2_ProbNNmu;
  Double_t        Pi2_ProbNNp;
  Double_t        Pi2_ProbNNpi;
  Double_t        Pi2_Q;
  Int_t           K_TRUEID;
  Int_t           K_MC_MOTHER_ID;
  Int_t           K_MC_MOTHER_KEY;
  Int_t           K_MC_GD_MOTHER_ID;
  Int_t           K_MC_GD_MOTHER_KEY;
  Int_t           K_MC_GD_GD_MOTHER_ID;
  Int_t           K_MC_GD_GD_MOTHER_KEY;
  Double_t        K_TRUEP_E;
  Double_t        K_TRUEP_X;
  Double_t        K_TRUEP_Y;
  Double_t        K_TRUEP_Z;
  Double_t        K_TRUEPT;
  Double_t        K_TRUEORIGINVERTEX_X;
  Double_t        K_TRUEORIGINVERTEX_Y;
  Double_t        K_TRUEORIGINVERTEX_Z;
  Double_t        K_TRUEENDVERTEX_X;
  Double_t        K_TRUEENDVERTEX_Y;
  Double_t        K_TRUEENDVERTEX_Z;
  Bool_t          K_TRUEISSTABLE;
  Double_t        K_TRUETAU;
  Double_t        K_IP;
  Double_t        K_IPChi2;
  Double_t        K_IsMuon;
  Double_t        K_PX;
  Double_t        K_PY;
  Double_t        K_PZ;
  Double_t        K_PIDK;
  Double_t        K_ProbNNe;
  Double_t        K_ProbNNghost;
  Double_t        K_ProbNNk;
  Double_t        K_ProbNNmu;
  Double_t        K_ProbNNp;
  Double_t        K_ProbNNpi;
  Double_t        K_Q;
  Int_t           Mu_TRUEID;
  Int_t           Mu_MC_MOTHER_ID;
  Int_t           Mu_MC_MOTHER_KEY;
  Int_t           Mu_MC_GD_MOTHER_ID;
  Int_t           Mu_MC_GD_MOTHER_KEY;
  Int_t           Mu_MC_GD_GD_MOTHER_ID;
  Int_t           Mu_MC_GD_GD_MOTHER_KEY;
  Double_t        Mu_TRUEP_E;
  Double_t        Mu_TRUEP_X;
  Double_t        Mu_TRUEP_Y;
  Double_t        Mu_TRUEP_Z;
  Double_t        Mu_TRUEPT;
  Double_t        Mu_TRUEORIGINVERTEX_X;
  Double_t        Mu_TRUEORIGINVERTEX_Y;
  Double_t        Mu_TRUEORIGINVERTEX_Z;
  Double_t        Mu_TRUEENDVERTEX_X;
  Double_t        Mu_TRUEENDVERTEX_Y;
  Double_t        Mu_TRUEENDVERTEX_Z;
  Bool_t          Mu_TRUEISSTABLE;
  Double_t        Mu_TRUETAU;
  Double_t        Mu_IP;
  Double_t        Mu_IPChi2;
  Double_t        Mu_IsMuon;
  Double_t        Mu_PX;
  Double_t        Mu_PY;
  Double_t        Mu_PZ;
  Double_t        Mu_PIDmu;
  Double_t        Mu_ProbNNe;
  Double_t        Mu_ProbNNghost;
  Double_t        Mu_ProbNNk;
  Double_t        Mu_ProbNNmu;
  Double_t        Mu_ProbNNp;
  Double_t        Mu_ProbNNpi;
  Double_t        Mu_Q;
  Bool_t          Mu_L0Global_Dec;
  Bool_t          Mu_L0Global_TIS;
  Bool_t          Mu_L0Global_TOS;
  Bool_t          Mu_Hlt1Global_Dec;
  Bool_t          Mu_Hlt1Global_TIS;
  Bool_t          Mu_Hlt1Global_TOS;
  Bool_t          Mu_Hlt1Phys_Dec;
  Bool_t          Mu_Hlt1Phys_TIS;
  Bool_t          Mu_Hlt1Phys_TOS;
  Bool_t          Mu_Hlt2Global_Dec;
  Bool_t          Mu_Hlt2Global_TIS;
  Bool_t          Mu_Hlt2Global_TOS;
  Bool_t          Mu_Hlt2Phys_Dec;
  Bool_t          Mu_L0MuonDecision_Dec;
  Bool_t          Mu_L0MuonDecision_TIS;
  Bool_t          Mu_L0MuonDecision_TOS;
  Bool_t          Mu_Hlt1TrackAllL0Decision_Dec;
  Bool_t          Mu_Hlt1TrackAllL0Decision_TIS;
  Bool_t          Mu_Hlt1TrackAllL0Decision_TOS;
  Bool_t          Mu_Hlt1TrackMuonDecision_Dec;
  Bool_t          Mu_Hlt1TrackMuonDecision_TIS;
  Bool_t          Mu_Hlt1TrackMuonDecision_TOS;
  Bool_t          Mu_Hlt1SingleMuonHighPTDecision_Dec;
  Bool_t          Mu_Hlt1SingleMuonHighPTDecision_TIS;
  Bool_t          Mu_Hlt1SingleMuonHighPTDecision_TOS;
  Bool_t          Mu_Hlt2SingleMuonDecision_Dec;
  Bool_t          Mu_Hlt2SingleMuonDecision_TIS;
  Bool_t          Mu_Hlt2SingleMuonDecision_TOS;
  UInt_t          nCandidate;
  ULong64_t       totCandidates;
  ULong64_t       EventInSequence;
  UInt_t          runNumber;
  ULong64_t       eventNumber;
  UInt_t          BCID;
  Int_t           BCType;
  UInt_t          OdinTCK;
  UInt_t          L0DUTCK;
  UInt_t          HLTTCK;
  ULong64_t       GpsTime;
  Short_t         Polarity;
  Int_t           nPV;
  Float_t         PVX[100];   //[nPV]
  Float_t         PVY[100];   //[nPV]
  Float_t         PVZ[100];   //[nPV]
  Float_t         PVXERR[100];   //[nPV]
  Float_t         PVYERR[100];   //[nPV]
  Float_t         PVZERR[100];   //[nPV]
  Float_t         PVCHI2[100];   //[nPV]
  Float_t         PVNDOF[100];   //[nPV]
  Float_t         PVNTRACKS[100];   //[nPV]
  Int_t           L0Global;
  UInt_t          Hlt1Global;
  UInt_t          Hlt2Global;
  Int_t           L0MuonDecision;
  UInt_t          L0nSelections;
  Int_t           Hlt1TrackAllL0Decision;
  Int_t           Hlt1TrackMuonDecision;
  Int_t           Hlt1SingleMuonHighPTDecision;
  UInt_t          Hlt1nSelections;
  Int_t           Hlt2SingleMuonDecision;
  Int_t           Hlt2TopoMu2BodyBBDTDecision;
  Int_t           Hlt2TopoMu3BodyBBDTDecision;
  Int_t           Hlt2TopoMu4BodyBBDTDecision;
  Int_t           Hlt2Topo2BodyBBDTDecision;
  Int_t           Hlt2Topo3BodyBBDTDecision;
  Int_t           Hlt2Topo4BodyBBDTDecision;
  UInt_t          Hlt2nSelections;
  Int_t           MaxRoutingBits;
  Float_t         RoutingBits[64];   //[MaxRoutingBits]
  Int_t           B_Nu_TRUEID;
  Double_t        B_Nu_TRUEP_E;
  Double_t        B_Nu_TRUEP_X;
  Double_t        B_Nu_TRUEP_Y;
  Double_t        B_Nu_TRUEP_Z;
  Int_t           D_Nu_TRUEID;
  Double_t        D_Nu_TRUEP_E;
  Double_t        D_Nu_TRUEP_X;
  Double_t        D_Nu_TRUEP_Y;
  Double_t        D_Nu_TRUEP_Z;
  Int_t           Pi1_Nu_TRUEID;
  Double_t        Pi1_Nu_TRUEP_E;
  Double_t        Pi1_Nu_TRUEP_X;
  Double_t        Pi1_Nu_TRUEP_Y;
  Double_t        Pi1_Nu_TRUEP_Z;
  Int_t           Pi2_Nu_TRUEID;
  Double_t        Pi2_Nu_TRUEP_E;
  Double_t        Pi2_Nu_TRUEP_X;
  Double_t        Pi2_Nu_TRUEP_Y;
  Double_t        Pi2_Nu_TRUEP_Z;
  Int_t           K_Nu_TRUEID;
  Double_t        K_Nu_TRUEP_E;
  Double_t        K_Nu_TRUEP_X;
  Double_t        K_Nu_TRUEP_Y;
  Double_t        K_Nu_TRUEP_Z;
  Int_t           Mu_Nu_TRUEID;
  Double_t        Mu_Nu_TRUEP_E;
  Double_t        Mu_Nu_TRUEP_X;
  Double_t        Mu_Nu_TRUEP_Y;
  Double_t        Mu_Nu_TRUEP_Z;
  
  //PIDCorr variables                                                                                                                                                            
  Double_t        Mu_PIDmu_corr;
  Double_t        Mu_ProbNNmu_corr;
  Double_t        Pi1_PIDK_corr;
  Double_t        Pi1_ProbNNpi_corr;
  Double_t        Pi2_PIDK_corr;
  Double_t        Pi2_ProbNNpi_corr;
  Double_t        K_PIDK_corr;
  Double_t        K_ProbNNk_corr;
  // TM variables
  Double_t        B_IsBdCat1_excl;
  Double_t        B_IsBdCat1_incl;
  Double_t        B_IsBdCat2_excl;
  Double_t        B_IsBdCat2_incl;
  Double_t        B_IsBdCat4_incl;

 private :
  // List of branches
  TBranch        *b_B_TRUEID;   //!
  TBranch        *b_B_MC_MOTHER_ID;   //!
  TBranch        *b_B_MC_MOTHER_KEY;   //!
  TBranch        *b_B_MC_GD_MOTHER_ID;   //!
  TBranch        *b_B_MC_GD_MOTHER_KEY;   //!
  TBranch        *b_B_MC_GD_GD_MOTHER_ID;   //!
  TBranch        *b_B_MC_GD_GD_MOTHER_KEY;   //!
  TBranch        *b_B_TRUEP_E;   //!
  TBranch        *b_B_TRUEP_X;   //!
  TBranch        *b_B_TRUEP_Y;   //!
  TBranch        *b_B_TRUEP_Z;   //!
  TBranch        *b_B_TRUEPT;   //!
  TBranch        *b_B_TRUEORIGINVERTEX_X;   //!
  TBranch        *b_B_TRUEORIGINVERTEX_Y;   //!
  TBranch        *b_B_TRUEORIGINVERTEX_Z;   //!
  TBranch        *b_B_TRUEENDVERTEX_X;   //!
  TBranch        *b_B_TRUEENDVERTEX_Y;   //!
  TBranch        *b_B_TRUEENDVERTEX_Z;   //!
  TBranch        *b_B_TRUEISSTABLE;   //!
  TBranch        *b_B_TRUETAU;   //!
  TBranch        *b_B_DOCA;   //!
  TBranch        *b_B_FDChi2;   //!
  TBranch        *b_B_M;   //!
  TBranch        *b_B_PVX;   //!
  TBranch        *b_B_PVY;   //!
  TBranch        *b_B_PVZ;   //!
  TBranch        *b_B_PX;   //!
  TBranch        *b_B_PY;   //!
  TBranch        *b_B_PZ;   //!
  TBranch        *b_B_VChi2;   //!
  TBranch        *b_B_VX;   //!
  TBranch        *b_B_VY;   //!
  TBranch        *b_B_VZ;   //!
  TBranch        *b_B_BKGCAT;   //!
  TBranch        *b_B_SV_X;   //!
  TBranch        *b_B_SV_Y;   //!
  TBranch        *b_B_SV_Z;   //!
  TBranch        *b_B_SV_CHI2;   //!
  TBranch        *b_B_SV_NDOF;   //!
  TBranch        *b_B_PV_X;   //!
  TBranch        *b_B_PV_Y;   //!
  TBranch        *b_B_PV_Z;   //!
  TBranch        *b_B_PV_CHI2;   //!
  TBranch        *b_B_PV_NDOF;   //!
  TBranch        *b_B_MCORR;   //!
  TBranch        *b_B_MCORRERR;   //!
  TBranch        *b_B_MCORRFULLERR;   //!
  TBranch        *b_B_Q2SOL1;   //!
  TBranch        *b_B_Q2SOL2;   //!
  TBranch        *b_B_L0Global_Dec;   //!
  TBranch        *b_B_L0Global_TIS;   //!
  TBranch        *b_B_L0Global_TOS;   //!
  TBranch        *b_B_Hlt1Global_Dec;   //!
  TBranch        *b_B_Hlt1Global_TIS;   //!
  TBranch        *b_B_Hlt1Global_TOS;   //!
  TBranch        *b_B_Hlt1Phys_Dec;   //!
  TBranch        *b_B_Hlt1Phys_TIS;   //!
  TBranch        *b_B_Hlt1Phys_TOS;   //!
  TBranch        *b_B_Hlt2Global_Dec;   //!
  TBranch        *b_B_Hlt2Global_TIS;   //!
  TBranch        *b_B_Hlt2Global_TOS;   //!
  TBranch        *b_B_Hlt2Phys_Dec;   //!
  TBranch        *b_B_Hlt2TopoMu2BodyBBDTDecision_Dec;   //!
  TBranch        *b_B_Hlt2TopoMu2BodyBBDTDecision_TIS;   //!
  TBranch        *b_B_Hlt2TopoMu2BodyBBDTDecision_TOS;   //!
  TBranch        *b_B_Hlt2TopoMu3BodyBBDTDecision_Dec;   //!
  TBranch        *b_B_Hlt2TopoMu3BodyBBDTDecision_TIS;   //!
  TBranch        *b_B_Hlt2TopoMu3BodyBBDTDecision_TOS;   //!
  TBranch        *b_B_Hlt2TopoMu4BodyBBDTDecision_Dec;   //!
  TBranch        *b_B_Hlt2TopoMu4BodyBBDTDecision_TIS;   //!
  TBranch        *b_B_Hlt2TopoMu4BodyBBDTDecision_TOS;   //!
  TBranch        *b_B_Hlt2Topo2BodyBBDTDecision_Dec;   //!
  TBranch        *b_B_Hlt2Topo2BodyBBDTDecision_TIS;   //!
  TBranch        *b_B_Hlt2Topo2BodyBBDTDecision_TOS;   //!
  TBranch        *b_B_Hlt2Topo3BodyBBDTDecision_Dec;   //!
  TBranch        *b_B_Hlt2Topo3BodyBBDTDecision_TIS;   //!
  TBranch        *b_B_Hlt2Topo3BodyBBDTDecision_TOS;   //!
  TBranch        *b_B_Hlt2Topo4BodyBBDTDecision_Dec;   //!
  TBranch        *b_B_Hlt2Topo4BodyBBDTDecision_TIS;   //!
  TBranch        *b_B_Hlt2Topo4BodyBBDTDecision_TOS;   //!
  TBranch        *b_D_TRUEID;   //!
  TBranch        *b_D_MC_MOTHER_ID;   //!
  TBranch        *b_D_MC_MOTHER_KEY;   //!
  TBranch        *b_D_MC_GD_MOTHER_ID;   //!
  TBranch        *b_D_MC_GD_MOTHER_KEY;   //!
  TBranch        *b_D_MC_GD_GD_MOTHER_ID;   //!
  TBranch        *b_D_MC_GD_GD_MOTHER_KEY;   //!
  TBranch        *b_D_TRUEP_E;   //!
  TBranch        *b_D_TRUEP_X;   //!
  TBranch        *b_D_TRUEP_Y;   //!
  TBranch        *b_D_TRUEP_Z;   //!
  TBranch        *b_D_TRUEPT;   //!
  TBranch        *b_D_TRUEORIGINVERTEX_X;   //!
  TBranch        *b_D_TRUEORIGINVERTEX_Y;   //!
  TBranch        *b_D_TRUEORIGINVERTEX_Z;   //!
  TBranch        *b_D_TRUEENDVERTEX_X;   //!
  TBranch        *b_D_TRUEENDVERTEX_Y;   //!
  TBranch        *b_D_TRUEENDVERTEX_Z;   //!
  TBranch        *b_D_TRUEISSTABLE;   //!
  TBranch        *b_D_TRUETAU;   //!
  TBranch        *b_D_DOCAMAX;   //!
  TBranch        *b_D_FDChi2;   //!
  TBranch        *b_D_IP;   //!
  TBranch        *b_D_IPChi2;   //!
  TBranch        *b_D_M;   //!
  TBranch        *b_D_PVX;   //!
  TBranch        *b_D_PVY;   //!
  TBranch        *b_D_PVZ;   //!
  TBranch        *b_D_PX;   //!
  TBranch        *b_D_PY;   //!
  TBranch        *b_D_PZ;   //!
  TBranch        *b_D_VChi2;   //!
  TBranch        *b_D_VX;   //!
  TBranch        *b_D_VY;   //!
  TBranch        *b_D_VZ;   //!
  TBranch        *b_D_BKGCAT;   //!
  TBranch        *b_Pi1_TRUEID;   //!
  TBranch        *b_Pi1_MC_MOTHER_ID;   //!
  TBranch        *b_Pi1_MC_MOTHER_KEY;   //!
  TBranch        *b_Pi1_MC_GD_MOTHER_ID;   //!
  TBranch        *b_Pi1_MC_GD_MOTHER_KEY;   //!
  TBranch        *b_Pi1_MC_GD_GD_MOTHER_ID;   //!
  TBranch        *b_Pi1_MC_GD_GD_MOTHER_KEY;   //!
  TBranch        *b_Pi1_TRUEP_E;   //!
  TBranch        *b_Pi1_TRUEP_X;   //!
  TBranch        *b_Pi1_TRUEP_Y;   //!
  TBranch        *b_Pi1_TRUEP_Z;   //!
  TBranch        *b_Pi1_TRUEPT;   //!
  TBranch        *b_Pi1_TRUEORIGINVERTEX_X;   //!
  TBranch        *b_Pi1_TRUEORIGINVERTEX_Y;   //!
  TBranch        *b_Pi1_TRUEORIGINVERTEX_Z;   //!
  TBranch        *b_Pi1_TRUEENDVERTEX_X;   //!
  TBranch        *b_Pi1_TRUEENDVERTEX_Y;   //!
  TBranch        *b_Pi1_TRUEENDVERTEX_Z;   //!
  TBranch        *b_Pi1_TRUEISSTABLE;   //!
  TBranch        *b_Pi1_TRUETAU;   //!
  TBranch        *b_Pi1_IP;   //!
  TBranch        *b_Pi1_IPChi2;   //!
  TBranch        *b_Pi1_IsMuon;   //!
  TBranch        *b_Pi1_PX;   //!
  TBranch        *b_Pi1_PY;   //!
  TBranch        *b_Pi1_PZ;   //!
  TBranch        *b_Pi1_PIDK;   //!
  TBranch        *b_Pi1_ProbNNe;   //!
  TBranch        *b_Pi1_ProbNNghost;   //!
  TBranch        *b_Pi1_ProbNNk;   //!
  TBranch        *b_Pi1_ProbNNmu;   //!
  TBranch        *b_Pi1_ProbNNp;   //!
  TBranch        *b_Pi1_ProbNNpi;   //!
  TBranch        *b_Pi1_Q;   //!
  TBranch        *b_Pi2_TRUEID;   //!
  TBranch        *b_Pi2_MC_MOTHER_ID;   //!
  TBranch        *b_Pi2_MC_MOTHER_KEY;   //!
  TBranch        *b_Pi2_MC_GD_MOTHER_ID;   //!
  TBranch        *b_Pi2_MC_GD_MOTHER_KEY;   //!
  TBranch        *b_Pi2_MC_GD_GD_MOTHER_ID;   //!
  TBranch        *b_Pi2_MC_GD_GD_MOTHER_KEY;   //!
  TBranch        *b_Pi2_TRUEP_E;   //!
  TBranch        *b_Pi2_TRUEP_X;   //!
  TBranch        *b_Pi2_TRUEP_Y;   //!
  TBranch        *b_Pi2_TRUEP_Z;   //!
  TBranch        *b_Pi2_TRUEPT;   //!
  TBranch        *b_Pi2_TRUEORIGINVERTEX_X;   //!
  TBranch        *b_Pi2_TRUEORIGINVERTEX_Y;   //!
  TBranch        *b_Pi2_TRUEORIGINVERTEX_Z;   //!
  TBranch        *b_Pi2_TRUEENDVERTEX_X;   //!
  TBranch        *b_Pi2_TRUEENDVERTEX_Y;   //!
  TBranch        *b_Pi2_TRUEENDVERTEX_Z;   //!
  TBranch        *b_Pi2_TRUEISSTABLE;   //!
  TBranch        *b_Pi2_TRUETAU;   //!
  TBranch        *b_Pi2_IP;   //!
  TBranch        *b_Pi2_IPChi2;   //!
  TBranch        *b_Pi2_IsMuon;   //!
  TBranch        *b_Pi2_PX;   //!
  TBranch        *b_Pi2_PY;   //!
  TBranch        *b_Pi2_PZ;   //!
  TBranch        *b_Pi2_PIDK;   //!
  TBranch        *b_Pi2_ProbNNe;   //!
  TBranch        *b_Pi2_ProbNNghost;   //!
  TBranch        *b_Pi2_ProbNNk;   //!
  TBranch        *b_Pi2_ProbNNmu;   //!
  TBranch        *b_Pi2_ProbNNp;   //!
  TBranch        *b_Pi2_ProbNNpi;   //!
  TBranch        *b_Pi2_Q;   //!
  TBranch        *b_K_TRUEID;   //!
  TBranch        *b_K_MC_MOTHER_ID;   //!
  TBranch        *b_K_MC_MOTHER_KEY;   //!
  TBranch        *b_K_MC_GD_MOTHER_ID;   //!
  TBranch        *b_K_MC_GD_MOTHER_KEY;   //!
  TBranch        *b_K_MC_GD_GD_MOTHER_ID;   //!
  TBranch        *b_K_MC_GD_GD_MOTHER_KEY;   //!
  TBranch        *b_K_TRUEP_E;   //!
  TBranch        *b_K_TRUEP_X;   //!
  TBranch        *b_K_TRUEP_Y;   //!
  TBranch        *b_K_TRUEP_Z;   //!
  TBranch        *b_K_TRUEPT;   //!
  TBranch        *b_K_TRUEORIGINVERTEX_X;   //!
  TBranch        *b_K_TRUEORIGINVERTEX_Y;   //!
  TBranch        *b_K_TRUEORIGINVERTEX_Z;   //!
  TBranch        *b_K_TRUEENDVERTEX_X;   //!
  TBranch        *b_K_TRUEENDVERTEX_Y;   //!
  TBranch        *b_K_TRUEENDVERTEX_Z;   //!
  TBranch        *b_K_TRUEISSTABLE;   //!
  TBranch        *b_K_TRUETAU;   //!
  TBranch        *b_K_IP;   //!
  TBranch        *b_K_IPChi2;   //!
  TBranch        *b_K_IsMuon;   //!
  TBranch        *b_K_PX;   //!
  TBranch        *b_K_PY;   //!
  TBranch        *b_K_PZ;   //!
  TBranch        *b_K_PIDK;   //!
  TBranch        *b_K_ProbNNe;   //!
  TBranch        *b_K_ProbNNghost;   //!
  TBranch        *b_K_ProbNNk;   //!
  TBranch        *b_K_ProbNNmu;   //!
  TBranch        *b_K_ProbNNp;   //!
  TBranch        *b_K_ProbNNpi;   //!
  TBranch        *b_K_Q;   //!
  TBranch        *b_Mu_TRUEID;   //!
  TBranch        *b_Mu_MC_MOTHER_ID;   //!
  TBranch        *b_Mu_MC_MOTHER_KEY;   //!
  TBranch        *b_Mu_MC_GD_MOTHER_ID;   //!
  TBranch        *b_Mu_MC_GD_MOTHER_KEY;   //!
  TBranch        *b_Mu_MC_GD_GD_MOTHER_ID;   //!
  TBranch        *b_Mu_MC_GD_GD_MOTHER_KEY;   //!
  TBranch        *b_Mu_TRUEP_E;   //!
  TBranch        *b_Mu_TRUEP_X;   //!
  TBranch        *b_Mu_TRUEP_Y;   //!
  TBranch        *b_Mu_TRUEP_Z;   //!
  TBranch        *b_Mu_TRUEPT;   //!
  TBranch        *b_Mu_TRUEORIGINVERTEX_X;   //!
  TBranch        *b_Mu_TRUEORIGINVERTEX_Y;   //!
  TBranch        *b_Mu_TRUEORIGINVERTEX_Z;   //!
  TBranch        *b_Mu_TRUEENDVERTEX_X;   //!
  TBranch        *b_Mu_TRUEENDVERTEX_Y;   //!
  TBranch        *b_Mu_TRUEENDVERTEX_Z;   //!
  TBranch        *b_Mu_TRUEISSTABLE;   //!
  TBranch        *b_Mu_TRUETAU;   //!
  TBranch        *b_Mu_IP;   //!
  TBranch        *b_Mu_IPChi2;   //!
  TBranch        *b_Mu_IsMuon;   //!
  TBranch        *b_Mu_PX;   //!
  TBranch        *b_Mu_PY;   //!
  TBranch        *b_Mu_PZ;   //!
  TBranch        *b_Mu_PIDmu;   //!
  TBranch        *b_Mu_ProbNNe;   //!
  TBranch        *b_Mu_ProbNNghost;   //!
  TBranch        *b_Mu_ProbNNk;   //!
  TBranch        *b_Mu_ProbNNmu;   //!
  TBranch        *b_Mu_ProbNNp;   //!
  TBranch        *b_Mu_ProbNNpi;   //!
  TBranch        *b_Mu_Q;   //!
  TBranch        *b_Mu_L0Global_Dec;   //!
  TBranch        *b_Mu_L0Global_TIS;   //!
  TBranch        *b_Mu_L0Global_TOS;   //!
  TBranch        *b_Mu_Hlt1Global_Dec;   //!
  TBranch        *b_Mu_Hlt1Global_TIS;   //!
  TBranch        *b_Mu_Hlt1Global_TOS;   //!
  TBranch        *b_Mu_Hlt1Phys_Dec;   //!
  TBranch        *b_Mu_Hlt1Phys_TIS;   //!
  TBranch        *b_Mu_Hlt1Phys_TOS;   //!
  TBranch        *b_Mu_Hlt2Global_Dec;   //!
  TBranch        *b_Mu_Hlt2Global_TIS;   //!
  TBranch        *b_Mu_Hlt2Global_TOS;   //!
  TBranch        *b_Mu_Hlt2Phys_Dec;   //!
  TBranch        *b_Mu_L0MuonDecision_Dec;   //!
  TBranch        *b_Mu_L0MuonDecision_TIS;   //!
  TBranch        *b_Mu_L0MuonDecision_TOS;   //!
  TBranch        *b_Mu_Hlt1TrackAllL0Decision_Dec;   //!
  TBranch        *b_Mu_Hlt1TrackAllL0Decision_TIS;   //!
  TBranch        *b_Mu_Hlt1TrackAllL0Decision_TOS;   //!
  TBranch        *b_Mu_Hlt1TrackMuonDecision_Dec;   //!
  TBranch        *b_Mu_Hlt1TrackMuonDecision_TIS;   //!
  TBranch        *b_Mu_Hlt1TrackMuonDecision_TOS;   //!
  TBranch        *b_Mu_Hlt1SingleMuonHighPTDecision_Dec;   //!
  TBranch        *b_Mu_Hlt1SingleMuonHighPTDecision_TIS;   //!
  TBranch        *b_Mu_Hlt1SingleMuonHighPTDecision_TOS;   //!
  TBranch        *b_Mu_Hlt2SingleMuonDecision_Dec;   //!
  TBranch        *b_Mu_Hlt2SingleMuonDecision_TIS;   //!
  TBranch        *b_Mu_Hlt2SingleMuonDecision_TOS;   //!
  TBranch        *b_nCandidate;   //!
  TBranch        *b_totCandidates;   //!
  TBranch        *b_EventInSequence;   //!
  TBranch        *b_runNumber;   //!
  TBranch        *b_eventNumber;   //!
  TBranch        *b_BCID;   //!
  TBranch        *b_BCType;   //!
  TBranch        *b_OdinTCK;   //!
  TBranch        *b_L0DUTCK;   //!
  TBranch        *b_HLTTCK;   //!
  TBranch        *b_GpsTime;   //!
  TBranch        *b_Polarity;   //!
  TBranch        *b_nPV;   //!
  TBranch        *b_PVX;   //!
  TBranch        *b_PVY;   //!
  TBranch        *b_PVZ;   //!
  TBranch        *b_PVXERR;   //!
  TBranch        *b_PVYERR;   //!
  TBranch        *b_PVZERR;   //!
  TBranch        *b_PVCHI2;   //!
  TBranch        *b_PVNDOF;   //!
  TBranch        *b_PVNTRACKS;   //!
  TBranch        *b_L0Global;   //!
  TBranch        *b_Hlt1Global;   //!
  TBranch        *b_Hlt2Global;   //!
  TBranch        *b_L0MuonDecision;   //!
  TBranch        *b_L0nSelections;   //!
  TBranch        *b_Hlt1TrackAllL0Decision;   //!
  TBranch        *b_Hlt1TrackMuonDecision;   //!
  TBranch        *b_Hlt1SingleMuonHighPTDecision;   //!
  TBranch        *b_Hlt1nSelections;   //!
  TBranch        *b_Hlt2SingleMuonDecision;   //!
  TBranch        *b_Hlt2TopoMu2BodyBBDTDecision;   //!
  TBranch        *b_Hlt2TopoMu3BodyBBDTDecision;   //!
  TBranch        *b_Hlt2TopoMu4BodyBBDTDecision;   //!
  TBranch        *b_Hlt2Topo2BodyBBDTDecision;   //!
  TBranch        *b_Hlt2Topo3BodyBBDTDecision;   //!
  TBranch        *b_Hlt2Topo4BodyBBDTDecision;   //!
  TBranch        *b_Hlt2nSelections;   //!
  TBranch        *b_MaxRoutingBits;   //!
  TBranch        *b_RoutingBits;   //!
  TBranch        *b_B_Nu_TRUEID;   //!
  TBranch        *b_B_Nu_TRUEP_E;   //!
  TBranch        *b_B_Nu_TRUEP_X;   //!
  TBranch        *b_B_Nu_TRUEP_Y;   //!
  TBranch        *b_B_Nu_TRUEP_Z;   //!
  TBranch        *b_D_Nu_TRUEID;   //!
  TBranch        *b_D_Nu_TRUEP_E;   //!
  TBranch        *b_D_Nu_TRUEP_X;   //!
  TBranch        *b_D_Nu_TRUEP_Y;   //!
  TBranch        *b_D_Nu_TRUEP_Z;   //!
  TBranch        *b_Pi1_Nu_TRUEID;   //!
  TBranch        *b_Pi1_Nu_TRUEP_E;   //!
  TBranch        *b_Pi1_Nu_TRUEP_X;   //!
  TBranch        *b_Pi1_Nu_TRUEP_Y;   //!
  TBranch        *b_Pi1_Nu_TRUEP_Z;   //!
  TBranch        *b_Pi2_Nu_TRUEID;   //!
  TBranch        *b_Pi2_Nu_TRUEP_E;   //!
  TBranch        *b_Pi2_Nu_TRUEP_X;   //!
  TBranch        *b_Pi2_Nu_TRUEP_Y;   //!
  TBranch        *b_Pi2_Nu_TRUEP_Z;   //!
  TBranch        *b_K_Nu_TRUEID;   //!
  TBranch        *b_K_Nu_TRUEP_E;   //!
  TBranch        *b_K_Nu_TRUEP_X;   //!
  TBranch        *b_K_Nu_TRUEP_Y;   //!
  TBranch        *b_K_Nu_TRUEP_Z;   //!
  TBranch        *b_Mu_Nu_TRUEID;   //!
  TBranch        *b_Mu_Nu_TRUEP_E;   //!
  TBranch        *b_Mu_Nu_TRUEP_X;   //!
  TBranch        *b_Mu_Nu_TRUEP_Y;   //!
  TBranch        *b_Mu_Nu_TRUEP_Z;   //!

  //PIDCorr variables                                                                                                                                                            
  TBranch        *b_Mu_PIDmu_corr;
  TBranch        *b_Mu_ProbNNmu_corr;
  TBranch        *b_Pi1_PIDK_corr;
  TBranch        *b_Pi1_ProbNNpi_corr;
  TBranch        *b_Pi2_PIDK_corr;
  TBranch        *b_Pi2_ProbNNpi_corr;
  TBranch        *b_K_PIDK_corr;
  TBranch        *b_K_ProbNNk_corr;
  // TM variables
  TBranch        *b_B_IsBdCat1_excl;
  TBranch        *b_B_IsBdCat1_incl;
  TBranch        *b_B_IsBdCat2_excl;
  TBranch        *b_B_IsBdCat2_incl;
  TBranch        *b_B_IsBdCat4_incl;

};
#endif

#ifdef KpipiSelection_cxx
KpipiSelection::KpipiSelection(TTree *tree, Bool_t ismc, Bool_t isbkg, Bool_t isb2dd)
: outFileName("output/test.root"), outFileCandidates("Candidates/candidates.txt"), debug(false), rew(), fChain(0), isMC(ismc), isBkg(isbkg), isB2DD(isb2dd)
{
  doTruthMatch  = false;
  doKinCuts     = false;
  doKinVeto     = false;
  doPIDCuts     = false;
  doPIDhadro    = false;
  doPIDmu       = false;
  doTriggCuts   = false;
  doTriggL0     = false;
  doTriggHlt1   = false;
  doTriggHlt2   = false;
  if (tree) Init(tree);
}

KpipiSelection::~KpipiSelection()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Bool_t KpipiSelection::SetChain(TTree *tree, Bool_t ismc, Bool_t isbkg, Bool_t isb2dd)
{
  if (!tree) return false;
  isMC = ismc;
  isBkg = isbkg;
  isB2DD = isb2dd;
  return Init(tree);
}

Int_t KpipiSelection::GetEntry(Long64_t entry)
{
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}

Long64_t KpipiSelection::LoadTree(Long64_t entry)
{
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent)
    fCurrent = fChain->GetTreeNumber();
  return centry;
}

Bool_t KpipiSelection::Init(TTree *tree)
{
  // Set branch addresses and branch pointers
  if (!tree) return false;
  fChain = tree;
  fCurrent = -1;

  fChain->SetBranchAddress("B_DOCA", &B_DOCA, &b_B_DOCA);
  fChain->SetBranchAddress("B_FDChi2", &B_FDChi2, &b_B_FDChi2);
  fChain->SetBranchAddress("B_M", &B_M, &b_B_M);
  fChain->SetBranchAddress("B_PVX", &B_PVX, &b_B_PVX);
  fChain->SetBranchAddress("B_PVY", &B_PVY, &b_B_PVY);
  fChain->SetBranchAddress("B_PVZ", &B_PVZ, &b_B_PVZ);
  fChain->SetBranchAddress("B_PX", &B_PX, &b_B_PX);
  fChain->SetBranchAddress("B_PY", &B_PY, &b_B_PY);
  fChain->SetBranchAddress("B_PZ", &B_PZ, &b_B_PZ);
  fChain->SetBranchAddress("B_VChi2", &B_VChi2, &b_B_VChi2);
  fChain->SetBranchAddress("B_VX", &B_VX, &b_B_VX);
  fChain->SetBranchAddress("B_VY", &B_VY, &b_B_VY);
  fChain->SetBranchAddress("B_VZ", &B_VZ, &b_B_VZ);
  fChain->SetBranchAddress("B_SV_X", &B_SV_X, &b_B_SV_X);
  fChain->SetBranchAddress("B_SV_Y", &B_SV_Y, &b_B_SV_Y);
  fChain->SetBranchAddress("B_SV_Z", &B_SV_Z, &b_B_SV_Z);
  fChain->SetBranchAddress("B_SV_CHI2", &B_SV_CHI2, &b_B_SV_CHI2);
  fChain->SetBranchAddress("B_SV_NDOF", &B_SV_NDOF, &b_B_SV_NDOF);
  fChain->SetBranchAddress("B_PV_X", &B_PV_X, &b_B_PV_X);
  fChain->SetBranchAddress("B_PV_Y", &B_PV_Y, &b_B_PV_Y);
  fChain->SetBranchAddress("B_PV_Z", &B_PV_Z, &b_B_PV_Z);
  fChain->SetBranchAddress("B_PV_CHI2", &B_PV_CHI2, &b_B_PV_CHI2);
  fChain->SetBranchAddress("B_PV_NDOF", &B_PV_NDOF, &b_B_PV_NDOF);
  fChain->SetBranchAddress("B_MCORR", &B_MCORR, &b_B_MCORR);
  fChain->SetBranchAddress("B_MCORRERR", &B_MCORRERR, &b_B_MCORRERR);
  fChain->SetBranchAddress("B_MCORRFULLERR", &B_MCORRFULLERR, &b_B_MCORRFULLERR);
  fChain->SetBranchAddress("B_Q2SOL1", &B_Q2SOL1, &b_B_Q2SOL1);
  fChain->SetBranchAddress("B_Q2SOL2", &B_Q2SOL2, &b_B_Q2SOL2);
  fChain->SetBranchAddress("B_L0Global_Dec", &B_L0Global_Dec, &b_B_L0Global_Dec);
  fChain->SetBranchAddress("B_L0Global_TIS", &B_L0Global_TIS, &b_B_L0Global_TIS);
  fChain->SetBranchAddress("B_L0Global_TOS", &B_L0Global_TOS, &b_B_L0Global_TOS);
  fChain->SetBranchAddress("B_Hlt1Global_Dec", &B_Hlt1Global_Dec, &b_B_Hlt1Global_Dec);
  fChain->SetBranchAddress("B_Hlt1Global_TIS", &B_Hlt1Global_TIS, &b_B_Hlt1Global_TIS);
  fChain->SetBranchAddress("B_Hlt1Global_TOS", &B_Hlt1Global_TOS, &b_B_Hlt1Global_TOS);
  fChain->SetBranchAddress("B_Hlt1Phys_Dec", &B_Hlt1Phys_Dec, &b_B_Hlt1Phys_Dec);
  fChain->SetBranchAddress("B_Hlt1Phys_TIS", &B_Hlt1Phys_TIS, &b_B_Hlt1Phys_TIS);
  fChain->SetBranchAddress("B_Hlt1Phys_TOS", &B_Hlt1Phys_TOS, &b_B_Hlt1Phys_TOS);
  fChain->SetBranchAddress("B_Hlt2Global_Dec", &B_Hlt2Global_Dec, &b_B_Hlt2Global_Dec);
  fChain->SetBranchAddress("B_Hlt2Global_TIS", &B_Hlt2Global_TIS, &b_B_Hlt2Global_TIS);
  fChain->SetBranchAddress("B_Hlt2Global_TOS", &B_Hlt2Global_TOS, &b_B_Hlt2Global_TOS);
  fChain->SetBranchAddress("B_Hlt2Phys_Dec", &B_Hlt2Phys_Dec, &b_B_Hlt2Phys_Dec);
  fChain->SetBranchAddress("B_Hlt2TopoMu2BodyBBDTDecision_Dec", &B_Hlt2TopoMu2BodyBBDTDecision_Dec, &b_B_Hlt2TopoMu2BodyBBDTDecision_Dec);
  fChain->SetBranchAddress("B_Hlt2TopoMu2BodyBBDTDecision_TIS", &B_Hlt2TopoMu2BodyBBDTDecision_TIS, &b_B_Hlt2TopoMu2BodyBBDTDecision_TIS);
  fChain->SetBranchAddress("B_Hlt2TopoMu2BodyBBDTDecision_TOS", &B_Hlt2TopoMu2BodyBBDTDecision_TOS, &b_B_Hlt2TopoMu2BodyBBDTDecision_TOS);
  fChain->SetBranchAddress("B_Hlt2TopoMu3BodyBBDTDecision_Dec", &B_Hlt2TopoMu3BodyBBDTDecision_Dec, &b_B_Hlt2TopoMu3BodyBBDTDecision_Dec);
  fChain->SetBranchAddress("B_Hlt2TopoMu3BodyBBDTDecision_TIS", &B_Hlt2TopoMu3BodyBBDTDecision_TIS, &b_B_Hlt2TopoMu3BodyBBDTDecision_TIS);
  fChain->SetBranchAddress("B_Hlt2TopoMu3BodyBBDTDecision_TOS", &B_Hlt2TopoMu3BodyBBDTDecision_TOS, &b_B_Hlt2TopoMu3BodyBBDTDecision_TOS);
  fChain->SetBranchAddress("B_Hlt2TopoMu4BodyBBDTDecision_Dec", &B_Hlt2TopoMu4BodyBBDTDecision_Dec, &b_B_Hlt2TopoMu4BodyBBDTDecision_Dec);
  fChain->SetBranchAddress("B_Hlt2TopoMu4BodyBBDTDecision_TIS", &B_Hlt2TopoMu4BodyBBDTDecision_TIS, &b_B_Hlt2TopoMu4BodyBBDTDecision_TIS);
  fChain->SetBranchAddress("B_Hlt2TopoMu4BodyBBDTDecision_TOS", &B_Hlt2TopoMu4BodyBBDTDecision_TOS, &b_B_Hlt2TopoMu4BodyBBDTDecision_TOS);
  fChain->SetBranchAddress("B_Hlt2Topo2BodyBBDTDecision_Dec", &B_Hlt2Topo2BodyBBDTDecision_Dec, &b_B_Hlt2Topo2BodyBBDTDecision_Dec);
  fChain->SetBranchAddress("B_Hlt2Topo2BodyBBDTDecision_TIS", &B_Hlt2Topo2BodyBBDTDecision_TIS, &b_B_Hlt2Topo2BodyBBDTDecision_TIS);
  fChain->SetBranchAddress("B_Hlt2Topo2BodyBBDTDecision_TOS", &B_Hlt2Topo2BodyBBDTDecision_TOS, &b_B_Hlt2Topo2BodyBBDTDecision_TOS);
  fChain->SetBranchAddress("B_Hlt2Topo3BodyBBDTDecision_Dec", &B_Hlt2Topo3BodyBBDTDecision_Dec, &b_B_Hlt2Topo3BodyBBDTDecision_Dec);
  fChain->SetBranchAddress("B_Hlt2Topo3BodyBBDTDecision_TIS", &B_Hlt2Topo3BodyBBDTDecision_TIS, &b_B_Hlt2Topo3BodyBBDTDecision_TIS);
  fChain->SetBranchAddress("B_Hlt2Topo3BodyBBDTDecision_TOS", &B_Hlt2Topo3BodyBBDTDecision_TOS, &b_B_Hlt2Topo3BodyBBDTDecision_TOS);
  fChain->SetBranchAddress("B_Hlt2Topo4BodyBBDTDecision_Dec", &B_Hlt2Topo4BodyBBDTDecision_Dec, &b_B_Hlt2Topo4BodyBBDTDecision_Dec);
  fChain->SetBranchAddress("B_Hlt2Topo4BodyBBDTDecision_TIS", &B_Hlt2Topo4BodyBBDTDecision_TIS, &b_B_Hlt2Topo4BodyBBDTDecision_TIS);
  fChain->SetBranchAddress("B_Hlt2Topo4BodyBBDTDecision_TOS", &B_Hlt2Topo4BodyBBDTDecision_TOS, &b_B_Hlt2Topo4BodyBBDTDecision_TOS);
  fChain->SetBranchAddress("D_DOCAMAX", &D_DOCAMAX, &b_D_DOCAMAX);
  fChain->SetBranchAddress("D_FDChi2", &D_FDChi2, &b_D_FDChi2);
  fChain->SetBranchAddress("D_IP", &D_IP, &b_D_IP);
  fChain->SetBranchAddress("D_IPChi2", &D_IPChi2, &b_D_IPChi2);
  fChain->SetBranchAddress("D_M", &D_M, &b_D_M);
  fChain->SetBranchAddress("D_PVX", &D_PVX, &b_D_PVX);
  fChain->SetBranchAddress("D_PVY", &D_PVY, &b_D_PVY);
  fChain->SetBranchAddress("D_PVZ", &D_PVZ, &b_D_PVZ);
  fChain->SetBranchAddress("D_PX", &D_PX, &b_D_PX);
  fChain->SetBranchAddress("D_PY", &D_PY, &b_D_PY);
  fChain->SetBranchAddress("D_PZ", &D_PZ, &b_D_PZ);
  fChain->SetBranchAddress("D_VChi2", &D_VChi2, &b_D_VChi2);
  fChain->SetBranchAddress("D_VX", &D_VX, &b_D_VX);
  fChain->SetBranchAddress("D_VY", &D_VY, &b_D_VY);
  fChain->SetBranchAddress("D_VZ", &D_VZ, &b_D_VZ);
  fChain->SetBranchAddress("Pi1_IP", &Pi1_IP, &b_Pi1_IP);
  fChain->SetBranchAddress("Pi1_IPChi2", &Pi1_IPChi2, &b_Pi1_IPChi2);
  fChain->SetBranchAddress("Pi1_IsMuon", &Pi1_IsMuon, &b_Pi1_IsMuon);
  fChain->SetBranchAddress("Pi1_PX", &Pi1_PX, &b_Pi1_PX);
  fChain->SetBranchAddress("Pi1_PY", &Pi1_PY, &b_Pi1_PY);
  fChain->SetBranchAddress("Pi1_PZ", &Pi1_PZ, &b_Pi1_PZ);
  fChain->SetBranchAddress("Pi1_PIDK", &Pi1_PIDK, &b_Pi1_PIDK);
  fChain->SetBranchAddress("Pi1_ProbNNe", &Pi1_ProbNNe, &b_Pi1_ProbNNe);
  fChain->SetBranchAddress("Pi1_ProbNNghost", &Pi1_ProbNNghost, &b_Pi1_ProbNNghost);
  fChain->SetBranchAddress("Pi1_ProbNNk", &Pi1_ProbNNk, &b_Pi1_ProbNNk);
  fChain->SetBranchAddress("Pi1_ProbNNmu", &Pi1_ProbNNmu, &b_Pi1_ProbNNmu);
  fChain->SetBranchAddress("Pi1_ProbNNp", &Pi1_ProbNNp, &b_Pi1_ProbNNp);
  fChain->SetBranchAddress("Pi1_ProbNNpi", &Pi1_ProbNNpi, &b_Pi1_ProbNNpi);
  fChain->SetBranchAddress("Pi1_Q", &Pi1_Q, &b_Pi1_Q);
  fChain->SetBranchAddress("Pi2_IP", &Pi2_IP, &b_Pi2_IP);
  fChain->SetBranchAddress("Pi2_IPChi2", &Pi2_IPChi2, &b_Pi2_IPChi2);
  fChain->SetBranchAddress("Pi2_IsMuon", &Pi2_IsMuon, &b_Pi2_IsMuon);
  fChain->SetBranchAddress("Pi2_PX", &Pi2_PX, &b_Pi2_PX);
  fChain->SetBranchAddress("Pi2_PY", &Pi2_PY, &b_Pi2_PY);
  fChain->SetBranchAddress("Pi2_PZ", &Pi2_PZ, &b_Pi2_PZ);
  fChain->SetBranchAddress("Pi2_PIDK", &Pi2_PIDK, &b_Pi2_PIDK);
  fChain->SetBranchAddress("Pi2_ProbNNe", &Pi2_ProbNNe, &b_Pi2_ProbNNe);
  fChain->SetBranchAddress("Pi2_ProbNNghost", &Pi2_ProbNNghost, &b_Pi2_ProbNNghost);
  fChain->SetBranchAddress("Pi2_ProbNNk", &Pi2_ProbNNk, &b_Pi2_ProbNNk);
  fChain->SetBranchAddress("Pi2_ProbNNmu", &Pi2_ProbNNmu, &b_Pi2_ProbNNmu);
  fChain->SetBranchAddress("Pi2_ProbNNp", &Pi2_ProbNNp, &b_Pi2_ProbNNp);
  fChain->SetBranchAddress("Pi2_ProbNNpi", &Pi2_ProbNNpi, &b_Pi2_ProbNNpi);
  fChain->SetBranchAddress("Pi2_Q", &Pi2_Q, &b_Pi2_Q);
  fChain->SetBranchAddress("K_IP", &K_IP, &b_K_IP);
  fChain->SetBranchAddress("K_IPChi2", &K_IPChi2, &b_K_IPChi2);
  fChain->SetBranchAddress("K_IsMuon", &K_IsMuon, &b_K_IsMuon);
  fChain->SetBranchAddress("K_PX", &K_PX, &b_K_PX);
  fChain->SetBranchAddress("K_PY", &K_PY, &b_K_PY);
  fChain->SetBranchAddress("K_PZ", &K_PZ, &b_K_PZ);
  fChain->SetBranchAddress("K_PIDK", &K_PIDK, &b_K_PIDK);
  fChain->SetBranchAddress("K_ProbNNe", &K_ProbNNe, &b_K_ProbNNe);
  fChain->SetBranchAddress("K_ProbNNghost", &K_ProbNNghost, &b_K_ProbNNghost);
  fChain->SetBranchAddress("K_ProbNNk", &K_ProbNNk, &b_K_ProbNNk);
  fChain->SetBranchAddress("K_ProbNNmu", &K_ProbNNmu, &b_K_ProbNNmu);
  fChain->SetBranchAddress("K_ProbNNp", &K_ProbNNp, &b_K_ProbNNp);
  fChain->SetBranchAddress("K_ProbNNpi", &K_ProbNNpi, &b_K_ProbNNpi);
  fChain->SetBranchAddress("K_Q", &K_Q, &b_K_Q);
  fChain->SetBranchAddress("Mu_IP", &Mu_IP, &b_Mu_IP);
  fChain->SetBranchAddress("Mu_IPChi2", &Mu_IPChi2, &b_Mu_IPChi2);
  fChain->SetBranchAddress("Mu_IsMuon", &Mu_IsMuon, &b_Mu_IsMuon);
  fChain->SetBranchAddress("Mu_PX", &Mu_PX, &b_Mu_PX);
  fChain->SetBranchAddress("Mu_PY", &Mu_PY, &b_Mu_PY);
  fChain->SetBranchAddress("Mu_PZ", &Mu_PZ, &b_Mu_PZ);
  fChain->SetBranchAddress("Mu_PIDmu", &Mu_PIDmu, &b_Mu_PIDmu);
  fChain->SetBranchAddress("Mu_ProbNNe", &Mu_ProbNNe, &b_Mu_ProbNNe);
  fChain->SetBranchAddress("Mu_ProbNNghost", &Mu_ProbNNghost, &b_Mu_ProbNNghost);
  fChain->SetBranchAddress("Mu_ProbNNk", &Mu_ProbNNk, &b_Mu_ProbNNk);
  fChain->SetBranchAddress("Mu_ProbNNmu", &Mu_ProbNNmu, &b_Mu_ProbNNmu);
  fChain->SetBranchAddress("Mu_ProbNNp", &Mu_ProbNNp, &b_Mu_ProbNNp);
  fChain->SetBranchAddress("Mu_ProbNNpi", &Mu_ProbNNpi, &b_Mu_ProbNNpi);
  fChain->SetBranchAddress("Mu_Q", &Mu_Q, &b_Mu_Q);
  fChain->SetBranchAddress("Mu_L0Global_Dec", &Mu_L0Global_Dec, &b_Mu_L0Global_Dec);
  fChain->SetBranchAddress("Mu_L0Global_TIS", &Mu_L0Global_TIS, &b_Mu_L0Global_TIS);
  fChain->SetBranchAddress("Mu_L0Global_TOS", &Mu_L0Global_TOS, &b_Mu_L0Global_TOS);
  fChain->SetBranchAddress("Mu_Hlt1Global_Dec", &Mu_Hlt1Global_Dec, &b_Mu_Hlt1Global_Dec);
  fChain->SetBranchAddress("Mu_Hlt1Global_TIS", &Mu_Hlt1Global_TIS, &b_Mu_Hlt1Global_TIS);
  fChain->SetBranchAddress("Mu_Hlt1Global_TOS", &Mu_Hlt1Global_TOS, &b_Mu_Hlt1Global_TOS);
  fChain->SetBranchAddress("Mu_Hlt1Phys_Dec", &Mu_Hlt1Phys_Dec, &b_Mu_Hlt1Phys_Dec);
  fChain->SetBranchAddress("Mu_Hlt1Phys_TIS", &Mu_Hlt1Phys_TIS, &b_Mu_Hlt1Phys_TIS);
  fChain->SetBranchAddress("Mu_Hlt1Phys_TOS", &Mu_Hlt1Phys_TOS, &b_Mu_Hlt1Phys_TOS);
  fChain->SetBranchAddress("Mu_Hlt2Global_Dec", &Mu_Hlt2Global_Dec, &b_Mu_Hlt2Global_Dec);
  fChain->SetBranchAddress("Mu_Hlt2Global_TIS", &Mu_Hlt2Global_TIS, &b_Mu_Hlt2Global_TIS);
  fChain->SetBranchAddress("Mu_Hlt2Global_TOS", &Mu_Hlt2Global_TOS, &b_Mu_Hlt2Global_TOS);
  fChain->SetBranchAddress("Mu_Hlt2Phys_Dec", &Mu_Hlt2Phys_Dec, &b_Mu_Hlt2Phys_Dec);
  fChain->SetBranchAddress("Mu_L0MuonDecision_Dec", &Mu_L0MuonDecision_Dec, &b_Mu_L0MuonDecision_Dec);
  fChain->SetBranchAddress("Mu_L0MuonDecision_TIS", &Mu_L0MuonDecision_TIS, &b_Mu_L0MuonDecision_TIS);
  fChain->SetBranchAddress("Mu_L0MuonDecision_TOS", &Mu_L0MuonDecision_TOS, &b_Mu_L0MuonDecision_TOS);
  fChain->SetBranchAddress("Mu_Hlt1TrackAllL0Decision_Dec", &Mu_Hlt1TrackAllL0Decision_Dec, &b_Mu_Hlt1TrackAllL0Decision_Dec);
  fChain->SetBranchAddress("Mu_Hlt1TrackAllL0Decision_TIS", &Mu_Hlt1TrackAllL0Decision_TIS, &b_Mu_Hlt1TrackAllL0Decision_TIS);
  fChain->SetBranchAddress("Mu_Hlt1TrackAllL0Decision_TOS", &Mu_Hlt1TrackAllL0Decision_TOS, &b_Mu_Hlt1TrackAllL0Decision_TOS);
  fChain->SetBranchAddress("Mu_Hlt1TrackMuonDecision_Dec", &Mu_Hlt1TrackMuonDecision_Dec, &b_Mu_Hlt1TrackMuonDecision_Dec);
  fChain->SetBranchAddress("Mu_Hlt1TrackMuonDecision_TIS", &Mu_Hlt1TrackMuonDecision_TIS, &b_Mu_Hlt1TrackMuonDecision_TIS);
  fChain->SetBranchAddress("Mu_Hlt1TrackMuonDecision_TOS", &Mu_Hlt1TrackMuonDecision_TOS, &b_Mu_Hlt1TrackMuonDecision_TOS);
  fChain->SetBranchAddress("Mu_Hlt1SingleMuonHighPTDecision_Dec", &Mu_Hlt1SingleMuonHighPTDecision_Dec, &b_Mu_Hlt1SingleMuonHighPTDecision_Dec);
  fChain->SetBranchAddress("Mu_Hlt1SingleMuonHighPTDecision_TIS", &Mu_Hlt1SingleMuonHighPTDecision_TIS, &b_Mu_Hlt1SingleMuonHighPTDecision_TIS);
  fChain->SetBranchAddress("Mu_Hlt1SingleMuonHighPTDecision_TOS", &Mu_Hlt1SingleMuonHighPTDecision_TOS, &b_Mu_Hlt1SingleMuonHighPTDecision_TOS);
  fChain->SetBranchAddress("Mu_Hlt2SingleMuonDecision_Dec", &Mu_Hlt2SingleMuonDecision_Dec, &b_Mu_Hlt2SingleMuonDecision_Dec);
  fChain->SetBranchAddress("Mu_Hlt2SingleMuonDecision_TIS", &Mu_Hlt2SingleMuonDecision_TIS, &b_Mu_Hlt2SingleMuonDecision_TIS);
  fChain->SetBranchAddress("Mu_Hlt2SingleMuonDecision_TOS", &Mu_Hlt2SingleMuonDecision_TOS, &b_Mu_Hlt2SingleMuonDecision_TOS);
  fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
  fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
  fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
  fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
  fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
  fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
  fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
  fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
  fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
  //  fChain->SetBranchAddress("HLTTCK", &HLTTCK, &b_HLTTCK);
  fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
  fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
  fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
  fChain->SetBranchAddress("PVX", PVX, &b_PVX);
  fChain->SetBranchAddress("PVY", PVY, &b_PVY);
  fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
  fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
  fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
  fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
  fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
  fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
  fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
  fChain->SetBranchAddress("L0Global", &L0Global, &b_L0Global);
  fChain->SetBranchAddress("Hlt1Global", &Hlt1Global, &b_Hlt1Global);
  fChain->SetBranchAddress("Hlt2Global", &Hlt2Global, &b_Hlt2Global);
  fChain->SetBranchAddress("L0MuonDecision", &L0MuonDecision, &b_L0MuonDecision);
  fChain->SetBranchAddress("L0nSelections", &L0nSelections, &b_L0nSelections);
  fChain->SetBranchAddress("Hlt1TrackAllL0Decision", &Hlt1TrackAllL0Decision, &b_Hlt1TrackAllL0Decision);
  fChain->SetBranchAddress("Hlt1TrackMuonDecision", &Hlt1TrackMuonDecision, &b_Hlt1TrackMuonDecision);
  fChain->SetBranchAddress("Hlt1SingleMuonHighPTDecision", &Hlt1SingleMuonHighPTDecision, &b_Hlt1SingleMuonHighPTDecision);
  fChain->SetBranchAddress("Hlt1nSelections", &Hlt1nSelections, &b_Hlt1nSelections);
  fChain->SetBranchAddress("Hlt2SingleMuonDecision", &Hlt2SingleMuonDecision, &b_Hlt2SingleMuonDecision);
  fChain->SetBranchAddress("Hlt2TopoMu2BodyBBDTDecision", &Hlt2TopoMu2BodyBBDTDecision, &b_Hlt2TopoMu2BodyBBDTDecision);
  fChain->SetBranchAddress("Hlt2TopoMu3BodyBBDTDecision", &Hlt2TopoMu3BodyBBDTDecision, &b_Hlt2TopoMu3BodyBBDTDecision);
  fChain->SetBranchAddress("Hlt2TopoMu4BodyBBDTDecision", &Hlt2TopoMu4BodyBBDTDecision, &b_Hlt2TopoMu4BodyBBDTDecision);
  fChain->SetBranchAddress("Hlt2Topo2BodyBBDTDecision", &Hlt2Topo2BodyBBDTDecision, &b_Hlt2Topo2BodyBBDTDecision);
  fChain->SetBranchAddress("Hlt2Topo3BodyBBDTDecision", &Hlt2Topo3BodyBBDTDecision, &b_Hlt2Topo3BodyBBDTDecision);
  fChain->SetBranchAddress("Hlt2Topo4BodyBBDTDecision", &Hlt2Topo4BodyBBDTDecision, &b_Hlt2Topo4BodyBBDTDecision);
  fChain->SetBranchAddress("Hlt2nSelections", &Hlt2nSelections, &b_Hlt2nSelections);
  fChain->SetBranchAddress("MaxRoutingBits", &MaxRoutingBits, &b_MaxRoutingBits);
  fChain->SetBranchAddress("RoutingBits", RoutingBits, &b_RoutingBits);

  if (!isMC) return true;

  fChain->SetBranchAddress("B_TRUEID", &B_TRUEID, &b_B_TRUEID);
  fChain->SetBranchAddress("B_MC_MOTHER_ID", &B_MC_MOTHER_ID, &b_B_MC_MOTHER_ID);
  fChain->SetBranchAddress("B_MC_MOTHER_KEY", &B_MC_MOTHER_KEY, &b_B_MC_MOTHER_KEY);
  fChain->SetBranchAddress("B_MC_GD_MOTHER_ID", &B_MC_GD_MOTHER_ID, &b_B_MC_GD_MOTHER_ID);
  fChain->SetBranchAddress("B_MC_GD_MOTHER_KEY", &B_MC_GD_MOTHER_KEY, &b_B_MC_GD_MOTHER_KEY);
  fChain->SetBranchAddress("B_MC_GD_GD_MOTHER_ID", &B_MC_GD_GD_MOTHER_ID, &b_B_MC_GD_GD_MOTHER_ID);
  fChain->SetBranchAddress("B_MC_GD_GD_MOTHER_KEY", &B_MC_GD_GD_MOTHER_KEY, &b_B_MC_GD_GD_MOTHER_KEY);
  fChain->SetBranchAddress("B_TRUEP_E", &B_TRUEP_E, &b_B_TRUEP_E);
  fChain->SetBranchAddress("B_TRUEP_X", &B_TRUEP_X, &b_B_TRUEP_X);
  fChain->SetBranchAddress("B_TRUEP_Y", &B_TRUEP_Y, &b_B_TRUEP_Y);
  fChain->SetBranchAddress("B_TRUEP_Z", &B_TRUEP_Z, &b_B_TRUEP_Z);
  fChain->SetBranchAddress("B_TRUEPT", &B_TRUEPT, &b_B_TRUEPT);
  fChain->SetBranchAddress("B_TRUEORIGINVERTEX_X", &B_TRUEORIGINVERTEX_X, &b_B_TRUEORIGINVERTEX_X);
  fChain->SetBranchAddress("B_TRUEORIGINVERTEX_Y", &B_TRUEORIGINVERTEX_Y, &b_B_TRUEORIGINVERTEX_Y);
  fChain->SetBranchAddress("B_TRUEORIGINVERTEX_Z", &B_TRUEORIGINVERTEX_Z, &b_B_TRUEORIGINVERTEX_Z);
  fChain->SetBranchAddress("B_TRUEENDVERTEX_X", &B_TRUEENDVERTEX_X, &b_B_TRUEENDVERTEX_X);
  fChain->SetBranchAddress("B_TRUEENDVERTEX_Y", &B_TRUEENDVERTEX_Y, &b_B_TRUEENDVERTEX_Y);
  fChain->SetBranchAddress("B_TRUEENDVERTEX_Z", &B_TRUEENDVERTEX_Z, &b_B_TRUEENDVERTEX_Z);
  fChain->SetBranchAddress("B_TRUEISSTABLE", &B_TRUEISSTABLE, &b_B_TRUEISSTABLE);
  fChain->SetBranchAddress("B_TRUETAU", &B_TRUETAU, &b_B_TRUETAU);
  fChain->SetBranchAddress("B_BKGCAT", &B_BKGCAT, &b_B_BKGCAT);
  fChain->SetBranchAddress("D_TRUEID", &D_TRUEID, &b_D_TRUEID);
  fChain->SetBranchAddress("D_MC_MOTHER_ID", &D_MC_MOTHER_ID, &b_D_MC_MOTHER_ID);
  fChain->SetBranchAddress("D_MC_MOTHER_KEY", &D_MC_MOTHER_KEY, &b_D_MC_MOTHER_KEY);
  fChain->SetBranchAddress("D_MC_GD_MOTHER_ID", &D_MC_GD_MOTHER_ID, &b_D_MC_GD_MOTHER_ID);
  fChain->SetBranchAddress("D_MC_GD_MOTHER_KEY", &D_MC_GD_MOTHER_KEY, &b_D_MC_GD_MOTHER_KEY);
  fChain->SetBranchAddress("D_MC_GD_GD_MOTHER_ID", &D_MC_GD_GD_MOTHER_ID, &b_D_MC_GD_GD_MOTHER_ID);
  fChain->SetBranchAddress("D_MC_GD_GD_MOTHER_KEY", &D_MC_GD_GD_MOTHER_KEY, &b_D_MC_GD_GD_MOTHER_KEY);
  fChain->SetBranchAddress("D_TRUEP_E", &D_TRUEP_E, &b_D_TRUEP_E);
  fChain->SetBranchAddress("D_TRUEP_X", &D_TRUEP_X, &b_D_TRUEP_X);
  fChain->SetBranchAddress("D_TRUEP_Y", &D_TRUEP_Y, &b_D_TRUEP_Y);
  fChain->SetBranchAddress("D_TRUEP_Z", &D_TRUEP_Z, &b_D_TRUEP_Z);
  fChain->SetBranchAddress("D_TRUEPT", &D_TRUEPT, &b_D_TRUEPT);
  fChain->SetBranchAddress("D_TRUEORIGINVERTEX_X", &D_TRUEORIGINVERTEX_X, &b_D_TRUEORIGINVERTEX_X);
  fChain->SetBranchAddress("D_TRUEORIGINVERTEX_Y", &D_TRUEORIGINVERTEX_Y, &b_D_TRUEORIGINVERTEX_Y);
  fChain->SetBranchAddress("D_TRUEORIGINVERTEX_Z", &D_TRUEORIGINVERTEX_Z, &b_D_TRUEORIGINVERTEX_Z);
  fChain->SetBranchAddress("D_TRUEENDVERTEX_X", &D_TRUEENDVERTEX_X, &b_D_TRUEENDVERTEX_X);
  fChain->SetBranchAddress("D_TRUEENDVERTEX_Y", &D_TRUEENDVERTEX_Y, &b_D_TRUEENDVERTEX_Y);
  fChain->SetBranchAddress("D_TRUEENDVERTEX_Z", &D_TRUEENDVERTEX_Z, &b_D_TRUEENDVERTEX_Z);
  fChain->SetBranchAddress("D_TRUEISSTABLE", &D_TRUEISSTABLE, &b_D_TRUEISSTABLE);
  fChain->SetBranchAddress("D_TRUETAU", &D_TRUETAU, &b_D_TRUETAU);
  fChain->SetBranchAddress("D_BKGCAT", &D_BKGCAT, &b_D_BKGCAT);
  fChain->SetBranchAddress("Pi1_TRUEID", &Pi1_TRUEID, &b_Pi1_TRUEID);
  fChain->SetBranchAddress("Pi1_MC_MOTHER_ID", &Pi1_MC_MOTHER_ID, &b_Pi1_MC_MOTHER_ID);
  fChain->SetBranchAddress("Pi1_MC_MOTHER_KEY", &Pi1_MC_MOTHER_KEY, &b_Pi1_MC_MOTHER_KEY);
  fChain->SetBranchAddress("Pi1_MC_GD_MOTHER_ID", &Pi1_MC_GD_MOTHER_ID, &b_Pi1_MC_GD_MOTHER_ID);
  fChain->SetBranchAddress("Pi1_MC_GD_MOTHER_KEY", &Pi1_MC_GD_MOTHER_KEY, &b_Pi1_MC_GD_MOTHER_KEY);
  fChain->SetBranchAddress("Pi1_MC_GD_GD_MOTHER_ID", &Pi1_MC_GD_GD_MOTHER_ID, &b_Pi1_MC_GD_GD_MOTHER_ID);
  fChain->SetBranchAddress("Pi1_MC_GD_GD_MOTHER_KEY", &Pi1_MC_GD_GD_MOTHER_KEY, &b_Pi1_MC_GD_GD_MOTHER_KEY);
  fChain->SetBranchAddress("Pi1_TRUEP_E", &Pi1_TRUEP_E, &b_Pi1_TRUEP_E);
  fChain->SetBranchAddress("Pi1_TRUEP_X", &Pi1_TRUEP_X, &b_Pi1_TRUEP_X);
  fChain->SetBranchAddress("Pi1_TRUEP_Y", &Pi1_TRUEP_Y, &b_Pi1_TRUEP_Y);
  fChain->SetBranchAddress("Pi1_TRUEP_Z", &Pi1_TRUEP_Z, &b_Pi1_TRUEP_Z);
  fChain->SetBranchAddress("Pi1_TRUEPT", &Pi1_TRUEPT, &b_Pi1_TRUEPT);
  fChain->SetBranchAddress("Pi1_TRUEORIGINVERTEX_X", &Pi1_TRUEORIGINVERTEX_X, &b_Pi1_TRUEORIGINVERTEX_X);
  fChain->SetBranchAddress("Pi1_TRUEORIGINVERTEX_Y", &Pi1_TRUEORIGINVERTEX_Y, &b_Pi1_TRUEORIGINVERTEX_Y);
  fChain->SetBranchAddress("Pi1_TRUEORIGINVERTEX_Z", &Pi1_TRUEORIGINVERTEX_Z, &b_Pi1_TRUEORIGINVERTEX_Z);
  fChain->SetBranchAddress("Pi1_TRUEENDVERTEX_X", &Pi1_TRUEENDVERTEX_X, &b_Pi1_TRUEENDVERTEX_X);
  fChain->SetBranchAddress("Pi1_TRUEENDVERTEX_Y", &Pi1_TRUEENDVERTEX_Y, &b_Pi1_TRUEENDVERTEX_Y);
  fChain->SetBranchAddress("Pi1_TRUEENDVERTEX_Z", &Pi1_TRUEENDVERTEX_Z, &b_Pi1_TRUEENDVERTEX_Z);
  fChain->SetBranchAddress("Pi1_TRUEISSTABLE", &Pi1_TRUEISSTABLE, &b_Pi1_TRUEISSTABLE);
  fChain->SetBranchAddress("Pi1_TRUETAU", &Pi1_TRUETAU, &b_Pi1_TRUETAU);
  fChain->SetBranchAddress("Pi2_TRUEID", &Pi2_TRUEID, &b_Pi2_TRUEID);
  fChain->SetBranchAddress("Pi2_MC_MOTHER_ID", &Pi2_MC_MOTHER_ID, &b_Pi2_MC_MOTHER_ID);
  fChain->SetBranchAddress("Pi2_MC_MOTHER_KEY", &Pi2_MC_MOTHER_KEY, &b_Pi2_MC_MOTHER_KEY);
  fChain->SetBranchAddress("Pi2_MC_GD_MOTHER_ID", &Pi2_MC_GD_MOTHER_ID, &b_Pi2_MC_GD_MOTHER_ID);
  fChain->SetBranchAddress("Pi2_MC_GD_MOTHER_KEY", &Pi2_MC_GD_MOTHER_KEY, &b_Pi2_MC_GD_MOTHER_KEY);
  fChain->SetBranchAddress("Pi2_MC_GD_GD_MOTHER_ID", &Pi2_MC_GD_GD_MOTHER_ID, &b_Pi2_MC_GD_GD_MOTHER_ID);
  fChain->SetBranchAddress("Pi2_MC_GD_GD_MOTHER_KEY", &Pi2_MC_GD_GD_MOTHER_KEY, &b_Pi2_MC_GD_GD_MOTHER_KEY);
  fChain->SetBranchAddress("Pi2_TRUEP_E", &Pi2_TRUEP_E, &b_Pi2_TRUEP_E);
  fChain->SetBranchAddress("Pi2_TRUEP_X", &Pi2_TRUEP_X, &b_Pi2_TRUEP_X);
  fChain->SetBranchAddress("Pi2_TRUEP_Y", &Pi2_TRUEP_Y, &b_Pi2_TRUEP_Y);
  fChain->SetBranchAddress("Pi2_TRUEP_Z", &Pi2_TRUEP_Z, &b_Pi2_TRUEP_Z);
  fChain->SetBranchAddress("Pi2_TRUEPT", &Pi2_TRUEPT, &b_Pi2_TRUEPT);
  fChain->SetBranchAddress("Pi2_TRUEORIGINVERTEX_X", &Pi2_TRUEORIGINVERTEX_X, &b_Pi2_TRUEORIGINVERTEX_X);
  fChain->SetBranchAddress("Pi2_TRUEORIGINVERTEX_Y", &Pi2_TRUEORIGINVERTEX_Y, &b_Pi2_TRUEORIGINVERTEX_Y);
  fChain->SetBranchAddress("Pi2_TRUEORIGINVERTEX_Z", &Pi2_TRUEORIGINVERTEX_Z, &b_Pi2_TRUEORIGINVERTEX_Z);
  fChain->SetBranchAddress("Pi2_TRUEENDVERTEX_X", &Pi2_TRUEENDVERTEX_X, &b_Pi2_TRUEENDVERTEX_X);
  fChain->SetBranchAddress("Pi2_TRUEENDVERTEX_Y", &Pi2_TRUEENDVERTEX_Y, &b_Pi2_TRUEENDVERTEX_Y);
  fChain->SetBranchAddress("Pi2_TRUEENDVERTEX_Z", &Pi2_TRUEENDVERTEX_Z, &b_Pi2_TRUEENDVERTEX_Z);
  fChain->SetBranchAddress("Pi2_TRUEISSTABLE", &Pi2_TRUEISSTABLE, &b_Pi2_TRUEISSTABLE);
  fChain->SetBranchAddress("Pi2_TRUETAU", &Pi2_TRUETAU, &b_Pi2_TRUETAU);
  fChain->SetBranchAddress("K_TRUEID", &K_TRUEID, &b_K_TRUEID);
  fChain->SetBranchAddress("K_MC_MOTHER_ID", &K_MC_MOTHER_ID, &b_K_MC_MOTHER_ID);
  fChain->SetBranchAddress("K_MC_MOTHER_KEY", &K_MC_MOTHER_KEY, &b_K_MC_MOTHER_KEY);
  fChain->SetBranchAddress("K_MC_GD_MOTHER_ID", &K_MC_GD_MOTHER_ID, &b_K_MC_GD_MOTHER_ID);
  fChain->SetBranchAddress("K_MC_GD_MOTHER_KEY", &K_MC_GD_MOTHER_KEY, &b_K_MC_GD_MOTHER_KEY);
  fChain->SetBranchAddress("K_MC_GD_GD_MOTHER_ID", &K_MC_GD_GD_MOTHER_ID, &b_K_MC_GD_GD_MOTHER_ID);
  fChain->SetBranchAddress("K_MC_GD_GD_MOTHER_KEY", &K_MC_GD_GD_MOTHER_KEY, &b_K_MC_GD_GD_MOTHER_KEY);
  fChain->SetBranchAddress("K_TRUEP_E", &K_TRUEP_E, &b_K_TRUEP_E);
  fChain->SetBranchAddress("K_TRUEP_X", &K_TRUEP_X, &b_K_TRUEP_X);
  fChain->SetBranchAddress("K_TRUEP_Y", &K_TRUEP_Y, &b_K_TRUEP_Y);
  fChain->SetBranchAddress("K_TRUEP_Z", &K_TRUEP_Z, &b_K_TRUEP_Z);
  fChain->SetBranchAddress("K_TRUEPT", &K_TRUEPT, &b_K_TRUEPT);
  fChain->SetBranchAddress("K_TRUEORIGINVERTEX_X", &K_TRUEORIGINVERTEX_X, &b_K_TRUEORIGINVERTEX_X);
  fChain->SetBranchAddress("K_TRUEORIGINVERTEX_Y", &K_TRUEORIGINVERTEX_Y, &b_K_TRUEORIGINVERTEX_Y);
  fChain->SetBranchAddress("K_TRUEORIGINVERTEX_Z", &K_TRUEORIGINVERTEX_Z, &b_K_TRUEORIGINVERTEX_Z);
  fChain->SetBranchAddress("K_TRUEENDVERTEX_X", &K_TRUEENDVERTEX_X, &b_K_TRUEENDVERTEX_X);
  fChain->SetBranchAddress("K_TRUEENDVERTEX_Y", &K_TRUEENDVERTEX_Y, &b_K_TRUEENDVERTEX_Y);
  fChain->SetBranchAddress("K_TRUEENDVERTEX_Z", &K_TRUEENDVERTEX_Z, &b_K_TRUEENDVERTEX_Z);
  fChain->SetBranchAddress("K_TRUEISSTABLE", &K_TRUEISSTABLE, &b_K_TRUEISSTABLE);
  fChain->SetBranchAddress("K_TRUETAU", &K_TRUETAU, &b_K_TRUETAU);
  fChain->SetBranchAddress("Mu_TRUEID", &Mu_TRUEID, &b_Mu_TRUEID);
  fChain->SetBranchAddress("Mu_MC_MOTHER_ID", &Mu_MC_MOTHER_ID, &b_Mu_MC_MOTHER_ID);
  fChain->SetBranchAddress("Mu_MC_MOTHER_KEY", &Mu_MC_MOTHER_KEY, &b_Mu_MC_MOTHER_KEY);
  fChain->SetBranchAddress("Mu_MC_GD_MOTHER_ID", &Mu_MC_GD_MOTHER_ID, &b_Mu_MC_GD_MOTHER_ID);
  fChain->SetBranchAddress("Mu_MC_GD_MOTHER_KEY", &Mu_MC_GD_MOTHER_KEY, &b_Mu_MC_GD_MOTHER_KEY);
  fChain->SetBranchAddress("Mu_MC_GD_GD_MOTHER_ID", &Mu_MC_GD_GD_MOTHER_ID, &b_Mu_MC_GD_GD_MOTHER_ID);
  fChain->SetBranchAddress("Mu_MC_GD_GD_MOTHER_KEY", &Mu_MC_GD_GD_MOTHER_KEY, &b_Mu_MC_GD_GD_MOTHER_KEY);
  fChain->SetBranchAddress("Mu_TRUEP_E", &Mu_TRUEP_E, &b_Mu_TRUEP_E);
  fChain->SetBranchAddress("Mu_TRUEP_X", &Mu_TRUEP_X, &b_Mu_TRUEP_X);
  fChain->SetBranchAddress("Mu_TRUEP_Y", &Mu_TRUEP_Y, &b_Mu_TRUEP_Y);
  fChain->SetBranchAddress("Mu_TRUEP_Z", &Mu_TRUEP_Z, &b_Mu_TRUEP_Z);
  fChain->SetBranchAddress("Mu_TRUEPT", &Mu_TRUEPT, &b_Mu_TRUEPT);
  fChain->SetBranchAddress("Mu_TRUEORIGINVERTEX_X", &Mu_TRUEORIGINVERTEX_X, &b_Mu_TRUEORIGINVERTEX_X);
  fChain->SetBranchAddress("Mu_TRUEORIGINVERTEX_Y", &Mu_TRUEORIGINVERTEX_Y, &b_Mu_TRUEORIGINVERTEX_Y);
  fChain->SetBranchAddress("Mu_TRUEORIGINVERTEX_Z", &Mu_TRUEORIGINVERTEX_Z, &b_Mu_TRUEORIGINVERTEX_Z);
  fChain->SetBranchAddress("Mu_TRUEENDVERTEX_X", &Mu_TRUEENDVERTEX_X, &b_Mu_TRUEENDVERTEX_X);
  fChain->SetBranchAddress("Mu_TRUEENDVERTEX_Y", &Mu_TRUEENDVERTEX_Y, &b_Mu_TRUEENDVERTEX_Y);
  fChain->SetBranchAddress("Mu_TRUEENDVERTEX_Z", &Mu_TRUEENDVERTEX_Z, &b_Mu_TRUEENDVERTEX_Z);
  fChain->SetBranchAddress("Mu_TRUEISSTABLE", &Mu_TRUEISSTABLE, &b_Mu_TRUEISSTABLE);
  fChain->SetBranchAddress("Mu_TRUETAU", &Mu_TRUETAU, &b_Mu_TRUETAU);

  fChain->SetBranchAddress("Mu_PIDmu_corr",    &Mu_PIDmu_corr,    &b_Mu_PIDmu_corr);  
  fChain->SetBranchAddress("Mu_ProbNNmu_corr", &Mu_ProbNNmu_corr, &b_Mu_ProbNNmu_corr);  
  fChain->SetBranchAddress("K_PIDK_corr",    &K_PIDK_corr,    &b_K_PIDK_corr);  
  fChain->SetBranchAddress("K_ProbNNk_corr", &K_ProbNNk_corr, &b_K_ProbNNk_corr);  
  fChain->SetBranchAddress("Pi1_PIDK_corr",    &Pi1_PIDK_corr,    &b_Pi1_PIDK_corr);  
  fChain->SetBranchAddress("Pi1_ProbNNpi_corr", &Pi1_ProbNNpi_corr, &b_Pi1_ProbNNpi_corr);  
  fChain->SetBranchAddress("Pi2_PIDK_corr",    &Pi2_PIDK_corr,    &b_Pi2_PIDK_corr);  
  fChain->SetBranchAddress("Pi2_ProbNNpi_corr", &Pi2_ProbNNpi_corr, &b_Pi2_ProbNNpi_corr);  


  if (isBkg) return true;

  fChain->SetBranchAddress("B_Nu_TRUEID", &B_Nu_TRUEID, &b_B_Nu_TRUEID);
  fChain->SetBranchAddress("B_Nu_TRUEP_E", &B_Nu_TRUEP_E, &b_B_Nu_TRUEP_E);
  fChain->SetBranchAddress("B_Nu_TRUEP_X", &B_Nu_TRUEP_X, &b_B_Nu_TRUEP_X);
  fChain->SetBranchAddress("B_Nu_TRUEP_Y", &B_Nu_TRUEP_Y, &b_B_Nu_TRUEP_Y);
  fChain->SetBranchAddress("B_Nu_TRUEP_Z", &B_Nu_TRUEP_Z, &b_B_Nu_TRUEP_Z);
  fChain->SetBranchAddress("D_Nu_TRUEID", &D_Nu_TRUEID, &b_D_Nu_TRUEID);
  fChain->SetBranchAddress("D_Nu_TRUEP_E", &D_Nu_TRUEP_E, &b_D_Nu_TRUEP_E);
  fChain->SetBranchAddress("D_Nu_TRUEP_X", &D_Nu_TRUEP_X, &b_D_Nu_TRUEP_X);
  fChain->SetBranchAddress("D_Nu_TRUEP_Y", &D_Nu_TRUEP_Y, &b_D_Nu_TRUEP_Y);
  fChain->SetBranchAddress("D_Nu_TRUEP_Z", &D_Nu_TRUEP_Z, &b_D_Nu_TRUEP_Z);
  fChain->SetBranchAddress("Pi1_Nu_TRUEID", &Pi1_Nu_TRUEID, &b_Pi1_Nu_TRUEID);
  fChain->SetBranchAddress("Pi1_Nu_TRUEP_E", &Pi1_Nu_TRUEP_E, &b_Pi1_Nu_TRUEP_E);
  fChain->SetBranchAddress("Pi1_Nu_TRUEP_X", &Pi1_Nu_TRUEP_X, &b_Pi1_Nu_TRUEP_X);
  fChain->SetBranchAddress("Pi1_Nu_TRUEP_Y", &Pi1_Nu_TRUEP_Y, &b_Pi1_Nu_TRUEP_Y);
  fChain->SetBranchAddress("Pi1_Nu_TRUEP_Z", &Pi1_Nu_TRUEP_Z, &b_Pi1_Nu_TRUEP_Z);
  fChain->SetBranchAddress("Pi2_Nu_TRUEID", &Pi2_Nu_TRUEID, &b_Pi2_Nu_TRUEID);
  fChain->SetBranchAddress("Pi2_Nu_TRUEP_E", &Pi2_Nu_TRUEP_E, &b_Pi2_Nu_TRUEP_E);
  fChain->SetBranchAddress("Pi2_Nu_TRUEP_X", &Pi2_Nu_TRUEP_X, &b_Pi2_Nu_TRUEP_X);
  fChain->SetBranchAddress("Pi2_Nu_TRUEP_Y", &Pi2_Nu_TRUEP_Y, &b_Pi2_Nu_TRUEP_Y);
  fChain->SetBranchAddress("Pi2_Nu_TRUEP_Z", &Pi2_Nu_TRUEP_Z, &b_Pi2_Nu_TRUEP_Z);
  fChain->SetBranchAddress("K_Nu_TRUEID", &K_Nu_TRUEID, &b_K_Nu_TRUEID);
  fChain->SetBranchAddress("K_Nu_TRUEP_E", &K_Nu_TRUEP_E, &b_K_Nu_TRUEP_E);
  fChain->SetBranchAddress("K_Nu_TRUEP_X", &K_Nu_TRUEP_X, &b_K_Nu_TRUEP_X);
  fChain->SetBranchAddress("K_Nu_TRUEP_Y", &K_Nu_TRUEP_Y, &b_K_Nu_TRUEP_Y);
  fChain->SetBranchAddress("K_Nu_TRUEP_Z", &K_Nu_TRUEP_Z, &b_K_Nu_TRUEP_Z);
  fChain->SetBranchAddress("Mu_Nu_TRUEID", &Mu_Nu_TRUEID, &b_Mu_Nu_TRUEID);
  fChain->SetBranchAddress("Mu_Nu_TRUEP_E", &Mu_Nu_TRUEP_E, &b_Mu_Nu_TRUEP_E);
  fChain->SetBranchAddress("Mu_Nu_TRUEP_X", &Mu_Nu_TRUEP_X, &b_Mu_Nu_TRUEP_X);
  fChain->SetBranchAddress("Mu_Nu_TRUEP_Y", &Mu_Nu_TRUEP_Y, &b_Mu_Nu_TRUEP_Y);
  fChain->SetBranchAddress("Mu_Nu_TRUEP_Z", &Mu_Nu_TRUEP_Z, &b_Mu_Nu_TRUEP_Z);

   fChain->SetBranchAddress("B_IsBdCat1_excl", &B_IsBdCat1_excl, &b_B_IsBdCat1_excl);
   fChain->SetBranchAddress("B_IsBdCat1_incl", &B_IsBdCat1_incl, &b_B_IsBdCat1_incl);
   fChain->SetBranchAddress("B_IsBdCat2_excl", &B_IsBdCat2_excl, &b_B_IsBdCat2_excl);
   fChain->SetBranchAddress("B_IsBdCat2_incl", &B_IsBdCat2_incl, &b_B_IsBdCat2_incl);
   fChain->SetBranchAddress("B_IsBdCat4_incl", &B_IsBdCat4_incl, &b_B_IsBdCat4_incl);

  return true;
}

#endif // #ifdef KpipiSelection_cxx
