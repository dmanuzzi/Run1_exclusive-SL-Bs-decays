#include "iostream"
#include "TChain.h"
#include "TObject.h"
#include "TROOT.h"
#include "TString.h"
#include "KKpiSelection.C"
using namespace std;

int launchSelection_KKpi(TString nfin, TString nchain,  TString nfout,
			 bool isMC, bool isbkg, bool isB2DD,
			 bool doTruthMatch,
			 bool doKin,
			 bool doPIDCuts,
			 bool doTriggCuts,
			 bool doKinVeto,
			 bool doPIDhadro,
			 bool doPIDmu,
			 bool doL0,
			 bool doHLT1,
			 bool doHLT2){
  
  TChain *chain = new TChain(nchain);
  cout << "Input file: " << nfin << endl;
  chain->Add(nfin);
  //  chain->Print();                                                                                                                                         
  KKpiSelection selector;
  selector.SetChain(chain, isMC, isbkg, isB2DD);
  selector.doTruthMatch= doTruthMatch;
  selector.doKinCuts = doKin ;
  selector.doPIDCuts = doPIDCuts;
  selector.doTriggCuts = doTriggCuts;
  selector.doKinVeto = doKinVeto;
  selector.doPIDhadro = doPIDhadro;
  selector.doPIDmu = doPIDmu;
  selector.doTriggL0 = doL0;
  selector.doTriggHlt1 = doHLT1;
  selector.doTriggHlt2 = doHLT2;

  selector.rew = FormFactorsRew();
  selector.rew.SetCurrentModel(1.16,1.37,0.845,0.921,"HQET2");//https://svnweb.cern.ch/trac/lhcb/browser/DBASE/tags/Gen/DecFiles/v27r55/dkfiles/Bs_Dsmunu=cocktail,hqet2,DsmuInAcc.dec                                                                                                                                          
  selector.rew.SetNewModel(1.16,0.921,1.37,0.845,"HQET2");

  selector.outFileName = nfout;
  selector.Loop();
  delete chain;
  return 1;
}
