#!/bin/bash
fabios='/eos/lhcb/user/f/fferrari/'

#path=$1
#tag=$2
#nfout=$3

here=$(pwd)
for mode in 'BsSignal'  'BdKKpi' 'BdKpipi' 'BsDstaunu' 'Bs2DD' 'Lb2LcD' 'Bu2DD' 'B02DD' 'Bu2DKmuX'  'B02DKmuX' 'B2DDK' 'BuKKpi' 'BuKpipi' 'B2DKpipimu' 'B2DKKpimu'
do
    for pol in 'Up' 'Down'
    do
	path=$fabios/$mode/$pol
	tag='*.root'
	Dtype='MC'
	MODE=$mode
	if [ $mode == 'B2DKpipimu' ]
	then
	    MODE='Kpipi'
	    Dtype='Data'
	elif [ $mode == 'B2DKKpimu' ]
	then
	    MODE='KKpi'
	    Dtype='Data'
	fi

	POL='Up'
	if [ $pol == 'Down' ]
	then
	    POL='Dw'
	fi

	echo '=============================================='
	cd $path
	echo $MODE
	nfout=$Dtype'-2016-'$MODE$POL'-noCuts_fList.txt'
	echo $nfout
	ls $tag
	ls $tag > $here/noCuts/$nfout
	cd $here
    done
done