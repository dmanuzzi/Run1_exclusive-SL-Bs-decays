import shelve
import os


inputDBpath1 = '/home/LHCB/fferrari/exclusive-SL-Bs-decays/Run1/optionsDVTuples/MC/db/'
inputDBpath2 = '/home/LHCB/fferrari/exclusive-SL-Bs-decays/Run1/optionsDVTuples/Data/db/'

gangadirPath = '/home/LHCB/fferrari/gangadir/'
outputPath = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/fList/noCuts/'

os.system('ls %sGangaDBJob*.db > db_list.txt'%(inputDBpath1))
os.system('ls %sGangaDBJob*.db >>db_list.txt'%(inputDBpath2))

f_dbs = open('db_list.txt', 'r')
l_dbs = []
for fname in  f_dbs.readlines():
    l_dbs.append(fname.replace('\n', ''))
print l_dbs
print ' number of jobs :',len(l_dbs)
for ndb in l_dbs:
    db = shelve.open(ndb, 'r')
    if len(db.keys())==0:
        print '*************************'
        print ndb,'\n is a empty database'
        print '*************************'
        continue
    njob = db.keys()[0]
    job = db[njob]
    if len(job.keys())==0:
        print '*************************'
        print ndb,'\n is a empty job'
        print '*************************'
        continue

    try:
        mode,year,pol,pythia,filt = (job[0]['JobName'].replace('_', ' ')).split()
    except:
        mode,year,pol = (job[0]['JobName'].replace('_', ' ')).split()

    if pol == 'Down': pol = 'Dw'

    kind = 'MC'
    tuplename = 'B2DmuMCNtuple.root'

    if mode == 'KKpi': 
        kind = 'Data'
        tuplename = 'B2DKKpimuNtuple.root'
    if mode == 'Kpipi': 
        kind = 'Data'
        tuplename = 'B2DKpipimuNtuple.root'

    nfout = ''
    if kind == 'MC':
        nfout = 'fList_{kind}-{year}-{mode}{pol}-noCuts_{pythia}{filt}.txt'
        nfout = nfout.format(kind=kind,year=year,mode=mode,pol=pol,pythia=pythia,filt=filt)
    elif kind == 'Data':
        nfout = 'fList_{kind}-{year}-{mode}{pol}-noCuts.txt'
        nfout = nfout.format(kind=kind,year=year,mode=mode,pol=pol)


    fout = open(outputPath+nfout, 'w')
    j=0
    nsubjobs = len(job.keys())
    for nsubjob in job.keys():
        subjob=job[nsubjob]
        if subjob['Downloaded']=='True':
            fout.write(gangadirPath+'%s/%s/%s\n'%(str(njob),str(nsubjob),tuplename))
            j=j+1
    if j!=0:
        print ' job n. ',njob,' has ',j,'/'+str(nsubjobs)+' downloaded subjobs. Created file : ',nfout
    else:
        os.system('rm %s'%(outputPath+nfout))
        print ' job n. ',njob,' has ',j,'/'+str(nsubjobs)+' downloaded subjobs. Removed file : ',nfout

           
