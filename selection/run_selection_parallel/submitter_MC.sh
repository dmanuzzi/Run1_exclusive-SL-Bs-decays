#!/bin/bash
here=$(pwd)
src='/home/LHCB/dmanuzzi/Bs2DsBrRatio/exclusive-SL-Bs-decays/selection/run_selection_parallel/srcSelection_parallel'
DI_noCuts='root://eoslhcb.cern.ch//eos/lhcb/user/d/dmanuzzi/Bs2DsBrRatio_tuples/output/'
DI='/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples/output/'
DO='/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples/output/'
cd $src

#python submit.py -c KKpiSelection_run2 -N 6 -si noCuts -so noTrigg_noPID_noOffL -n MC -m Bs2DD -M Up -di $DI_noCuts -do $DO
python submit.py -c KKpiSelection_run2 -N 6 -si noTrigg_noPID_noOffL -so noTrigg_noPID   -n MC -m Bs2DD -M Up -di $DI -do $DO
python submit.py -c KKpiSelection_run2 -N 6 -si noTrigg_noPID_noOffL -so noTrigg_noPIDmu -n MC -m Bs2DD -M Up -di $DI -do $DO

cd $here