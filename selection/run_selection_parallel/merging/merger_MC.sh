#!/bin/bash
here=$(pwd)
path='/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/MC/FinalSelection'

cd $path
for mode in *
do
    echo
    echo '********************************************'
    echo '         '$mode
    echo '********************************************'
    hadd $mode/MC-2011-$mode-AfterSelection-Filtered.root $mode/MagDw/MC-2011-${mode}Dw-FinalSelection_Pythia*Filtered_*.root $mode/MagUp/MC-2011-${mode}Up-FinalSelection_Pythia*Filtered_*.root
    hadd $mode/MC-2012-$mode-AfterSelection-Filtered.root $mode/MagDw/MC-2012-${mode}Dw-FinalSelection_Pythia*Filtered_*.root $mode/MagUp/MC-2012-${mode}Up-FinalSelection_Pythia*Filtered_*.root
    hadd $mode/MC-2011-$mode-AfterSelection-Unfiltered.root $mode/MagDw/MC-2011-${mode}Dw-FinalSelection_Pythia*Unfiltered_*.root $mode/MagUp/MC-2011-${mode}Up-FinalSelection_Pythia*Unfiltered_*.root
    hadd $mode/MC-2012-$mode-AfterSelection-Unfiltered.root $mode/MagDw/MC-2012-${mode}Dw-FinalSelection_Pythia*Unfiltered_*.root $mode/MagUp/MC-2012-${mode}Up-FinalSelection_Pythia*Unfiltered_*.root

done
cd $here
