#!/bin/bash
#path='/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/Data/FinalSelection'
#path='/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/Data/noTrigg'
path='/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/tuples/Data/noHadronicPID'
rm   $path/Kpipi/Data*.root
hadd $path/Kpipi/Data-2011-Kpipi-noHadronicPID.root $path/Kpipi/MagDw/Data-2011*.root $path/Kpipi/MagUp/Data-2011*.root
hadd $path/Kpipi/Data-2012-Kpipi-noHadronicPID.root $path/Kpipi/MagDw/Data-2012*.root $path/Kpipi/MagUp/Data-2012*.root
hadd $path/Kpipi/Data-Kpipi-noHadronicPID.root $path/Kpipi/MagDw/Data-*.root $path/Kpipi/MagUp/Data-*.root

rm   $path/KKpi/Data*.root
hadd $path/KKpi/Data-2011-KKpi-noHadronicPID.root $path/KKpi/MagDw/Data-2011*.root $path/KKpi/MagUp/Data-2011*.root
hadd $path/KKpi/Data-2012-KKpi-noHadronicPID.root $path/KKpi/MagDw/Data-2012*.root $path/KKpi/MagUp/Data-2012*.root
hadd $path/KKpi/Data-KKpi-noHadronicPID.root $path/KKpi/MagDw/Data-*.root $path/KKpi/MagUp/Data-*.root

rm   $path/Kpipi/SS*.root
hadd $path/Kpipi/SS-2011-Kpipi-noHadronicPID.root $path/Kpipi/MagDw/SS-2011*.root $path/Kpipi/MagUp/SS-2011*.root
hadd $path/Kpipi/SS-2012-Kpipi-noHadronicPID.root $path/Kpipi/MagDw/SS-2012*.root $path/Kpipi/MagUp/SS-2012*.root
hadd $path/Kpipi/SS-Kpipi-noHadronicPID.root $path/Kpipi/MagDw/SS-*.root $path/Kpipi/MagUp/SS-*.root

rm   $path/KKpi/SS*.root
hadd $path/KKpi/SS-2011-KKpi-noHadronicPID.root $path/KKpi/MagDw/SS-2011*.root $path/KKpi/MagUp/SS-2011*.root
hadd $path/KKpi/SS-2012-KKpi-noHadronicPID.root $path/KKpi/MagDw/SS-2012*.root $path/KKpi/MagUp/SS-2012*.root
hadd $path/KKpi/SS-KKpi-noHadronicPID.root $path/KKpi/MagDw/SS-*.root $path/KKpi/MagUp/SS-*.root


