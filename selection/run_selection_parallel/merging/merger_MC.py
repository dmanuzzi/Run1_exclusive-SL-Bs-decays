import os 

DO = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/'

#polarities = ['Up', 'Dw']
#pythias = ['Pythia6', 'Pythia8', 'Pythia8-ReDecay']

MCtypes = [
    ('BdKpipi'  ,'2011','Unfiltered'),
    ('BdKpipi'  ,'2012','Unfiltered'),
    ('BdKpipi'  ,'2011','-ReDecayUnfiltered'),
    ('BdKpipi'  ,'2012','-ReDecayUnfiltered'),
    ('BuKpipi'  ,'2011','Unfiltered'),
    ('BuKpipi'  ,'2012','Unfiltered'),
    ('BuKpipi'  ,'2011','-ReDecayUnfiltered'),
    ('BuKpipi'  ,'2012','-ReDecayUnfiltered'),
    ('BsSignal' ,'2011','Filtered'  ),
    ('BsSignal' ,'2012','Filtered'  ),
    ('BsSignal' ,'2012','Unfiltered'),
    ('BdKKpi'   ,'2011','Filtered'  ),
    ('BdKKpi'   ,'2012','Filtered'  ),
    ('BdKKpi'   ,'2012','Unfiltered'),  
    ('B02DD'    ,'2012','Unfiltered'),
    ('B02DKmuX' ,'2012','Unfiltered'),
    ('B2DDK'    ,'2012','Unfiltered'),
    ('Bs2DD'    ,'2012','Unfiltered'),
    ('Bu2DD'    ,'2012','Unfiltered'),
    ('Bu2DKmuX' ,'2012','Unfiltered'),
    ('BuKKpi'   ,'2011','Filtered'  ),
    ('Lb2LcD'   ,'2012','Unfiltered')
    ]
'''
MCtypes = [
    ('BdKpipi'  ,'2011','-ReDecayUnfiltered'),
    ('BdKpipi'  ,'2012','-ReDecayUnfiltered'),
    ('BuKpipi'  ,'2011','-ReDecayUnfiltered'),
    ('BuKpipi'  ,'2012','-ReDecayUnfiltered')
]
'''
MCtypes = [
    ('BdKpipi'  ,'2011','Unfiltered'),
    ('BdKpipi'  ,'2012','Unfiltered')
]
cutSteps = [
#    'noCuts', 
#    'noTrigg_noPID_noOffL', 
#    'noTrigg_noPID',       
#    'noTrigg_noPIDmu',      
#    'noTrigg',           
#    'noHLT',               
#    'noHLT2',     
#    'FinalSelection'
    'noHadronicPID'
    ]

for mode, year,filt  in MCtypes:
    for cutStep in cutSteps:
        print '\n====================================================='
        nfin = DO+'MC/{cutStep}/{mode}/Mag{pol}/MC-{year}-{mode}{pol}-{cutStep}_{pythia}{filt}_*.root'
        nfinDw8 = nfin.format(cutStep=cutStep, mode=mode, year=year, filt=filt, pythia='Pythia8', pol='Dw')
        nfinUp8 = nfin.format(cutStep=cutStep, mode=mode, year=year, filt=filt, pythia='Pythia8', pol='Up')

        nfinDw6 = ''
        nfinUp6 = ''
        if 'ReDecay' not in filt:
            nfinDw6 = nfin.format(cutStep=cutStep, mode=mode, year=year, filt=filt, pythia='Pythia6', pol='Dw')
            nfinUp6 = nfin.format(cutStep=cutStep, mode=mode, year=year, filt=filt, pythia='Pythia6', pol='Up')


        cutStep_2 = cutStep
        if cutStep == 'FinalSelection' : cutStep_2='AfterSelection'
        filt_tmp = filt
        if 'ReDecay' in filt:
            filt_tmp = filt.replace('-', '')
        nfout_tmp = DO+'MC/{cutStep}/{mode}/MC-{year}-{mode}-{cutStep_2}_{filt}.root'
        nfout     = nfout_tmp.format(cutStep=cutStep, cutStep_2=cutStep_2, mode=mode, year=year, filt=filt_tmp)
        command = 'hadd %s %s %s %s %s'%(nfout,  nfinDw8, nfinUp8, nfinDw6, nfinUp6)
        print command
        os.system('rm '+nfout)
        os.system(command)


#        break
#    break



