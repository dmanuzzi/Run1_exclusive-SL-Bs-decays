import os 

DO = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/'

#polarities = ['Up', 'Dw']
#pythias = ['Pythia6', 'Pythia8', 'Pythia8-ReDecay']

MCtypes = [
    ('BdKpipi'  ,'Unfiltered'),
    ('BdKpipi'  ,'Unfiltered'),
    ('BdKpipi'  ,'-ReDecayUnfiltered'),
    ('BdKpipi'  ,'-ReDecayUnfiltered'),
    ('BuKpipi'  ,'Unfiltered'),
    ('BuKpipi'  ,'Unfiltered'),
    ('BuKpipi'  ,'-ReDecayUnfiltered'),
    ('BuKpipi'  ,'-ReDecayUnfiltered'),
    ('BsSignal' ,'Filtered'  ),
    ('BsSignal' ,'Filtered'  ),
    ('BsSignal' ,'Unfiltered'),
    ('BdKKpi'   ,'Filtered'  ),
    ('BdKKpi'   ,'Filtered'  ),
    ('BdKKpi'   ,'Unfiltered'),  
    ('B02DD'    ,'Unfiltered'),
    ('B02DKmuX' ,'Unfiltered'),
    ('B2DDK'    ,'Unfiltered'),
    ('Bs2DD'    ,'Unfiltered'),
    ('Bu2DD'    ,'Unfiltered'),
    ('Bu2DKmuX' ,'Unfiltered'),
    ('BuKKpi'   ,'Filtered'  ),
    ('Lb2LcD'   ,'Unfiltered')
    ]
cutSteps = [
#    'noCuts', 
#    'noTrigg_noPID_noOffL', 
#    'noTrigg_noPID',       
#    'noTrigg_noPIDmu',      
#    'noTrigg',           
#    'noHLT',               
#    'noHLT2',     
    'FinalSelection'
#    'noHadronicPID'
    ]

for mode, filt  in MCtypes:
    for cutStep in cutSteps:
        print '\n====================================================='
        nfin = DO+'MC/{cutStep}/{mode}/Mag{pol}/MC-*-{mode}{pol}-{cutStep}_{pythia}{filt}_*.root'
        nfinDw8 = nfin.format(cutStep=cutStep, mode=mode, filt=filt, pythia='Pythia8', pol='Dw')
        nfinUp8 = nfin.format(cutStep=cutStep, mode=mode, filt=filt, pythia='Pythia8', pol='Up')

        nfinDw6 = ''
        nfinUp6 = ''
        if 'ReDecay' not in filt:
            nfinDw6 = nfin.format(cutStep=cutStep, mode=mode, filt=filt, pythia='Pythia6', pol='Dw')
            nfinUp6 = nfin.format(cutStep=cutStep, mode=mode, filt=filt, pythia='Pythia6', pol='Up')


        cutStep_2 = cutStep
        if cutStep == 'FinalSelection' : cutStep_2='AfterSelection'
        filt_tmp = filt
        if 'ReDecay' in filt:
            filt_tmp = filt.replace('-', '')
        nfout_tmp = DO+'MC/{cutStep}/{mode}/MC-{mode}-{cutStep_2}_{filt}.root'
        nfout     = nfout_tmp.format(cutStep=cutStep, cutStep_2=cutStep_2, mode=mode, filt=filt_tmp)
        command = 'hadd %s %s %s %s %s'%(nfout,  nfinDw8, nfinUp8, nfinDw6, nfinUp6)
        print command
        os.system('rm '+nfout)
        os.system(command)


#        break
#    break



