import os 
from waitORgo import waitORgo
import time

here = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/'
src  = here+'/srcSelection_parallel/'
DO = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/'

polarities = ['Up', 'Dw']
DATAtypes = [
    ('Kpipi'  ,'2011', 100),
    ('Kpipi'  ,'2012', 100),
    ('KKpi'   ,'2011', 100),
    ('KKpi'   ,'2012', 100)
    ]
cutSteps = [
#    ('noCuts',         'noTrigg_noPID'),
#    ('noCuts',  'noTrigg'),
#    ('noTrigg',        'FinalSelection'),
    ('noCuts',        'noHadronicPID')
#    ('noCuts',               'noTrigg_noPID_noOffL')
#    ('noTrigg_noPID_noOffL', 'noTrigg_noPID'),
#    ('noTrigg_noPID',        'noTrigg_noPID-PIDCorr'),
#    ('noTrigg_noPID-PIDCorr', 'noTrigg_noPIDmu'),
#    ('noTrigg_noPID',        'noTrigg_noPIDmu'),
#    ('noTrigg_noPIDmu',      'noTrigg'),
#    ('noTrigg',              'noHLT'),
#    ('noHLT',                'noHLT2'),
#    ('noHLT2',               'FinalSelection'),
#    ('noTrigg_noPID',        'noHadronicPID') 
    ]

dirOUT=DO
for selIN, selOUT in cutSteps:
    while (not waitORgo()):
        time.sleep(5)

    for mode, year, njobs in DATAtypes:
        code = 'KKpiSelection'
        if mode == 'Kpipi':
            code = 'KpipiSelection'
        for pol in polarities:
            command_tmp = 'python '+src+'/submit.py -c {code} -N {njobs} -si {selIN} -so {selOUT}  -n Data -m {mode} -M {pol}  -y {year}  -do {dirOUT}' 
            command =command_tmp.format(code=code, njobs=njobs, selIN=selIN, selOUT=selOUT, mode=mode, pol=pol, year=year, dirOUT=dirOUT)
            print command
            os.system(command)
                





