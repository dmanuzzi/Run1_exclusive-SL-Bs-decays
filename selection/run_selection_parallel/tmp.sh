#!/bin/bash
. $VO_LHCB_SW_DIR/lib/LbLogin.sh
cd /storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC/noHadronicPID/Lb2LcD/MagDw/
lb-run ROOT/6.06.02 python /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/srcSelection_parallel/run.py -c KKpiSelection -d /storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC/noTrigg_noPID/Lb2LcD/MagDw//MC-2012-Lb2LcDDw-noTrigg_noPID_Pythia8UnfilteredPIDCorr_0.root  -in MC-2012-Lb2LcDDw-noTrigg_noPID_Pythia8UnfilteredPIDCorr -out MC-2012-Lb2LcDDw-noHadronicPID_Pythia8Unfiltered  -i 0  -dout /storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/MC/noHadronicPID/Lb2LcD/MagDw/ 
