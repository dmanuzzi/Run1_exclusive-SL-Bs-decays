import os

def waitORgo():
    os.system('qstat > tmp_qstat.txt')
    fqstat = open('tmp_qstat.txt', 'r')
    lqstat = fqstat.read()
    lqstat = lqstat.split('\n')[:-1]
    q = 0
    r = 0
    c = 0

    for l in lqstat:
        if ' Q ' in l: q+=1
        elif ' R ' in l: r+=1
        elif ' C ' in l: c+=1
        else: continue

    fqstat.close()
    os.system('rm tmp_qstat.txt')

    t = q+r+c
    if t == 0:
        print 'no jobs are present in qstat'
        print '    ==> GO'
        return True
    print '%d jobs are in queue. %d jobs are running. %d jobs are completed.'%(q,r,c)
    if c == t:
        print '    ==> GO'
        return True
    else:
        print '    ==> WAIT'
        return False
