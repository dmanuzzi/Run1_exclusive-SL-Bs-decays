import os 
from waitORgo import waitORgo
import time
here = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/'
src  = here+'/srcSelection_parallel/'
DO = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/'

polarities = ['Up', 'Dw']
#polarities = ['Up']
pythias = ['Pythia6', 'Pythia8', 'Pythia8-ReDecay']
pythias = ['Pythia6', 'Pythia8']
#pythias = ['Pythia6']
#pythias = ['Pythia8-ReDecay']

MCtypes = [
    ('BdKpipi'  ,'2011','Unfiltered', 1),
    ('BdKpipi'  ,'2012','Unfiltered', 1),
    ('BuKpipi'  ,'2011','Unfiltered', 1),
    ('BuKpipi'  ,'2012','Unfiltered', 1),
    ('BsSignal' ,'2011','Filtered'  , 1),
    ('BsSignal' ,'2012','Filtered'  , 1),
    ('BsSignal' ,'2012','Unfiltered', 1),
    ('BdKKpi'   ,'2011','Filtered'  , 1),
    ('BdKKpi'   ,'2012','Filtered'  , 1),
    ('BdKKpi'   ,'2012','Unfiltered', 1),  
    ('B02DD'    ,'2012','Unfiltered', 1),
    ('B02DKmuX' ,'2012','Unfiltered', 1),
    ('B2DDK'    ,'2012','Unfiltered', 1),
    ('Bs2DD'    ,'2012','Unfiltered', 1),
    ('Bu2DD'    ,'2012','Unfiltered', 1),
    ('Bu2DKmuX' ,'2012','Unfiltered', 1),
    ('BuKKpi'   ,'2011','Filtered'  , 1),
    ('Lb2LcD'   ,'2012','Unfiltered', 1)
    ]
'''
MCtypes = [
    ('BdKpipi'   ,'2011','Unfiltered'  , 1),
    ('BdKpipi'   ,'2012','Unfiltered'  , 1)
]
'''
cutSteps = [
    ('noCuts',               'noTrigg_noPID_noOffL'),
    ('noTrigg_noPID_noOffL', 'noTrigg_noPID'),
    ('noTrigg_noPID',        'noTrigg_noPID-PIDGen'),
    ('noTrigg_noPID-PIDGen', 'noTrigg_noPID-PIDCorr'),
    ('noTrigg_noPID-PIDCorr','noTrigg_noPIDmu'),
    ('noTrigg_noPID',        'noTrigg_noPIDmu'),
    ('noTrigg_noPIDmu',      'noTrigg'),
    ('noTrigg',              'noHLT'),
    ('noHLT',                'noHLT2'),
    ('noHLT2',               'FinalSelection'),
    ('noTrigg_noPID-PIDCorr', 'noHadronicPID') 
    ]

dirOUT=DO
for selIN, selOUT in cutSteps:
    labelIN = "''"
    if '-PIDGen'  in selIN:
        selIN   = selIN.replace('-PIDGen', '')
        labelIN = 'PIDGen'
    if '-PIDCorr' in selIN:
        selIN   = selIN.replace('-PIDCorr', '')
        labelIN = 'PIDCorr'

    labelOUT = "''"                
    if '-PIDGen' in selOUT:
        selOUT  = selOUT.replace('-PIDGen', '')
        labelOUT= 'PIDGen'
    if '-PIDCorr' in selOUT:
        selOUT  = selOUT.replace('-PIDCorr', '')
        labelOUT= 'PIDCorr'

    while (not waitORgo()):
        time.sleep(5)

    for mode, year, filt, njobs in MCtypes:
        code = 'KKpiSelection'
        if mode in ['BdKpipi', 'BuKpipi']:
            code = 'KpipiSelection'
        for pythia in pythias:
            if (pythia == 'Pythia8-ReDecay' and not ('Kpipi' in mode)): 
                continue
            if (pythia == 'Pythia8-ReDecay'):
                njobs=20
            for pol in polarities:
                command_tmp = 'python '+src+'/submit.py -c {code} -N {njobs} -si {selIN} -so {selOUT}  -n MC -m {mode} -M {pol} -p {pythia} -y {year} -f {filt} -do {dirOUT} -li {labelIN} -lo {labelOUT}' 
                command =command_tmp.format(code=code, njobs=njobs, selIN=selIN, selOUT=selOUT, mode=mode, pol=pol, pythia=pythia, year=year, filt=filt, dirOUT=dirOUT, labelIN=labelIN, labelOUT=labelOUT)
                print command
                os.system(command)
                





