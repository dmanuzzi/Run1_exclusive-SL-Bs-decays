import ROOT
import os
import argparse

def setUp(tagIN,tagOUT,dirOUT,index):
  nchain = "DMutuple"
  if 'SS' in tagIN:
      nchain = "SSDMutuple"
  else:
    nchain = "DMutuple"

  if 'noCuts' in tagIN: 
    if 'SS' in tagIN:
      nchain = "SSDMutuple/SSDMutuple"
    else:
      nchain = "DMutuple/DMutuple"

  nfout = dirOUT+tagOUT+"_"+str(index)+".root"
#  nfout = "./"+tagOUT+".root"

  isMC = 0
  isbkg = 0
  isB2DD = 0

  if 'MC' in tagIN: 
    isMC = 1
    isbkg= 1
    for sig in ['BsSignal', 'BdKKpi', 'BdKpipi']:
      if sig in tagIN: 
        isbkg = 0
        break
    
    for B2DD in ['Bs2DD','Bu2DD','B02DD','B2DDK','Lb2LcD']:
      if B2DD in tagIN: 
        isB2DD = 1
        break


  doTruthMatch = 0
  doKin = 0
  doPIDCuts = 0
  doTriggCuts = 0
  doKinVeto = 0
  doPIDhadro = 0
  doPIDmu = 0
  doL0 = 0
  doHLT1 = 0
  doHLT2 = 0

  if 'noTrigg_noPID_noOffL' in tagOUT:
    doTruthMatch = 1
  elif 'noTrigg_noPIDmu'    in tagOUT:
    doTruthMatch,doKin,doKinVeto,doPIDCuts,doPIDhadro=1,1,1,1,1
  elif 'noTrigg_noPID'      in tagOUT:
    doTruthMatch,doKin,doKinVeto=1,1,1
  elif 'noTrigg'            in tagOUT:
    doTruthMatch,doKin,doKinVeto,doPIDCuts,doPIDhadro,doPIDmu=1,1,1,1,1,1
  elif 'noHLT2'             in tagOUT:
    doTruthMatch,doKin,doKinVeto,doPIDCuts,doPIDhadro,doPIDmu,doTriggCuts,doL0,doHLT1=1,1,1,1,1,1,1,1,1
  elif 'noHLT'              in tagOUT:
    doTruthMatch,doKin,doKinVeto,doPIDCuts,doPIDhadro,doPIDmu,doTriggCuts,doL0=1,1,1,1,1,1,1,1
  elif 'noHadronicPID'      in tagOUT:
    doTruthMatch,doKin,doPIDCuts,doTriggCuts,doKinVeto,doPIDmu,doL0,doHLT1,doHLT2 = 1,1,1,1,1,1,1,1,1    
  elif 'FinalSelection'     in tagOUT:
    doTruthMatch,doKin,doPIDCuts,doTriggCuts,doKinVeto,doPIDhadro,doPIDmu,doL0,doHLT1,doHLT2 = 1,1,1,1,1,1,1,1,1,1


  if 'noTrigg_noPID_noOffL' in tagIN:
      doTruthMatch = 0
  elif 'noTrigg_noPIDmu'    in tagIN:
    doTruthMatch,doKin,doKinVeto,doPIDhadro=0,0,0,0
  elif 'noTrigg_noPID'      in tagIN:
    doTruthMatch,doKin,doKinVeto=0,0,0
  elif 'noTrigg'            in tagIN:
    doTruthMatch,doKin,doKinVeto,doPIDCuts,doPIDhadro,doPIDmu=0,0,0,0,0,0
  elif 'noHLT2'             in tagIN:
    doTruthMatch,doKin,doKinVeto,doPIDCuts,doPIDhadro,doPIDmu,doL0,doHLT1=0,0,0,0,0,0,0,0
  elif 'noHLT'              in tagIN:
    doTruthMatch,doKin,doKinVeto,doPIDCuts,doPIDhadro,doPIDmu,doL0=0,0,0,0,0,0,0
  elif 'noHadronicPID'      in tagIN:
    doTruthMatch,doKin,doTriggCuts,doKinVeto,doPIDmu,doL0,doHLT1,doHLT2 = 0,0,0,0,0,0,0,0
  elif 'FinalSelection'     in tagIN:
    doTruthMatch,doKin,doPIDCuts,doTriggCuts,doKinVeto,doPIDhadro,doPIDmu,doL0,doHLT1,doHLT2 = 0,0,0,0,0,0,0,0,0,0


  if 'Data' in tagIN: doTruthMatch = 0

  return nchain,nfout,isMC,isbkg,isB2DD,doTruthMatch,doKin,doPIDCuts,doTriggCuts,doKinVeto,doPIDhadro,doPIDmu,doL0,doHLT1,doHLT2



print 'here run.py'
parser = argparse.ArgumentParser()
parser.add_argument('-c'  ,'--code'  , type = str, dest = 'code', default = 'sel')
parser.add_argument('-d'  ,'--data'  , nargs='+' , dest = 'data')
parser.add_argument('-in'  ,'--tagIN' , type = str, dest = 'tagIN')
#parser.add_argument('-din' ,'--dirIN' , type = str, dest = 'dirIN')
parser.add_argument('-out' ,'--tagOUT', type = str, dest = 'tagOUT')
parser.add_argument('-dout','--dirOUT', type = str, dest = 'dirOUT')
parser.add_argument('-i'   ,'--index' , type = int, dest = 'index')
args = parser.parse_args()

text = 'Analysing file'
for inputFile in args.data:
  text += ' %s'%inputFile
print text

ROOT.gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/srcSelection/%s.C+'%(args.code))

nchain,nfout,isMC,isbkg,isB2DD,doTruthMatch,doKin,doPIDCuts,doTriggCuts,doKinVeto,doPIDhadro,doPIDmu,doL0,doHLT1,doHLT2 = setUp(args.tagIN,args.tagOUT,args.dirOUT,args.index)


chain = ROOT.TChain(nchain)
for inputFile in args.data: 
  chain.Add(inputFile)
 
sel = __import__('ROOT.%s'%args.code,globals(),locals(),['%s'%args.code])
FFrew = __import__('ROOT.FormFactorsRew',globals(),locals(),['FormFactorsRew'])
selector = sel()
selector.SetChain(chain, isMC, isbkg, isB2DD)
selector.doTruthMatch = doTruthMatch
selector.doKinCuts    = doKin
selector.doPIDCuts    = doPIDCuts
selector.doTriggCuts  = doTriggCuts
selector.doKinVeto    = doKinVeto
selector.doPIDhadro   = doPIDhadro
selector.doPIDmu      = doPIDmu
selector.doTriggL0    = doL0
selector.doTriggHlt1  = doHLT1
selector.doTriggHlt2  = doHLT2
selector.rew = FFrew();
selector.rew.SetCurrentModel(1.16,1.37,0.845,0.921,"HQET2");
selector.rew.SetNewModel(1.16,0.921,1.37,0.845,"HQET2");

selector.outFileName  = nfout
selector.Loop()

