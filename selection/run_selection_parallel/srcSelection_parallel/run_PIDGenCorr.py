import ROOT
import os
import argparse


print 'here run_PIDGenCorr.py'
parser = argparse.ArgumentParser()
parser.add_argument('-c'  ,'--code'  , type = str, dest = 'code', default = 'sel')
parser.add_argument('-d'  ,'--data'  , nargs='+' , dest = 'data')
parser.add_argument('-in'  ,'--tagIN' , type = str, dest = 'tagIN')
#parser.add_argument('-din' ,'--dirIN' , type = str, dest = 'dirIN')
parser.add_argument('-out' ,'--tagOUT', type = str, dest = 'tagOUT')
parser.add_argument('-dout','--dirOUT', type = str, dest = 'dirOUT')
parser.add_argument('-i'   ,'--index' , type = int, dest = 'index')
args = parser.parse_args()

##selecting KKpi or Kpipi PIDCorr launcher
code = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/PIDCorr_PIDGen/'
tool =  ''
mode = ''
if 'PIDGen' in args.tagOUT:
  tool = 'PIDGen'
  mode = 'KKpi_Kpipi'
elif 'PIDCorr' in args.tagOUT:
  tool = 'PIDCorr'
  if 'KKpi' in args.code:
    mode = 'KKpi'
  elif 'Kpipi' in args.code:
    mode = 'Kpipi'
code = code+'%s_%s.py'%(mode, tool)

## merging the input files
text = ''
for inputFile in args.data:
  text += ' %s'%inputFile
print 'Analysing files: ',text
###name of the output file
nfout = args.dirOUT+args.tagOUT+"_"+str(args.index)+'.root'

nfin  = args.dirOUT+args.tagOUT+"_"+str(args.index)+'_tmp.root'
if len(args.data)>1:
  print 'rm '+nfin
  os.system('rm '+nfin)
  print 'hadd %s %s'%(nfin, text)
  os.system('hadd %s %s'%(nfin, text))
else:
  nfin = text

## launch PIDCorr
command = 'lb-run -c best Urania/v7r0 python %s %s %s'%(code, nfin, nfout)
print command
os.system(command)

if len(args.data)>1:
  print 'rm '+nfin
  os.system('rm '+nfin)

 
