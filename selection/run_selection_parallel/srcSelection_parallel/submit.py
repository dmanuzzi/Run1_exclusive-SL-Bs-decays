import ROOT
import os

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-c','--code'   , type = str  , dest = 'code'  , default = 'sel')
parser.add_argument('-n','--name'   , type = str  , dest = 'name'  , default = 'Data')
parser.add_argument('-y','--year'   , type = str  , dest = 'year'  , default = '2016')
parser.add_argument('-m','--mode'   , type = str  , dest = 'mode'  , default = 'KKpi')
parser.add_argument('-M','--Mag'    , type = str  , dest = 'Mag'   , default = 'Up')
parser.add_argument('-p','--pythia' , type = str  , dest = 'pythia', default = '')
parser.add_argument('-f','--filt'   , type = str  , dest = 'filt'  , default = '')
parser.add_argument('-N','--njobs'  , type = float, dest = 'njobs' , default = 100.)
parser.add_argument('-si','--selIN' , type = str  , dest = 'selIN' , default = 'noCuts')
parser.add_argument('-so','--selOUT', type = str  , dest = 'selOUT', default = 'FinalSelection')
parser.add_argument('-li','--labelIN',type = str  , dest = 'labelIN',default = '')
parser.add_argument('-lo','--labelOUT',type= str  , dest = 'labelOUT',default= '')

parser.add_argument('-di','--dirIN' , type = str  , dest = 'dirIN' , default = '')
parser.add_argument('-do','--dirOUT', type = str  , dest = 'dirOUT', default = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/Bs2Dsmunu/tuples_run1/')

args = parser.parse_args()
print '---> code        : ', args.code
print '---> sample type : ', args.name
print '---> year        : ', args.Mag
print '---> decay mode  : ', args.mode
print '---> polarity    : ', args.Mag
print '---> pythia      : ', args.pythia
print '---> filter      : ', args.filt
print '---> njobs       : ', args.njobs
print '---> selIN       : ', args.selIN
print '---> selOUT      : ', args.selOUT
print '---> labelIN     : ', args.labelIN
print '---> labelOUT    : ', args.labelOUT
print '---> dirIN       : ', args.dirIN
print '---> dirOUT      : ', args.dirOUT



path = '{name}/{sel}/{mode}/Mag{Mag}/'
tmpname = args.name
if 'SS' in tmpname: tmpname = 'Data'
tupledirOUT= args.dirOUT+path.format(name=tmpname, sel=args.selOUT, mode=args.mode, Mag=args.Mag)
print '===> dir OUT     : ', tupledirOUT
tag = '{name}-{year}-{mode}{Mag}-{sel}_{pythia}{filt}{label}'
if tmpname == 'Data':
  tag = tag.replace('_', '')
tagIN = tag.format(name=args.name, year=args.year, mode=args.mode, Mag=args.Mag, sel=args.selIN , pythia=args.pythia, filt=args.filt, label=args.labelIN)
tagOUT= tag.format(name=args.name, year=args.year, mode=args.mode, Mag=args.Mag, sel=args.selOUT, pythia=args.pythia, filt=args.filt, label=args.labelOUT)
print '===> tag IN      : ', tagIN
print '===> tag OUT     : ',tagOUT
if not os.path.exists(tupledirOUT): 
  os.mkdir(tupledirOUT)
  print '..... created out dir : ', tupledirOUT
 


try:
  ROOT.gROOT.ProcessLine('.L /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/srcSelection/%s.C+'%(args.code))
except:
  print 'xxx> Compilation failed'
  exit

path_fList = '/home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/fList/'
nfin = '%s/%s/fList_%s.txt'%(path_fList, args.selIN, tagIN)
if args.selIN == 'noCuts':
  nfin = nfin.replace('SS','Data')
else:
  tupledirIN = args.dirOUT+path.format(name=tmpname, sel=args.selIN , mode=args.mode, Mag=args.Mag)
  print '===> dir IN      : ', tupledirIN
  os.system("%s/make_fList.sh '%s' '%s_*.root' '%s'"%(path_fList,tupledirIN, tagIN, nfin))

inFile = open(nfin,'r')
files = inFile.read()
files = files.split('\n')[:-1]

if len(files) == 0: 
  print "xxx> NO input list"
  exit
print '===> Input list  : ', nfin

nFilesPerJob = int(len(files)/args.njobs)
print '===> n file per job: ',nFilesPerJob
if nFilesPerJob == 0: nFilesPerJob = 1
fileListPerJob = [files[x:x+nFilesPerJob] for x in xrange(0, len(files), nFilesPerJob)] #list of list for thread
print '===> File List per job: \n', fileListPerJob

#os.system('rm %s/*%s_*'%(tupledirOUT,tagOUT))
i = 0
for filelist in fileListPerJob:
  jobname  = '%s_%d' % (tagOUT,i)
  outFile  = tupledirOUT+'out/stdout_%s.out'%(jobname)
  errFile  = tupledirOUT+'err/stderr_%s.err'%(jobname)
  tupleFile= tupledirOUT+'%s_%d.root'%(tagOUT,i)
  optsfile = open('tmp.sh','w')
  optsfile.write('#!/bin/bash\n')
  optsfile.write('. $VO_LHCB_SW_DIR/lib/LbLogin.sh\n')
  optsfile.write('cd %s\n' % (tupledirOUT))
  filePerJob = ''
  for file in filelist:
    filePerJob += '%s '%file

  runner = 'run.py'
  if ('PIDCorr' in tagOUT) or ('PIDGen' in tagOUT):
    runner = 'run_PIDGenCorr.py'
  print 'runner : ',runner
  optsfile.write('lb-run ROOT/6.06.02 python /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/srcSelection_parallel/'+runner+' -c %s -d %s -in %s -out %s  -i %d  -dout %s \n' % (args.code, filePerJob, tagIN, tagOUT, i, tupledirOUT))
  optsfile.close()
  os.chmod('tmp.sh',0755)
  print '..... tmp.sh ready'
  command = 'qsub -N %s -o %s -e %s ./tmp.sh' % (jobname,outFile,errFile)

  print '..... submit command : ', command

#  os.system('. $VO_LHCB_SW_DIR/lib/LbLogin.sh')
#  os.system('cd %s' % (tupledirOUT))
#  os.system('lb-run ROOT/6.06.02 python /home/LHCB/dmanuzzi/Bs2DsBrRatio/Run1_exclusive-SL-Bs-decays/selection/run_selection_parallel/srcSelection_parallel/'+runner+' -c %s -d %s -in %s -out %s  -i %d  -dout %s \n' % (args.code, filePerJob, tagIN, tagOUT, i, tupledirOUT))
  os.system(command)
  print '..... job %d submitted'%(i)
  
  i = i+1

