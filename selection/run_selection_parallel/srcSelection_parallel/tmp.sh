#!/bin/bash
. $VO_LHCB_SW_DIR/lib/LbLogin.sh
cd /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/Data/noTrigg_noPID/Kpipi/MagDw/
lb-run ROOT/6.06.02 python /home/LHCB/dmanuzzi/Bs2DsBrRatio/exclusive-SL-Bs-decays/selection/run_selection_parallel/srcSelection_parallel/run.py -c KpipiSelection_run2 -d B2DKpipimuNtuple99.root B2DKpipimuNtuple9.root  -in Data-2016-KpipiDw-noCuts -out Data-2016-KpipiDw-noTrigg_noPID  -i 101 -din root://eoslhcb.cern.ch//eos/lhcb/user/d/dmanuzzi/Bs2DsBrRatio_tuples/output/Data/noCuts/Kpipi/MagDw/ -dout /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/Data/noTrigg_noPID/Kpipi/MagDw/ 
