#!/bin/bash
cd srcSelection_parallel/
python submit.py -c KKpiSelection_run2  -N 100 -si noCuts -so noTrigg_noPID -n Data -m KKpi  -M Up 
python submit.py -c KKpiSelection_run2  -N 100 -si noCuts -so noTrigg_noPID -n Data -m KKpi  -M Dw
python submit.py -c KpipiSelection_run2 -N 100 -si noCuts -so noTrigg_noPID -n Data -m Kpipi -M Up 
python submit.py -c KpipiSelection_run2 -N 100 -si noCuts -so noTrigg_noPID -n Data -m Kpipi -M Dw

#python submit.py -c KpipiSelection_run2 -N 100 -si noCuts -so noTrigg -n Data -m Kpipi -M Up 
#python submit.py -c KpipiSelection_run2 -N 100 -si noCuts -so noTrigg -n Data -m Kpipi -M Dw
#python submit.py -c KKpiSelection_run2  -N 100 -si noCuts -so noTrigg -n Data -m KKpi -M Up 
#python submit.py -c KKpiSelection_run2  -N 100 -si noCuts -so noTrigg -n Data -m KKpi -M Dw
#python submit.py -c KpipiSelection_run2 -N 100 -si noCuts -so noTrigg -n SSData -m Kpipi -M Up 
#python submit.py -c KpipiSelection_run2 -N 100 -si noCuts -so noTrigg -n SSData -m Kpipi -M Dw
#python submit.py -c KKpiSelection_run2  -N 100 -si noCuts -so noTrigg -n SSData -m KKpi -M Up 
#python submit.py -c KKpiSelection_run2  -N 100 -si noCuts -so noTrigg -n SSData -m KKpi -M Dw


#python submit.py -c KpipiSelection_run2 -N 10 -si noTrigg -so FinalSelection -n Data -m Kpipi -M Up -di /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/
#python submit.py -c KpipiSelection_run2 -N 10 -si noTrigg -so FinalSelection -n Data -m Kpipi -M Dw -di /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/
#python submit.py -c KKpiSelection_run2  -N 10 -si noTrigg -so FinalSelection -n Data -m KKpi  -M Up -di /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/
#python submit.py -c KKpiSelection_run2  -N 10 -si noTrigg -so FinalSelection -n Data -m KKpi  -M Dw -di /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/

#python submit.py -c KpipiSelection_run2 -N 10 -si noTrigg -so FinalSelection -n SSData -m Kpipi -M Up -di /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/
#python submit.py -c KpipiSelection_run2 -N 10 -si noTrigg -so FinalSelection -n SSData -m Kpipi -M Dw -di /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/
#python submit.py -c KKpiSelection_run2  -N 10 -si noTrigg -so FinalSelection -n SSData -m KKpi  -M Up -di /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/
#python submit.py -c KKpiSelection_run2  -N 10 -si noTrigg -so FinalSelection -n SSData -m KKpi  -M Dw -di /home/LHCB/dmanuzzi/Bs2DsBrRatio/tuples/output/
cd ..
